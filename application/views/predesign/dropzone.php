<?php 
	get_instance()->js[] = '<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>';
	$accept = empty($accept)?'image/*,.pdf,.docx,.xlsx,.xls,.doc,.odt,.ods,.jfif':$accept;	
	$inpName = empty($inpName)?$name:$inpName;
?>
<?php get_instance()->hcss[] = '
	<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css" type="text/css" rel="stylesheet" />
	<style>
		.dropzone{
			padding: 0;
			border: 1px solid #e2e5ed;
		}
		.dropzone .dz-preview .dz-image{
			border-radius:0;
		}
		.dz-error-message{
			top: 60px !important;
			opacity: 1 !important;
		}

		.dropzone.dz-started .dz-message {
		    display: block;
		    margin: 20px 0 10px;
		    width: 100%;
		}
		
		.dropzone .dz-preview .dz-image img {
		    max-width:100%;
		}
	</style>
'; ?>
<input type="hidden" id="field-<?= $name ?>" name="<?= $name ?>" value="<?= @implode(',',$files) ?>">
<div id="<?= $name ?>" action="<?= base_url() ?><?= $handle ?>/upload_file/<?= $inpName ?>" class="dropzone"></div>
<script>
	var docs = ['pdf','docx','xlsx','xls','doc','odt','ods','jfif'];
	var docImg = '<?= base_url("img/documentos.jpeg"); ?>';
	window.afterLoad.push(function(){
		<?= $name ?>.dropzone.ficheros = [];
		<?= $name ?>.dropzone.options.paramName = '<?= 's'.substr(md5($inpName),0,8) ?>';
		<?= $name ?>.dropzone.options.acceptedFiles = '<?= $accept ?>';
		<?= $name ?>.dropzone.options.maxFilesize = 100; //4mb
		<?= $name ?>.dropzone.options.addRemoveLinks = <?= empty($unset_delete)?'true':'false' ?>;
		<?= $name ?>.dropzone.options.uploadMultiple = false;
		<?= $name ?>.dropzone.options.dictRemoveFile = 'Borrar fichero';
		<?= $name ?>.dropzone.options.dictCancelUploadConfirmation = '¿Estás seguro que deseas eliminar este fichero?, esta acción no tiene reversa';
		<?= $name ?>.dropzone.options.dictDefaultMessage = 'Suelta aquí tus ficheros o pulsa aquí para adjuntar algún fichero';		
		<?= $name ?>.dropzone.on('success',function(file, response){
			file.remoteName = response.files[0].name;
			file.remoteName.url = response.files[0].url;
			<?= $name ?>.dropzone.ficheros.push(response.files[0].name);
			$("#field-<?= $name ?>").val(<?= $name ?>.dropzone.ficheros.join(','));
			var thumb = response.files[0].url;
			if(docs.indexOf(thumb.split('.').pop())>-1){
				thumb = '<?= base_url() ?>img/documentos.jpeg';
			}
			$(file.previewElement).find('img').attr('src',thumb);
			$(file.previewElement.querySelectorAll(".dz-error-mark")).after('<a href="'+response.files[0].url+'" target="_new" style="font-size: 14px;text-align: center;display: block;cursor: pointer;">Abrir documento</a>');
			if(typeof(window.afterUploadFile) !== 'undefined'){
				window.afterUploadFile(file,response);
			}
		});
		<?= $name ?>.dropzone.on('removedfile',function(file,response){
			if(typeof file.remoteName !== 'undefined'){
				var i = <?= $name ?>.dropzone.ficheros.indexOf(file.remoteName);
				<?= $name ?>.dropzone.ficheros.splice(i,1);
				$.get('<?= base_url() ?><?= $handle ?>/delete_file/<?= $inpName ?>/'+file.remoteName+'?_=1588886173343',{},function(data){

				});
				$("#field-<?= $name ?>").val(<?= $name ?>.dropzone.ficheros.join(','));	
				if(typeof(window.afterUploadFile) !== 'undefined'){
					window.afterRemoveFile(file,response);
				}			
			}
		});		
		<?php if(!empty($files)): ?>
			//Importar ficheros por default
			var files<?= $name ?> = $("#field-<?= $name ?>").val();
			files<?= $name ?> = files<?= $name ?>.split(',');
			for(var i in files<?= $name ?>){
				var mockFile = {name: files<?= $name ?>[i],remoteName:files<?= $name ?>[i] };
				<?= $name ?>.dropzone.files.push(files<?= $name ?>[i]);
				<?= $name ?>.dropzone.options.addedfile.call(<?= $name ?>.dropzone, mockFile);
				var <?= $name ?>thumb = '<?= base_url() ?><?= $path ?>'+files<?= $name ?>[i];				
				if(docs.indexOf(<?= $name ?>thumb.split('.').pop())>-1){
					<?= $name ?>thumb = '<?= base_url() ?>img/documentos.jpeg';
				}
				<?= $name ?>.dropzone.options.thumbnail.call(<?= $name ?>.dropzone, mockFile, <?= $name ?>thumb);
				mockFile.previewElement.classList.add('dz-success');
				mockFile.previewElement.classList.add('dz-complete');
				<?= $name ?>.dropzone.ficheros.push(files<?= $name ?>[i]);
				$($("#<?= $name ?>").find('.dz-error-mark')[i]).after('<a href="<?= base_url() ?><?= $path ?>'+files<?= $name ?>[i]+'" target="_new" style="font-size: 14px;text-align: center;display: block;cursor: pointer;">Abrir fichero</a>');
			}
		<?php endif ?>
		$(".dz-default.dz-message span").html('Agrega aquí tus documentos');
		$(document).on('click','.dz-error-message',function(){
			$(this).remove();
		});
	});
</script>