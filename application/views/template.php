<!doctype html>
<html lang="en">
<head>
    <!-- Librerias -->
    <meta charset="utf-8" />    
    <link rel="icon" type="image/png" href="<?= base_url() ?>img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>BIMBO</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" href="<?= base_url().'img/favicon.ico' ?>" type="image/x-icon"/>
    <!--  Social tags      -->
    <meta name="keywords" content="Keywords">
    <meta name="description" content="Description">
    <!-- Bootstrap core CSS     -->
    <link href="<?= base_url() ?>css/template/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?= base_url() ?>css/template/material-dashboard.css" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?= base_url() ?>css/template/demo.css" rel="stylesheet" />
    <!--  Estilos Personales    -->
    <link href="<?= base_url() ?>css/template/main.css" rel="stylesheet" />
    <link href="<?= base_url() ?>css/template/queries.css" rel="stylesheet" />
    <link href="<?= base_url() ?>css/template/cliente.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <?php 
    if(!empty($css_files) && !empty($js_files)):
    foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
    <?php endif; ?>
</head>

<body class="sidebar-mini">
    <?php $this->load->view($view); ?>

    <script>
        $(document).ready(function(){
            doAfterLoad();
        });

        function doAfterLoad(){
            for(var i in window.afterLoad){
                window.afterLoad[i]();
            }
        }
    </script>
</body>

<?php $this->load->view('includes/template/modales');?>
<!-- Librerias -->
<?php $this->load->view('includes/template/scripts');?>
</html>
