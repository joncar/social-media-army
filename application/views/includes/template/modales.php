<!-- Modal Login -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      	<div class="loginmodal-container text-center img-modal-registro" id="modal-login">
            <img src="img/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="padding2020">
            	<b>Recuperar Contraseña</b><br>Ingresa tu correo electrónico y te enviaremos<br>las instrucciones para reestablecer tu cuenta
            </div>
            <form>
                <input type="text" name="user" placeholder="Tu correo">
                <input type="submit" name="login" class="login loginmodal-submit" value="ENVIAR">
            </form>
            <div class="login-help">
                <a href="registro.php">Crear Cuenta</a> - <a href="#">Más información</a>
            </div>
      	</div>
    </div>
</div>
<!-- Termina Modal Login -->

<!-- Modal Registro -->
<div class="modal fade" id="login-registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      	<div class="loginmodal-container text-center img-modal-registro" id="modal-login">
            <img src="<?= base_url() ?>img/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="padding2020">
            	<b>Marcas</b><br>Selecciona las marcas con las que trabajas
            </div>
          	<form>
            	<input type="text" name="user" placeholder="Username">
            	<input type="submit" name="login" class="login loginmodal-submit" value="ACEPTAR">
          	</form>
      	</div>
    </div>
</div>
<!-- Termina Modal Registro -->

<!-- Modal Enviar Brief -->
<div class="modal fade" id="enviar-brief" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b>Enviar Brief</b><br>
                  Selecciona el Proyecto que deseas enviar
                </div>
                <form class="form-horizontal">
                    <div class="select-agencias">
                        <select class="selectpicker" data-style="select-with-transition" multiple title="Selecciona el Proyecto" data-size="7">
                          <option disabled>Selecciona el Proyecto</option>
                          <option value="2">Servicio 1</option>
                          <option value="3">Servicio 2</option>
                          <option value="4">Servicio 3</option>
                          <option value="5">Servicio 4</option>
                        </select>
                    </div>
                </form>

                <input style="display: none;" class="swal2-input">
                <input style="display: none;" class="swal2-file" type="file">
                <div class="swal2-range" style="display: none;">
                    <output></output><input type="range">
                </div>
                <select style="display: none;" class="swal2-select"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
                <textarea style="display: none;" class="swal2-textarea"></textarea>
                <div class="swal2-validationerror" style="display: none;"></div>
                <hr class="swal2-spacer" style="display: block;">
                <button type="button" class="swal2-confirm btn btn-success">Crear Nuevo Brief</button>
                <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Enviar</button>
                <span class="swal2-close" style="display: none;">×</span>
            </div>
    </div>
</div>
<!-- Termina Modal Enviar Brief -->

<!-- Modal Contactar agencia -->
<div class="modal fade" id="contacto-agencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b>Contactar Agencia</b>
                </div>
                <div class="form-group">
                    <label class="control-label">Nombre</label>
                    <input  maxlength="100" type="text" class="form-control" placeholder="..."  />
                </div>
                <div class="form-group">
                    <label class="control-label">Correo</label>
                    <input  maxlength="100" type="text" class="form-control" placeholder="..."  />
                </div>
                <div class="form-group">
                    <label class="control-label">Comentarios</label>
                    <input  maxlength="100" type="textarea" class="form-control" placeholder="..."  />
                </div>

                <input style="display: none;" class="swal2-input">
                <input style="display: none;" class="swal2-file" type="file">
                <div class="swal2-range" style="display: none;">
                    <output></output><input type="range">
                </div>
                <select style="display: none;" class="swal2-select"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
                <textarea style="display: none;" class="swal2-textarea"></textarea>
                <div class="swal2-validationerror" style="display: none;"></div>
                <hr class="swal2-spacer" style="display: block;">
                <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Cancelar</button>
                <button type="button" class="swal2-confirm btn btn-success">Enviar comentarios</button>
                <span class="swal2-close" style="display: none;">×</span>
            </div>
    </div>
</div>
<!-- Termina Contactar agencia -->

<!-- Modal Proceso de Registro -->
<div class="modal fade modal-registro" id="agregar-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      	<div class="loginmodal-container text-center img-modal-registro" id="modal-login">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-registro-modal">
            	<b>Nuevo Proyecto</b><br>A continuación, llena todos los campos
            </div>

            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                    	<a href="#step-1" type="button" class="btn btn-primary">1</a><p>Datos</p>
                    </div>
                    <div class="stepwizard-step">
                    	<a href="#step-2" type="button" class="btn btn-default" disabled="disabled">2</a><p>Especificaciones</p>
                    </div>
                    <div class="stepwizard-step">
                    	<a href="#step-3" type="button" class="btn btn-default" disabled="disabled">3</a><p>Fechas</p>
                    </div>
                    <div class="stepwizard-step">
                    	<a href="#step-4" type="button" class="btn btn-default" disabled="disabled">4</a><p>Filtros</p>
                    </div>
                    <div class="stepwizard-step">
                    	<a href="#step-5" type="button" class="btn btn-default" disabled="disabled">5</a><p>Documentos</p>
                    </div>
                    <div class="stepwizard-step">
                    	<a href="#step-6" type="button" class="btn btn-default" disabled="disabled">6</a><p>Finalizar</p>
                    </div>
                </div>
            </div>

            <form role="form">
                <div class="row setup-content" id="step-1">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <div class="padding2020">
                                <h3 class="text-uppercase"><b>Datos del Proyecto</b></h3>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nombre del Proyecto</label>
                                <input  maxlength="100" type="text" class="form-control" placeholder="..."  />
                            </div>

                            <h3 class="text-uppercase"><b>Agregar Supervisor o Colaboradores</b></h3>
                            <small>*Puedes agregar más campos</small>

                            <div class="form-group input-group">
                            	<label class="control-label">Supervisor</label>
                                <input type="text" name="multiple[]" class="form-control" placeholder="...">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-add">
                                      <i class="material-icons">add_circle_outline</i>
                                  </button>
                                </span>
                            </div>

                            <div class="form-group input-group">
                            	<label class="control-label">Colaboradores</label>
                                <input type="text" name="multiple[]" class="form-control" placeholder="...">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-add">
                                      <i class="material-icons">add_circle_outline</i>
                                  </button>
                                </span>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 padding0">
                                    <div class="col-sm-6">
                                        <div class="select-agencias">
                                            <select class="selectpicker" data-style="select-with-transition" multiple title="País" data-size="7">
                                              <option disabled>Selecciona</option>
                                              <option value="cero">Selecciona tu País</option>
                                              <option value="AF">Afganistán</option>
                                              <option value="AL">Albania</option>
                                              <option value="DE">Alemania</option>
                                              <option value="AD">Andorra</option>
                                              <option value="AO">Angola</option>
                                              <option value="AI">Anguilla</option>
                                              <option value="AQ">Antártida</option>
                                              <option value="AG">Antigua y Barbuda</option>
                                              <option value="AN">Antillas Holandesas</option>
                                              <option value="SA">Arabia Saudí</option>
                                              <option value="DZ">Argelia</option>
                                              <option value="AR">Argentina</option>
                                              <option value="AM">Armenia</option>
                                              <option value="AW">Aruba</option>
                                              <option value="AU">Australia</option>
                                              <option value="AT">Austria</option>
                                              <option value="AZ">Azerbaiyán</option>
                                              <option value="BS">Bahamas</option>
                                              <option value="BH">Bahrein</option>
                                              <option value="BD">Bangladesh</option>
                                              <option value="BB">Barbados</option>
                                              <option value="BE">Bélgica</option>
                                              <option value="BZ">Belice</option>
                                              <option value="BJ">Benin</option>
                                              <option value="BM">Bermudas</option>
                                              <option value="BY">Bielorrusia</option>
                                              <option value="MM">Birmania</option>
                                              <option value="BO">Bolivia</option>
                                              <option value="BA">Bosnia y Herzegovina</option>
                                              <option value="BW">Botswana</option>
                                              <option value="BR">Brasil</option>
                                              <option value="BN">Brunei</option>
                                              <option value="BG">Bulgaria</option>
                                              <option value="BF">Burkina Faso</option>
                                              <option value="BI">Burundi</option>
                                              <option value="BT">Bután</option>
                                              <option value="CV">Cabo Verde</option>
                                              <option value="KH">Camboya</option>
                                              <option value="CM">Camerún</option>
                                              <option value="CA">Canadá</option>
                                              <option value="TD">Chad</option>
                                              <option value="CL">Chile</option>
                                              <option value="CN">China</option>
                                              <option value="CY">Chipre</option>
                                              <option value="VA">Ciudad del Vaticano (Santa Sede)</option>
                                              <option value="CO">Colombia</option>
                                              <option value="KM">Comores</option>
                                              <option value="CG">Congo</option>
                                              <option value="CD">Congo, República Democrática del</option>
                                              <option value="KR">Corea</option>
                                              <option value="KP">Corea del Norte</option>
                                              <option value="CI">Costa de Marfíl</option>
                                              <option value="CR">Costa Rica</option>
                                              <option value="HR">Croacia (Hrvatska)</option>
                                              <option value="CU">Cuba</option>
                                              <option value="DK">Dinamarca</option>
                                              <option value="DJ">Djibouti</option>
                                              <option value="DM">Dominica</option>
                                              <option value="EC">Ecuador</option>
                                              <option value="EG">Egipto</option>
                                              <option value="SV">El Salvador</option>
                                              <option value="AE">Emiratos Árabes Unidos</option>
                                              <option value="ER">Eritrea</option>
                                              <option value="SI">Eslovenia</option>
                                              <option value="ES">España</option>
                                              <option value="US">Estados Unidos</option>
                                              <option value="EE">Estonia</option>
                                              <option value="ET">Etiopía</option>
                                              <option value="FJ">Fiji</option>
                                              <option value="PH">Filipinas</option>
                                              <option value="FI">Finlandia</option>
                                              <option value="FR">Francia</option>
                                              <option value="GA">Gabón</option>
                                              <option value="GM">Gambia</option>
                                              <option value="GE">Georgia</option>
                                              <option value="GH">Ghana</option>
                                              <option value="GI">Gibraltar</option>
                                              <option value="GD">Granada</option>
                                              <option value="GR">Grecia</option>
                                              <option value="GL">Groenlandia</option>
                                              <option value="GP">Guadalupe</option>
                                              <option value="GU">Guam</option>
                                              <option value="GT">Guatemala</option>
                                              <option value="GY">Guayana</option>
                                              <option value="GF">Guayana Francesa</option>
                                              <option value="GN">Guinea</option>
                                              <option value="GQ">Guinea Ecuatorial</option>
                                              <option value="GW">Guinea-Bissau</option>
                                              <option value="HT">Haití</option>
                                              <option value="HN">Honduras</option>
                                              <option value="HU">Hungría</option>
                                              <option value="IN">India</option>
                                              <option value="ID">Indonesia</option>
                                              <option value="IQ">Irak</option>
                                              <option value="IR">Irán</option>
                                              <option value="IE">Irlanda</option>
                                              <option value="BV">Isla Bouvet</option>
                                              <option value="CX">Isla de Christmas</option>
                                              <option value="IS">Islandia</option>
                                              <option value="KY">Islas Caimán</option>
                                              <option value="CK">Islas Cook</option>
                                              <option value="CC">Islas de Cocos o Keeling</option>
                                              <option value="FO">Islas Faroe</option>
                                              <option value="HM">Islas Heard y McDonald</option>
                                              <option value="FK">Islas Malvinas</option>
                                              <option value="MP">Islas Marianas del Norte</option>
                                              <option value="MH">Islas Marshall</option>
                                              <option value="UM">Islas menores de Estados Unidos</option>
                                              <option value="PW">Islas Palau</option>
                                              <option value="SB">Islas Salomón</option>
                                              <option value="SJ">Islas Svalbard y Jan Mayen</option>
                                              <option value="TK">Islas Tokelau</option>
                                              <option value="TC">Islas Turks y Caicos</option>
                                              <option value="VI">Islas Vírgenes (EEUU)</option>
                                              <option value="VG">Islas Vírgenes (Reino Unido)</option>
                                              <option value="WF">Islas Wallis y Futuna</option>
                                              <option value="IL">Israel</option>
                                              <option value="IT">Italia</option>
                                              <option value="JM">Jamaica</option>
                                              <option value="JP">Japón</option>
                                              <option value="JO">Jordania</option>
                                              <option value="KZ">Kazajistán</option>
                                              <option value="KE">Kenia</option>
                                              <option value="KG">Kirguizistán</option>
                                              <option value="KI">Kiribati</option>
                                              <option value="KW">Kuwait</option>
                                              <option value="LA">Laos</option>
                                              <option value="LS">Lesotho</option>
                                              <option value="LV">Letonia</option>
                                              <option value="LB">Líbano</option>
                                              <option value="LR">Liberia</option>
                                              <option value="LY">Libia</option>
                                              <option value="LI">Liechtenstein</option>
                                              <option value="LT">Lituania</option>
                                              <option value="LU">Luxemburgo</option>
                                              <option value="MK">Macedonia, Ex-República Yugoslava de</option>
                                              <option value="MG">Madagascar</option>
                                              <option value="MY">Malasia</option>
                                              <option value="MW">Malawi</option>
                                              <option value="MV">Maldivas</option>
                                              <option value="ML">Malí</option>
                                              <option value="MT">Malta</option>
                                              <option value="MA">Marruecos</option>
                                              <option value="MQ">Martinica</option>
                                              <option value="MU">Mauricio</option>
                                              <option value="MR">Mauritania</option>
                                              <option value="YT">Mayotte</option>
                                              <option value="MX" selected>México</option>
                                              <option value="FM">Micronesia</option>
                                              <option value="MD">Moldavia</option>
                                              <option value="MC">Mónaco</option>
                                              <option value="MN">Mongolia</option>
                                              <option value="MS">Montserrat</option>
                                              <option value="MZ">Mozambique</option>
                                              <option value="NA">Namibia</option>
                                              <option value="NR">Nauru</option>
                                              <option value="NP">Nepal</option>
                                              <option value="NI">Nicaragua</option>
                                              <option value="NE">Níger</option>
                                              <option value="NG">Nigeria</option>
                                              <option value="NU">Niue</option>
                                              <option value="NF">Norfolk</option>
                                              <option value="NO">Noruega</option>
                                              <option value="NC">Nueva Caledonia</option>
                                              <option value="NZ">Nueva Zelanda</option>
                                              <option value="OM">Omán</option>
                                              <option value="NL">Países Bajos</option>
                                              <option value="PA">Panamá</option>
                                              <option value="PG">Papúa Nueva Guinea</option>
                                              <option value="PK">Paquistán</option>
                                              <option value="PY">Paraguay</option>
                                              <option value="PE">Perú</option>
                                              <option value="PN">Pitcairn</option>
                                              <option value="PF">Polinesia Francesa</option>
                                              <option value="PL">Polonia</option>
                                              <option value="PT">Portugal</option>
                                              <option value="PR">Puerto Rico</option>
                                              <option value="QA">Qatar</option>
                                              <option value="UK">Reino Unido</option>
                                              <option value="CF">República Centroafricana</option>
                                              <option value="CZ">República Checa</option>
                                              <option value="ZA">República de Sudáfrica</option>
                                              <option value="DO">República Dominicana</option>
                                              <option value="SK">República Eslovaca</option>
                                              <option value="RE">Reunión</option>
                                              <option value="RW">Ruanda</option>
                                              <option value="RO">Rumania</option>
                                              <option value="RU">Rusia</option>
                                              <option value="EH">Sahara Occidental</option>
                                              <option value="KN">Saint Kitts y Nevis</option>
                                              <option value="WS">Samoa</option>
                                              <option value="AS">Samoa Americana</option>
                                              <option value="SM">San Marino</option>
                                              <option value="VC">San Vicente y Granadinas</option>
                                              <option value="SH">Santa Helena</option>
                                              <option value="LC">Santa Lucía</option>
                                              <option value="ST">Santo Tomé y Príncipe</option>
                                              <option value="SN">Senegal</option>
                                              <option value="SC">Seychelles</option>
                                              <option value="SL">Sierra Leona</option>
                                              <option value="SG">Singapur</option>
                                              <option value="SY">Siria</option>
                                              <option value="SO">Somalia</option>
                                              <option value="LK">Sri Lanka</option>
                                              <option value="PM">St Pierre y Miquelon</option>
                                              <option value="SZ">Suazilandia</option>
                                              <option value="SD">Sudán</option>
                                              <option value="SE">Suecia</option>
                                              <option value="CH">Suiza</option>
                                              <option value="SR">Surinam</option>
                                              <option value="TH">Tailandia</option>
                                              <option value="TW">Taiwán</option>
                                              <option value="TZ">Tanzania</option>
                                              <option value="TJ">Tayikistán</option>
                                              <option value="TF">Territorios franceses del Sur</option>
                                              <option value="TP">Timor Oriental</option>
                                              <option value="TG">Togo</option>
                                              <option value="TO">Tonga</option>
                                              <option value="TT">Trinidad y Tobago</option>
                                              <option value="TN">Túnez</option>
                                              <option value="TM">Turkmenistán</option>
                                              <option value="TR">Turquía</option>
                                              <option value="TV">Tuvalu</option>
                                              <option value="UA">Ucrania</option>
                                              <option value="UG">Uganda</option>
                                              <option value="UY">Uruguay</option>
                                              <option value="UZ">Uzbekistán</option>
                                              <option value="VU">Vanuatu</option>
                                              <option value="VE">Venezuela</option>
                                              <option value="VN">Vietnam</option>
                                              <option value="YE">Yemen</option>
                                              <option value="YU">Yugoslavia</option>
                                              <option value="ZM">Zambia</option>
                                              <option value="ZW">Zimbabue</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="select-agencias">
                                            <select class="selectpicker" data-style="select-with-transition" multiple title="Categoría" data-size="7">
                                              <option disabled>Selecciona</option>
                                              <option value="2">Categoría 1</option>
                                              <option value="3">Categoría 2</option>
                                              <option value="4">Categoría 3</option>
                                              <option value="5">Categoría 4</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 padding0">
                                    <div class="col-sm-6">
                                        <div class="select-agencias">
                                            <select class="selectpicker" data-style="select-with-transition" multiple title="Marca" data-size="7">
                                              <option disabled>Selecciona</option>
                                              <option value="2">Marca 1</option>
                                              <option value="3">Marca 2</option>
                                              <option value="4">Marca 3</option>
                                              <option value="5">Marca 4</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="select-agencias">
                                            <select class="selectpicker" data-style="select-with-transition" multiple title="Producto" data-size="7">
                                              <option disabled>Selecciona</option>
                                              <option value="2">Producto 1</option>
                                              <option value="3">Producto 2</option>
                                              <option value="4">Producto 3</option>
                                              <option value="5">Producto 4</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 margen-btn-agregar">
                                <button class="btn btn-primary nextBtn pull-right" type="button" id="btn-proyecto-propuestas">
                                	Continuar <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row setup-content" id="step-2">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <div class="padding2020">
                                <h3 class="text-uppercase"><b>Detalles del Proyecto</b></h3>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Solicitud puntual</label>
                                <textarea class="form-control" id="textarea" name="textarea" placeholder="Máx. 300 carácteres"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Objetivo del Proyecto</label>
                                <textarea class="form-control" id="textarea" name="textarea" placeholder="¿Cuál es el objetivo que se debe cumplir?"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Target</label>
                                <textarea class="form-control" id="textarea" name="textarea" placeholder="¿A quién dirigiremos esta iniciativa?"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Especificaciones / Observaciones generales</label>
                                <textarea class="form-control" id="textarea" name="textarea" placeholder="Ingresa las especificaciones puntuales que se deben seguir"></textarea>
                            </div>
                            <div class="col-sm-12 margen-btn-agregar">
                                <button class="btn btn-primary nextBtn pull-right" type="button" id="btn-proyecto-propuestas">
                                	Continuar <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row setup-content" id="step-3">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <div class="padding2020">
                                <h3 class="text-uppercase"><b>Ingresa la fecha límite para cada etapa del proyecto</b></h3>
                                <small>*Puedes agregar más campos</small>
                            </div>
                            <div class="col-sm-12 padding0">
                                <div class="col-sm-6">
                                    <div class="form-group input-group">
                                        <label class="control-label"><i class="fa fa-calendar" aria-hidden="true"></i>  Invitación a Agencias</label>
                                        <input type="text" class="form-control" id="reservation">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="form-group">
                                                    <input type="text" class="form-control datetimepicker" value="10/05/2016" />
                                                </div>
                                            </div>
                                        </div>

                                        <div id="sliderRegular"></div>
                                        <div id="sliderDouble"></div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group input-group">
                                        <label class="control-label"><i class="fa fa-calendar" aria-hidden="true"></i>  Preguntas y Respuestas</label>
                                        <input type="text" class="form-control" id="reservation">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 padding0">
                                <div class="col-sm-6 padding0">
                                    <div class="form-group input-group">
                                        <label class="control-label"><i class="fa fa-calendar" aria-hidden="true"></i>  Desarrollo de Propuestas</label>
                                        <input type="text" class="form-control" id="reservation">
                                    </div>
                                </div>
                                <div class="col-sm-6 padding0">
                                    <div class="form-group input-group">
                                        <label class="control-label"><i class="fa fa-calendar" aria-hidden="true"></i>  Selección del Ganador</label>
                                        <input type="text" class="form-control" id="reservation">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Nombre de la Etapa</label>
                                <input  maxlength="100" type="text" class="form-control" placeholder="..."  />
                            </div>

                            <div class="col-sm-12 margen-btn-agregar">
                                <button class="btn btn-primary nextBtn pull-right" type="button" id="btn-proyecto-propuestas">
                                	Continuar <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row setup-content" id="step-4">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <div class="padding2020">
                                <h3 class="text-uppercase">
                                	<b>Agregar filtros de Agencias</small></b>
                                </h3>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 padding0">
                                    <div class="select-agencias">
                                        <select class="selectpicker" data-style="select-with-transition" multiple title="Agregar filtros" data-size="7">
                                          <option disabled>Selecciona</option>
                                          <option value="2">Filtro 1</option>
                                          <option value="3">Filtro 2</option>
                                          <option value="4">Filtro 3</option>
                                          <option value="5">Filtro 4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 margen-btn-agregar">
                                <button class="btn btn-primary nextBtn pull-right" type="button" id="btn-proyecto-propuestas">
                                	Continuar <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row setup-content" id="step-5">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <div class="padding2020">
                                <h3 class="text-uppercase"><b>Documentos</b></h3>
                                <small>Carga los documentos relevantes para el Brief.<br>Puedes cargar videos desde un link externo o en formato .mp4</small>
                            </div>
                            <div class="form-group input-group">
                            	<label class="control-label">Agrega un documento  <i class="fa fa-video-camera" aria-hidden="true"></i></label>
                                <input type="text" name="multiple[]" class="form-control" placeholder="Ingresa url">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-add">
                                      <i class="material-icons">add_circle_outline</i>
                                  </button>
                                </span>
                            </div>
                            <div class="form-group input-group">
                            	<label class="control-label">Agrega vídeos desde una url  <i class="fa fa-video-camera" aria-hidden="true"></i></label>
                                <input type="text" name="multiple[]" class="form-control" placeholder="Ingresa url">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-add">
                                      <i class="material-icons">add_circle_outline</i>
                                  </button>
                                </span>
                            </div>
                            <div class="col-sm-12 margen-btn-agregar">
                                <button class="btn btn-primary nextBtn pull-right" type="button" id="btn-proyecto-propuestas">
                                	Continuar <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row setup-content" id="step-6">
                    <div class="col-xs-12">
                        <div class="col-md-12">

                            <div class="padding2020">
                                <h3 class="text-uppercase"><b>¡Felicidades!</b></h3>Haz creado el siguiente proyecto
                            </div>

                            <div class="row">

                                <div class="card-content">
                                    <div class="menu-filtros col-sm-12">
                                        <ul class="nav nav-pills nav-pills-warning btn-registro-modal">
                                            <li class="active"><a href="#pill1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                                            <li><a href="#pill2" data-toggle="tab" aria-expanded="false">Especificaciones</a></li>
                                            <li><a href="#pill3" data-toggle="tab" aria-expanded="false">Fechas</a></li>
                                            <li><a href="#pill4" data-toggle="tab" aria-expanded="false">Filtros</a></li>
                                            <li><a href="#pill5" data-toggle="tab" aria-expanded="false">Documentos</a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-content contenedor-facturas">

                                        <div class="tab-pane active" id="pill1">

                                        <div class="col-sm-12">
                                            <div class="card-content">
                                              <div class="material-datatables">
                                                  <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                                                      <div class="row">
                                                      	<div class="col-sm-12">
                                                          <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">
                                                            <tbody class="text-left">
                                                          	<tr class="odd" role="row">
                                                                  <td tabindex="0" class="sorting_1">Nombre del proyecto:</td>
                                                                  <td class="texto-tabla-registro">Nombre del Proyecto</td>
                                                              </tr>
                                                              <tr class="even" role="row">
                                                                  <td tabindex="0" class="sorting_1">Supervisor</td>
                                                                  <td class="texto-tabla-registro">Nombre del Supervisor</td>
                                                              </tr>
                                                              <tr class="odd" role="row">
                                                                  <td tabindex="0" class="sorting_1">Colaboradores</td>
                                                                  <td class="texto-tabla-registro">Nombre del Colaborador</td>
                                                              </tr>
                                                              <tr class="even" role="row">
                                                                  <td tabindex="0" class="sorting_1">País</td>
                                                                  <td class="texto-tabla-registro">País</td>
                                                               </tr>
                                                              <tr class="odd" role="row">
                                                                  <td class="sorting_1" tabindex="0">Categoría</td>
                                                                  <td class="texto-tabla-registro">Nombre de la categoría</td>
                                                              </tr>
                                                              <tr class="even" role="row">
                                                                  <td tabindex="0" class="sorting_1">Marca</td>
                                                                  <td class="texto-tabla-registro">Nombre de la marca</td>
                                                              </tr>

                                                              <tr class="even" role="row">
                                                                  <td tabindex="0" class="sorting_1">Producto</td>
                                                                  <td class="texto-tabla-registro">Nombre del producto</td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                   </div>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                        </div>

                                        <!-- Opcion 2 -->
                                        <div class="tab-pane" id="pill2">

                                          <div class="col-sm-12">
                                              <div class="card-content">
                                                <div class="material-datatables">
                                                    <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                                                        <div class="row">
                                                          <div class="col-sm-12">
                                                            <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">
                                                              <tbody class="text-left">
                                                              <tr class="odd" role="row">
                                                                    <td tabindex="0" class="sorting_1">Solicitud puntual:</td>
                                                                    <td class="texto-tabla-registro">Solicitud</td>
                                                                </tr>
                                                                <tr class="even" role="row">
                                                                    <td tabindex="0" class="sorting_1">Objetivo del proyecto:</td>
                                                                    <td class="texto-tabla-registro">Objetivo</td>
                                                                </tr>
                                                                <tr class="odd" role="row">
                                                                    <td tabindex="0" class="sorting_1">Target:</td>
                                                                    <td class="texto-tabla-registro">Target</td>
                                                                </tr>
                                                                <tr class="even" role="row">
                                                                    <td tabindex="0" class="sorting_1">Especificaciones:</td>
                                                                    <td class="texto-tabla-registro">Especificaciones</td>
                                                                 </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                     </div>
                                                  </div>
                                              </div>
                                            </div>
                                          </div>

                                        </div>
                                        <!-- Opcion 2 -->


                                        <!-- Opcion 3 -->
                                        <div class="tab-pane" id="pill3">

                                          <div class="col-sm-12">
                                              <div class="card-content">
                                                <div class="material-datatables">
                                                    <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                                                        <div class="row">
                                                          <div class="col-sm-12">
                                                            <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">
                                                              <tbody class="text-left">
                                                              <tr class="odd" role="row">
                                                                    <td tabindex="0" class="sorting_1">Invitación a agencias:</td>
                                                                    <td class="texto-tabla-registro">00/00/0000</td>
                                                                </tr>
                                                                <tr class="even" role="row">
                                                                    <td tabindex="0" class="sorting_1">Preguntas y respuestas</td>
                                                                    <td class="texto-tabla-registro">00/00/0000</td>
                                                                </tr>
                                                                <tr class="odd" role="row">
                                                                    <td tabindex="0" class="sorting_1">Desarrollo de propuestas:</td>
                                                                    <td class="texto-tabla-registro">00/00/0000</td>
                                                                </tr>
                                                                <tr class="even" role="row">
                                                                    <td tabindex="0" class="sorting_1">Selección del ganador:</td>
                                                                    <td class="texto-tabla-registro">00/00/0000</td>
                                                                 </tr>
                                                                 <tr class="even" role="row">
                                                                     <td tabindex="0" class="sorting_1">Nombre de la etapa:</td>
                                                                     <td class="texto-tabla-registro">Etapa</td>
                                                                  </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                     </div>
                                                  </div>
                                              </div>
                                            </div>
                                          </div>

                                        </div>
                                        <!-- Opcion 3 -->



                                        <!-- Opcion 4 -->
                                        <div class="tab-pane" id="pill4">

                                          <div class="col-sm-12">
                                              <div class="card-content">
                                                <div class="material-datatables">
                                                    <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                                                        <div class="row">
                                                          <div class="col-sm-12">
                                                            <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">
                                                              <tbody class="text-left">
                                                                <tr class="even" role="row">
                                                                    <td tabindex="0" class="sorting_1">Filtros</td>
                                                                    <td class="texto-tabla-registro">Filtro 1, Filtro2, Filtro 3, Filtro4, Filtro5</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                     </div>
                                                  </div>
                                              </div>
                                            </div>
                                          </div>

                                        </div>
                                        <!-- Opcion 4 -->

                                        <!-- Opcion 5 -->
                                        <div class="tab-pane" id="pill5">

                                          <div class="col-sm-12">
                                              <div class="card-content">
                                                <div class="material-datatables">
                                                    <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                                                        <div class="row">
                                                          <div class="col-sm-12">
                                                            <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">
                                                              <tbody class="text-left">
                                                                <tr class="even" role="row">
                                                                    <td tabindex="0" class="sorting_1">Documento</td>
                                                                    <td class="texto-tabla-registro">Nombre del documento</td>
                                                                    <td class="text-right">
                                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                                        	<i class="material-icons">remove_red_eye</i>
                                                                        </a>
                                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                                        	<i class="material-icons">close</i>
                                                                        </a>
                                                                    </td>
                                                                 </tr>
                                                                 <tr class="even" role="row">
                                                                     <td tabindex="0" class="sorting_1">Documento</td>
                                                                     <td class="texto-tabla-registro">Nombre del documento</td>
                                                                     <td class="text-right">
                                                                         <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                                         	<i class="material-icons">remove_red_eye</i>
                                                                         </a>
                                                                         <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                                         	<i class="material-icons">close</i>
                                                                         </a>
                                                                     </td>
                                                                  </tr>
                                                                  <tr class="even" role="row">
                                                                      <td tabindex="0" class="sorting_1">Documento</td>
                                                                      <td class="texto-tabla-registro">Nombre del documento</td>
                                                                      <td class="text-right">
                                                                          <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                                          	<i class="material-icons">remove_red_eye</i>
                                                                          </a>
                                                                          <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                                          	<i class="material-icons">close</i>
                                                                          </a>
                                                                      </td>
                                                                   </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                     </div>
                                                  </div>
                                              </div>
                                            </div>
                                          </div>

                                        </div>

                                        <div class="col-sm-12 padding2020">
                                            <h3 class="text-uppercase"><b>Resumen de fechas</b></h3>
                                        </div>

                                        <div class="col-sm-12">
                                          <div id="chart_div"></div>
                                        </div>


                                        <div class="col-sm-12 invitacion-modal-registro text-center">
                                            <div class="col-sm-12">Ahora selecciona las agencias que deseas<br>invitar a participar de este pitch</div>
                                            <div class="col-sm-12">
                                                <a href="agencias.php">
                                                  <button class="btn btn-success btn-lg margen-btn-agregar">
                                                    Invitar Agencias al Proyecto <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                                  </button>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- Opcion 5 -->




                                    </div>
                                </div>


                            </div>



                        </div>
                    </div>
                </div>
            </form>

      	</div>
    </div>
</div>
<!-- Termina Modal Proceso de Registro -->

<!-- Modal Eliminar Proyecto -->
<div class="modal fade" id="eliminar-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b>Eliminar Proyecto</b><br>
                  ¿Seguro que deseas eliminar permanentemente el Proyecto:<br>"Nombre del Proyecto"?
                </div>

                <input style="display: none;" class="swal2-input">
                <input style="display: none;" class="swal2-file" type="file">
                <div class="swal2-range" style="display: none;">
                    <output></output><input type="range">
                </div>
                <select style="display: none;" class="swal2-select"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
                <textarea style="display: none;" class="swal2-textarea"></textarea>
                <div class="swal2-validationerror" style="display: none;"></div>
                <hr class="swal2-spacer" style="display: block;">
                <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">NO</button>
                <button type="button" class="swal2-confirm btn btn-success">SI</button>
                <span class="swal2-close" style="display: none;">×</span>
            </div>
    </div>
</div>
<!-- Termina Modal Eliminar Proyecto -->

<!-- Modal Pausar Proyecto -->
<div class="modal fade" id="pausar-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b>Pausar Proyecto</b><br>
                  ¿Seguro que deseas pausar el Proyecto:<br>"Nombre del Proyecto"?
                </div>

                <input style="display: none;" class="swal2-input">
                <input style="display: none;" class="swal2-file" type="file">
                <div class="swal2-range" style="display: none;">
                    <output></output><input type="range">
                </div>
                <select style="display: none;" class="swal2-select"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
                <textarea style="display: none;" class="swal2-textarea"></textarea>
                <div class="swal2-validationerror" style="display: none;"></div>
                <hr class="swal2-spacer" style="display: block;">
                <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">NO</button>
                <button type="button" class="swal2-confirm btn btn-success">SI</button>
                <span class="swal2-close" style="display: none;">×</span>
            </div>
    </div>
</div>
<!-- Termina Modal Pausar Proyecto -->

<!-- Modal Invitar Proyecto -->
<div class="modal fade" id="invitar-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b>Confirmación</b><br>
                  ¿Seguro que deseas invitar a la Agencia<br>
                  "Nombre de la Agencia"<br>
                  a tu Proyecto:<br>"Nombre del Proyecto"?
                </div>

                <input style="display: none;" class="swal2-input">
                <input style="display: none;" class="swal2-file" type="file">
                <div class="swal2-range" style="display: none;">
                    <output></output><input type="range">
                </div>
                <select style="display: none;" class="swal2-select"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
                <textarea style="display: none;" class="swal2-textarea"></textarea>
                <div class="swal2-validationerror" style="display: none;"></div>
                <hr class="swal2-spacer" style="display: block;">
                <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Cancelar</button>
                <button type="button" class="swal2-confirm btn btn-success">Invitar</button>
                <span class="swal2-close" style="display: none;">×</span>
            </div>
    </div>
</div>
<!-- Termina Modal Invitar Proyecto -->

<!-- Modal Invitacion Rechazada Proyecto -->
<div class="modal fade" id="rechazo-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b>Invitación rechazada</b><br>
                  La agencia:<br>"Nombre de la Agencia"<br>
                  rechazó la invitación al Proyecto por el siguiente motivo:<br>
                  Motivo.
                </div>

                <input style="display: none;" class="swal2-input">
                <input style="display: none;" class="swal2-file" type="file">
                <div class="swal2-range" style="display: none;">
                    <output></output><input type="range">
                </div>
                <select style="display: none;" class="swal2-select"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
                <textarea style="display: none;" class="swal2-textarea"></textarea>
                <div class="swal2-validationerror" style="display: none;"></div>
                <hr class="swal2-spacer" style="display: block;">
                <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
                <span class="swal2-close" style="display: none;">×</span>
            </div>
    </div>
</div>
<!-- Termina Modal Invitacion Rechazada Proyecto -->

<!-- Modal Editar Proyecto -->
<div class="modal fade" id="editar-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      	<div class="loginmodal-container text-center img-modal-registro" id="modal-login">
            <img src="../img/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="padding2020">
            	<b>Editar información del Proyecto</b>
            </div>

            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1primary" data-toggle="tab">Datos del Proyecto</a></li>
                            <li><a href="#tab2primary" data-toggle="tab">Objetivos y Lineamientos</a></li>
                            <li><a href="#tab3primary" data-toggle="tab">Fechas</a></li>
                            <li><a href="#tab4primary" data-toggle="tab">Lineamientos</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                    	<!-- Tab 1 -->
                        <div class="tab-pane fade in active" id="tab1primary">
                            <div class="form-group">
                                <label class="control-label">Nombre del Proyecto</label>
                                <input  maxlength="100" type="text" class="form-control" placeholder="..."  />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Correo del responsable</label>
                                <input maxlength="100" type="mail" class="form-control" placeholder="..." />
                            </div>
                            <div class="form-group">
                                <label class="control-label">País</label>
                                  <select id="selectbasic" name="selectbasic" class="form-control">
                                      <option value="cero">Selecciona tu País</option>
                                      <option value="AF">Afganistán</option>
                                      <option value="AL">Albania</option>
                                      <option value="DE">Alemania</option>
                                      <option value="AD">Andorra</option>
                                      <option value="AO">Angola</option>
                                      <option value="AI">Anguilla</option>
                                      <option value="AQ">Antártida</option>
                                      <option value="AG">Antigua y Barbuda</option>
                                      <option value="AN">Antillas Holandesas</option>
                                      <option value="SA">Arabia Saudí</option>
                                      <option value="DZ">Argelia</option>
                                      <option value="AR">Argentina</option>
                                      <option value="AM">Armenia</option>
                                      <option value="AW">Aruba</option>
                                      <option value="AU">Australia</option>
                                      <option value="AT">Austria</option>
                                      <option value="AZ">Azerbaiyán</option>
                                      <option value="BS">Bahamas</option>
                                      <option value="BH">Bahrein</option>
                                      <option value="BD">Bangladesh</option>
                                      <option value="BB">Barbados</option>
                                      <option value="BE">Bélgica</option>
                                      <option value="BZ">Belice</option>
                                      <option value="BJ">Benin</option>
                                      <option value="BM">Bermudas</option>
                                      <option value="BY">Bielorrusia</option>
                                      <option value="MM">Birmania</option>
                                      <option value="BO">Bolivia</option>
                                      <option value="BA">Bosnia y Herzegovina</option>
                                      <option value="BW">Botswana</option>
                                      <option value="BR">Brasil</option>
                                      <option value="BN">Brunei</option>
                                      <option value="BG">Bulgaria</option>
                                      <option value="BF">Burkina Faso</option>
                                      <option value="BI">Burundi</option>
                                      <option value="BT">Bután</option>
                                      <option value="CV">Cabo Verde</option>
                                      <option value="KH">Camboya</option>
                                      <option value="CM">Camerún</option>
                                      <option value="CA">Canadá</option>
                                      <option value="TD">Chad</option>
                                      <option value="CL">Chile</option>
                                      <option value="CN">China</option>
                                      <option value="CY">Chipre</option>
                                      <option value="VA">Ciudad del Vaticano (Santa Sede)</option>
                                      <option value="CO">Colombia</option>
                                      <option value="KM">Comores</option>
                                      <option value="CG">Congo</option>
                                      <option value="CD">Congo, República Democrática del</option>
                                      <option value="KR">Corea</option>
                                      <option value="KP">Corea del Norte</option>
                                      <option value="CI">Costa de Marfíl</option>
                                      <option value="CR">Costa Rica</option>
                                      <option value="HR">Croacia (Hrvatska)</option>
                                      <option value="CU">Cuba</option>
                                      <option value="DK">Dinamarca</option>
                                      <option value="DJ">Djibouti</option>
                                      <option value="DM">Dominica</option>
                                      <option value="EC">Ecuador</option>
                                      <option value="EG">Egipto</option>
                                      <option value="SV">El Salvador</option>
                                      <option value="AE">Emiratos Árabes Unidos</option>
                                      <option value="ER">Eritrea</option>
                                      <option value="SI">Eslovenia</option>
                                      <option value="ES">España</option>
                                      <option value="US">Estados Unidos</option>
                                      <option value="EE">Estonia</option>
                                      <option value="ET">Etiopía</option>
                                      <option value="FJ">Fiji</option>
                                      <option value="PH">Filipinas</option>
                                      <option value="FI">Finlandia</option>
                                      <option value="FR">Francia</option>
                                      <option value="GA">Gabón</option>
                                      <option value="GM">Gambia</option>
                                      <option value="GE">Georgia</option>
                                      <option value="GH">Ghana</option>
                                      <option value="GI">Gibraltar</option>
                                      <option value="GD">Granada</option>
                                      <option value="GR">Grecia</option>
                                      <option value="GL">Groenlandia</option>
                                      <option value="GP">Guadalupe</option>
                                      <option value="GU">Guam</option>
                                      <option value="GT">Guatemala</option>
                                      <option value="GY">Guayana</option>
                                      <option value="GF">Guayana Francesa</option>
                                      <option value="GN">Guinea</option>
                                      <option value="GQ">Guinea Ecuatorial</option>
                                      <option value="GW">Guinea-Bissau</option>
                                      <option value="HT">Haití</option>
                                      <option value="HN">Honduras</option>
                                      <option value="HU">Hungría</option>
                                      <option value="IN">India</option>
                                      <option value="ID">Indonesia</option>
                                      <option value="IQ">Irak</option>
                                      <option value="IR">Irán</option>
                                      <option value="IE">Irlanda</option>
                                      <option value="BV">Isla Bouvet</option>
                                      <option value="CX">Isla de Christmas</option>
                                      <option value="IS">Islandia</option>
                                      <option value="KY">Islas Caimán</option>
                                      <option value="CK">Islas Cook</option>
                                      <option value="CC">Islas de Cocos o Keeling</option>
                                      <option value="FO">Islas Faroe</option>
                                      <option value="HM">Islas Heard y McDonald</option>
                                      <option value="FK">Islas Malvinas</option>
                                      <option value="MP">Islas Marianas del Norte</option>
                                      <option value="MH">Islas Marshall</option>
                                      <option value="UM">Islas menores de Estados Unidos</option>
                                      <option value="PW">Islas Palau</option>
                                      <option value="SB">Islas Salomón</option>
                                      <option value="SJ">Islas Svalbard y Jan Mayen</option>
                                      <option value="TK">Islas Tokelau</option>
                                      <option value="TC">Islas Turks y Caicos</option>
                                      <option value="VI">Islas Vírgenes (EEUU)</option>
                                      <option value="VG">Islas Vírgenes (Reino Unido)</option>
                                      <option value="WF">Islas Wallis y Futuna</option>
                                      <option value="IL">Israel</option>
                                      <option value="IT">Italia</option>
                                      <option value="JM">Jamaica</option>
                                      <option value="JP">Japón</option>
                                      <option value="JO">Jordania</option>
                                      <option value="KZ">Kazajistán</option>
                                      <option value="KE">Kenia</option>
                                      <option value="KG">Kirguizistán</option>
                                      <option value="KI">Kiribati</option>
                                      <option value="KW">Kuwait</option>
                                      <option value="LA">Laos</option>
                                      <option value="LS">Lesotho</option>
                                      <option value="LV">Letonia</option>
                                      <option value="LB">Líbano</option>
                                      <option value="LR">Liberia</option>
                                      <option value="LY">Libia</option>
                                      <option value="LI">Liechtenstein</option>
                                      <option value="LT">Lituania</option>
                                      <option value="LU">Luxemburgo</option>
                                      <option value="MK">Macedonia, Ex-República Yugoslava de</option>
                                      <option value="MG">Madagascar</option>
                                      <option value="MY">Malasia</option>
                                      <option value="MW">Malawi</option>
                                      <option value="MV">Maldivas</option>
                                      <option value="ML">Malí</option>
                                      <option value="MT">Malta</option>
                                      <option value="MA">Marruecos</option>
                                      <option value="MQ">Martinica</option>
                                      <option value="MU">Mauricio</option>
                                      <option value="MR">Mauritania</option>
                                      <option value="YT">Mayotte</option>
                                      <option value="MX" selected>México</option>
                                      <option value="FM">Micronesia</option>
                                      <option value="MD">Moldavia</option>
                                      <option value="MC">Mónaco</option>
                                      <option value="MN">Mongolia</option>
                                      <option value="MS">Montserrat</option>
                                      <option value="MZ">Mozambique</option>
                                      <option value="NA">Namibia</option>
                                      <option value="NR">Nauru</option>
                                      <option value="NP">Nepal</option>
                                      <option value="NI">Nicaragua</option>
                                      <option value="NE">Níger</option>
                                      <option value="NG">Nigeria</option>
                                      <option value="NU">Niue</option>
                                      <option value="NF">Norfolk</option>
                                      <option value="NO">Noruega</option>
                                      <option value="NC">Nueva Caledonia</option>
                                      <option value="NZ">Nueva Zelanda</option>
                                      <option value="OM">Omán</option>
                                      <option value="NL">Países Bajos</option>
                                      <option value="PA">Panamá</option>
                                      <option value="PG">Papúa Nueva Guinea</option>
                                      <option value="PK">Paquistán</option>
                                      <option value="PY">Paraguay</option>
                                      <option value="PE">Perú</option>
                                      <option value="PN">Pitcairn</option>
                                      <option value="PF">Polinesia Francesa</option>
                                      <option value="PL">Polonia</option>
                                      <option value="PT">Portugal</option>
                                      <option value="PR">Puerto Rico</option>
                                      <option value="QA">Qatar</option>
                                      <option value="UK">Reino Unido</option>
                                      <option value="CF">República Centroafricana</option>
                                      <option value="CZ">República Checa</option>
                                      <option value="ZA">República de Sudáfrica</option>
                                      <option value="DO">República Dominicana</option>
                                      <option value="SK">República Eslovaca</option>
                                      <option value="RE">Reunión</option>
                                      <option value="RW">Ruanda</option>
                                      <option value="RO">Rumania</option>
                                      <option value="RU">Rusia</option>
                                      <option value="EH">Sahara Occidental</option>
                                      <option value="KN">Saint Kitts y Nevis</option>
                                      <option value="WS">Samoa</option>
                                      <option value="AS">Samoa Americana</option>
                                      <option value="SM">San Marino</option>
                                      <option value="VC">San Vicente y Granadinas</option>
                                      <option value="SH">Santa Helena</option>
                                      <option value="LC">Santa Lucía</option>
                                      <option value="ST">Santo Tomé y Príncipe</option>
                                      <option value="SN">Senegal</option>
                                      <option value="SC">Seychelles</option>
                                      <option value="SL">Sierra Leona</option>
                                      <option value="SG">Singapur</option>
                                      <option value="SY">Siria</option>
                                      <option value="SO">Somalia</option>
                                      <option value="LK">Sri Lanka</option>
                                      <option value="PM">St Pierre y Miquelon</option>
                                      <option value="SZ">Suazilandia</option>
                                      <option value="SD">Sudán</option>
                                      <option value="SE">Suecia</option>
                                      <option value="CH">Suiza</option>
                                      <option value="SR">Surinam</option>
                                      <option value="TH">Tailandia</option>
                                      <option value="TW">Taiwán</option>
                                      <option value="TZ">Tanzania</option>
                                      <option value="TJ">Tayikistán</option>
                                      <option value="TF">Territorios franceses del Sur</option>
                                      <option value="TP">Timor Oriental</option>
                                      <option value="TG">Togo</option>
                                      <option value="TO">Tonga</option>
                                      <option value="TT">Trinidad y Tobago</option>
                                      <option value="TN">Túnez</option>
                                      <option value="TM">Turkmenistán</option>
                                      <option value="TR">Turquía</option>
                                      <option value="TV">Tuvalu</option>
                                      <option value="UA">Ucrania</option>
                                      <option value="UG">Uganda</option>
                                      <option value="UY">Uruguay</option>
                                      <option value="UZ">Uzbekistán</option>
                                      <option value="VU">Vanuatu</option>
                                      <option value="VE">Venezuela</option>
                                      <option value="VN">Vietnam</option>
                                      <option value="YE">Yemen</option>
                                      <option value="YU">Yugoslavia</option>
                                      <option value="ZM">Zambia</option>
                                      <option value="ZW">Zimbabue</option>
                                  </select>

                                <label class="control-label">Categoría</label>
                                  <select id="selectbasic" name="selectbasic" class="form-control">
                                    <option value="1">Categoría 1</option>
                                    <option value="2">Categoría 2</option>
                                    <option value="3">Categoría 3</option>
                                    <option value="4">Categoría 4</option>
                                    <option value="5">Categoría 5</option>
                                  </select>

                                 <label class="control-label">Marca</label>
                                  <select id="selectbasic" name="selectbasic" class="form-control">
                                    <option value="1">Marca 1</option>
                                    <option value="2">Marca 2</option>
                                    <option value="3">Marca 3</option>
                                    <option value="4">Marca 4</option>
                                    <option value="5">Marca 5</option>
                                  </select>

                                 <label class="control-label">Producto</label>
                                  <select id="selectbasic" name="selectbasic" class="form-control">
                                    <option value="1">Producto 1</option>
                                    <option value="2">Producto 2</option>
                                    <option value="3">Producto 3</option>
                                    <option value="4">Producto 4</option>
                                    <option value="5">Producto 5</option>
                                  </select>
                            </div>
                        </div>
                        <!-- Termina Tab 1 -->

                        <!-- Termina Tab 2 -->
                        <div class="tab-pane fade" id="tab2primary">
                            <div class="form-group">
                                <label class="control-label">Solicitud puntual</label>
                                <textarea class="form-control" id="textarea" name="textarea"
                                placeholder="Ingresa la solicitud del proyecto."></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Objetivo del Proyecto</label>
                                <textarea class="form-control" id="textarea" name="textarea"
                                placeholder="¿Cuál es el objetivo que se debe cumplir?"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Público Objetivo</label>
                                <textarea class="form-control" id="textarea" name="textarea"
                                placeholder="¿A quién dirigiremos esta iniciativa?"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Especificaciones / Observaciones generales</label>
                                <textarea class="form-control" id="textarea" name="textarea"
                                placeholder="Ingresa las especificaciones puntuales que se deben seguir"></textarea>
                            </div>
                        </div>
                        <!-- Termina Tab 2 -->

                        <!-- Termina Tab 3 -->
                        <div class="tab-pane fade" id="tab3primary">
                            <div class="form-group input-group">
                            	<label class="control-label">Invitación  <i class="fa fa-calendar" aria-hidden="true"></i></label>
                                <input type="text" name="multiple[]" class="form-control" placeholder="dd/mm/aaa">
                                <span class="input-group-btn">
                                	<button type="button" class="btn btn-default btn-add">+</button>
                                </span>
                            </div>
                            <div class="form-group input-group">
                            	<label class="control-label">Sesión de Preguntas y Respuestas  <i class="fa fa-calendar" aria-hidden="true"></i></label>
                                <input type="text" name="multiple[]" class="form-control" placeholder="dd/mm/aaa">
                                <span class="input-group-btn">
                                	<button type="button" class="btn btn-default btn-add">+</button>
                                </span>
                            </div>
                            <div class="form-group input-group">
                            	<label class="control-label">Desarrollo de Propuestas  <i class="fa fa-calendar" aria-hidden="true"></i></label>
                                <input type="text" name="multiple[]" class="form-control" placeholder="dd/mm/aaa">
                                <span class="input-group-btn">
                                	<button type="button" class="btn btn-default btn-add">+</button>
                                </span>
                            </div>
                            <div class="form-group input-group">
                                <label class="control-label">Otro</label>
                                <select id="selectbasic" name="selectbasic" class="form-control">
                                    <option value="1">Otro 1</option>
                                    <option value="2">Otro 2</option>
                                    <option value="3">Otro 3</option>
                                    <option value="4">Otro 4</option>
                                    <option value="5">Otro 5</option>
                                </select>
                            </div>
                            <div class="form-group input-group">
                            	<label class="control-label">Nombre de la Etapa  <i class="fa fa-calendar" aria-hidden="true"></i></label>
                                <input type="text" name="multiple[]" class="form-control" placeholder="dd/mm/aaa">
                                <span class="input-group-btn">
                                	<button type="button" class="btn btn-default btn-add">+</button>
                                </span>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Selección del Ganador  <i class="fa fa-calendar" aria-hidden="true"></i></label>
                                <input maxlength="200" type="text" class="form-control" placeholder="dd/mm/aaa" />
                            </div>
                        </div>
                        <!-- Termina Tab 3 -->

                        <!-- Termina Tab 4 -->
                        <div class="tab-pane fade" id="tab4primary">
                            <div class="form-group input-group">
                            	<label class="control-label">Agrega un Documento  <i class="fa fa-upload" aria-hidden="true"></i></label>
                                <input type="file" name="multiple[]" class="form-control" placeholder="Selecciona archivo">
                                <span class="input-group-btn">
                                	<button type="button" class="btn btn-default btn-add">+</button>
                                </span>
                            </div>
                            <div class="form-group input-group">
                            	<label class="control-label">Agrega vídeos desde una url  <i class="fa fa-video-camera" aria-hidden="true"></i></label>
                                <input type="text" name="multiple[]" class="form-control" placeholder="Ingresa url">
                                <span class="input-group-btn">
                                	<button type="button" class="btn btn-default btn-add">+</button>
                                </span>
                            </div>
                        </div>
                        <!-- Termina Tab 4 -->
                    </div>
                </div>
            </div>

            <form>
                <div class="col-sm-12 padding0">
                  <div class="col-sm-6"><input type="submit" name="login" class="login loginmodal-submit" value="Cancelar"></div>
                  <div class="col-sm-6"><input type="submit" name="login" class="login loginmodal-submit" value="Guardar"></div>
                </div>
          	</form>
      	</div>
    </div>
</div>
<!-- Termina Modal Editar Proyecto -->

<!-- Modal Cancelar Proyecto -->
<div class="modal fade" id="cancelar-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Cancelar Proyecto</b><br>
              ¿Seguro que deseas cancelar el proyecto<br>
              "Nombre del Proyecto"?
            </div>

            <div class="form-group">
                <label class="control-label">Envia una explicación a las agencias participantes</label>
                <textarea class="form-control" id="textarea" name="textarea" placeholder="Se ha cancelado este proyecto, porque..."></textarea>
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Cancelar Proyecto -->

<!-- Modal Agradecer participación-->
<div class="modal fade" id="agradecer-participacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Agradecer participación</b><br>
              Agradece a la agencia y brinda de porqué su propuesta no fue elegida.
            </div>

            <div class="form-group">
                <label class="control-label">Su propuesta fue rechazada porque...</label>
                <textarea class="form-control" id="textarea" name="textarea" placeholder="Comentarios..."></textarea>
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal agradecer participación -->

<!-- Modal Agencia ganadora-->
<div class="modal fade" id="agencia-ganadora" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Seleccionar Ganador</b><br>
              Estas seleccionando a:<br>
              "Nombre de la Agencia"<br>
              Para llevar acabo el proyecto<br>
              "Nombre del Proyecto"
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Cancelar</button>
            <a title="Agencia ganadora" href="#agencia-ganadora-final" data-toggle="modal">
                <button type="button" class="swal2-confirm btn btn-success">Confirmar</button>
            </a>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal agencia ganadora -->

<!-- Modal Agencia ganadora Final-->
<div class="modal fade" id="agencia-ganadora-final" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Felicidades,<br>has completado tu proceso de selección</b><br>
              Notificaremos a:<br>
              "Nombre de la Agencia"
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-confirm btn btn-success">Finalizar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal agencia ganadora Final -->

<!-- Modal Fecha de presentación-->
<div class="modal fade" id="fecha-presentacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Editar fecha de presentación</b><br>
              Edita la fecha o modo de presentación, notificaremos a la agencia.
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Presentación Presencial</label>
                </div>
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Presentación AV</label>
                </div>
            </div>

            <div class="form-group">
                <label class="label-control">Selecciona la fecha</label>
                <input class="form-control datepicker" value="10/10/2016" style="" type="text">
                <span class="material-input"></span>
            </div>

            <div class="form-group">
                <label class="label-control">Selecciona el horario</label>
                <input class="form-control timepicker" value="14:00" style="" type="text">
                <span class="material-input"></span>
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success">Enviar cambios</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Fecha de presentación -->

<!-- Modal Invitar presentación-->
<div class="modal fade" id="invitar-presentacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Invitar a presentar</b><br>
              Invita a la agencia a presentar su propuesta
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Presentación Presencial</label>
                </div>
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Presentación AV</label>
                </div>
            </div>

            <div class="form-group">
                <label class="label-control">Selecciona la fecha</label>
                <input class="form-control datepicker" value="10/10/2016" style="" type="text">
                <span class="material-input"></span>
            </div>

            <div class="form-group">
                <label class="label-control">Selecciona el horario</label>
                <input class="form-control timepicker" value="14:00" style="" type="text">
                <span class="material-input"></span>
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success">Invitar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Invitar presentación-->

<!-- Modal Propuestas-->
<div class="modal fade" id="propuestas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Propuesta</b><br>
              "Nombre de la Agencia"<br>
              Documentos de la Propuesta
            </div>







            <div class="card-content">
              <div class="material-datatables">
                  <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                    <div class="row">
                          <div class="col-xs-6 col-sm-6">
                                <div class="select-agencias">
                                    <select class="selectpicker" data-style="select-with-transition"
                                    multiple title="Filtrar por" data-size="7">
                                      <option disabled>Filtrar</option>
                                      <option value="2">Filtro 1</option>
                                      <option value="2">Filtro 2</option>
                                      <option value="2">Filtro 3</option>
                                    </select>
                                </div>
                          </div>
                          <div class="col-xs-6 col-sm-6">
                            <div class="dataTables_filter" id="datatables_filter">
                                  <div id="wrap">
                                    <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="row botones-ajustes">
                          <div class="col-sm-2">
                               <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Importar XML</button></a>
                          </div>
                          <div class="col-sm-2">
                              <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Agregar Usuario</button></a>
                          </div>
                          <div class="col-sm-2">
                              <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Descargar XML</button></a>
                          </div>
                          <div class="col-sm-2">
                              <a title="Agregar usuario" href="#agregar-usuario-admin" data-toggle="modal"><button class="btn btn-primary btn-round" id="btn-ver-agencia">Agregar usuario</button></a>
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-12">
                          <table aria-describedby="datatables_info" role="grid" id="datatables"
                              class="table table-striped table-no-bordered table-hover dataTable dtr-inline"
                              style="width: 100%;" cellspacing="0" width="100%">

                          <thead>
                              <tr role="row">
                                    <th aria-sort="descending" aria-label="Name: activate to sort column ascending"
                                      style="width: 163px;" colspan="1" rowspan="1" aria-controls="datatables"
                                      tabindex="0" class="sorting_desc">ID</th>

                                      <th aria-label="Position: activate to sort column ascending"
                                      style="width: 241px;" colspan="1" rowspan="1" aria-controls="datatables"
                                      tabindex="0" class="sorting">Usuario</th>

                                      <th aria-label="Office: activate to sort column ascending"
                                      style="width: 123px;" colspan="1" rowspan="1" aria-controls="datatables"
                                      tabindex="0" class="sorting">Perfil</th>

                                      <th aria-label="Actions: activate to sort column ascending"
                                      style="width: 116px;" colspan="1" rowspan="1" aria-controls="datatables"
                                      tabindex="0" class="disabled-sorting text-right sorting">Acciones
                                      </th>
                                  </tr>
                          </thead>

                              <tfoot>
                                  <tr>
                                    <th colspan="1" rowspan="1">ID</th>
                                      <th colspan="1" rowspan="1">Usuario</th>
                                      <th colspan="1" rowspan="1">Perfil</th>
                                      <th colspan="1" rowspan="1" class="text-right">Acciones</th>
                                   </tr>
                              </tfoot>

                      <tbody>
                        <tr class="odd" role="row">
                              <td tabindex="0" class="sorting_1">1</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Administrador</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                          </tr>

                          <tr class="even" role="row">
                              <td tabindex="0" class="sorting_1">2</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Supervisor</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                          </tr>

                          <tr class="odd" role="row">
                              <td tabindex="0" class="sorting_1">3</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Usuario</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                          </tr>

                          <tr class="even" role="row">
                              <td tabindex="0" class="sorting_1">4</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Usuario</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                           </tr>

                          <tr class="odd" role="row">
                              <td class="sorting_1" tabindex="0">5</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Usuario</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                          </tr>

                          <tr class="even" role="row">
                              <td tabindex="0" class="sorting_1">6</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Usuario</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                          </tr>


                       </tbody>
                  </table>

                  </div>
               </div>





               

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>


    <div class="modal-dialog">
      	<div class="loginmodal-container text-center img-modal-registro" id="modal-login">
            <img src="../img/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="padding2020">
            	<b>Propuesta<br>"Nombre de la Agencia"</b><br>
              Documentos de la Propuesta
            </div>
            <div class="padding2020">
                <table class="table text-left">
                    <thead>
                      <tr>
                        <th scope="col">Nombre del Documento</th>
                        <th scope="col">Fecha</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">
                          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>  Nombre del Documento.pfp
                        </th>
                        <td><b>00 / 00/ 00</b></td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <i class="fa fa-file-text" aria-hidden="true"></i> Nombre del archivo.word
                        </th>
                        <td><b>00 / 00/ 00</b></td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <i class="fa fa-video-camera" aria-hidden="true"></i> Nombre del video.mp4
                        </th>
                        <td><b>00 / 00/ 00</b></td>
                      </tr>
                    </tbody>
                </table>










                <div class="card-content">
                  <div class="material-datatables">
                      <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                        <div class="row">
                              <div class="col-xs-6 col-sm-6">
                                    <div class="select-agencias">
                                        <select class="selectpicker" data-style="select-with-transition"
                                        multiple title="Filtrar por" data-size="7">
                                          <option disabled>Filtrar</option>
                                          <option value="2">Filtro 1</option>
                                          <option value="2">Filtro 2</option>
                                          <option value="2">Filtro 3</option>
                                        </select>
                                    </div>
                              </div>
                              <div class="col-xs-6 col-sm-6">
                                <div class="dataTables_filter" id="datatables_filter">
                                      <div id="wrap">
                                        <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row botones-ajustes">
                              <div class="col-sm-2">
                                   <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Importar XML</button></a>
                              </div>
                              <div class="col-sm-2">
                                  <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Agregar Usuario</button></a>
                              </div>
                              <div class="col-sm-2">
                                  <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Descargar XML</button></a>
                              </div>
                              <div class="col-sm-2">
                                  <a title="Agregar usuario" href="#agregar-usuario-admin" data-toggle="modal"><button class="btn btn-primary btn-round" id="btn-ver-agencia">Agregar usuario</button></a>
                              </div>
                          </div>

                          <div class="row">
                            <div class="col-sm-12">
                              <table aria-describedby="datatables_info" role="grid" id="datatables"
                                  class="table table-striped table-no-bordered table-hover dataTable dtr-inline"
                                  style="width: 100%;" cellspacing="0" width="100%">

                              <thead>
                                  <tr role="row">
                                        <th aria-sort="descending" aria-label="Name: activate to sort column ascending"
                                          style="width: 163px;" colspan="1" rowspan="1" aria-controls="datatables"
                                          tabindex="0" class="sorting_desc">ID</th>

                                          <th aria-label="Position: activate to sort column ascending"
                                          style="width: 241px;" colspan="1" rowspan="1" aria-controls="datatables"
                                          tabindex="0" class="sorting">Usuario</th>

                                          <th aria-label="Office: activate to sort column ascending"
                                          style="width: 123px;" colspan="1" rowspan="1" aria-controls="datatables"
                                          tabindex="0" class="sorting">Perfil</th>

                                          <th aria-label="Actions: activate to sort column ascending"
                                          style="width: 116px;" colspan="1" rowspan="1" aria-controls="datatables"
                                          tabindex="0" class="disabled-sorting text-right sorting">Acciones
                                          </th>
                                      </tr>
                              </thead>

                                  <tfoot>
                                      <tr>
                                        <th colspan="1" rowspan="1">ID</th>
                                          <th colspan="1" rowspan="1">Usuario</th>
                                          <th colspan="1" rowspan="1">Perfil</th>
                                          <th colspan="1" rowspan="1" class="text-right">Acciones</th>
                                       </tr>
                                  </tfoot>

                          <tbody>
                            <tr class="odd" role="row">
                                  <td tabindex="0" class="sorting_1">1</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Administrador</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="even" role="row">
                                  <td tabindex="0" class="sorting_1">2</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Supervisor</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="odd" role="row">
                                  <td tabindex="0" class="sorting_1">3</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Usuario</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="even" role="row">
                                  <td tabindex="0" class="sorting_1">4</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Usuario</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                               </tr>

                              <tr class="odd" role="row">
                                  <td class="sorting_1" tabindex="0">5</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Usuario</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="even" role="row">
                                  <td tabindex="0" class="sorting_1">6</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Usuario</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                              </tr>


                           </tbody>
                      </table>

                      </div>
                   </div>







            </div>
          	<form>
                <input type="submit" name="login" class="login loginmodal-submit" value="Aceptar">
          	</form>
      	</div>
    </div>
</div>
<!-- Termina Modal Propuestas -->

<!-- Modal Ganador Seleccionado-->
<div class="modal fade" id="ganador-seleccionado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Ganador Seleccionado</b><br>
              La agencia:<br>
              "Nombre de la Agencia"<br>
              fue seleccionada como ganadora por la siguiente razón:<br>
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
              fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Ganador Seleccionado -->

<!-- Modal Rechazado-->
<div class="modal fade" id="rechazado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Motivo de rechazo</b><br>
              La propuesta fue rechazada porque:<br>
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
              fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Rechazado-->

<!-- Modal Agregar usuario Admin -->
<div class="modal fade" id="agregar-usuario-admin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Nuevo Usuario</b><br>
              Envia una invitación a un nuevo usuario
            </div>
            <div class="form-group">
                <label class="control-label">Correo</label>
                <input  maxlength="100" type="email" class="form-control" placeholder="..."  />
                <form class="form-horizontal">
                    <div class="select-agencias">
                        <select class="selectpicker" data-style="select-with-transition" multiple title="Selecciona Perfil" data-size="7">
                          <option disabled>Selecciona Perfil</option>
                          <option value="2">Perfil 1</option>
                          <option value="3">Perfil 2</option>
                          <option value="4">Perfil 3</option>
                          <option value="5">Perfil 4</option>
                        </select>
                    </div>
                </form>
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default btn-add">+</button>
                </span>
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success">Agregar usuario</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Agregar usuario admin -->

<!-- Modal Invitación Admin-->
<div class="modal fade" id="invitacion-admin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Reenviar invitación</b><br>
              Enviaremos la invitación al siguiente correo
            </div>
            <div class="form-group">
                <label class="control-label">Correo</label>
                <input  maxlength="100" type="email" class="form-control" placeholder="..."  />
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success">Reenviar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Invitación Admin-->

<!-- Modal Agregar Producto Admin-->
<div class="modal fade" id="agregar-producto-admin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Agregar Producto</b><br>
              Agrega un logotipo de 250 x 250px en formato .png con fondo transparente.
            </div>

            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                <div class="fileinput-new thumbnail img-circle">
                    <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail img-circle" style=""></div>
                <div>
                    <span class="btn btn-round btn-rose btn-file">
                        <span class="fileinput-new">Agregar foto</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="hidden"><input name="..." type="file">
                    <div class="ripple-container"></div></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">Nombre del producto</label>
                <input  maxlength="100" type="text" class="form-control" placeholder="..."  />
            </div>

            <form class="form-horizontal">
                <div class="select-agencias">
                    <select class="selectpicker" data-style="select-with-transition" multiple title="Marca" data-size="7">
                      <option disabled>Selecciona Marca</option>
                      <option value="2">Marca 1</option>
                      <option value="3">Marca 2</option>
                      <option value="4">Marca 3</option>
                      <option value="5">Marca 4</option>
                    </select>
                </div>
            </form>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success">Agregar producto</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Agregar Producto Admin-->

<!-- Modal Agregar imagen Admin-->
<div class="modal fade" id="agregar-imagen-admin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Agregar Imagen</b>
            </div>

            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                <div class="fileinput-new thumbnail img-circle">
                    <img src="<?= base_url() ?>img/login/logo-bimbo.png" alt="Logo Bimbo">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail img-circle" style=""></div>
                <div>
                    <span class="btn btn-round btn-rose btn-file">
                        <span class="fileinput-new">Agregar foto</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="hidden"><input name="..." type="file">
                    <div class="ripple-container"></div></span>
                </div>
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Invitación Admin-->

<!-- Script Proceso de Registro de Proyecto -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$(document).ready(function () {
    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
    allWells.hide();
    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });
    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
    $('div.setup-panel div a.btn-primary').trigger('click');
});
</script>
