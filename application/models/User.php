<?php

class User extends CI_Model{
        var $log = false;

        function __construct(){
                parent::__construct();
                if(!empty($_SESSION['user'])){
                        $this->log = TRUE;
                        $this->set_variables();
                        $this->createKeyChain();
                        $this->db->update('user',array('ultima_conexion'=>date("Y-m-d H:i:s")),array('id'=>$this->id));
                }                
        }
        
        function createKeyChain(){
            unset($this->keychain);
            unset($_SESSION['keychain']);
            if(empty($this->keychain)){
                if(empty($_SESSION['keychain'])){
                    $keychain = array();
                    foreach($this->getAccess('funciones.nombre')->result() as $f){
                        if(!in_array($f->nombre,$keychain)){
                            $keychain[] = $f->nombre;
                        }
                    }
                    $this->keychain = $keychain;
                    $_SESSION['keychain'] = json_encode($this->keychain);
                }else{                    
                    $this->keychain = json_decode($_SESSION['keychain']);
                }
            }
            else{
                if(!is_array($this->keychain)){
                    $this->keychain = json_decode($this->keychain);
                }
            }
        }
        
        function filtrarMenu(){            
            $menu = array();
            $this->db->order_by('orden','ASC');
            foreach($this->db->get_where('menus',array('menus_id'=>NULL))->result() as $m){
                $menu[$m->nombre] = $m;
                $menu[$m->nombre]->submenu = array();
                $this->db->order_by('orden','ASC');
                foreach($this->db->get_where('menus',array('menus_id'=>$m->id))->result() as $m2){
                    $funcion = explode('/',$m2->url);
                    if(in_array($funcion[count($funcion)-1],$this->keychain) || $m2->request_permission==0){
                        $menu[$m->nombre]->submenu[] = $m2;
                    }
                }
            }
            return $menu;
        }

        
        function getItemMenu($ruta,$label = ''){
            $funcion = explode('/',$ruta);
            $label = empty($label)?  ucfirst(str_replace('_',' ',$funcion)):$label;
            if(in_array($funcion[count($funcion)-1],$this->keychain)){
                return '<li><a href="'.base_url($ruta).'">'.$label.'</a></li>';
            }else{
                return '';
            }
        }

        function login($user,$pass)
        {
                $this->db->where('email',$user);
                $this->db->where('password',md5($pass));

                $r = $this->db->get('user');
                if($r->num_rows()>0 && $r->row()->status==1)
                {
                    $this->getParameters($r);                    
                    return true;
                }
                else{
                    return false;
                }
        }

        function login_short($id)
        {
            $user = $this->db->get_where('user',array('id'=>$id,'status'=>1));
            if($user->num_rows()>0){
                $this->getParameters($user);
                return true;
            }
            return false;
        }

        function getParameters($row)
        {
            $r = $row->row();
            $field_data = $this->db->field_data('user');                    
            foreach($field_data as $f){
                $_SESSION[$f->name] = $r->{$f->name};
            }                    
            $this->set_variables();
        }

        function set_variables(){
            foreach($_SESSION as $n=>$x){
                $this->$n = $x;
            }
            $_SESSION['user'] = $_SESSION['id'];

            if($this->tipo_usuario=='Agencia'){             
                   
                $this->db->select('agencias.*, paises.icono');                
                $this->db->join('agencias','agencias.id = agencias_user.agencias_id');
                $this->db->join('paises','paises.nombre = agencias.paises','left');
                $logo = $this->db->get_where('agencias_user',array('agencias_user.user_id'=>$this->id));
                if($logo->num_rows()>0){                    
                    $_SESSION['logo'] = base_url().'img/agencias/'.$logo->row()->logo;
                    $_SESSION['empresa'] = $logo->row()->id;
                    $_SESSION['bandera'] = base_url().'img/Banderas/'.$logo->row()->icono;
                }else{
                    $_SESSION['logo'] = base_url().'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';
                }
                
            }elseif($this->tipo_usuario=='Cliente'){
                $this->db->select('empresas.id,empresas.logo,paises.icono');
                $this->db->join('empresas','empresas.id = user_empresas.empresas_id');
                $this->db->join('paises','paises.id = empresas.paises_id');
                $empresas = $this->db->get_where('user_empresas',array('user_id'=>$this->id));
                if($empresas->num_rows()>0){
                    $empresas = $empresas->row();
                    $_SESSION['logo'] = empty($empresas->logo)?base_url().'Theme/Cliente/assets/img/login/subir-logo.png':base_url('img/empresas/logos/'.$empresas->logo);
                    $_SESSION['empresa'] = $empresas->id;
                    $_SESSION['bandera'] = base_url().'img/Banderas/'.$empresas->icono;
                }else{
                    $_SESSION['logo'] = base_url().'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';
                    $_SESSION['empresa'] = 0;
                }
            }else{
                $_SESSION['logo'] = base_url().'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';
            }            


            $this->logo = $_SESSION['logo'];
            $this->user = $_SESSION['user'];
        }
        
        function setVariable($nombre,$valor){
            $_SESSION[$nombre] = $valor;
            $this->set_variables();
        }

        function unlog()
        {
                session_unset();
        }
        
        function getOperations($operation){
            switch($operation){
                case 'list':
                case 'read':
                case 'print':
                case 'success':
                    return 'lectura';
                break;
                case 'add':
                case 'edit':
                case 'delete':
                    return 'escritura';
                break;
                default: return 'lectura';
                 break;
            }
        }
        
        
        function getAccess($select,$where = array(), $user = ''){
            if(!empty($_SESSION['user'])){
                $this->db->select($select);
                $this->db->join('user_group','user_group.user = user.id');
                $this->db->join('grupos','grupos.id = user_group.grupo');
                $this->db->join('funcion_grupo','funcion_grupo.grupo = grupos.id');
                $this->db->join('funciones','funciones.id = funcion_grupo.funcion');                
                $where['user'] = empty($user)?$this->user:$user;
                $access = $this->db->get_where('user',$where);
                return $access;
            }
            else {
                header("Location:".base_url());
                exit;
            }
        }
        public $allrequired = array(
            'resenar','del','showResult','favoritos','enviarPropuesta','chat','sendMessage','aceptarInvitacion','briefs','rankear','editar_empresa','setMessage','proyectos_documentos','verAgencia','calificar','buscar','ss'
        );
        function hasAccess(){
            $funcion = $this->router->fetch_method();            
            if($this->admin==0 && !in_array($funcion,$this->allrequired)){
                $this->load->library('ajax_grocery_crud');
                $crud = new ajax_grocery_crud();            
                $fun = $this->getOperations($crud->getParameters());//Almacenar si es lectura o escritura  
                $permisos = $this->getAccess('grupos.*',array('funciones.nombre'=>$funcion,$fun=>1));
                return $permisos->num_rows()>0 || $this->router->fetch_class()=='panel'?TRUE:FALSE;
            }else{
                return true;
            }
        }
}
?>
