<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos de la empresa</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Usuarios</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Dominios</a></li>    
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <?= $output ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('user_empresas')
             ->set_subject('Usuarios')
             ->set_theme('bootstrap2')
             ->where('empresas_id',$valor)
             ->set_url('cliente/admin/empresas_user/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('empresas_dominios')
             ->set_subject('Dominios')
             ->set_theme('bootstrap2')
             ->where('empresas_id',$valor)
             ->set_url('cliente/admin/empresas_dominios/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>
  </div>

</div>