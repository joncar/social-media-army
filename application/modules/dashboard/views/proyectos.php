<?php $proyecto = $this->querys->showResult($valor,TRUE); ?>
<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos del proyecto</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Fechas</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Usuarios</a></li>
    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Briefs</a></li>
    <li role="presentation"><a href="#assets" aria-controls="settings" role="tab" data-toggle="tab">Assets</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <?= $output ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('proyectos_fechas')
             ->set_subject('Usuarios')
             ->set_theme('bootstrap2')
             ->where('proyectos_id',$valor)
             ->set_url('proyectos/admin/proyectos_fechas/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('proyectos_user')
             ->set_subject('Dominios')
             ->set_theme('bootstrap2')
             ->columns('user_id','status')
             ->where('proyectos_id',$valor)
             ->set_url('proyectos/admin/proyectos_user/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="settings">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('briefs')
             ->set_subject('Clientes')
             ->set_theme('bootstrap2')
             ->columns('agencias_id','status','propuesta','fecha_modificacion')
             ->where('proyectos_id',$valor)
             ->set_url('proyectos/admin/briefs/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>

    <div role="tabpanel" class="tab-pane" id="assets">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('proyectos_documentos')
             ->set_subject('Assets')
             ->set_theme('bootstrap2')             
             ->where('proyectos_id',$valor)
             ->set_url('proyectos/admin/proyectos_documentos/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>
  </div>

</div>

<!-- Modal Ganador Seleccionado-->
<div class="modal fade" id="share_folder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
            </div>
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">           
            <?php foreach($proyecto->participantes->result() as $b): ?>
                <div class="folders" id="componentFolder<?= $b->brief_id ?>" style="display: none">
                    <div class="titulo-modal">
                      Agencia <?= $b->nombre ?>
                    </div>
                    <?php $this->load->view('predesign/dropzone',[
                        'handle'=>'proyectos/admin/briefs',
                        'name'=>'folder'.$b->brief_id,
                        'inpName'=>'ficheros',
                        'path'=>'proyectosFiles/',
                        'files'=>array_filter(explode(',',$b->ficheros)),
                        'unset_delete'=>false
                    ]) 
                    ?>
                </div>
            <?php endforeach ?>

        </div>
    </div>
</div>
<!-- Termina Modal Ganador Seleccionado -->

<script>
    window.activeFolderBrief = 0;
    function shareFolder(brief){
        window.activeFolderBrief = brief;
        $('.folders').hide();
        $("#componentFolder"+brief).show();
        $("#share_folder").modal('toggle');
    }

    window.afterUploadFile = function(file,response){
        $.post('<?= base_url() ?>proyectos/admin/briefs/'+window.activeFolderBrief+'/addFile',{fichero:$('#field-folder'+window.activeFolderBrief).val(),'file':file.remoteName},function(data){});
    }
    window.afterRemoveFile = function(file,response){
        $.post('<?= base_url() ?>proyectos/admin/briefs/'+window.activeFolderBrief+'/removeFile',{fichero:$('#field-folder'+window.activeFolderBrief).val(),'file':file.remoteName},function(data){});
    }
</script>