<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation"><a href="<?= base_url('agencia/admin/agencias/edit/'.$y); ?>">Datos de la agencia</a></li>
    <li role="presentation"><a href="<?= base_url('agencia/admin/agencias/edit/'.$y); ?>?back=2">Usuarios</a></li>
    <li role="presentation"><a href="<?= base_url('agencia/admin/agencias/edit/'.$y); ?>?back=3">Dominios</a></li>
    <li role="presentation"><a href="<?= base_url('agencia/admin/agencias/edit/'.$y); ?>?back=4">Clientes</a></li>
    <li role="presentation"><a href="<?= base_url('agencia/admin/agencias/edit/'.$y); ?>?back=5">Categorías</a></li>
    <li role="presentation"><a href="<?= base_url('agencia/admin/agencias/edit/'.$y); ?>?back=6">Resenas</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <?= $output ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('agencias_user')
             ->set_subject('Usuarios')
             ->set_theme('bootstrap2')
             ->where('agencias_id',$valor)
             ->set_url('agencia/admin/agencias_user/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('agencias_dominios')
             ->set_subject('Dominios')
             ->set_theme('bootstrap2')
             ->where('agencias_id',$valor)
             ->set_url('agencia/admin/agencias_dominios/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="settings">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('agencias_clientes')
             ->set_subject('Clientes')
             ->set_theme('bootstrap2')
             ->where('agencias_id',$valor)
             ->set_url('agencia/admin/agencias_clientes/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>

    <div role="tabpanel" class="tab-pane" id="categorias">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('agencias_categorias')
             ->set_subject('Clientes')
             ->set_theme('bootstrap2')
             ->where('agencias_id',$valor)
             ->set_url('agencia/admin/agencias_categorias/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>

    <div role="tabpanel" class="tab-pane" id="resenas">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('agencias_resenas')
             ->set_subject('Reseñas')
             ->set_theme('bootstrap2')
             ->where('agencias_id',$valor)
             ->set_url('agencia/admin/agencias_resenas/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>
  </div>

</div>