<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" <?= empty($_GET['back'])?' class="active"':'' ?>><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos de la agencia</a></li>
    <li role="presentation" <?= @$_GET['back']=='2'?' class="active"':'' ?>><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Usuarios</a></li>
    <li role="presentation" <?= @$_GET['back']=='3'?' class="active"':'' ?>><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Dominios</a></li>
    <li role="presentation" <?= @$_GET['back']=='4'?' class="active"':'' ?>><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Clientes</a></li>
    <li role="presentation" <?= @$_GET['back']=='5'?' class="active"':'' ?>><a href="#categorias" aria-controls="settings" role="tab" data-toggle="tab">Categorias</a></li>
    <li role="presentation" <?= @$_GET['back']=='6'?' class="active"':'' ?>><a href="#resenas" aria-controls="settings" role="tab" data-toggle="tab">Reseñas</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content"> 
    <div role="tabpanel" class="tab-pane <?= empty($_GET['back'])?' active':'' ?>" id="home">
      <?= $output ?>
    </div>
    <div role="tabpanel" class="tab-pane <?= @$_GET['back']=='2'?' active':'' ?>" id="profile">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('agencias_user')
             ->set_subject('Usuarios')
             ->set_theme('bootstrap2')
             ->where('agencias_id',$valor)
             ->set_url('agencia/admin/agencias_user/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>
    <div role="tabpanel" class="tab-pane <?= @$_GET['back']=='3'?' active':'' ?>" id="messages">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('agencias_dominios')
             ->set_subject('Dominios')
             ->set_theme('bootstrap2')
             ->where('agencias_id',$valor)
             ->set_url('agencia/admin/agencias_dominios/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>
    <div role="tabpanel" class="tab-pane <?= @$_GET['back']=='4'?' active':'' ?>" id="settings">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('agencias_clientes')
             ->set_subject('Clientes')
             ->set_theme('bootstrap2')
             ->where('agencias_id',$valor)
             ->set_url('agencia/admin/agencias_clientes/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>

    <div role="tabpanel" class="tab-pane <?= @$_GET['back']=='5'?' active':'' ?>" id="categorias">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('agencias_categorias')
             ->set_subject('Clientes')
             ->set_theme('bootstrap2')
             ->where('agencias_id',$valor)
             ->set_url('agencia/admin/agencias_categorias/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>

    <div role="tabpanel" class="tab-pane <?= @$_GET['back']=='5'?' active':'' ?>" id="resenas">
      <?php 
        $crud = new ajax_grocery_crud();
        $crud->set_table('agencias_resenas')
             ->set_subject('Reseñas')
             ->set_theme('bootstrap2')
             ->where('agencias_id',$valor)
             ->set_url('agencia/admin/agencias_resenas/'.$valor.'/');
        echo $crud->render(1)->output;
      ?>
    </div>
  </div>

</div>