<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Proyectos extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }

        function proyectos($x = "",$y = ""){
        	$crud = $this->crud_function('','');
        	$crud->set_theme('generic');
            $crud->columns('id','nombre','imagen','responsable','agencias_invitadas','inicio','fase');
            $crud->set_field_upload('imagen','img/proyectos_uploads/img');
            $crud->callback_column('responsable',function($val,$row){
                $this->db->join('user','user.id = proyectos_user.user_id')
                         ->where('tipo',1);
                $user = $this->db->get_where('proyectos_user',array('proyectos_id'=>$row->id));
                if($user->num_rows()>0){
                    $user = $user->row();
                    return $user->nombre.' '.$user->apellido;
                }
            });
            $crud->callback_column('agencias_invitadas',function($val,$row){
                return (string)$this->db->get_where('briefs',array('proyectos_id'=>$row->id))->num_rows();
            });

            $crud->callback_column('inicio',function($val,$row){
                return @date("d/m/Y",strtotime($this->db->get_where('proyectos_fechas',array('proyectos_id'=>$row->id))->row()->desde));
            });

            $crud->callback_column('fase',function($val,$row){
                $proyecto = $this->querys->showResult($row->id,true);
                return isset($proyecto->fase)?$proyecto->fase->nombre:'Finalizado';
            });
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_searchs('imagen','responsable','agencias_invitadas','inicio','fase');
        	$crud = $crud->render('','Theme/Cliente/cruds/');
        	$crud->view = 'proyectos';
        	$this->loadView($crud);
        }           
    }
?>
