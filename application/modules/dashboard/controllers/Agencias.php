<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Agencias extends Panel{
        function __construct() {
            parent::__construct();
        }

        function agencias($x = '',$y = ''){
        	$crud = $this->crud_function('','');
        	$crud->set_theme('generic');
        	$crud->columns('logo','nombre','razon_social','estatus_agencias_id','categorias_agencias_id');
        	$crud->set_field_upload('logo','img/agencias')
        		 ->set_field_upload('logo2','img/agencias')        		 
        		 ->field_type('mapa','map','')
                 ->field_type('fecha_ingreso','hidden',date("Y-m-d H:i:s"))
        		 ->unset_read()        		 
        		 ->unset_print()
        		 ->unset_jquery()
        		 ->unset_export()
        		 ->display_as('categorias_agencias_id','Categoria')
        		 ->display_as('estatus_agencias_id','Estatus');
	 		$crud->set_lang_string('insert_success_message','Se han almacenado sus datos con éxito, <script>document.location.href="'.base_url('dashboard/agencias/agencias/edit/').'/{id}";</script>');        	$out = $crud->render('','Theme/Cliente/cruds/');
        	if($crud->getParameters()=='edit' && is_numeric($y)){        		
        		$out = $this->get_edit_tabs($out,$y);
        	}else{
        		$out->view = 'agencias';
        	}
        	$out->agencia = $y;
        	$this->loadView($out);
        }

        function get_edit_tabs($out,$y = ''){        	    	
    		//Lists
    		$out->view = 'admin-agencias';
    		$out->users = $this->userList($y);
    		$out->js_files = array_merge($out->js_files,$out->users->js_files);
    		$out->css_files = array_merge($out->css_files,$out->users->css_files);
    		$out->users = $out->users->output;    		
    		return $out;
        }

        function userList($y){
        	$this->as['agencias'] = 'user';
        	$crud = $this->crud_function('','');            
        	$crud->unset_jquery();
            $user = $this->db->get_where('agencias_user',array('agencias_id'=>$y));
            if($user->num_rows()>0){
                $crud->where('id','-1');
                foreach($user->result() as $u){
                    $crud->or_where('id',$u->user_id);
                }
            }else{
                $crud->where('id','-1');
            }
            $crud->columns('nombre','apellido','email','puesto','status');
            $crud->set_url('dashboard/agencias/user/'.$y.'/');
        	$crud->set_theme('generic');        	
        	$crud = $crud->render(1,'Theme/Cliente/cruds/');
        	return $crud;
        }

        function user($y){
            $this->as['agencias'] = 'user';
            $crud = $this->crud_function('','');            
            $crud->unset_jquery();
            $crud->set_theme('generic'); 
            $this->y = $y;
            $crud->set_field_upload('foto','img/fotos');
            $crud->field_type('ultima_conexion','hidden')
                 ->field_type('status','dropdown',array('0'=>'Bloqueado','1'=>'Activo'))
                 ->field_type('admin','hidden',0)
                 ->field_type('fecha_registro','hidden')
                 ->field_type('fecha_actualizacion','hidden')
                 ->field_type('password','password')
                 ->field_type('fecha_registro','hidden',date("Y-m-d"))
                 ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                 ->field_type('tipo_usuario','hidden','Agencia');
            $crud->columns('nombre','apellido','email','puesto','status');
            if($crud->getParameters()=='add'){
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            }
            $crud->callback_before_insert(function($post){                               
                $post['password'] = md5($post['password']);                
                return $post;
            });
            $crud->callback_before_update(function($post,$primary){                                
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });

            $crud->callback_after_insert(function($post,$primary){
                //Lo metemos en el grupo de clientes
                $this->db->insert('user_group',array('user'=>$primary,'grupo'=>3));                
                $this->db->insert('agencias_user',array('agencias_id'=>$this->y,'user_id'=>$primary));
            });
            $crud->callback_after_update(function($post,$primary){});
            //Filtramos los usuarios de esta empresa
            $user = $this->db->get_where('agencias_user',array('agencias_id'=>$y));
            if($user->num_rows()>0){
                $crud->where('id','-1');
                foreach($user->result() as $u){
                    $crud->or_where('id',$u->user_id);
                }
            }else{
                $crud->where('id','-1');
            }
            $crud = $crud->render('','Theme/Cliente/cruds/');
            $this->loadView($crud);
        }
        
    }
?>
