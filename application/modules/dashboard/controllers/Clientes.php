<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Clientes extends Panel{
        function __construct() {
            parent::__construct();
        }

        function clientes($x = "",$y = ""){
        	$this->as['clientes'] = 'empresas';
        	$crud = $this->crud_function('','');
        	$crud->set_theme('generic');
        	$crud->set_field_upload('logo','img/empresas/logos/');
        	$crud->columns('id','nombre','logo','usuarios','proyectos','status','registro');
        	$crud->unset_delete()
        		 ->unset_read()
        		 ->unset_jquery()
        		 ->unset_searchs('usuarios','proyectos','status','registro');
        	


        	$crud->callback_column('usuarios',function($val,$row){
        		return $this->db->get_where('user_empresas',array('empresas_id'=>$row->id))->num_rows();
        	});
        	$crud->callback_column('proyectos',function($val,$row){
        		$user = $this->db->get_where('user_empresas',array('empresas_id'=>$row->id,'tipo'=>1));
        		if($user->num_rows()>0){
        			$user = $user->row()->user_id;
        			$this->db->join('proyectos','proyectos.id = proyectos_user.proyectos_id','inner');
        			$this->db->group_by('proyectos.id');
        			return $this->db->get_where('proyectos_user',array('user_id'=>$user))->num_rows();
        		}
        		return (string)'0';
        	});
        	$crud->callback_column('status',function($val,$row){
        		$user = $this->db->get_where('user_empresas',array('empresas_id'=>$row->id,'tipo'=>1));
        		if($user->num_rows()>0){
        			$user = $user->row()->user_id;        			
        			return $this->db->get_where('user',array('id'=>$user))->row()->status==1?'Activo':'Bloqueado';
        		}
        		return (string)'N/A';
        	});
        	$crud->callback_column('registro',function($val,$row){
        		$user = $this->db->get_where('user_empresas',array('empresas_id'=>$row->id,'tipo'=>1));
        		if($user->num_rows()>0){
        			$user = $user->row()->user_id;        			
        			return $this->db->get_where('user',array('id'=>$user))->row()->fecha_registro;
        		}
        		return (string)'N/A';
        	});
        	$crud->set_lang_string('insert_success_message','Se han almacenado sus datos con éxito, <script>document.location.href="'.base_url('dashboard/clientes/clientes/edit/').'/{id}";</script>');
        	$out = $crud->render('','Theme/Cliente/cruds/');
        	if($crud->getParameters()=='edit' && is_numeric($y)){        		
        		$out = $this->get_edit_tabs($out,$y);
        	}else{
        		$out->view = 'clientes';
        	}
        	$this->loadView($out);
        }

        function get_edit_tabs($out,$y = ''){
        	$user = $this->db->get_where('user_empresas',array('empresas_id'=>$y,'tipo'=>1));
    		$out->view = 'ajustes-clientes';
    		if($user->num_rows()>0){
    			$user = $user->row()->user_id;   
    			$out->user = $this->getUser($user,$y);     			
    		}else{
    			$out->user = $this->getUser('',$y);
    		}
    		$out->js_files = array_merge($out->js_files,$out->user->js_files);
    		$out->user = $out->user->output;
    		//Lists
    		$out->users = $this->userList($y);
    		$out->js_files = array_merge($out->js_files,$out->users->js_files);
    		$out->css_files = array_merge($out->css_files,$out->users->css_files);
    		$out->users = $out->users->output;
    		$out->invitaciones = $this->invitacionesList($y)->output;
    		$out->productos = $this->productosList($y)->output;
    		$out->marcas = $this->marcasList($y)->output;
    		return $out;
        }

        function getUser($user = '',$y = ''){
        	$this->as['clientes'] = 'user';            
        	$crud = $this->crud_function('','');
        	$crud->unset_jquery();
        	$crud->set_theme('generic');
        	$action = empty($user)?2:3;
            $crud->primary_key_value = $user;
            $crud->set_url('dashboard/clientes/user/'.$y.'/');
            $crud->set_field_upload('foto','img/fotos');
            $crud->field_type('ultima_conexion','hidden')
                 ->field_type('status','dropdown',array('0'=>'Bloqueado','1'=>'Activo'))
                 ->field_type('admin','hidden',0)
                 ->field_type('fecha_registro','hidden')
                 ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                 ->field_type('password','password')
                 ->field_type('tipo_usuario','hidden','Cliente');
        	$crud = $crud->render($action,'Theme/Cliente/cruds/');
        	return $crud;
        }

        function userList($y){            
        	$this->as['clientes'] = 'user';
        	$crud = $this->crud_function('','');            
        	$crud->unset_jquery();
            $user = $this->db->get_where('user_empresas',array('empresas_id'=>$y,'tipo !='=>1));
            if($user->num_rows()>0){
                $crud->where('id','-1');
                foreach($user->result() as $u){
                    $crud->or_where('id',$u->user_id);
                }
            }else{
                $crud->where('id','-1');
            }
            $crud->columns('nombre','apellido','email','puesto','status','tipo_usuario');
            $crud->set_url('dashboard/clientes/user/'.$y.'/');
        	$crud->set_theme('generic');        	
        	$crud = $crud->render(1,'Theme/Cliente/cruds/');
        	return $crud;
        }

        function invitacionesList($x){
        	$this->as['clientes'] = 'invitaciones';
        	$crud = $this->crud_function('','');
        	$crud->where('empresas_id',$x);
            $crud->set_url('dashboard/clientes/invitaciones/'.$x.'/');
        	$crud->unset_jquery();
        	$crud->set_theme('generic');        	
        	$crud = $crud->render(1,'Theme/Cliente/cruds/');
        	return $crud;
        }

        function productosList($x){
        	$this->as['clientes'] = 'productos';
        	$crud = $this->crud_function('','');
            $crud->set_url('dashboard/clientes/productos/'.$x.'/');
        	$crud->where('empresas_id',$x);
        	$crud->unset_jquery();
        	$crud->set_theme('generic');        	
        	$crud = $crud->render(1,'Theme/Cliente/cruds/');
        	return $crud;
        }

        function marcasList($x){
        	$this->as['clientes'] = 'marcas';
        	$crud = $this->crud_function('','');
            $crud->set_url('dashboard/clientes/marcas/'.$x.'/');
        	$crud->where('empresas_id',$x);
        	$crud->unset_jquery();
        	$crud->set_theme('generic');        	
        	$crud = $crud->render(1,'Theme/Cliente/cruds/');
        	return $crud;
        }

        function user($y){
            $this->as['clientes'] = 'user';
            $crud = $this->crud_function('','');            
            $crud->unset_jquery();
            $crud->set_theme('generic'); 
            $this->y = $y;
            $crud->set_field_upload('foto','img/fotos');
            $crud->field_type('ultima_conexion','hidden')
                 ->field_type('status','dropdown',array('0'=>'Bloqueado','1'=>'Activo'))
                 ->field_type('admin','hidden',0)
                 ->field_type('fecha_registro','hidden')
                 ->field_type('fecha_actualizacion','hidden')
                 ->field_type('password','password')
                 ->field_type('fecha_registro','hidden',date("Y-m-d"))
                 ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                 ->field_type('tipo_usuario','dropdown',array('2'=>'Administrador','3'=>'Supervisor','4'=>'Usuario'));
            $crud->columns('nombre','apellido','email','puesto','status','tipo');
            $crud->set_lang_string('insert_success_message','Sus datos han sido almacenados con éxito <script>document.location.reload();</script>');
            if($crud->getParameters()=='add'){
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            }
            $crud->callback_before_insert(function($post){
                $this->tipo = $post['tipo_usuario'];
                $post['tipo_usuario'] = 'Cliente';
                $post['password'] = md5($post['password']);                
                return $post;
            });
            $crud->callback_before_update(function($post,$primary){
                $this->tipo = $post['tipo_usuario'];
                $post['tipo_usuario'] = 'Cliente';
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });

            $crud->callback_after_insert(function($post,$primary){
                //Lo metemos en el grupo de clientes
                $this->db->insert('user_group',array('user'=>$primary,'grupo'=>2));                
                $this->db->insert('user_empresas',array('empresas_id'=>$this->y,'user_id'=>$primary,'tipo'=>$this->tipo));
            });

            $crud->callback_after_update(function($post,$primary){
                //Lo metemos en el grupo de clientes                        
                $user = $this->db->get_where('user_empresas',array('empresas_id'=>$this->y,'user_id'=>$primary));
                if($user->num_rows()>0){
                    $this->db->update('user_empresas',array('tipo'=>$this->tipo),array('empresas_id'=>$this->y,'user_id'=>$primary));
                }else{
                    $this->db->insert('user_empresas',array('empresas_id'=>$this->y,'user_id'=>$primary,'tipo'=>$this->tipo));
                }
            });

            $crud->callback_column('tipo',function($val,$row){
                $tipo = $this->db->get_where('user_empresas',array('empresas_id'=>$this->y,'user_id'=>$row->id));
                if($tipo->num_rows()>0){
                    switch($tipo->row()->tipo){
                        case '1':return 'Fundador';
                        case '2':return 'Administrador';
                        case '3':return 'Supervisor';
                        case '4':return 'Usuario';                    
                    }
                }else{
                    return 'N/A';
                }
            });

            //Filtramos los usuarios de esta empresa
            $user = $this->db->get_where('user_empresas',array('empresas_id'=>$y,'tipo !='=>1));
            if($user->num_rows()>0){
                $crud->where('id','-1');
                foreach($user->result() as $u){
                    $crud->or_where('id',$u->user_id);
                }
            }else{
                $crud->where('id','-1');
            }
            $crud = $crud->render('','Theme/Cliente/cruds/');
            $this->loadView($crud);
        }

        function invitaciones($x){
            $this->as['clientes'] = 'invitaciones';
            $crud = $this->crud_function('','');
            $crud->field_type('empresas_id','hidden',$x)
                 ->field_type('status','dropdown',array('1'=>'Enviada'));
            $crud->unset_jquery();
            $crud->set_theme('generic');            
            $crud->where('empresas_id',$x);
            $crud = $crud->render('','Theme/Cliente/cruds/');
            $this->loadView($crud);
        }

        function marcas($x){
            $this->as['clientes'] = 'marcas';
            $crud = $this->crud_function('','');
            $crud->field_type('empresas_id','hidden',$x);
            $crud->unset_jquery();
            $crud->set_theme('generic');            
            $crud->where('empresas_id',$x);
            $crud->set_field_upload('imagen','img/marcas');
            $crud = $crud->render('','Theme/Cliente/cruds/');
            $this->loadView($crud);
        }

        function productos($x){
            $this->as['clientes'] = 'productos';
            $crud = $this->crud_function('','');
            $crud->field_type('empresas_id','hidden',$x);
            $crud->set_relation('marcas_id','marcas','nombre',array('empresas_id'=>$x));
            $crud->set_no_using_ajax('marcas_id');
            $crud->unset_jquery();
            $crud->set_theme('generic');            
            $crud->where('empresas_id',$x);
            $crud->set_field_upload('imagen','img/productos');
            $crud = $crud->render('','Theme/Cliente/cruds/');
            $this->loadView($crud);
        }
        
    }
?>
