<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Dashboard extends Panel{
        function __construct() {
            parent::__construct();
        }
        function paises($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);   
            $crud->set_field_upload('icono','img/Banderas');           
            //$crud->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }

        function super_poderes($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);               
            //$crud->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }
        function tipos_servicio($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);                          
            $output = $crud->render();
            $this->loadView($output);
        }
        function tipos_certificacion($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);              
            //$crud->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }
        function content_services($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);              
            $crud->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }
        function tradicional_mkt($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);              
            $crud->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }
        function creative_services($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);              
            $crud->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }
        function social_services($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);              
            $crud->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }
        function especial_mkt($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);              
            $crud->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }
        function strategic_service($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);              
            $crud->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }
        function tecnical_services($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);              
            $crud->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }

        function servicios($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $output = $crud->render();
            $this->loadView($output);
        }

        function marcas($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);              
            //$crud->unset_delete();
            $crud->add_action('Productos','',base_url('dashboard/productos').'/');
            $output = $crud->render();
            $this->loadView($output);
        }

        function productos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y); 
            $crud->field_type('marcas_id','hidden',$x)
                 ->where('marcas_id',$x)
                 ->unset_columns('marcas_id');             
            $crud->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }

        function ranking_categorias($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);  
            $crud->set_subject('Tipo de calificación');
            $crud->add_action('Elementos','',base_url('dashboard/ranking_items').'/');                         
            $output = $crud->render();
            $output->title = 'Tipo de calificación';
            $this->loadView($output);
        }

        function ranking_items($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);  
            $crud->set_subject('Elemento de calificación');
            $crud->field_type('ranking_categorias_id','hidden',$x)
                 ->where('ranking_categorias_id',$x)
                 ->unset_columns('ranking_categorias_id');             
            $output = $crud->render();
            $output->title = 'Elemento de calificación';
            $this->loadView($output);
        }

        function estatus_agencias($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);  
            $crud->unset_delete();
            $crud->unset_read();
            $output = $crud->render();            
            $this->loadView($output);
        }
        
    }
?>
