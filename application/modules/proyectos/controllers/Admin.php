<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function categorias_proyectos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $output = $crud->render();
            $this->loadView($output);
        }
        function proyectos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->field_type('status','dropdown',array('1'=>'Activo','2'=>'Pausado','3'=>'Cancelado','4'=>'Culminado'));            
            $crud->columns('imagen','nombre','status','fecha_registro','creador');
            $crud->field_type('calificaciones','hidden');
            $crud->set_field_upload('imagen','img/proyectos_uploads/img');            
            $crud->set_relation_n_n('paises','proyectos_paises','paises','proyectos_id','paises_id','nombre');
            $crud->set_relation_n_n('marcas','proyectos_marcas','marcas','proyectos_id','marcas_id','nombre');
            $crud->set_relation_n_n('productos','proyectos_productos','productos','proyectos_id','productos_id','nombre');
            $crud->set_relation_n_n('categorias_proyectos','proyectos_categorias_proyectos','categorias_proyectos','proyectos_id','categorias_proyectos_id','nombre');
            $crud->set_relation_n_n('servicios','proyectos_servicios','servicios','proyectos_id','servicios_id','nombre');
            if($crud->getParameters()!='list'){
                $crud->set_relation('creador','user','{nombre} {apellido}',array('tipo_usuario'=>'Cliente'));
            }else{
                $crud->set_relation('creador','user','{nombre} {apellido}');
            }
            $crud->callback_after_delete(function($primary){
                $this->db = get_instance()->db;
                $this->db->delete('proyectos_user',array('proyectos_id'=>$primary));
                $this->db->delete('proyectos_paises',array('proyectos_id'=>$primary));
                $this->db->delete('proyectos_marcas',array('proyectos_id'=>$primary));
                $this->db->delete('proyectos_productos',array('proyectos_id'=>$primary));
                $this->db->delete('proyectos_categorias_proyectos',array('proyectos_id'=>$primary));                
                $this->db->delete('proyectos_fechas',array('proyectos_id'=>$primary));
                $this->db->delete('proyectos_servicios',array('proyectos_id'=>$primary));
                $this->db->delete('briefs',array('proyectos_id'=>$primary));
            });
            $output = $crud->render();
            if($crud->getParameters()=='edit'){
                $output->output = $this->load->view('proyectos',array('output'=>$output->output,'valor'=>$y),TRUE,'dashboard');
                $agencias = new ajax_grocery_crud();
                $agencias = $agencias->set_table('agencias')->set_subject('agencias')->set_theme('bootstrap2')->render(1);
                $output->js_files = array_merge($output->js_files,$agencias->js_files);
                $output->css_files = array_merge($output->css_files,$agencias->css_files);
            }
            $this->loadView($output);
        }

        function briefs($x = '',$y = ''){
            if(is_numeric($x)){
                switch($y){
                    case 'addFile':
                    case 'removeFile':
                        $this->db->update('briefs',['ficheros'=>$_POST['fichero']],['id'=>$x]);
                        die();
                    break;
                }                
            }
            $crud = $this->crud_function('','');            
            $crud->where('proyectos_id',$x)
                 ->field_type('proyectos_id','hidden',$x)                 
                 ->unset_columns('proyectos_id','mensaje','av','presentacion','presencial','calificacion','calificacion_data','ficheros')
                 ->field_type('status','dropdown',array('-1'=>'Rechazado','1'=>'Enviado','2'=>'Aceptada','3'=>'Presentacion','4'=>'Ganadora'))
                 ->field_type('av','hidden')
                 ->set_field_upload('propuesta','files')
                 ->set_field_upload('ficheros','proyectosFiles')
                 ->set_relation('agencias_id','agencias','nombre');             
            $crud->callback_column('propuesta',function($val,$row){
                $anterior = !empty($row->propuesta)?'<a target="_new" href="'.base_url().'files/'.$row->propuesta.'">OLD</a> ':'';
                return $anterior.'
                  <a href="javascript:void(0)" onclick="shareFolder('.$row->id.')">
                    <i class="fa fa-folder fa-2x"></i>
                  </a>';
            });
            $output = $crud->render();
            $this->loadView($output);
        }

        function proyectos_fechas($x = ''){
            $crud = $this->crud_function('','');            
            $crud->where('proyectos_id',$x)
                 ->field_type('proyectos_id','hidden',$x);
            $output = $crud->render();
            $this->loadView($output);
        }

        function proyectos_user($x = ''){
            $crud = $this->crud_function('','');            
            $crud->where('proyectos_id',$x)
                 ->field_type('proyectos_id','hidden',$x)
                 ->unset_columns('proyectos_id')
                 ->field_type('tipo','dropdown',array('1'=>'Administrador','2'=>'Supervisor','3'=>'Colaborador'))
                 ->display_as('user_id','Usuario')
                 ->set_relation('user_id','user','{nombre} {apellido}');
            $output = $crud->render();
            $this->loadView($output);
        }

        function proyectos_documentos($x = ''){
            $crud = $this->crud_function('','');            
            $crud->where('proyectos_id',$x)
                 ->field_type('proyectos_id','hidden',$x)
                 ->set_field_upload('documento','img/proyectos_uploads/files/');
            $output = $crud->render();
            $this->loadView($output);
        }


    }
?>
