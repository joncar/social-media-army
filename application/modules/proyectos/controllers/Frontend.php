<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }

        function aprobar($x = '',$y = '',$z = ''){
        	$y = base64_decode($y);
        	$z = base64_decode($z);
            if(is_numeric($y)){
            	$user = $this->db->get_where('user',array('id'=>$y));
            	$proyecto = $this->db->get_where('proyectos',array('id'=>$z));
            	if($user->num_rows()>0 && $proyecto->num_rows()>0){
            		if($x=='x2'){
	            		$this->db->select('user.*,proyectos_user.proyectos_id,proyectos.nombre as proyecto');
	                    $this->db->join('user','user.id = proyectos_user.user_id');
	                    $this->db->join('proyectos','proyectos.id = proyectos_user.proyectos_id');
	                    foreach($this->db->get_where('proyectos_user',array('proyectos_id'=>$proyecto->row()->id))->result() as $p){
	                        $p->link = base_url('cliente/proyecto/verProyecto/'.$proyecto->row()->id);     
	                        $this->db->join('agencias','agencias.id = agencias_user.agencias_id');
	                        $p->agencia = $this->db->get_where('agencias_user',array('agencias_user.user_id'=>$y))->row();                   
	                        $agencia = $p->agencia->id;
	                        $p->agencia = $p->agencia->nombre;
	                        $idnotif = $proyecto->row()->status==2?17:19;
	                        //get_instance()->enviarcorreo((object)$p,$idnotif,'',$p->link);
	                        $this->db->delete('proyectos_status',array('proyectos_id'=>$proyecto->row()->id,'estado !='=>$proyecto->row()->status));
	                        $this->db->insert('proyectos_status',array('proyectos_id'=>$proyecto->row()->id,'estado'=>$proyecto->row()->status,'user_id'=>$agencia));
	                        //redirect($p->link);
	                    }
                	}
                	if($x=='x1'){

                		$this->db->select('user.*,proyectos.nombre as proyecto');
	                    $this->db->join('user','user.id = proyectos_user.user_id');
	                    $this->db->join('proyectos','proyectos.id = proyectos_user.proyectos_id');
	                    foreach($this->db->get_where('proyectos_user',array('proyectos_id'=>$proyecto->row()->id))->result() as $p){
	                        $p->link = base_url('cliente/proyecto/verProyecto/'.$proyecto->row()->id);     	                        
	                        $p->nombre_colaborador = $user->row()->nombre.' '.$user->row()->apellido;                   
	                        $idnotif = $proyecto->row()->status==2?23:24;
	                        //get_instance()->enviarcorreo((object)$p,$idnotif,'',$p->link);
	                    }


	                    $this->db->select('user.*, agencias.nombre as agencia, proyectos.nombre as proyecto');
	                    $this->db->join('agencias_user','agencias_user.agencias_id = briefs.agencias_id');
	                    $this->db->join('agencias','agencias.id = briefs.agencias_id');
	                    $this->db->join('user','agencias_user.user_id = user.id');
	                    $this->db->join('proyectos','proyectos.id = briefs.proyectos_id');
	                    foreach($this->db->get_where('briefs',array('proyectos_id'=>$proyecto->row()->id))->result() as $p){
	                        $p->link = base_url('agencia/proyecto/verProyecto/'.$proyecto->row()->id);                        
	                        $p->nombre_colaborador = $user->row()->nombre.' '.$user->row()->apellido;  
	                        $idnotif = $proyecto->row()->status==2?23:24;
	                        //get_instance()->enviarcorreo((object)$p,$idnotif,'',$p->link);
	                    }
                	}
            	}
            }
        }
    }
?>
