<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Seguridad extends Panel{
        function __construct() {
            parent::__construct();
        }

        function ajustes(){
            $crud = $this->crud_function('','');
            $crud->unset_add()->unset_delete()->unset_read()->unset_export()->unset_print();
            $crud->field_type('keywords','tags');
            $crud->columns('id');
            $crud->field_type('analytics','string')
                 ->field_type('cookies','string')
                 ->field_type('idiomas','tags')
                 ->set_field_upload('banner','img')
                 ->set_field_upload('favicon','img')
                 ->set_field_upload('logo','img')
                 ->set_field_upload('fondo','img')
                 ->field_type('mapa','tags')
                 ->display_as('idiomas','Idiomas (Separados por coma[,])')
                 ->display_as('mapa','Mapa (Formato[Lat, Lon])');
            $crud = $crud->render();
            $this->loadView($crud);
        } 

        function menus(){
            $crud = $this->crud_function('','');  
            $crud->set_relation('menus_id','menus','nombre',array('menus_id'=>NULL));                 
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function grupos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('funciones','funcion_grupo','funciones','grupo','funcion','{nombre}');
            $crud->set_relation_n_n('miembros','user_group','user','grupo','user','{nombre} {apellido} {email}');                                 
            $output = $crud->render();
            $this->loadView($output);
        }               
        
        function funciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user($x = '',$y = ''){
            $this->norequireds = array('foto');
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('tipo_usuario','enum',array('Agencia','Cliente','Dashboard'));
            $crud->field_type('password','password');            
            $crud->field_type('fecha_registro','hidden',date("Y-m-d"));            
            $crud->field_type('fecha_actualizacion','hidden',date("Y-m-d"));            
            $crud->unset_columns('password','');
            $crud->set_field_upload('foto','img/fotos');
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);                
                return $post;
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_before_delete(function($primary){
                get_instance()->db->delete('user_group',array('user'=>$primary));
                get_instance()->db->delete('user_empresas',array('user_id'=>$primary));
                get_instance()->db->delete('agencias_user',array('user_id'=>$primary));
                return $post;
            });
            $crud->columns('foto','nombre','email','status');
            $crud->add_action('Usar usuario','',base_url('seguridad/usar/').'/');
            $crud->set_relation_n_n('grupos','user_group','grupos','user','grupo','nombre');
            $output = $crud->render();
            $this->loadView($output);
        }

        function usar($id){
            //session_destroy();
            $this->user->login_short($id);
            redirect('panel');
        }
        
        function perfil($x = '',$y = ''){
            $this->as['perfil'] = 'user';
            $crud = $this->crud_function($x,$y);    
            $crud->where('id',$this->user->id);
            $crud->fields('nombre','password','email','foto','area','puesto');
            $crud->field_type('password','password');
            //$crud->field_type('foto','image',array('path'=>'img/fotos','width'=>'300px','height'=>'300px'));
            $crud->set_field_upload('foto','img/fotos');
            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                    $post['password'] = md5($post['password']);                
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$id){
                $this->user->login_short($id);
            });
            $crud->unset_back_to_list()->unset_list()->unset_add()->unset_delete()->unset_export()->unset_print()->unset_read();
            $output = $crud->render();
            //$output->scripts = get_header_crud($output->css_files, $output->js_files);
            $this->loadView($output);
        }

	    function acciones($x = '',$y = ''){            
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user_insertion($post,$id = ''){
            if(!empty($id)){
                $post['pass'] = $this->db->get_where('user',array('id'=>$id))->row()->password!=$post['password']?md5($post['password']):$post['password'];
            }
            else $post['pass'] = md5($post['pass']);
            return $post;
        }
    }
?>
