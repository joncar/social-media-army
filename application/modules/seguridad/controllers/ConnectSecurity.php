<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class ConnectSecurity extends Main{
        function __construct() {
            parent::__construct();
        }

        function configure(){
        	include APPPATH.'libraries/OAuth2/Autoloader.php';
        	$dsn      = 'mysql:dbname=oauth;host=localhost';
			$username = 'root';
			$password = 'Gp6HHMGF4M';
			OAuth2\Autoloader::register();	
			$storage = new OAuth2\Storage\Pdo(array('dsn' => $dsn, 'username' => $username, 'password' => $password));				
			// $dsn is the Data Source Name for your database, for exmaple "mysql:dbname=my_oauth2_db;host=localhost"			
			$this->storage = $storage;
			// Pass a storage object or array of storage objects to the OAuth2 server class
			return new OAuth2\Server($this->storage);
        }
        
        function connect(){
        	$server = $this->configure();        	
			// Add the "Client Credentials" grant type (it is the simplest of the grant types)
			$server->addGrantType(new OAuth2\GrantType\ClientCredentials($this->storage));

			// Add the "Authorization Code" grant type (this is where the oauth magic happens)
			$server->addGrantType(new OAuth2\GrantType\AuthorizationCode($this->storage));

			// Handle a request for an OAuth2.0 Access Token and send the response to the client
			$server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
        }

        function api(){
        	$server = $this->configure();

			// Handle a request to a resource and authenticate the access token
			if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
			    $server->getResponse()->send();
			    die;
			}
			echo json_encode(array('success' => true, 'message' => 'You accessed my APIs!'));
        }
    }
?>
