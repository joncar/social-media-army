<?php

require_once APPPATH . '/controllers/Panel.php';

class Admin extends Panel {

    const __GRUPOID__ = 2;

    function __construct() {
        parent::__construct();
    }

    function categorias_agencias($x = '', $y = '') {
        $crud = $this->crud_function($x, $y);
        $crud->set_subject('Categoria');
        $output = $crud->render();
        $output->title = 'Categoria';
        $this->loadView($output);
    }

    function categorias_servicios($x = '', $y = '') {
        $crud = $this->crud_function($x, $y);
        $crud->set_subject('Categoria');
        $crud->add_action('<i class="fa fa-bookmark"></i> Requisitos', '', base_url('agencia/admin/requisitos_categorias_servicios') . '/');
        $output = $crud->render();
        $output->title = 'Categoria';
        $this->loadView($output);
    }

    function requisitos_categorias_servicios($x) {
        $crud = $this->crud_function('', '');
        $crud->where('categorias_servicios_id', $x);
        if ($crud->getParameters() != 'list') {
            $crud->field_type('categorias_servicios_id', 'hidden', $x);
        }
        $dropdown = array();
        foreach ($this->db->get('view_servicios')->result() as
                $d) {
            $dropdown[$d->id] = $d->nombre;
        }
        $crud->field_type('view_servicios_id', 'dropdown', $dropdown);
        $output = $crud->render();
        $output->title = 'Categoria';
        $this->loadView($output);
    }

    function notificarNuevoIngreso($agencia,$notificacion = 36){
        //Enviar correo
        $post = $this->db->get_where('agencias',['id'=>$agencia]);
        if($post->num_rows()>0){
            $post = $post->row();
            $post = (array)$post;
            $post['link'] = base_url('agencia/frontend/registro/' . base64_encode($agencia));
            $this->enviarcorreo((object) $post, $notificacion);        
        }
    }

    function agencias($x = '', $y = '') {
        if($x == 'notificar'){
            $this->notificarNuevoIngreso($y);
            redirect('agencia/admin/agencias/success');
            die();
        }
        $crud = $this->crud_function($x, $y);
        $crud->set_field_upload('logo', 'img/agencias')
            ->set_field_upload('presentacion', 'img/agencias')     
            ->set_field_upload('documentos', 'img/agencias')                
            ->field_type('filtros', 'tags')
            ->field_type('fecha_ingreso', 'hidden', date("Y-m-d H:i:s"))
            ->field_type('logo2', 'hidden', '')
            ->field_type('fecha_proveedor', 'hidden', '')
            ->field_type('presidente', 'hidden', '')            
            ->field_type('alcance', 'hidden', '')
            ->field_type('user_id', 'hidden', '')
            ->field_type('Ciudad', 'hidden', '')
            ->field_type('calle', 'hidden', '')
            ->field_type('mapa', 'map', '')                
            ->field_type('categorias_agencias_id', 'hidden', '')
            ->field_type('fecha_inscripcion_proveedor', 'hidden', date("Y-m-d H:i:s"));

        $paises = array();
        foreach ($this->db->get_where('paises')->result() as
                $p) {
            $paises[] = $p->nombre;
        }
        $crud->field_type('paises', 'set', $paises);

        $super_poder = array();
        foreach ($this->db->get_where('super_poderes')->result() as
                $p) {
            $super_poder[] = $p->nombre;
        }
        $crud->field_type('super_poder', 'set', $super_poder);

        if ($crud->getParameters() == 'add') {
            $crud->fields(
                    'nombre', 'fecha_ingreso', 'fecha_inscripcion_proveedor', 'tipos_certificacion_id', 'estatus_agencias_id', 'contacto', 'email', 'agencias_grupos_id', 'logo'
            );
        }

        $crud->display_as('tipos_certificacion_id', 'Tipo de Certificación')
             ->display_as('fecha_inscripcion_proveedor', 'Fecha ultima revisión')
             ->display_as('paises_id','Pais')
             ->display_as('paises','Cobertura');

        $crud->callback_field('documentos',function($val){
            return $this->load->view('predesign/dropzone',[
                        'handle'=>'agencia/admin/agencias',
                        'name'=>'documentos',                        
                        'path'=>'img/agencias/',
                        'files'=>array_filter(explode(',',$val))                        
            ],TRUE);
        });
        $crud->callback_after_insert(function($post, $primary) {
            $post['link'] = base_url('agencia/frontend/registro/' . base64_encode($primary));
            get_instance()->enviarcorreo((object) $post, 36);
        });
        /*$crud->callback_before_update(function($post, $primary) {
            $this->db = get_instance()->db;
            $this->enviarcorreo = get_instance()->enviarcorreo;
            $status = get_instance()->db->get_where('estatus_agencias',['id'=>$post['estatus_agencias_id']])->row();
            if ($status->mostrar_listados == 1) {
                $agencia = $this->db->get_where('agencias', array('id' => $primary))->row();
                if ($agencia->status == 0) {
                    //Enviar correo                    
                    get_instance()->notificarNuevoIngreso($primary,43);
                    $this->db->update('user', array('status' => 1), array('email' => $post['email']));
                }
            }
        });*/
        $crud->callback_after_update(function($post, $primary) {
            $status = get_instance()->db->get_where('estatus_agencias',['id'=>$post['estatus_agencias_id']])->row()->mostrar_listados;
            foreach (get_instance()->db->get_where('agencias_user', array('agencias_id' => $primary))->result() as $a) {
                get_instance()->db->update('user', array('status' => $status), array('id' => $a->user_id));
            }
        });
        $crud->callback_after_delete(function($primary) {
            get_instance()->db->delete('agencias_servicios', array('agencias_id' => $primary));
            get_instance()->db->delete('agencias_categorias', array('agencias_id' => $primary));
            get_instance()->db->delete('agencias_clientes', array('agencias_id' => $primary));
            get_instance()->db->delete('agencias_dominios', array('agencias_id' => $primary));
            get_instance()->db->delete('agencias_favoritas', array('agencias_id' => $primary));
            //get_instance()->db->delete('agencias_grupos', array('agencias_id' => $primary));
            //get_instance()->db->delete('agencias_ranking', array('agencias_id' => $primary));
            get_instance()->db->delete('agencias_user', array('agencias_id' => $primary));
            get_instance()->db->delete('briefs', array('agencias_id' => $primary));
        });

        $crud->columns('logo', 'nombre', 'email', 'fecha_inscripcion_proveedor');
        $crud->set_relation('agencias_grupos_id','agencias_grupos','nombre',array(),'nombre');
        $crud->set_relation_n_n('Servicios', 'agencias_servicios', 'servicios', 'agencias_id', 'servicios_id', 'nombre');
        $crud->order_by('estatus_agencias_id', 'ASC');
        $crud->add_action('Reenviar invitación','',base_url('agencia/admin/agencias/notificar').'/');
        $output = $crud->render();
        if ($crud->getParameters() == 'edit') {
            $output->output = $this->load->view('agencias', array('output' => $output->output, 'valor' => $y), TRUE, 'dashboard');
            $agencias = new ajax_grocery_crud();
            $agencias = $agencias->set_table('agencias')->set_subject('agencias')->set_theme('bootstrap2')->render(1);
            $output->js_files = array_merge($output->js_files, $agencias->js_files);
            $output->css_files = array_merge($output->css_files, $agencias->css_files);
        }
        $output->output = $this->load->view('agencias-mapa', array('output' => $output->output), TRUE, 'dashboard');
        $this->loadView($output);
    }

    function agencias_clientes($x) {
        $crud = $this->crud_function($x, '');
        $crud->set_subject('Cliente');
        $crud->where('agencias_id', $x);
        $crud->field_type('agencias_id', 'hidden', $x);
        //$crud->field_type('logo','image',array('path'=>'img/clientes/','width'=>'450px','height'=>'250px'));
        $crud->set_field_upload('logo', 'img/clientes');
        $crud->unset_back_to_list();
        $output = $crud->render();
        $output->output = $this->load->view('agenciasTabs', array('output' => $output->output,'y'=>$x), TRUE, 'dashboard');
        $this->loadView($output);
    }

    function agencias_dominios($x) {
        $crud = $this->crud_function($x, '');
        $crud->set_subject('Dominios');
        $crud->where('agencias_id', $x);
        $crud->field_type('agencias_id', 'hidden', $x);
        $crud->unset_back_to_list();
        $output = $crud->render();
        $output->output = $this->load->view('agenciasTabs', array('output' => $output->output,'y'=>$x), TRUE, 'dashboard');
        $this->loadView($output);
    }

    function agencias_user($x = '') {
        $crud = $this->crud_function($x, '');
        $crud->set_subject('Usuarios');
        $crud->where('agencias_id', $x);
        $crud->set_relation('user_id', 'user', '{nombre} {apellido} {email}');
        $crud->field_type('agencias_id', 'hidden', $x);
        $crud->unset_back_to_list();
        $output = $crud->render();
        $output->output = $this->load->view('agenciasTabs', array('output' => $output->output,'y'=>$x), TRUE, 'dashboard');
        $this->loadView($output);
    }

    function agencias_categorias($x) {
        $crud = $this->crud_function($x, '');
        $crud->where('agencias_id', $x);
        $crud->set_subject('Categorias');
        $crud->field_type('agencias_id', 'hidden', $x);
        $crud->unset_back_to_list();
        $output = $crud->render();
        $output->output = $this->load->view('agenciasTabs', array('output' => $output->output,'y'=>$x), TRUE, 'dashboard');
        $this->loadView($output);
    }

    function agencias_grupos() {
        $crud = $this->crud_function('', '');
        $crud->set_subject('Grupos');
        $crud->unset_back_to_list();
        $output = $crud->render();        
        $this->loadView($output);
    }

    function organizaciones() {
        $crud = $this->crud_function('', '');
        $crud->set_relation_n_n('organizaciones','organizaciones_paises','paises','organizaciones_id','paises_id','nombre','priority');
        $crud->field_type('estado','true_false',[0=>'Inactivo',1=>'Activa']);
        $output = $crud->render();        
        $this->loadView($output);
    }

    function agencias_resenas($x) {
        $crud = $this->crud_function($x, '');
        $crud->set_subject('Reseña');
        $crud->where('agencias_id', $x);        
        $crud->unset_back_to_list();
        $output = $crud->render();
        $output->output = $this->load->view('agenciasTabs', array('output' => $output->output,'y'=>$x), TRUE, 'dashboard');
        $this->loadView($output);
    }

    

}

?>
