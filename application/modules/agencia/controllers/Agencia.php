<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Agencia extends Panel{
        const __GRUPOID__ = 2;
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }
        function agencia_perfil($x = '',$y = ''){
            $this->as['agencia_perfil'] = 'user';
            $crud = $this->crud_function('','');
            $crud->set_theme('generic2')
                 ->set_subject('datos de agencia');
            $crud->fields('nombre','apellido','password','foto')
                 ->field_type('password','password')
                 ->set_field_upload('foto','img/fotos')
                 ->unset_back_to_list();
            if($crud->getParameters()=='edit' && is_numeric($y) && $y!=$this->user->id){
                redirect('agencia/cuenta');
            }
            $crud->callback_before_update(function($post,$primary){
                $pass = get_instance()->db->get_where('user',array('id'=>$primary))->row()->password;
                if($pass!=$post['password']){
                    $post['password'] = md5($post['password']);
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$primary){
                get_instance()->user->login_short($primary);
            });
            $output = $crud->render('','Theme/Cliente/cruds/');
            $output->view = 'perfil';            
            $this->loadView($output);

        }
        function agencias($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);  
            $crud->set_theme('agencias');
            $crud->set_field_upload('logo','img/agencias')
                 ->field_type('filtros','tags')
                 ->field_type('tipo_servicio','tags')
                 ->field_type('fecha_ingreso','hidden',date("Y-m-d H:i:s"));
            $crud->display_as('user_id','Usuario Administrador');
            $crud->callback_column('link',function($val,$row){
                return base_url('agencia/');
            });
            
            $crud->unset_delete();
            $crud->columns('logo','agencias_nombre','user_id','telefono','correo','categorias_agencias_id','estatus_agencias_id','link');
            if($x=='read' && is_numeric($y)){
                $crud->set_theme('bootstrap2');
                $output = $crud->render();
                $output->css_files = array();
                $output->js_files = array();
                $agencia = $this->db->get_where('agencias',array('id'=>$y));
                if($agencia->num_rows()>0){
                    $output->output = $this->load->view('read',array('agencia'=>$agencia->row()),TRUE);
                }else{
                    redirect('agencia/agencias');
                }
            }else{
                $output = $crud->render('','Theme/Cliente/crudss/');
                
            }
            $this->loadView($output);
        }

        function verAgencia($id){
            if(is_numeric($id)){
                $agencia = $this->querys->getAgencia($id);
                if(!empty($agencia)){
                    $this->loadView(array('view'=>'agencia','agencia'=>$agencia,'title'=>$agencia->nombre));                                    
                }else{
                    throw new exception('La agencia a consultar no se encuentra disponible',404);
                }
            }
        }

        

        function mensajes(){
            $this->loadView('mensajes');
        }

        function usuarios($return = 1){
            $crud = new ajax_grocery_crud();
            $crud->unset_jquery();      
            $crud->set_subject('Mis usuarios');
            $crud->set_theme('generic');                          
            
            if($crud->getParameters()=='list'){
                $crud->set_table('agencias_user');  
                $crud->where('agencias_id',$this->user->empresa)
                     ->where('agencias_user.user_id !=',$this->user->id);
                $crud->set_relation('user_id','user','{nombre}|{apellido}|{email}');
                $crud->columns('je8701ad4.email');
                $crud->callback_column('je8701ad4.nombre',function($val,$row){return explode('|',$row->se8701ad4)[0];});
                $crud->callback_column('je8701ad4.apellido',function($val,$row){return explode('|',$row->se8701ad4)[1];});
                $crud->callback_column('je8701ad4.email',function($val,$row){return explode('|',$row->se8701ad4)[2];});
                $crud->display_as('je8701ad4.nombre','Nombre')
                     ->display_as('je8701ad4.apellido','Apellido')
                     ->display_as('je8701ad4.email','Email');
                $crud->unset_delete();
                $crud->unset_edit();
                $crud->unset_read();
            }else{
                $crud->set_table('user');
                $crud->field_type('fecha_registro','hidden',date("Y-m-d"))
                     ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                     ->field_type('status','hidden',1)
                     ->field_type('foto','invisible')
                     ->field_type('tipo_usuario','hidden','Cliente') 
                     ->field_type('admin','hidden',0) 
                     ->field_type('nombre','hidden','default') 
                     ->field_type('apellido','hidden','default') 
                     ->field_type('direccion','hidden','default')
                     ->field_type('puesto','hidden','default')
                     ->field_type('ultima_conexion','hidden',date("Y-m-d"))
                     ->field_type('fecha_registro','hidden',date("Y-m-d"))
                     ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                     ->field_type('password','hidden','12345678');                
                if($crud->getParameters()=='add'){
                    $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]|callback_validar_email');
                }
                $crud->callback_before_insert(function($post){
                    $post['password'] = md5($post['password']);                
                    return $post;
                });
                $crud->callback_after_insert(function($post,$primary){
                    get_instance()->db->insert('agencias_user',array('user_id'=>$primary,'agencias_id'=>get_instance()->user->empresa));
                    $post['link'] = base_url('registro/validar/Agencia/'.base64_encode($primary));
                    get_instance()->enviarcorreo((object)$post,8);
                });
                $crud->callback_before_update(function($post,$primary){
                    if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                    $post['password'] = md5($post['password']);
                    return $post;
                });
            }
            $crud->required_fields_array();
            $crud->set_url('agencia/usuarios/');
            $crud->unset_back_to_list();
            $crud = $crud->render('','Theme/Cliente/cruds/');           
            if($return==1){
                return $crud;
            }else{
                $crud->view = 'crud';
                $this->loadView($crud);
            }            
        }

        function validar_email($email){
            list($name,$dominio) = explode('@',$email);
            if($this->db->get_where('agencias_dominios',array('nombre'=>$dominio,'agencias_id'=>$this->user->empresa))->num_rows()==0){
                $this->form_validation->set_message('validar_email','Correo eletcrónico no autorizado, pongase en contacto con un administrador para añadirlo al sistema.');
                return false;
            }
            return true;
        }



        function perfil(){
            $crud = new ajax_grocery_crud();
            $crud->unset_jquery();      
            $crud->set_subject('Empresa');
            $crud->set_theme('generic'); 
            $crud->set_table('agencias');
            $crud->where('id',$this->user->empresa);
            $crud->unset_back_to_list();
            $crud->unset_list()->unset_delete()->unset_read()->unset_print();
            $crud->field_type('user_id','hidden',$this->user->id)
                 ->field_type('tipos_certificacion_id','invisible')
                 ->field_type('email','invisible')
                 ->field_type('mapa','invisible')
                 ->field_type('categorias_agencias_id','invisible')
                 ->field_type('tipos_certificacion_id','invisible');
            $crud->set_field_upload('logo','img/agencias');
            $crud->set_field_upload('logo2','img/agencias');
            $crud = $crud->render('','Theme/Cliente/cruds/');
            $crud->view = 'crud';
            $this->loadView($crud);                
        }

        function configuracion(){
            $user = $this->usuarios(1);            
            $this->loadView(array('view'=>'configuracion','user'=>$user));
        }

        function cuenta($x = '', $y = ''){
            $crud = new ajax_grocery_crud();
            $crud = $crud->set_table('agencias')
                         ->set_subject('Agencia')
                         ->set_theme('cuenta')
                         ->unset_jquery();
                         /*->required_fields_array();*/            
            $crud->set_field_upload('logo','img/agencias')
                 ->set_field_upload('presentacion','img/agencias')
                 ->field_type('logo2','hidden')
                 ->field_type('status','true_false',array(0=>'Inactiva',1=>'Activa'))
                 ->field_type('filtros','tags')
                 ->field_type('tipos_certificacion_id','readonly')
                 ;
            
            $paises = array();
            foreach ($this->db->get_where('paises')->result() as
                    $p) {
                $paises[] = $p->nombre;
            }
            $crud->field_type('paises', 'set', $paises);
        
            $crud->display_as('user_id','Usuario Administrador')
                 ->display_as('razon_social','Razón Social')
                 ->display_as('web','Sitio web')
                 ->display_as('telefono','Teléfono')
                 ->display_as('email','Correo de contacto')
                 ->display_as('fecha_constitucion','Fecha de constitución')
                 ->display_as('rfc','RFC')
                 ->display_as('contacto','Contacto')
                 ->display_as('ceo','CEO')
                 ->display_as('empleados','Número de empleados')
                 ->display_as('paises_id','País')
                 ->display_as('Ciudad','Ciudad')
                 ->display_as('estado','Estado')
                 ->display_as('colonia','Colonia')
                 ->display_as('delegacion','Delegación')
                 ->display_as('numero_exterior','No. Exterior')
                 ->display_as('numero_interior','No. Interior');
            if($crud->getParameters()!='edit'){
                if($crud->getParameters()=='add' || $crud->getParameters()=='list'){
                    redirect('agencia/cuenta/edit/'.$this->user->empresa);
                }
            }

            if($crud->getParameters()=='edit' && $y!=$this->user->empresa){
                redirect('agencia/cuenta/edit/'.$this->user->empresa);
            }

            //Bloquear campos ya llenos
            $requeridos = array();
            if($crud->getParameters()=='edit'){
                $agencia = $this->db->get_where('agencias',array('id'=>$y));
                if($agencia->num_rows()>0 && $this->user->empresa==$y){
                    foreach($agencia->row() as $n=>$v){
                        if(!empty($v) && $n!='logo' && $n!='logo2' && $n!='paises_id'){
                            $crud->field_type($n,'readonly');
                        }else{
                            $requeridos[] = $n;
                        }
                    }                    
                }else{
                    redirect('panel');
                }
            }
            $crud->columns('logo','nombre','presidente','email');
            $crud->unset_back_to_list();            
            $crud = $crud->render('','Theme/Agencia/cruds/');            
            $crud->view = 'datos-de-cuenta';            
            $this->loadView($crud);

        }

        function setMessage($id){
            
            $mensajes = (array)$this->querys->getPush($this->user->id);
            $encontrado = false;
            foreach($mensajes as $n=>$v){
                if($n==$id){
                    $encontrado = true;
                }
            }
            if($encontrado){                
                $m = array();
                foreach($mensajes as $n=>$v){
                    if($n!=$id){
                        $m[] = $v;
                    }else{
                        $mensaje = $v;
                    }
                }
                $mensajes = $m;
                
                $this->querys->refreshPush($this->user->id,(array)$mensajes);
                if(empty($mensaje->link)){
                    redirect($_SERVER['HTTP_REFERER']);
                }
                if(!empty($mensaje->link)){
                    redirect($mensaje->link);
                }
            }else{
                redirect($_SERVER['HTTP_REFERER']);
            }
        }

        function buscar(){
            if(!empty($_GET['q']))
            {
                $this->db->where('(empresa = 0 OR empresa = '.$this->user->empresa.')',NULL,TRUE);
                $this->db->like('nombre',$_GET['q']);
                $result = $this->db->get_where('view_buscador_agencias');
                $res = array();
                foreach($result->result() as $r){
                    $r->link = base_url().'agencia/'.$r->tipo.'/'.$r->id;
                    $res[] = $r;
                }
                echo json_encode($res);
            }
        }
        
    }
?>
