<?php 
require_once APPPATH.'/controllers/Panel.php';    
class Frontend extends Main{
	function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
    }



    protected function crud_function($x,$y,$controller = ''){
        $crud = new ajax_grocery_CRUD($controller);        
        $crud->set_theme('bootstrap2');
        $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
        $crud->set_table($table);
        $crud->set_subject(ucfirst($this->router->fetch_method()));
        if(!empty($this->norequireds)){
            $crud->norequireds = $this->norequireds;
        }
        $crud->required_fields_array();
        
        if(method_exists('panel',$this->router->fetch_method()))
         $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
        return $crud;
    }  

    function registro($id,$x = ''){
    	if(!empty($id) && is_numeric(base64_decode($id))){
    		if(!empty($_SESSION['user'])){
    			session_destroy();    	
    			echo '<script>document.location.reload();</script>';		
    			die();
    		}
    		$agencia = $this->db->get_where('agencias',array('id'=>base64_decode($id)));
    		if($agencia->num_rows()>0){
    			$agencia = $agencia->row();
    			get_instance()->agencia = $agencia;
    			if($this->user->log && $agencia->estatus_agencias_id==1){
    				$this->loadView(array('view'=>'Login/completar-registro3',
			        	'output'=>'<h1>Su agencia se encuentra en revisión, le enviaremos un mensaje cuando sea aprobada</h1>',
			        	'js_files'=>array()
			        ));
    			}if($this->user->log && $agencia->status==2){
    				$this->user->login_short($this->user->id);    				
    			}else{
			    	$this->as['registro'] = 'agencias';
			    	$crud = $this->crud_function('','');
			    	$crud->set_theme('completarAgencia2');         
			        $crud->fields(
			            'razon_social',	            
			            'fecha_constitucion',
			            'rfc',
			            'ceo',
			            'empleados',
			            'direccion',
			            'Ciudad',
			            'paises',
			            'cp',
			            'web',
			            'telefono'
			        );

			        $crud->required_fields(
			            'razon_social',	            
			            'fecha_constitucion',
			            'rfc',
			            'ceo',
			            'empleados',
			            'direccion',
			            'Ciudad',
			            
			            'cp',
			            'web',
			            'telefono'
			        );

			        if($x=='update_validation'){
			        	$crud->fields(
				            'razon_social',	            
				            'fecha_constitucion',
				            'rfc',
				            'ceo',
				            'empleados',
				            'direccion',
				            'Ciudad',
				            
				            'cp',
				            'web',
				            'telefono',
				            'password'
				        );
			        	$crud->set_rules('password','Password','required');
			        }

			        $paises = array();
			        foreach($this->db->get_where('paises')->result() as $p){
			        	$paises[] = $p->nombre;
			        }
			        $crud->field_type('paises','set',$paises);

			        $crud->display_as('categorias_agencias_id','Categoria')                
			             ->display_as('tipos_certificacion_id','Tipo de Certificación')
			             ->display_as('paises','Paises');
			        $crud->callback_after_update(function($post,$primary){
			        	$d = get_instance()->agencia;
			        	$this->db = get_instance()->db;
			            $this->db->insert('user',array('email'=>$d->email,'nombre'=>$d->contacto,'apellido'=>'','tipo_usuario'=>'Agencia','fecha_actualizacion'=>date("Y-m-d"),'password'=>md5($_POST['password']),'admin'=>0,'status'=>1));
			            $user = $this->db->insert_id();
				        $this->db->insert('agencias_user',array(
		                    'user_id'=>$user,
		                    'agencias_id'=>$primary
		                ));
		                $this->db->insert('user_group',array(
		                    'user'=>$user,
		                    'grupo'=>3
		                ));
		                $this->db->update('agencias',array('estatus_agencias_id'=>2),array('id'=>$primary));
			        });
			        $crud->unset_list()
			        	 ->unset_add()
			        	 ->unset_read()
			        	 ->unset_delete()
			        	 ->unset_print()
			        	 ->unset_export()
			        	 ->unset_back_to_list();
			        $crud->set_lang_string('update_success_message','Sus datos han sido almacenados con éxito <script>document.location.href = "'.base_url('panel').'"</script>');
			        if(empty($x)){
			        	$crud->set_primary_key_value(base64_decode($id));
			        	$output = $crud->render('3','Theme/Login/cruds/');
			        }else{
			        	$output = $crud->render('','Theme/Login/cruds/');
			    	}
			        $this->load->view('Login/completar-registro3',array(
			        	'output'=>$output->output,
			        	'js_files'=>$output->js_files
			        ));
		    	}
	    	}
	    }
    }
}