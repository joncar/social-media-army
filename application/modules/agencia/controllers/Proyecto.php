 <?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Proyecto extends Panel{
        const __GRUPOID__ = 2;
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }
        function proyectos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);              
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read();            
            $output = $crud->render('');
            unset($output->js_files);
            $output->view = 'proyectos';
            $this->loadView($output);
        }

        function showResult($id,$return = false){
            $this->querys->showResult($id);
        }

        function verProyecto($id){
            if(is_numeric($id)){
                //Validar brief
                $this->db->select('briefs.*, agencias.nombre');
                $this->db->join('agencias','agencias.id = briefs.agencias_id');
                $brief = $this->db->get_where('briefs',array('agencias_id'=>$this->user->empresa,'proyectos_id'=>$id));
                if($brief->num_rows()>0){
                    $proyecto = $this->querys->showResult($id,true);
                    $proyecto->agencias = new ajax_grocery_crud();
                    $proyecto->agencias->set_table('agencias_con_filtros');
                    $proyecto->agencias->field_type('status','true_false',array(0=>'Inactiva',1=>'Activa'))
                                       ->set_primary_key('id');
                    foreach($this->db->get_where('briefs',array('proyectos_id'=>$id))->result() as $b){
                        $proyecto->agencias->or_where('id',$b->agencias_id);
                    }
                    $proyecto->agencias->set_theme('agencias');
                    $proyecto->agencias->group_by('agencias_con_filtros.id');
                    $proyecto->agencias = $proyecto->agencias->render('','Theme/Agencia/cruds/')->output;
                    $this->loadView(array('view'=>'proyecto','proyecto'=>$proyecto,'brief'=>$brief->row()));
                }else{
                    throw new exception('No has sido invitado para ver este proyecto, por lo que no posees permisos para visualizarlo',403);
                }
            }
        }

        function mi_historial(){
            $this->as['mi_historial'] = 'view_proyectos_agencias';
            $crud = $this->crud_function("","");            
            $crud->group_by('id');
            $crud->order_by('view_proyectos_agencias.id','DESC');
            //$crud->where('porcentaje >=',100);
            $crud->where('user_id',$this->user->empresa);
            $crud->set_primary_key('id');           
            $crud->set_theme('historial');            
            $crud->columns('imagen','nombre','ganadora','agencia1','agencia2','agencia3','porcentaje','fecha','pais','categoria','producto','participante','descripcion');
            $crud->callback_column('ganadora',function($val,$row){
                $proyecto = $this->querys->showResult($row->id,true);
                return $proyecto;
            });
            $crud->callback_column('agencia1',function($val,$row){
                $proyecto = $this->querys->showResult($row->id,true);
                return $proyecto->briefs->num_rows()>0?base_url().'img/agencias/'.$proyecto->briefs->row(0)->logo:base_url().'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';
            });
            $crud->callback_column('agencia2',function($val,$row){
                $proyecto = $this->querys->showResult($row->id,true);
                return $proyecto->briefs->num_rows()>0?base_url().'img/agencias/'.$proyecto->briefs->row(1)->logo:base_url().'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';
            });
            $crud->callback_column('agencia3',function($val,$row){
                $proyecto = $this->querys->showResult($row->id,true);
                return $proyecto->briefs->num_rows()>0?base_url().'img/agencias/'.$proyecto->briefs->row(2)->logo:base_url().'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';
            });
            $crud->callback_column('fecha',function($val,$row){
                $proyecto = $this->querys->showResult($row->id,true);
                return $proyecto->fase;
            });
            $crud->callback_column('pais',function($val,$row){
                $proyecto = $this->querys->showResult($row->id,true);
                return $proyecto->paises;
            });
            $crud->callback_column('categoria',function($val,$row){
                $proyecto = $this->querys->showResult($row->id,true);
                return $proyecto->categorias;
            });
            $crud->callback_column('producto',function($val,$row){
                $proyecto = $this->querys->showResult($row->id,true);
                return $proyecto->productos;
            });
            $crud->callback_column('participante',function($val,$row){
                $proyecto = $this->querys->showResult($row->id,true);
                return $proyecto->invitaciones;
            });
            $crud = $crud->render('','Theme/Agencia/cruds/');     
            $crud->view = 'historial';                   
            $this->loadView($crud);
        }

        function enviarPropuesta(){
            if(!empty($_POST['brief']) && is_numeric($_POST['brief']) && !empty($_FILES)){
                $brief = $this->db->get_where('briefs',array('id'=>$_POST['brief'],'agencias_id'=>$this->user->empresa));
                if($brief->num_rows()>0){
                    $brief = $brief->row();
                    $primary = $brief->id;                    
                    $seudonimo = date('his').substr(md5($primary),0,8).'-'.basename($_FILES['propuesta']["name"]);
                    if(move_uploaded_file($_FILES['propuesta']["tmp_name"],'files/'.$seudonimo)){                        
                        if(!empty($brief->propuesta) && file_exists('files/'.$brief->propuesta)){
                            unlink('files/'.$brief->propuesta);
                        }
                        $this->db->update('briefs',array('propuesta'=>$seudonimo,'mensaje'=>''),array('id'=>$brief->id));
                        //Enviar correo
                        $this->db->join('user','user.id = proyectos_user.user_id');
                        $c = $this->db->get_where('proyectos_user',array('proyectos_id'=>$brief->proyectos_id,'tipo'=>1))->row();
                        $proyecto = $this->db->get_where('proyectos',array('id'=>$brief->proyectos_id))->row()->nombre;
                        $agencia = $this->db->get_where('agencias',array('id'=>$brief->agencias_id))->row()->nombre;
                        $link = base_url('cliente/proyecto/verProyecto/'.$brief->proyectos_id);
                        $this->enviarcorreo((object)array('link'=>$link,'email'=>$c->email,'nombre'=>$c->nombre,'apellido'=>$c->apellido,'proyecto'=>$proyecto,'agencia'=>$agencia),41,'',$link);
                        echo base_url('files/'.$seudonimo);
                    }else{
                        echo 'none';
                    }
                }else{
                    echo 'none';
                }

            }
        }

        function aceptarInvitacion(){
            if(!empty($_POST['brief']) && is_numeric($_POST['brief'])){
                $this->db->join('agencias','agencias.id = briefs.agencias_id');
                $brief = $this->db->get_where('briefs',array('briefs.id'=>$_POST['brief'],'agencias_id'=>$this->user->empresa,'briefs.status'=>1));
                if($brief->num_rows()>0){
                    $this->db->update('briefs',array('status'=>2),array('id'=>$_POST['brief']));
                    $proyecto = $brief->row()->proyectos_id;
                    
                    $this->db->select('user.*');
                    $this->db->join('user','user.id = proyectos_user.user_id');
                    foreach($this->db->get_where('proyectos_user',array('proyectos_id'=>$proyecto,'proyectos_user.tipo'=>1))->result() as $p){
                        $p->agencia = $brief->row()->nombre;
                        $p->proyecto = $this->db->get_where('proyectos',array('id'=>$proyecto))->row()->nombre;
                        $p->link = base_url('cliente/proyecto/verProyecto/'.$proyecto);
                        $this->enviarcorreo((object)$p,14,'',$p->link);
                    }
                }else{
                    echo 'none';
                }

            }
        }

        function briefs(){
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='edit'){
                $crud->required_fields('fecha_modificacion');
            }
            $crud->callback_before_update(function($post,$primary){
                $this->db = get_instance()->db;
                //Se rechazo la propuesta
                $brief = $this->db->get_where('briefs',array('id'=>$primary))->row();     
                $agencia = $this->db->get_where('agencias',array('id'=>$this->user->empresa))->row()->nombre;           
                $mensaje = $this->db->get_where('notificaciones',array('id'=>15));
                $proyecto = $this->db->get_where('proyectos',array('id'=>$brief->proyectos_id))->row();
                $this->db->join('user','user.id = proyectos_user.user_id');
                foreach($this->db->get_where('proyectos_user',array('proyectos_id'=>$proyecto->id))->result() as $emails){
                    $msj = $mensaje;
                    get_instance()->enviarcorreo((object)array('email'=>$emails->email,'agencia'=>$agencia,'apellido'=>$emails->apellido,'nombre'=>$emails->nombre,'proyecto'=>$proyecto->nombre,'mensaje'=>$post['mensaje']),15);
                }
                $post['mensaje'] = get_instance()->error($post['mensaje']);
                return $post;
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }


        function rechazarInvitacion(){
            if(!empty($_POST['brief']) && is_numeric($_POST['brief'])){
                $this->db->join('agencias','agencias.id = briefs.agencias_id');
                $brief = $this->db->get_where('briefs',array('briefs.id'=>$_POST['brief'],'agencias_id'=>$this->user->empresa,'briefs.status'=>1));
                if($brief->num_rows()>0){
                    $this->db->update('briefs',array('status'=>-1),array('id'=>$_POST['brief']));
                    $proyecto = $brief->row()->proyectos_id;
                    $this->db->select('user.*');
                    $this->db->join('user','user.id = proyectos_user.user_id');
                    foreach($this->db->get_where('proyectos_user',array('proyectos_id'=>$proyecto,'proyectos_user.tipo'=>1))->result() as $p){
                        $p->agencia = $brief->row()->nombre;
                        $p->proyecto = $this->db->get_where('proyectos',array('id'=>$proyecto))->row()->nombre;
                        $p->link = base_url('cliente/proyecto/verProyecto/'.$proyecto);
                        $this->enviarcorreo((object)$p,15,'',$p->link);
                    }
                }else{
                    echo 'none';
                }

            }
        }
        
    }
?>
