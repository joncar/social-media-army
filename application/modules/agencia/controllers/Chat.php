 <?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Chat extends Panel{
        const __GRUPOID__ = 2;
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }

        function getMessage($messages){
            $limit = 100;
            $row = 0;
            $mesgs = array();
            for($i=count($messages)-1;$i>=0;$i--){
                $row++;
                if($row<$limit){                
                    $mesgs[] = $messages[$i];
                }
            }
            return array_reverse($mesgs);
        }
        function salas($id){
            if(is_numeric($id)){
                //Validar brief
                $brief = $this->db->get_where('briefs',array('agencias_id'=>$this->user->empresa,'proyectos_id'=>$id));
                $proyectos = $this->db->get_where('view_proyectos',array('user_id'=>$this->user->id,'id'=>$id));
                if($brief->num_rows()>0 || $proyectos->num_rows()>0){
                    $proyecto = $this->querys->showResult($id,true);
                    $proyecto->agencias = new ajax_grocery_crud();
                    $proyecto->agencias->set_table('agencias_con_filtros');
                    $proyecto->agencias->field_type('status','true_false',array(0=>'Inactiva',1=>'Activa'))
                                       ->set_primary_key('id');
                    foreach($this->db->get_where('briefs',array('proyectos_id'=>$id))->result() as $b){
                        $proyecto->agencias->or_where('id',$b->agencias_id);
                    }
                    $proyecto->agencias->set_theme('agencias');
                    $proyecto->agencias->group_by('agencias_con_filtros.id');
                    $proyecto->agencias = $proyecto->agencias->render('','Theme/Agencia/cruds/')->output;
                    //Esta la carpeta creada?
                    if(!folder_exist('salas/'.$proyecto->id)){
                        mkdir('salas/'.$proyecto->id.'/',0777);
                        mkdir('salas/'.$proyecto->id.'/messages/',0777);
                        mkdir('salas/'.$proyecto->id.'/files/',0777);                        
                    }
                    /*if(!file_exists('salas/'.$proyecto->id.'/messages/messages-'.$this->user->empresa.'.json')){
                        file_put_contents('salas/'.$proyecto->id.'/messages/messages-'.$this->user->empresa.'.json',base64_encode('[]'));
                    }
                    $messages = json_decode(base64_decode(file_get_contents('salas/'.$proyecto->id.'/messages/messages-'.$this->user->empresa.'.json')));
                    $messages = $this->getMessage($messages);*/
                    if(!folder_exist('salas/'.$proyecto->id)){
                        mkdir('salas/'.$proyecto->id.'/',0777);
                        mkdir('salas/'.$proyecto->id.'/messages/',0777);
                        mkdir('salas/'.$proyecto->id.'/files/',0777);
                        file_put_contents('salas/'.$proyecto->id.'/messages/messages.json',base64_encode('[]'));
                    }
                    $messages = @json_decode(base64_decode(file_get_contents('salas/'.$proyecto->id.'/messages/messages.json')));
                    $messages = @$this->getMessage($messages);
                    $this->loadView(array('messages'=>$messages,'view'=>'sala-juntas','proyecto'=>$proyecto,'brief'=>$brief->row()));
                }else{
                    throw new exception('No has sido invitado para ver este proyecto, por lo que no posees permisos para visualizarlo',403);
                }
            }
        }

        function chat($id){
            if(is_numeric($id)){
                //Validar brief
                $brief = $this->db->get_where('briefs',array('agencias_id'=>$this->user->empresa,'proyectos_id'=>$id));
                $proyectos = $this->db->get_where('view_proyectos',array('user_id'=>$this->user->id,'id'=>$id));
                if($brief->num_rows()>0 || $proyectos->num_rows()>0){
                    $proyecto = $this->querys->showResult($id,true);
                    $proyecto->agencias = new ajax_grocery_crud();
                    $proyecto->agencias->set_table('agencias_con_filtros');
                    $proyecto->agencias->field_type('status','true_false',array(0=>'Inactiva',1=>'Activa'))
                                       ->set_primary_key('id');
                    foreach($this->db->get_where('briefs',array('proyectos_id'=>$id))->result() as $b){
                        $proyecto->agencias->or_where('id',$b->agencias_id);
                    }
                    $proyecto->agencias->set_theme('agencias');
                    $proyecto->agencias->group_by('agencias_con_filtros.id');
                    $proyecto->agencias = $proyecto->agencias->render('','Theme/Agencia/cruds/')->output;
                    if(!file_exists('salas/'.$proyecto->id.'/messages/messages.json')){
                        file_put_contents('salas/'.$proyecto->id.'/messages/messages.json',base64_encode('[]'));
                    }
                    /*$messages = json_decode(base64_decode(file_get_contents('salas/'.$proyecto->id.'/messages/messages-'.$this->user->empresa.'.json')));*/
                    $messages = json_decode(base64_decode(file_get_contents('salas/'.$proyecto->id.'/messages/messages.json')));
                    $messages = $this->getMessage($messages);
                    $result = array();
                    $result['mensajes'] = $this->load->view('Agencia/chat/_chat',array('messages'=>$messages,'proyecto'=>$proyecto,'brief'=>$brief->row()),TRUE);
                    $result['integrantes'] = $this->load->view('Agencia/chat/_integrantes',array('messages'=>$messages,'proyecto'=>$proyecto,'brief'=>$brief->row()),TRUE);
                    $result['fecha'] = empty($messages)?date("Y-m-d H:i:s"):$messages[count($messages)-1]->fecha;
                    echo json_encode($result);

                }else{
                    throw new exception('No has sido invitado para ver este proyecto, por lo que no posees permisos para visualizarlo',403);
                }
            }            
        }
        function sendMessage($id){
            if(is_numeric($id)){
                //Validar brief
                $brief = $this->db->get_where('briefs',array('agencias_id'=>$this->user->empresa,'proyectos_id'=>$id));
                $proyectos = $this->db->get_where('view_proyectos_agencias',array('user_id'=>$this->user->empresa,'id'=>$id));
                if($brief->num_rows()>0 || $proyectos->num_rows()>0){
                    $integrante = $this->db->get_where('view_integrantes_chat',array('proyectos_id'=>$id,'id'=>$this->user->empresa));
                    if($integrante->num_rows()>0){
                        $integrante = (array)$integrante->row();
                        $integrante['mensaje'] = $_POST['mensaje'];
                        $integrante['fichero'] = '';
                        $integrante['fecha'] = date("Y-m-d H:i:s");
                        $messages = json_decode(base64_decode(file_get_contents('salas/'.$id.'/messages/messages.json')));
                        $messages[] = $integrante;
                        $messages = json_encode($messages);
                        file_put_contents('salas/'.$id.'/messages/messages.json',base64_encode($messages));
                        $remitente = $this->db->get_where('agencias',array('id'=>$this->user->empresa));
                        if($remitente->num_rows()==0){
                            $remitente = $this->user->nombre;
                        }else{
                            $remitente = $remitente->row()->nombre;
                        }
                        //Send push                        
                        $integrantes = $this->db->get_where('view_integrantes_chat',array('proyectos_id'=>$id,'view_integrantes_chat.tipo'=>1));
                        foreach($integrantes->result() as $a){
                            $this->querys->sendPush($a->id,$remitente.' te ha enviado un mensaje por el chat: '.$_POST['mensaje'],base_url('cliente/chat/salas/'.$id),true,$proyectos->row()->nombre);
                        }
                    }
                }
            }      
        }
}