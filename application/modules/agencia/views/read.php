<div class="container-fluid">

<!-- Inicia Contenido -->
<div class="row">
  <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Agencia</div></div>
</div>

<div class="row contenedor-nuevo-proyecto-dashboard">
  <div class="col-xs-12 col-sm-12 padding0">

      <div class="col-xs-12 col-sm-4">
            <div class="card card-product" data-count="3">
                <div class="logo-agencia">
                  <img class="center-block img-responsive" src="<?= base_url().'img/fotos/'.$agencia->logo ?>" alt="Agencia">
                </div>
                <div class="card-content">
                    <h4 class="card-title"><b><?= $agencia->agencias_nombre ?></b></h4>
                    <div class="card-description"><p><?= @$agencia->dias ?> años trabajando para Grupo BIMBO</p></div>
                </div>
                <div class="card-footer">
                    <div class="col-xs-12 col-sm-12">
                        <div class="col-xs-4 col-sm-4 border-right text-center">
                          <div class="description-block">
                              <h5 class="description-header certificado-agencia">
                                <small class="description-text">Certificado</small>
                                <br>
                                <i class="fa fa-certificate" aria-hidden="true"></i> 
                                <b><?= @$agencia->certificacion ?></b>
                              </h5>
                          </div>
                        </div>

                        <div class="col-xs-4 col-sm-4 border-right text-center">
                          <div class="description-block">
                              <h5 class="description-header texto-rojo">
                                  <small class="description-text">Ranking</small>
                                  <br>
                                  <i class="fa fa-trophy" aria-hidden="true"></i> 
                                  <b>Excelente</b>
                              </h5>
                          </div>
                        </div>

                        <div class="col-xs-4 col-sm-4 text-center">
                          <div class="description-block">
                              <h5 class="description-header texto-verde">
                                  <small class="description-text">
                                    Estatus
                                  </small>
                                  <br>
                                  <i class="fa fa-check-circle-o" aria-hidden="true"></i> 
                                  <b><?= $agencia->status==0?'Inactiva':'Activa' ?></b>
                              </h5>
                          </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12">
                        <div class="col-xs-6 col-sm-6 iconos-favoritos-agencias">
                            <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal">
                              <button class="btn btn-primary btn-round" id="btn-enviar-brief">Enviar Brief</button>
                            </a>
                        </div>

                        <div class="col-xs-6 col-sm-6 iconos-favoritos-agencias">
                            <a title="Contactar Agencia" href="#contacto-agencia" data-toggle="modal">
                              <button class="btn btn-primary btn-round" id="btn-ver-agencia">
                                Contactar
                              </button>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
      </div>

      <div class="col-xs-12 col-sm-8 contenedor-descripcion-agencia2 margen-movil-servicios">
          <div class="col-sm-12">
              <span class="titulo-servicios"><b>Tipo de Servicios:</b></span>
          </div>

          <div class="col-sm-12 texto-gris">
                <div class="col-sm-12">
                    <div class="col-xs-4 col-sm-4">
                        <div class="card card-stats">
                            <div class="card-header" data-background-color="blue"><i class="material-icons">show_chart</i></div>
                            <div class="card-content">
                                <p class="category">Core services</p><h3 class="card-title"><span class="porcentaje-agencia texto-rojo">41%</span></h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4 col-sm-4">
                        <div class="card card-stats">
                            <div class="card-header" data-background-color="blue"><i class="material-icons">pie_chart</i></div>
                            <div class="card-content">
                                <p class="category">Core services</p><h3 class="card-title"><span class="porcentaje-agencia texto-morado">50%</span></h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4 col-sm-4">
                        <div class="card card-stats">
                            <div class="card-header" data-background-color="blue"><i class="material-icons">equalizer</i></div>
                            <div class="card-content">
                                <p class="category">Core services</p><h3 class="card-title"><span class="porcentaje-agencia texto-amarillo">5%</span></h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <span class="titulo-servicios"><b>Filtros:</b></span>
                </div>

                <div class="col-sm-12">
                      <?php foreach(explode(',',$agencia->filtros) as $e): ?>
                        <span class="label categorias-agencia"><?= $e ?></span>
                      <?php endforeach ?>                      
                </div>
            </div>
      </div>

  </div>
</div>

<div class="row bold-agencia">
  <div class="col-xs-12 col-sm-12 padding0">
      <div class="col-xs-12 col-sm-6">
          <div class="col-xs-12 col-sm-12">
              <div class="fondo-agencia-1">
                  <h1 class="titulo-agencia"><?= $agencia->agencias_nombre ?></h1>
                  <b>Fundado en:</b> <?= strftime('%d %B %y',strtotime($agencia->fundacion)) ?><br>
                  <b>Presidente:</b> <?= $agencia->presidente ?><br>
                  <b>CEO:</b> <?= @$agencia->ceo ?><br>
                  <b>Empleados:</b> <?= $agencia->empleados ?>
              </div>
          </div>

          <div class="col-xs-12 col-sm-12 padding0 img-agencias">
              <h1 class="titulo-agencia">Clientes</h1>
              <div class="col-xs-12 col-sm-12 padding0">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                      <img src="<?= base_url() ?>img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                      <img src="<?= base_url() ?>img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                      <img src="<?= base_url() ?>img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                      <img src="<?= base_url() ?>img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                  </div>
              </div>
              <div class="col-xs-12 col-sm-12 padding0">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                      <img src="<?= base_url() ?>img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                      <img src="<?= base_url() ?>img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                      <img src="<?= base_url() ?>img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                      <img src="<?= base_url() ?>img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                  </div>
              </div>
          </div>

          <div class="col-xs-12 col-sm-12 margen-calificaciones-movil">
              <div class="col-sm-12 fondo-agencia-2 text-center">
                      <div class="col-sm-6">
                          <div class="content text-center">
                              <b>Número de Calificaciones:</b><br>
                              <big>4.8/5</big>
                              <div class="calificaciones-agencia">
                                  <h3 class="card-title">
                                    <div class="iconos-calificacion-agencias">
                                        <i class="fa fa-star icono-activo" aria-hidden="true"></i>
                                        <i class="fa fa-star icono-activo" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o icono-activo" aria-hidden="true"></i>
                                        <i class="fa fa-star-o icono-inactivo" aria-hidden="true"></i>
                                        <i class="fa fa-star-o icono-inactivo" aria-hidden="true"></i>
                                    </div>
                                  </h3>
                              </div>
                              <p class="card-description">
                                  This is good if your company size is between 2 and 10 Persons.
                              </p>
                          </div>
                      </div>

                      <div class="col-sm-6">
                          <div class="content text-center">
                              <b>Número de Calificaciones:</b><br>
                              <big>4.8/5</big>
                              <div class="calificaciones-agencia2">
                                  <h3 class="card-title">
                                    <div class="iconos-calificacion-agencias">
                                        <i class="fa fa-star icono-activo" aria-hidden="true"></i>
                                        <i class="fa fa-star icono-activo" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o icono-activo" aria-hidden="true"></i>
                                        <i class="fa fa-star-o icono-inactivo" aria-hidden="true"></i>
                                        <i class="fa fa-star-o icono-inactivo" aria-hidden="true"></i>
                                    </div>
                                  </h3>
                              </div>
                              <p class="card-description">
                                  This is good if your company size is between 2 and 10 Persons.
                              </p>
                          </div>
                      </div>
              </div>
          </div>
      </div>

      <div class="col-xs-12 col-sm-6">
          <div class="col-xs-12 col-sm-12">
              <div class="fondo-agencia-1">
                  <h1 class="titulo-agencia">Datos de Contacto</h1>
                  <b>Dirección:</b> <?= $agencia->direccion ?><br>
                  <b>Sitio Web:</b> <a href="<?= $agencia->web ?>" target="blank"><?= $agencia->web ?></a><br>
                  <b>Teléfono:</b> <?= $agencia->telefono ?><br>
                  <b>Correo:</b> <?= $agencia->correo ?><br>
                  <div class="dirección-mapa">
                      <adress>Dirección:<br><?= $agencia->direccion ?></adress>
                  </div>
                  <!--Mapa -->
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3762.6247939573045!2d-99.16386104934756!3d19.428610986822218!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fed4c6178105%3A0x3ccb09543b7378a5!2sReforma+222+Centro+Financiero%2C+Ju%C3%A1rez%2C+06600+Ciudad+de+M%C3%A9xico%2C+CDMX!5e0!3m2!1ses-419!2smx!4v1517323764806" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>
          </div>
      </div>

  </div>