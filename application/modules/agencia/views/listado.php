<div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 padding0 titulo-secccion">
                          <div class="titulo-top">
                            Mostrando <span id="resultsPage">0</span> agencias
                          </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-3 col-sm-2">
                            <select class="selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7" tabindex="-98">
                            <option class="bs-title-option" value="">Organizar por:</option>
                                <option selected="">Organizar por:</option>
                                <option value="2">A-Z</option>
                                <option value="3">Otro</option>
                            </select>
                        </div>
                        <div class="col-xs-9 col-sm-10 pull-left">
                            <div class="col-xs-7 col-sm-9 col-md-9 col-lg-10">
                                <div id="wrap">
                                  <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1">
                                <div id="btn-filtros">
                                    <div id="accordion" role="tablist" aria-multiselectable="true">
                                        <div role="tab" id="headingOne">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                <i class="material-icons">filter_list</i> <small>Filtros</small>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-1 text-center">
                                <div class="icono-favoritos">
                                    <label class="fancy-checkbox" title="Mostrar sólo favoritos">
                                        <input type="checkbox" />
                                        <i class="fa fa-heart-o unchecked"></i>
                                        <i class="fa fa-heart checked"></i>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?php $this->load->view('menu-lateral-agencias');?>
                        </div>
                    </div>

                   <?= $crud->output ?>
                     <div class="row">
                          <div class="col-xs-12 col-sm-12 padding0 checkbox-radios">
                              <div class="col-sm-4 pull-right">
                                  <div class="checkbox" id="btn-seleccionar-agencias"><label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Seleccionar todas las Agencias</label></div>
                              </div>
                          </div>
                          <div class="col-xs-12 col-sm-12 padding0 checkbox-radios btn-seleccionar-agencias">
                              <div class="col-sm-2 pull-right margen-seleccionar-agencias">
                                  <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal">
                                    <button class="btn btn-primary btn-round" id="btn-enviar-brief">Enviar Brief a los seleccionados</button>
                                  </a>
                              </div>
                          </div>
                      </div>

                </div>
           </div>