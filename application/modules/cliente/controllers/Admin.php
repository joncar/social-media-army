<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        const __GRUPOID__ = 3;
        function __construct() {
            parent::__construct();
        } 
        function clientes($x = '',$y = ''){            
            $this->as['clientes'] = 'user';
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('estado_civil','enum',array('soltero'=>'Soltero','casado'=>'Casado','divorciado'=>'Divorciado','viudo'=>'Viudo'));
            $crud->field_type('sexo','enum',array('M'=>'Masculino','F'=>'Femenino'));
            $crud->field_type('password','password');
            $crud->field_type('repetir_password','password')
                 ->field_type('marcas_oficiales','tags');
            $crud->display_as('foto','Logo')
                 ->display_as('marcas_oficiales','Coloque sus marcas o productos separados por coma (,)');
            $crud->fields('nombre','apellido','email','password','puesto','razon_social','rfc','direccion','foto','marcas_oficiales');
            $crud->unset_columns('password','');
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');
            $crud->unset_print()
                 ->unset_export();            
            $crud->where('user.id',$this->user->id);
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::__GRUPOID__));            
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->columns('foto','nombre','email','status');            
            $output = $crud->render();
            
            $this->loadView($output);
        }

        function empresas_dominios($x){
            $crud = $this->crud_function($x,'');  
            $crud->where('empresas_id',$x);            
            $crud->field_type('empresas_id','hidden',$x);
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function empresas($x = '',$y = ''){
            $crud = $this->crud_function('','');  
            $crud->set_field_upload('logo','img/empresas/logos/');            
            $crud->columns('logo','nombre','email');
            $crud->callback_after_update(function($post,$primary){
                $status = $post['status']==0?0:1;                
                foreach(get_instance()->db->get_where('user_empresas',array('empresas_id'=>$primary))->result() as $a){
                    get_instance()->db->update('user',array('status'=>$status),array('id'=>$a->user_id));
                }
            });
            $output = $crud->render();
            if($crud->getParameters()=='edit'){
                $output->output = $this->load->view('empresas',array('output'=>$output->output,'valor'=>$y),TRUE,'dashboard');
                $agencias = new ajax_grocery_crud();
                $agencias = $agencias->set_table('agencias')->set_subject('agencias')->set_theme('bootstrap2')->render(1);
                $output->js_files = array_merge($output->js_files,$agencias->js_files);
                $output->css_files = array_merge($output->css_files,$agencias->css_files);
            }
            $this->loadView($output);
        }

        function empresas_user($x = ''){
            $this->as['empresas_user'] = 'user_empresas';
            $crud = $this->crud_function('','');
            $crud->set_relation('user_id','user','{nombre} {apellido} {email}');
            $crud->where('empresas_id',$x)
                 ->field_type('empresas_id','hidden',$x)
                 ->field_type('tipo','dropdown',array('1'=>'Fundador','2'=>'Administrador','3'=>'Supervisor','4'=>'Usuario'));
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
    }
?>
