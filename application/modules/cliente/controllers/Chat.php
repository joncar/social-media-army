 <?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Chat extends Panel{
        const __GRUPOID__ = 2;
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }
        function getMessage($messages){
            $limit = 100;
            $row = 0;
            $mesgs = array();            
            for($i=count($messages)-1;$i>=0;$i--){
                $row++;
                if($row<$limit){                
                    $mesgs[] = $messages[$i];
                }
            }
            return array_reverse($mesgs);
        }
        function salas($id){
            if(is_numeric($id)){
                //Validar brief
                $brief = $this->db->get_where('briefs',array('agencias_id'=>$this->user->empresa,'proyectos_id'=>$id));
                $this->db->where('getEmpresaOfProject(view_proyectos.id)',$this->user->empresa,TRUE);
                $proyectos = $this->db->get_where('view_proyectos',array('id'=>$id));
                if($brief->num_rows()>0 || $proyectos->num_rows()>0){
                    $proyecto = $this->querys->showResult($id,true);
                    $proyecto->agencias = new ajax_grocery_crud();
                    $proyecto->agencias->set_table('agencias_con_filtros');
                    $proyecto->agencias->field_type('status','true_false',array(0=>'Inactiva',1=>'Activa'))
                                       ->set_primary_key('id');
                    foreach($this->db->get_where('briefs',array('proyectos_id'=>$id))->result() as $b){
                        $proyecto->agencias->or_where('id',$b->agencias_id);
                    }
                    $proyecto->agencias->set_theme('agencias');
                    $proyecto->agencias->group_by('agencias_con_filtros.id');
                    $proyecto->agencias = $proyecto->agencias->render('','Theme/Agencia/cruds/')->output;
                    //Esta la carpeta creada?
                    if(!folder_exist('salas/'.$proyecto->id)){
                        mkdir('salas/'.$proyecto->id.'/',0777);
                        mkdir('salas/'.$proyecto->id.'/messages/',0777);
                        mkdir('salas/'.$proyecto->id.'/files/',0777);
                        file_put_contents('salas/'.$proyecto->id.'/messages/messages.json',base64_encode('[]'));
                    }
                    $messages = @json_decode(base64_decode(file_get_contents('salas/'.$proyecto->id.'/messages/messages.json')));
                    $messages = (array)$messages;
                    $messages = @$this->getMessage($messages);
                    $this->loadView(array('agencia'=>0,'messages'=>$messages,'view'=>'sala-juntas','proyecto'=>$proyecto,'brief'=>$brief->row()));
                }else{
                    throw new exception('No has sido invitado para ver este proyecto, por lo que no posees permisos para visualizarlo',403);
                }
            }
        }

        function chat($id){
            if(is_numeric($id)){
                //Validar brief
                $brief = $this->db->get_where('briefs',array('agencias_id'=>$this->user->empresa,'proyectos_id'=>$id));
                $this->db->where('getEmpresaOfProject(view_proyectos.id)',$this->user->empresa,TRUE);
                $proyectos = $this->db->get_where('view_proyectos',array('id'=>$id));
                if($brief->num_rows()>0 || $proyectos->num_rows()>0){
                    $proyecto = $this->querys->showResult($id,true);
                    $proyecto->agencias = new ajax_grocery_crud();
                    $proyecto->agencias->set_table('agencias_con_filtros');
                    $proyecto->agencias->field_type('status','true_false',array(0=>'Inactiva',1=>'Activa'))
                                       ->set_primary_key('id');
                    foreach($this->db->get_where('briefs',array('proyectos_id'=>$id))->result() as $b){
                        $proyecto->agencias->or_where('id',$b->agencias_id);
                    }
                    $proyecto->agencias->set_theme('agencias');
                    $proyecto->agencias->group_by('agencias_con_filtros.id');
                    $proyecto->agencias = $proyecto->agencias->render('','Theme/Agencia/cruds/')->output;
                    if(!file_exists('salas/'.$proyecto->id.'/messages/messages.json')){
                        file_put_contents('salas/'.$proyecto->id.'/messages/messages.json',base64_encode('[]'));
                    }
                    /*$messages = json_decode(base64_decode(file_get_contents('salas/'.$proyecto->id.'/messages/messages-'.$agencia.'.json')));*/
                    $messages = json_decode(base64_decode(file_get_contents('salas/'.$proyecto->id.'/messages/messages.json')));
                    
                    $messages = $this->getMessage($messages);
                    $result = array();
                    $result['mensajes'] = $this->load->view('Cliente/chat/_chat',array('messages'=>$messages,'proyecto'=>$proyecto,'brief'=>$brief->row()),TRUE);
                    $result['integrantes'] = $this->load->view('Cliente/chat/_integrantes',array('messages'=>$messages,'proyecto'=>$proyecto,'brief'=>$brief->row()),TRUE);
                    $result['fecha'] = empty($messages)?date("Y-m-d H:i:s"):$messages[count($messages)-1]->fecha;
                    echo json_encode($result);

                }else{
                    throw new exception('No has sido invitado para ver este proyecto, por lo que no posees permisos para visualizarlo',403);
                }
            }            
        }
        function sendMessage($id){
            if(is_numeric($id)){
                //Validar brief
                $brief = $this->db->get_where('briefs',array('agencias_id'=>$this->user->empresa,'proyectos_id'=>$id));
                $this->db->where('getEmpresaOfProject(view_proyectos.id)',$this->user->empresa,TRUE);
                $proyectos = $this->db->get_where('view_proyectos',array('id'=>$id));

                if($brief->num_rows()>0 || $proyectos->num_rows()>0){                    
                    $integrante = $this->db->get_where('view_integrantes_chat',array('proyectos_id'=>$id,'id'=>$this->user->id));
                    if($integrante->num_rows()>0){                        
                        $integrante = (array)$integrante->row();
                        $integrante['mensaje'] = $_POST['mensaje'];
                        $integrante['fichero'] = '';
                        $integrante['fecha'] = date("Y-m-d H:i:s");
                        $messages = json_decode(base64_decode(file_get_contents('salas/'.$id.'/messages/messages.json')));
                        $messages[] = $integrante;
                        $messages = json_encode($messages);
                        file_put_contents('salas/'.$id.'/messages/messages.json',base64_encode($messages));
                        //Send push
                        $this->db->select('agencias_user.*');
                        $this->db->join('agencias_user','agencias_id = view_integrantes_chat.id');
                        $integrantes = $this->db->get_where('view_integrantes_chat',array('proyectos_id'=>$id,'tipo'=>2));
                        foreach($integrantes->result() as $a){
                            $this->querys->sendPush($a->user_id,'Has recibido un mensaje por el chat: '.$_POST['mensaje'],base_url('agencia/chat/salas/'.$id),false,$proyectos->row()->nombre);
                        }
                    }
                }
            }      
        }

        function del($proyecto){
            if(is_numeric($proyecto) && !empty($_POST['id'])){
                //Validar brief
                $brief = $this->db->get_where('briefs',array('agencias_id'=>$this->user->empresa,'proyectos_id'=>$proyecto));
                $this->db->where('getEmpresaOfProject(view_proyectos.id)',$this->user->empresa,TRUE);
                $proyectos = $this->db->get_where('view_proyectos',array('id'=>$proyecto));

                if($brief->num_rows()>0 || $proyectos->num_rows()>0){                    
                    $integrante = $this->db->get_where('view_integrantes_chat',array('proyectos_id'=>$proyecto,'id'=>$this->user->id));
                    if($integrante->num_rows()>0){                        
                        $integrante = (array)$integrante->row();
                        $messages = json_decode(base64_decode(file_get_contents('salas/'.$proyecto.'/messages/messages.json')));                        
                        unset($messages[$_POST['id']]);
                        $messages = array_values($messages);                        
                        $messages = json_encode($messages);
                        file_put_contents('salas/'.$proyecto.'/messages/messages.json',base64_encode($messages));                        
                    }
                }
            }     
        }

        function ss(){
            $msj = 'W3siaWQiOiIxNTQiLCJ1bHRpbWFfY29uZXhpb24iOiIyMDIwLTA2LTIzIDA1OjA4OjAzIiwic3RhdHVzIjoiMCIsInN0YXR1c19icmllZiI6IjIiLCJub21icmUiOiJJZGVhcyBBZ2l0YWRhcyIsInRpcG8iOiIyIiwibG9nbyI6ImRlMzk0LWlkZWFzX2FnaXRhZGFzLnBuZyIsInByb3llY3Rvc19pZCI6IjMxIiwibWVuc2FqZSI6Ik9zIGhhY2Vtb3MgbGxlZ2FyIHVuYXMgcHJlZ3VudGFzOiAiLCJmaWNoZXJvIjoiIiwiZmVjaGEiOiIyMDIwLTA2LTIzIDA1OjA4OjAzIn0seyJpZCI6IjE1NCIsInVsdGltYV9jb25leGlvbiI6IjIwMjAtMDYtMjMgMDU6MDg6MTQiLCJzdGF0dXMiOiIwIiwic3RhdHVzX2JyaWVmIjoiMiIsIm5vbWJyZSI6IklkZWFzIEFnaXRhZGFzIiwidGlwbyI6IjIiLCJsb2dvIjoiZGUzOTQtaWRlYXNfYWdpdGFkYXMucG5nIiwicHJveWVjdG9zX2lkIjoiMzEiLCJtZW5zYWplIjoiT3MgaGFjZW1vcyBsbGVnYXIgdW5hcyBwcmVndW50YXM6ICIsImZpY2hlcm8iOiIiLCJmZWNoYSI6IjIwMjAtMDYtMjMgMDU6MDg6MTQifSx7ImlkIjoiMTU0IiwidWx0aW1hX2NvbmV4aW9uIjoiMjAyMC0wNi0yMyAwNTowODoyMiIsInN0YXR1cyI6IjAiLCJzdGF0dXNfYnJpZWYiOiIyIiwibm9tYnJlIjoiSWRlYXMgQWdpdGFkYXMiLCJ0aXBvIjoiMiIsImxvZ28iOiJkZTM5NC1pZGVhc19hZ2l0YWRhcy5wbmciLCJwcm95ZWN0b3NfaWQiOiIzMSIsIm1lbnNhamUiOiJPcyBoYWNlbW9zIGxsZWdhciB1bmFzIHByZWd1bnRhczogIiwiZmljaGVybyI6IiIsImZlY2hhIjoiMjAyMC0wNi0yMyAwNTowODoyMiJ9LHsiaWQiOiIxNTQiLCJ1bHRpbWFfY29uZXhpb24iOiIyMDIwLTA2LTIzIDA1OjA4OjQxIiwic3RhdHVzIjoiMCIsInN0YXR1c19icmllZiI6IjIiLCJub21icmUiOiJJZGVhcyBBZ2l0YWRhcyIsInRpcG8iOiIyIiwibG9nbyI6ImRlMzk0LWlkZWFzX2FnaXRhZGFzLnBuZyIsInByb3llY3Rvc19pZCI6IjMxIiwibWVuc2FqZSI6IktwaXM6IFx1MjAyMlx0XHUwMGJmQ1x1MDBmM21vIHNlIG1pZGUgZWwgXHUwMGU5eGl0byBkZSBlc3RlIHByb3llY3RvPyBcdTIwMjJcdFByZXN1cHVlc3RvIFx1MjAyMlx0SW5jbHVpciBkaXZpc2lcdTAwZjNuIHByb2R1Y2NpXHUwMGYzbiBcLyBtZWRpb3Mgc2kgc2Ugc2FiZS4gXHUyMDIyXHRcdTAwYmZRdVx1MDBlOSBxdWVyZW1vcyBxdWUgaGFnYSBsYSBhdWRpZW5jaWE/ICBcdTIwMjJcdFx1MDBiZlF1XHUwMGU5IGNhbWJpbyBxdWVyZW1vcyBwcm92b2NhciBlbiBsYSBhdWRpZW5jaWE/ICBcdTIwMjJcdFx1MDBiZkN1XHUwMGUxbCBlcyBsYSBtZWRpZGEgZGUgbGEgZnJlY3VlbmNpYSA0LDc/IFx1MjAyMlx0RWwgdGFyZ2V0IGZpbmFsbWVudGUgZXMgKzM1IG8gMjUtNTUgYVx1MDBmMW9zIFx1MjAyMlx0UGFja2FnaW5nOiBzZSBwdWVkZSBtb2RpZmljYXIgbyBzb2xvIGEgbW9kbyBkZSBlZGljaVx1MDBmM24gbGltaXRhZGEiLCJmaWNoZXJvIjoiIiwiZmVjaGEiOiIyMDIwLTA2LTIzIDA1OjA4OjQxIn0seyJpZCI6IjE2MiIsInVsdGltYV9jb25leGlvbiI6IjIwMjAtMDYtMjQgMTk6MTM6MzMiLCJzdGF0dXMiOiIwIiwic3RhdHVzX2JyaWVmIjoiMiIsIm5vbWJyZSI6IkplcnJ5IiwidGlwbyI6IjEiLCJsb2dvIjoiNzg3YmUtZ3J1cG8tYmltYm8tbG9nby5wbmciLCJwcm95ZWN0b3NfaWQiOiIzMSIsIm1lbnNhamUiOiJFcXVpcG8gSWRlYXMgQWdpdGFkYXM6IFx1MDBiZlF1XHUwMGU5IHF1ZXJlbW9zIHF1ZSBoYWdhIGxhIGF1ZGllbmNpYT8gUXVlIHRlbmdhIGEgRG9uZXR0ZXMgZW4gc3UgVE9NIHkgbG8gdnVlbHZhIFRyZW5kaW5nIFRvcGljIChTbGlkZSAzMSBkZWwgYnJpZWYpIFwvXC8gXHUwMGJmUXVcdTAwZTkgY2FtYmlvIHF1ZXJlbW9zIHByb3ZvY2FyIGVuIGxhIGF1ZGllbmNpYT8gR2VuZXJhciBlbCBcImJ1ZW4gcm9sbG9cIiB5IGxhIGFtaXN0YWQgYSB0cmF2XHUwMGU5cyBkZWwgZGlzZnJ1dGUgeSBsYSBwb3RlbmNpYSBkZSBjb21wYXJ0aXIgKFNsaWRlIDI5IGRlbCBicmllZikgXC9cLyBUYXJnZXQ6IDE1LTI1IHRyYW5zdmVyc2FsIGhhc3RhIDQwIChzbGlkZSAzOSBkZWwgYnJpZWYpIFwvXC8gTGFzIHByZWd1bnRhcyBwZW5kaWVudGVzIGRlIHJlc3B1ZXN0YSBzZSBlbmN1ZW50cmFuIGVuIHJldmlzaVx1MDBmM24uICAgICIsImZpY2hlcm8iOiIiLCJmZWNoYSI6IjIwMjAtMDYtMjQgMTk6MTM6MzMifSx7ImlkIjoiMTU0IiwidWx0aW1hX2NvbmV4aW9uIjoiMjAyMC0wNi0yNSAwNDoyNDoyNSIsInN0YXR1cyI6IjAiLCJzdGF0dXNfYnJpZWYiOiIyIiwibm9tYnJlIjoiSWRlYXMgQWdpdGFkYXMiLCJ0aXBvIjoiMiIsImxvZ28iOiJkZTM5NC1pZGVhc19hZ2l0YWRhcy5wbmciLCJwcm95ZWN0b3NfaWQiOiIzMSIsIm1lbnNhamUiOiJNdWNoYXMgZ3JhY2lhcyBwb3IgbGFzIHJlc3B1ZXN0YXMuIEVzdGFtb3MgaGFibGFuZG8gZGUgRG9udXRzLCBubyBkZSBEb25ldHRlcywgZXMgYXNcdTAwZWQgdmVyZGFkPyBFbiBlbCB0YXJnZXQgbm9zIGhhYmxhbiBkZSBsYSBzbGlkZSAzOSBwZXJvIGVsIGRvY3VtZW50byB0aWVuZSAzNyBwXHUwMGUxZ2luYXMuIGdyYWNpYXMgcG9yIGxhcyBhY2xhcmFjaW9uZXMuIiwiZmljaGVybyI6IiIsImZlY2hhIjoiMjAyMC0wNi0yNSAwNDoyNDoyNSJ9LHsiaWQiOiIxNTQiLCJ1bHRpbWFfY29uZXhpb24iOiIyMDIwLTA2LTI1IDA0OjI0OjM0Iiwic3RhdHVzIjoiMCIsInN0YXR1c19icmllZiI6IjIiLCJub21icmUiOiJJZGVhcyBBZ2l0YWRhcyIsInRpcG8iOiIyIiwibG9nbyI6ImRlMzk0LWlkZWFzX2FnaXRhZGFzLnBuZyIsInByb3llY3Rvc19pZCI6IjMxIiwibWVuc2FqZSI6Ik11Y2hhcyBncmFjaWFzIHBvciBsYXMgcmVzcHVlc3Rhcy4gRXN0YW1vcyBoYWJsYW5kbyBkZSBEb251dHMsIG5vIGRlIERvbmV0dGVzLCBlcyBhc1x1MDBlZCB2ZXJkYWQ/IEVuIGVsIHRhcmdldCBub3MgaGFibGFuIGRlIGxhIHNsaWRlIDM5IHBlcm8gZWwgZG9jdW1lbnRvIHRpZW5lIDM3IHBcdTAwZTFnaW5hcy4gZ3JhY2lhcyBwb3IgbGFzIGFjbGFyYWNpb25lcy4iLCJmaWNoZXJvIjoiIiwiZmVjaGEiOiIyMDIwLTA2LTI1IDA0OjI0OjM0In1d';
            $msj = base64_decode($msj);
            //echo $msj;
            echo base64_encode('[{"id":"154","ultima_conexion":"2020-06-23 05:08:03","status":"0","status_brief":"2","nombre":"Ideas Agitadas","tipo":"2","logo":"de394-ideas_agitadas.png","proyectos_id":"31","mensaje":"Os hacemos llegar unas preguntas: ","fichero":"","fecha":"2020-06-23 05:08:03"},{"id":"154","ultima_conexion":"2020-06-23 05:08:14","status":"0","status_brief":"2","nombre":"Ideas Agitadas","tipo":"2","logo":"de394-ideas_agitadas.png","proyectos_id":"31","mensaje":"Os hacemos llegar unas preguntas: ","fichero":"","fecha":"2020-06-23 05:08:14"},{"id":"154","ultima_conexion":"2020-06-23 05:08:22","status":"0","status_brief":"2","nombre":"Ideas Agitadas","tipo":"2","logo":"de394-ideas_agitadas.png","proyectos_id":"31","mensaje":"Os hacemos llegar unas preguntas: ","fichero":"","fecha":"2020-06-23 05:08:22"},{"id":"154","ultima_conexion":"2020-06-23 05:08:41","status":"0","status_brief":"2","nombre":"Ideas Agitadas","tipo":"2","logo":"de394-ideas_agitadas.png","proyectos_id":"31","mensaje":"Kpis: \u2022\t\u00bfC\u00f3mo se mide el \u00e9xito de este proyecto? \u2022\tPresupuesto \u2022\tIncluir divisi\u00f3n producci\u00f3n \/ medios si se sabe. \u2022\t\u00bfQu\u00e9 queremos que haga la audiencia? \u2022\t\u00bfQu\u00e9 cambio queremos provocar en la audiencia? \u2022\t\u00bfCu\u00e1l es la medida de la frecuencia 4,7? \u2022\tEl target finalmente es +35 o 25-55 a\u00f1os \u2022\tPackaging: se puede modificar o solo a modo de edici\u00f3n limitada","fichero":"","fecha":"2020-06-23 05:08:41"},{"id":"154","ultima_conexion":"2020-06-25 04:24:25","status":"0","status_brief":"2","nombre":"Ideas Agitadas","tipo":"2","logo":"de394-ideas_agitadas.png","proyectos_id":"31","mensaje":"Muchas gracias por las respuestas. Estamos hablando de Donuts, no de Donettes, es as\u00ed verdad? En el target nos hablan de la slide 39 pero el documento tiene 37 p\u00e1ginas. gracias por las aclaraciones.","fichero":"","fecha":"2020-06-25 04:24:25"},{"id":"154","ultima_conexion":"2020-06-25 04:24:34","status":"0","status_brief":"2","nombre":"Ideas Agitadas","tipo":"2","logo":"de394-ideas_agitadas.png","proyectos_id":"31","mensaje":"Muchas gracias por las respuestas. Estamos hablando de Donuts, no de Donettes, es as\u00ed verdad? En el target nos hablan de la slide 39 pero el documento tiene 37 p\u00e1ginas. gracias por las aclaraciones.","fichero":"","fecha":"2020-06-25 04:24:34"}]');
            
            //echo $msj;
        }
}
