<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Agencia extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }
        function agencias($x = '',$y = ''){
            if(!empty($x) && !empty($y)){
                if($x=='brief' && is_numeric($y)){
                    $brief = $this->querys->showResult($y,TRUE);
                    //print_r($_POST['brief']);
                    //die();
                    $_GET['pais'] = $brief->paises_res->row()->id;
                    $_GET['servicios'] = array();
                    /*foreach($brief->filtros_res->result() as $s){
                        $_GET['servicios'][] = $s->id;
                    }*/
                }else{
                    $_POST[$x] = $y;                
                }
            }
            $this->as['agencias'] = 'agencias_con_filtros';
            $crud = $this->crud_function($x,$y);            
            $crud->set_theme('agencias');   
            $crud->unset_jquery();       
            $crud->field_type('fecha_ingreso','hidden',date("Y-m-d H:i:s"))
                 ->field_type('mapa','map',array('width'=>'300px','height'=>'300px'));        
            //SEARCHS
            $crud->set_primary_key('id');            
            if(is_numeric($x)){                
                get_instance()->filtros = @$this->querys->showResult($x,true);
            }else{
                get_instance()->filtros = null;
            }
            /*** FILTRAR BUSQUEDA ***/
            $where = 'agencias_con_filtros.status = 1 ';
            if(!empty($_POST['favorito'])){
                $where.= 'AND has_fav(agencias_con_filtros.id,'.$_POST['favorito'].',1) = 1';
            }
            if(!empty($_POST['filtros'])){
                foreach($_POST['filtros'] as $n=>$v){
                    $where.= 'AND (';
                    foreach($v as $nn=>$vv){
                        if($n=='servicios'){
                            if(!empty($vv)){
                                $where.= ' OR has_service(agencias_con_filtros.id,'.$vv.',1) = 1';
                            }
                        }elseif($n=='categorias_agencias'){
                            $where.= ' OR has_categoria(agencias_con_filtros.id,'.$vv.',1) = 1';
                        }elseif($n=='marca'){
                            $where.= ' OR has_marca(agencias_con_filtros.id,"'.urldecode($vv).'") = 1';
                        }elseif($n=='paises'){                            
                            if(is_numeric($vv)){                                
                                $where.= " OR agencias_con_filtros.paises_id ='".$vv."'";
                            }
                        }elseif($n=='cobertura'){                            
                            if(is_numeric($vv)){
                                $pais = $this->db->get_where('paises',array('id'=>$vv))->row()->nombre;
                                $where.= " OR agencias_con_filtros.paises like '%".$pais."%'";
                            }
                        }else{
                            $where.= ' OR '.$n.' = \''.$vv.'\'';
                        }                        
                    }
                    $where.= ')';
                }
            }
            $where = str_replace('( OR ','(',$where);
            $where = str_replace(' AND ()','',$where);
            $crud->where($where,'ESCAPE',FALSE);
            /* END FILTROS */
            /* ORDENAR*/
            if(!empty($_POST['ordenar'])){
                $ordenar = explode('-',$_POST['ordenar']);                
                if(count($ordenar)==2){
                    $crud->order_by($ordenar[0],$ordenar[1]);
                }
            }
            /* END ORDENAR */

            $output = $crud->render('','Theme/Cliente/cruds/');
            $output->view = 'agencias';
            $output->title = "Agencias";
            $output->filtros = get_instance()->filtros;        
            $this->loadView($output);
        }



        function verAgencia($id){
            if(is_numeric($id)){
                $agencia = $this->querys->getAgencia($id);
                if(!empty($agencia)){
                    $this->loadView(array('view'=>'agencia','agencia'=>$agencia,'title'=>$agencia->nombre));                                    
                }else{
                    throw new exception('La agencia a consultar no se encuentra disponible',404);
                }
            }
        }

        function favoritos($id){
            if(is_numeric($id)){
                $favorito = $this->db->get_where('agencias_favoritas',array('agencias_id'=>$id,'user_id'=>$this->user->id));
                if($favorito->num_rows()>0){
                    $this->db->delete('agencias_favoritas',array('agencias_id'=>$id,'user_id'=>$this->user->id));
                }else{
                    $this->db->insert('agencias_favoritas',array('agencias_id'=>$id,'user_id'=>$this->user->id));
                }
            }
        }

        function rankear(){
            $this->form_validation->set_rules('agencias_id','Agencia','required');
            $this->form_validation->set_rules('proyectos_id','Proyecto','required');
            $this->form_validation->set_rules('tipo','Tipo ranking','required');
            $this->form_validation->set_rules('value','Valor','required');
            if($this->form_validation->run()){
                $ranking = $this->db->get_where('agencias_ranking',array('agencias_id'=>$_POST['agencias_id'],'proyectos_id'=>$_POST['proyectos_id'],'user_id'=>$this->user->id));
                if($ranking->num_rows()==0){
                    $this->db->insert('agencias_ranking',array('agencias_id'=>$_POST['agencias_id'],'proyectos_id'=>$_POST['proyectos_id'],'user_id'=>$this->user->id));                    
                }
                $this->db->update('agencias_ranking',array($_POST['tipo']=>$_POST['value']),array('agencias_id'=>$_POST['agencias_id'],'proyectos_id'=>$_POST['proyectos_id']));
            }else{
                echo json_encode(array('status'=>'fail'));
            }
        }

        function resenar(){
            $this->form_validation->set_rules('agencias_id','Agencia','required');            
            $this->form_validation->set_rules('value','Valor','required');
            if($this->form_validation->run()){                
                $this->db->insert('agencias_resenas',array('agencias_id'=>$_POST['agencias_id'],'value'=>$_POST['value'],'fecha'=>date("Y-m-d H:i:s"),'user_id'=>$this->user->id));                    
            }else{
                echo json_encode(array('status'=>'fail'));
            }
        }

        function calificar(){
            if(!empty($_POST)){
                if(is_numeric($_POST['rank']['agencias_id'])){
                    $id = $_POST['rank']['agencias_id'];
                    $data = array('calificaciones'=>array());
                    $promedio = 0;
                    $calificaciones = 0;
                    foreach($_POST['rank']['values'] as $n=>$p){
                        $data['calificaciones'][] = array($p,$_POST['rank']['tags'][$n]);
                        $calificaciones++;
                        $promedio+= $p;
                    }
                    $promedio = round($promedio/$calificaciones,0);
                    $data['observaciones'] = $_POST['rank']['observaciones'];
                    $this->db->update('briefs',array('calificacion'=>$promedio,'calificacion_data'=>json_encode($data)),array('id'=>$id));
                    echo $this->success('Datos almacenados con éxito');
                }                
            }
        }
        
    }
?>
