<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Cliente extends Panel{
        const __GRUPOID__ = 3;
        function __construct() {
            parent::__construct();
        }
        function ajustes(){
            $cruds[0] = $this->usuarios(1);
            $js = $cruds[0]->js_files;
            $css = $cruds[0]->css_files;
            $cruds[1] = $this->productos(1);
            $cruds[2] = $this->invitaciones(1);
            $cruds[3] = $this->marcas(1);
            $empresa = $this->db->get_where('empresas',array('id'=>$this->user->empresa));
            if($empresa->num_rows()>0){
                $this->loadView(array('view'=>'ajustes','js_files'=>$js,'css_files'=>$css,'cruds'=>$cruds,'empresa'=>$empresa->row()));
            }else{
                throw new Exception('Usted no posee una empresa asignada',404);
            }
        }
        
        function empresas(){
            $crud = $this->crud_function('','');    
            $crud->set_field_upload('logo','img/empresas/logos/');
            $crud->unset_delete()->unset_read()->unset_print()->unset_add()->unset_export()->unset_list();
            $crud->callback_after_update(function($post,$primary){
                $_SESSION['bandera'] = base_url().'img/Banderas/'.$this->db->get_where('paises',array('id'=>$post['paises_id']))->row()->icono;
                $_SESSION['logo'] = empty($post['logo'])?base_url().'Theme/Cliente/assets/img/login/subir-logo.png':'img/empresas/logos/'.$post['logo'];
            });
            $crud = $crud->render();            
        }

        function usuarios($return = 1,$y = ''){
            $crud = new ajax_grocery_crud();
            $crud->unset_jquery();      
            $crud->set_subject('Usuarios');
            $crud->set_theme('generic');                          
            
            if($crud->getParameters()=='list'){
                $crud->set_table('user_empresas')
                     ->where('empresas_id',$this->user->empresa)
                     ->where('user_id !=',$this->user->id)
                     ->where('tipo !=',1)
                     ->where('tipo !=',2);   
                $crud->set_relation('user_id','user','{nombre}|{apellido}|{email}');
                $crud->columns('je8701ad4.email','tipo','status');
                $crud->callback_column('je8701ad4.nombre',function($val,$row){return explode('|',$row->se8701ad4)[0];});
                $crud->callback_column('je8701ad4.apellido',function($val,$row){return explode('|',$row->se8701ad4)[1];});
                $crud->callback_column('je8701ad4.email',function($val,$row){return explode('|',$row->se8701ad4)[2];});
                $crud->callback_column('status',function($val,$row){
                    return $this->db->get_where('user',array('id'=>$row->user_id))->row()->status==1?'Activo':'Bloqueado';
                });
                $crud->field_type('tipo','dropdown',array('2'=>'Administrador','3'=>'Supervisor','4'=>'Colaborador'));
                $crud->display_as('je8701ad4.nombre','Nombre')
                         ->display_as('je8701ad4.apellido','Apellido')
                         ->display_as('je8701ad4.email','Email');
                $crud->unset_read();
                $crud->set_primary_key('user_id');
            }else{
                $crud->set_table('user');
                $crud->field_type('fecha_registro','hidden',date("Y-m-d"))
                     ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                     ->field_type('status','hidden',1)
                     ->field_type('foto','invisible')
                     ->field_type('tipo_usuario','hidden','Cliente') 
                     ->field_type('admin','hidden',0) 
                     ->field_type('nombre','hidden','default') 
                     ->field_type('apellido','hidden','default') 
                     ->field_type('direccion','hidden','default')
                     ->field_type('puesto','hidden','default')
                     ->field_type('ultima_conexion','hidden',date("Y-m-d"))
                     ->field_type('fecha_registro','hidden',date("Y-m-d"))
                     ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                     ->field_type('password','hidden','12345678')
                     ->field_type('tipo','dropdown',array('2'=>'Administrador','3'=>'Supervisor','4'=>'Colaborador'));                
                if($crud->getParameters()=='add'){
                    $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]|callback_validar_email');                    
                }elseif($crud->getParameters()!='edit'){
                    $crud->set_rules('email','Email','required|valid_email|callback_validar_email');                    
                }
                if($crud->getParameters(FALSE)=='add'){
                    $crud->fields('email','tipo','fecha_registro','fecha_actualizacion','status','foto','tipo_usuario','admin','nombre','apellido','direccion','puesto','ultima_conexion','fecha_registro','fecha_actualizacion','password');
                }
                elseif($crud->getParameters(FALSE)=='edit'){
                    get_instance()->usuario = $this->db->get_where('user',array('id'=>$y))->row();
                    $crud->callback_field('tipo',function($val){
                        $tipo = $this->db->get_where('user_empresas',array('user_id'=>get_instance()->usuario->id,'empresas_id'=>get_instance()->user->empresa))->row()->tipo;                        
                        return form_dropdown('tipo',array('2'=>'Administrador','3'=>'Supervisor','4'=>'Colaborador'),$tipo,'id="field-status" class="form-control"');
                    });
                    $crud->edit_fields('status','tipo')
                         ->field_type('status','dropdown',array('0'=>'Bloqueado','1'=>'Activo'));         
                }else{
                    $crud->edit_fields('status')
                         ->field_type('status','dropdown',array('0'=>'Bloqueado','1'=>'Activo'));         
                }
                $crud->callback_before_insert(function($post){
                    $post['password'] = md5($post['password']);                
                    return $post;
                });
                $crud->callback_after_insert(function($post,$primary){                                        
                    get_instance()->db->insert('user_empresas',array('user_id'=>$primary,'empresas_id'=>get_instance()->user->empresa,'tipo'=>$_POST['tipo']));
                    $post['link'] = base_url('registro/validar/Cliente/'.base64_encode($primary));
                    get_instance()->enviarcorreo((object)$post,8);
                });
                $crud->callback_before_update(function($post,$primary){
                    if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password']){
                        $post['password'] = md5($post['password']);
                    }
                    $this->db->update('user_empresas',array('tipo'=>$_POST['tipo']),array('user_id'=>$primary,'empresas_id'=>get_instance()->user->empresa));
                    return $post;
                });
                $crud->callback_after_delete(function($primary){
                    get_instance()->db->delete('user_empresas',array('user_id'=>$primary));
                });
            }
            $crud->required_fields_array();
            $crud->set_url('cliente/usuarios/');
            $crud->unset_back_to_list();   
            
            $crud = $crud->render('','Theme/Cliente/cruds/');           
            if($return==1){
                return $crud;
            }else{
                $crud->view = 'crud';
                $this->loadView($crud);
            }            
        } 

        function validar_email($email){
            list($name,$dominio) = explode('@',$email);
            if($this->db->get_where('empresas_dominios',array('nombre'=>$dominio,'empresas_id'=>$this->user->empresa))->num_rows()==0){
                $this->form_validation->set_message('validar_email','Correo eletcrónico no autorizado, pongase en contacto con un administrador para añadirlo al sistema.');
                return false;
            }
            return true;
        }
        
        function marcas($return = 1){
            $crud = new ajax_grocery_crud();
            $crud->set_table('marcas');
            $crud->set_subject('Mis marcas');
            $crud->set_theme('generic');
            $crud->unset_jquery();
            $crud->required_fields_array();
            $crud->columns('id','imagen','nombre','s7251cec0')
                 ->display_as('s7251cec0','País');
            $crud->field_type('empresas_id','hidden',$this->user->empresa);
            $crud->set_field_upload('imagen','img/marcas');
            $crud->set_url('cliente/marcas/');            
            $crud->unset_back_to_list();
            $crud->unset_delete()
                 ->unset_read();
            if(isset($_POST['paises'])){                
                $crud->display_as('nombre','nombre');
                foreach($_POST['paises'] as $p){
                    $crud->or_where('(paises_id = '.$p.' AND empresas_id = '.$this->user->empresa.')','ESCAPE');
                }
                $crud->limit(1000);
            }else{
                $crud->where('empresas_id',$this->user->empresa);
            }            
            $crud = $crud->render('','Theme/Cliente/cruds/');                
            if($return==1){
                return $crud;
            }else{
                $crud->view = 'crud';
                $this->loadView($crud);
            }            
        }

        function productos($return = 1){
            $crud = new ajax_grocery_crud();
            $crud->set_table('productos');
            $crud->set_subject('Mis productos');
            $crud->set_theme('generic');
            $crud->unset_jquery();
            $crud->required_fields_array();
            $crud->set_relation('marcas_id','marcas','nombre',array('empresas_id'=>$this->user->empresa));
            if(isset($_POST['marcas'])){    
                $crud->display_as('nombre','nombre');            
                foreach($_POST['marcas'] as $p){
                    $crud->or_where('(marcas_id = '.$p.' AND empresas_id = '.$this->user->empresa.')','ESCAPE');
                }
                $crud->columns('id','imagen','nombre','s280d925f');    
                $crud->limit(1000);                
            }else{
                $crud->columns('imagen','nombre');   
                $crud->where('empresas_id',$this->user->empresa);
            }  
            $crud->callback_column('s280d925f',function($val,$row){
                $this->db->join('paises','paises.id = marcas.paises_id')
                         ->select('paises.*');
                $pais = $this->db->get_where('marcas',array('marcas.id'=>$row->marcas_id))->row()->nombre;
                return $val.'-'.$pais;
            });
            $crud->display_as('productos.nombre','producto');
            $crud->field_type('empresas_id','hidden',$this->user->empresa);
            $crud->set_field_upload('imagen','img/productos');
            $crud->set_url('cliente/productos/');
            $crud->display_as('marcas_id','Marca');
            $crud->callback_field('marcas_id',function($val,$row){
                return form_dropdown_from_query('marcas_id','marcas','id','nombre',$val,'',false,'selectpicker');
            });
            $crud->unset_back_to_list();
            $crud->unset_delete()
                 ->unset_read();
            $crud = $crud->render('','Theme/Cliente/cruds/');                
            if($return==1){
                return $crud;
            }else{
                $crud->view = 'crud';
                $this->loadView($crud);
            }            
        }

        function invitaciones($return = 1){
            $crud = new ajax_grocery_crud();
            $crud->set_table('invitaciones');
            $crud->set_subject('Mis invitaciones');
            $crud->set_theme('generic2');
            $crud->unset_jquery();
            $crud->required_fields_array();
            $crud->columns('email');   
            $crud->where('empresas_id',$this->user->empresa);
            $crud->field_type('empresas_id','hidden',$this->user->empresa)
                 ->field_type('status','hidden',1)
                 ->field_type('nombre','hidden','default');
            $crud->unset_edit();
            $crud->callback_after_insert(function($post,$primary){
                $datos = $post;
                $datos['link'] = base_url('registro/agencia/add').'?xys1='.base64_encode($primary);
                //get_instance()->enviarcorreo((object)$datos,7,'',$datos['link']);
                get_instance()->enviarcorreo((object)$datos,9,'',$datos['link']);
            });
            $crud->set_rules('email','Email','required|valid_email|is_unique[invitaciones.email]');
            $crud->set_url('cliente/invitaciones/');
            $crud->unset_back_to_list();
            $crud = $crud->render('','Theme/Cliente/cruds/');                
            if(is_numeric($return) && $return==1){
                return $crud;
            }else{
                $crud->view = 'crud';
                $this->loadView($crud);
            }            
        }

        function editar_empresa(){
            $this->loadView('editar-empresa');
        }

        function facturacion(){
            $this->loadView('facturacion');
        }

        function buscar(){
            if(!empty($_GET['q']))
            {
                $this->db->where('(empresa = 0 OR empresa = '.$this->user->empresa.')',NULL,TRUE);                
                $this->db->like('nombre',$_GET['q']);
                $result = $this->db->get_where('view_buscador');
                $res = array();
                foreach($result->result() as $r){
                    $r->link = base_url().'cliente/'.$r->tipo.'/'.urlencode($r->id);
                    $res[] = $r;
                }
                echo json_encode($res);
            }
        }
        
    }
?>
