 <?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Proyecto extends Panel{
        const __GRUPOID__ = 2;
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }

        function proyectos($x = '',$y = ''){
            if(empty($x) || $x=='list' || $x=='ajax_list' || $x=='ajax_list_info'){
                //$this->as['proyectos'] = 'view_proyectos';
            }
            $crud = $this->crud_function($x,$y); 
            if($crud->getParameters()=='list'){
                //$crud->where('empresa_id',$this->user->empresa);
                //$crud->set_primary_key('id');
            }      
            $crud->callback_before_insert(function($post){
                $post['fecha_registro'] = date("Y-m-d H:i:s");
                $post['nombre'] = mb_strtoupper($post['nombre']);
                $post['creador'] = get_instance()->user->id;
                return $post;
            });  
            $crud->callback_before_update(function($post,$primary){
                $post['nombre'] = mb_strtoupper($post['nombre']);                
                return $post;
            });    
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->saveExtraFiles($post,$primary);
            });
            if($crud->getParameters()=='edit' && empty($_POST['status'])){
                $crud->callback_before_update(function($post,$primary){                
                    $proyecto = get_instance()->showResult($primary,TRUE);
                    $this->db->delete('proyectos_user',array('proyectos_id'=>$primary));                    
                    $this->db->delete('proyectos_fechas',array('proyectos_id'=>$primary));
                    $post['fecha_modificacion'] = date("Y-m-d H:i:s");
                    return $post;
                });
                $crud->callback_after_update(function($post,$primary){
                    $post['creador'] = get_instance()->db->get_where('proyectos',array('id'=>$primary))->row()->creador;                    
                    get_instance()->saveExtraFiles($post,$primary,FALSE);
                    return $post;
                });                
            }elseif(!empty($_POST['status'])){
                $crud->callback_after_update(function($post,$primary){                    
                    if(!empty($post['status']) && ($post['status']==1 || $post['status']==2)){
                        if($post['status']==2){
                            $idnotif = 16;                            
                        }
                        if($post['status']==1){
                            $idnotif = 18;
                            $enlace = '';
                        }
                        $this->db->select('user.*, proyectos.nombre as proyecto, proyectos.motivo_cancelacion');
                        $this->db->join('agencias_user','agencias_user.agencias_id = briefs.agencias_id');
                        $this->db->join('user','agencias_user.user_id = user.id');
                        $this->db->join('proyectos','proyectos.id = briefs.proyectos_id');
                        foreach($this->db->get_where('briefs',array('proyectos_id'=>$primary))->result() as $p){
                            $p->link = base_url('agencia/proyecto/verProyecto/'.$primary);
                            $p->enlace = base_url('proyectos/frontend/aprobar/x2/'.base64_encode($p->id).'/'.base64_encode($primary));
                            $p->razon = $p->motivo_cancelacion;
                            get_instance()->enviarcorreo((object)$p,$idnotif,'',$p->link);
                        }
                    }
                });      
            }

            $crud->callback_after_delete(function($primary){
                get_instance()->db->delete('proyectos_fechas',array('proyectos_id'=>$primary));
                get_instance()->db->delete('proyectos_categorias_proyectos',array('proyectos_id'=>$primary));
                get_instance()->db->delete('proyectos_servicios',array('proyectos_id'=>$primary));
                get_instance()->db->delete('proyectos_documentos',array('proyectos_id'=>$primary));                
                get_instance()->db->delete('proyectos_fechas',array('proyectos_id'=>$primary));
                get_instance()->db->delete('proyectos_marcas',array('proyectos_id'=>$primary));
                get_instance()->db->delete('proyectos_paises',array('proyectos_id'=>$primary));
                get_instance()->db->delete('proyectos_productos',array('proyectos_id'=>$primary));
                get_instance()->db->delete('proyectos_user',array('proyectos_id'=>$primary));                
            });

            if($crud->getParameters()=='edit' && !empty($_POST['status'])){
                $crud->required_fields('status');
            }
            $crud->set_field_upload('imagen','img/proyectos_uploads/img/');
            $crud->set_relation_n_n('paises','proyectos_paises','paises','proyectos_id','paises_id','nombre');
            $crud->set_relation_n_n('marcas','proyectos_marcas','marcas','proyectos_id','marcas_id','nombre');
            $crud->set_relation_n_n('productos','proyectos_productos','productos','proyectos_id','productos_id','nombre');
            $crud->set_relation_n_n('categorias_proyectos','proyectos_categorias_proyectos','categorias_proyectos','proyectos_id','categorias_proyectos_id','nombre');
            $crud->set_relation_n_n('servicios','proyectos_servicios','servicios','proyectos_id','servicios_id','nombre');            
            $output = $crud->render();
            $output->view = 'proyectos';
            $output->css_files = '';
            $output->js_files = '';
            $this->loadView($output);            
        }

        function briefs(){
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='edit'){
                $crud->required_fields('fecha_modificacion');
            }
            $crud->callback_before_update(function($post,$primary){
                


                $this->db = get_instance()->db;
                //Se rechazo la propuesta
                $brief = $this->db->get_where('briefs',array('id'=>$primary))->row();
                $this->db->select('briefs.*,agencias.email');
                $this->db->join('agencias','agencias.id = briefs.agencias_id');
                $brief2 = $this->db->get_where('briefs',array('proyectos_id'=>$brief->proyectos_id,'agencias_id !='=>$brief->agencias_id));                
                $proyecto = $this->db->get_where('proyectos',array('id'=>$brief->proyectos_id))->row();
                if(!empty($post['mensaje']) && empty($post['propuesta'])){
                    //Se elimina el pdf                    
                    unlink('files/'.$brief->propuesta);
                }
                if($post['status']==3){
                    if($post['presencial']==1){
                        $mensaje = 42;
                    }else{
                        $mensaje = 38;
                    }
                }elseif($post['status']==4){
                    $mensaje = 40;
                    //Enviar a los perdedores
                    foreach($brief2->result() as $a){                        
                        get_instance()->enviarcorreo((object)array('email'=>$a->email,'nombre'=>$a->nombre,'proyecto'=>$proyecto->nombre,'mensaje'=>$post['mensaje'],'fecha'=>$fecha),39);
                    }




                }else{
                    $mensaje = 37;
                }
                

                $this->db->join('user','user.id = agencias_user.user_id');
                foreach($this->db->get_where('agencias_user',array('agencias_id'=>$brief->agencias_id))->result() as $a){
                    $msj = $mensaje;
                    $fecha = date("d/m/Y H:i",strtotime($post['presentacion']));
                    get_instance()->enviarcorreo((object)array('email'=>$emails->email,'nombre'=>$emails->nombre,'proyecto'=>$proyecto->nombre,'mensaje'=>$post['mensaje'],'fecha'=>$fecha),$mensaje);
                }
                $post['mensaje'] = get_instance()->error('¡Atención! Se ha rechazado tu propuesta. '.$post['mensaje']);
                return $post;




            })->callback_after_update(function($post,$primary){                
                if(!empty($post['status']) && $post['status']==4){
                    $this->db = get_instance()->db;
                    $brief = $this->db->get_where('briefs',array('id'=>$primary))->row();
                    //$this->db->update('briefs',array('status'=>-1),array('id !='=>$primary,'proyectos_id'=>$brief->proyectos_id));
                    $this->db->update('proyectos',array('status'=>4),array('id'=>$brief->proyectos_id));
                }
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function showResult($id,$return = false){
            $this->querys->showResult($id);
        }

        function upload_file(){
            $filename = $_FILES['imagen']['name'];            
            $safefilename = trim(basename(stripslashes($filename)), ".\x00..\x20");
            $tmpfilename = $_FILES['imagen']['tmp_name'];
            $tempSafeFile = tempnam(sys_get_temp_dir(), "JLC"); //Create a Temp File to store content in a static env.
            switch (exif_imagetype($_FILES['imagen']['tmp_name'])) {
            case IMAGETYPE_JPEG:
                $safefilecontent = imagejpeg(imagecreatefromjpeg($tmpfilename), $tempSafeFile); //Get only file content created a new image and storeing it on my tempfile
                $extensions = array('jpg', 'jpeg');
                $mime = "image/jpeg";
                break;
            case IMAGETYPE_PNG:
                $safefilecontent = imagepng(imagecreatefrompng($tmpfilename), $tempSafeFile);
                $extensions = array('png');
                $mime = "image/png";
                break;
            case IMAGETYPE_GIF:
                $safefilecontent = imagegif(imagecreatefromgif($tmpfilename), $tempSafeFile);
                $extensions = array('gif');
                $mime = "image/gif";
                break;
            case IMAGETYPE_BMP:
                $safefilecontent = image2wbmp(imagecreatefromwbmp($tmpfilename), $tempSafeFile);
                $extensions = array('bmp');
                $mime = "image/x-MS-bmp";
                break;
            //There is a lot of other image types... I use this 4 just for a example
            default :
                throw new Exception("May its a unsafe image file!",500,null);
                break;
            }
            // Adjust incorrect image file extensions:
            if (!empty($extensions)) {
                $parts = explode('.', $safefilename);
                $extIndex = count($parts) - 1;
                $ext = strtolower(@$parts[$extIndex]);
                if (!in_array($ext, $extensions)) {
                    $parts[$extIndex] = $extensions[0];
                    $safefilename = implode('.', $parts);                                        
                }    
            }
            $safefilename = str_replace('á','a',$safefilename);
            $safefilename = str_replace('é','e',$safefilename);
            $safefilename = str_replace('í','i',$safefilename);
            $safefilename = str_replace('ó','o',$safefilename);
            $safefilename = str_replace('ú','u',$safefilename);
            $safefilename = str_replace('à','a',$safefilename);
            $safefilename = str_replace('è','e',$safefilename);
            $safefilename = str_replace('ì','i',$safefilename);
            $safefilename = str_replace('ò','o',$safefilename);
            $safefilename = str_replace('ù','u',$safefilename);
            $safefilename = str_replace('ñ','n',$safefilename);
            $safefilename = mb_strtolower($safefilename);
            $safefilename = str_replace(' ','-',$safefilename);
            $safefilename = preg_replace('/[^.a-z0-9\-]/', '', $safefilename);
            file_put_contents('img/proyectos_uploads/img/'.$safefilename,file_get_contents($tempSafeFile));
            return $safefilename;
            
        }

        function saveExtraFiles($post,$primary,$sendMail = TRUE){

            $this->db = get_instance()->db;
                //Guardamos la imagen                
                if(!empty($_FILES['imagen']['name'])){
                    $seudonimo = $this->upload_file();
                    $this->db->update('proyectos',array('imagen'=>$seudonimo),array('id'=>$primary));
                }
                
                
                //Guardamos los colaboradores
                $this->db->insert('proyectos_user',array(
                    'proyectos_id'=>$primary,
                    'user_id'=>$post['creador'],
                    'tipo'=>1
                ));
                /*print_r($primary);
                die();*/
                if(!empty($_POST['proyectos_colaboradores']['supervisores'])){
                foreach($_POST['proyectos_colaboradores']['supervisores'] as $n=>$v){
                    $this->db->insert('proyectos_user',array(
                        'proyectos_id'=>$primary,
                        'user_id'=>$v,
                        'tipo'=>2
                    ));
                    $user = $this->db->get_where('user',array('id'=>$v))->row();
                    $user->ejecutivo_nombre = $this->user->nombre;
                    $user->ejecutivo_apellido = $this->user->apellido;
                    $user->proyecto = $_POST['nombre'];
                    $user->link = base_url().'cliente/proyecto/verProyecto/'.$primary;
                    $user->pais = '';
                    foreach($_POST['paises'] as $p){
                        $user->pais.= @$this->db->get_where('paises',['id'=>$p])->row()->nombre;
                    }
                    if($sendMail){
                        $this->enviarcorreo((object)$user,13);
                    }
                }
                }
                if(!empty($_POST['proyectos_colaboradores']['colaboradores'])){
                foreach($_POST['proyectos_colaboradores']['colaboradores'] as $n=>$v){
                    $this->db->insert('proyectos_user',array(
                        'proyectos_id'=>$primary,
                        'user_id'=>$v,
                        'tipo'=>3
                    ));
                    $user = $this->db->get_where('user',array('id'=>$v))->row();
                    $user->link = base_url().'cliente/proyecto/verProyecto/'.$primary;
                    $user->pais = '';
                    foreach($_POST['paises'] as $p){
                        $user->pais.= @$this->db->get_where('paises',['id'=>$p])->row()->nombre;
                    }
                    if($sendMail){
                        $this->enviarcorreo((object)$user,12);
                    }
                }
                }

                //Guardamos fechas
                if(!empty($_POST['proyectos_fechas']['nombre'])){
                    foreach($_POST['proyectos_fechas']['nombre'] as $n=>$v){
                        $desde = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['proyectos_fechas']['fecha_desde'][$n])));
                        $hasta = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['proyectos_fechas']['fecha_hasta'][$n])));
                        $this->db->insert('proyectos_fechas',array(
                            'proyectos_id'=>$primary,
                            'nombre'=>$v,
                            'desde'=>$desde,
                            'hasta'=>$hasta
                        ));    
                    }                    
                }

                if(!empty($_POST['proyectos_documentos'])){
                    foreach($_POST['proyectos_documentos'] as $n=>$d){
                        if(!empty($d)){
                            $name = explode('-',$d,2);
                            $name = count($name)>1?$name[1]:$name[0];
                            $name = explode('.',$name);
                            $name = $name[0];
                            $name = str_replace('-',' ',$name);
                            $name = ucfirst($name);
                            $this->db->insert('proyectos_documentos',array(
                                'proyectos_id'=>$primary,
                                'documento'=>$d,
                                'nombre'=>$name,
                                'tipo'=>'1'
                            ));
                        }
                    }
                }


                if(!empty($_POST['proyectos_documentos_url'])){
                    foreach($_POST['proyectos_documentos_url'] as $d){
                        if(!empty($d)){                            
                            $this->db->insert('proyectos_documentos',array(
                                'proyectos_id'=>$primary,
                                'documento'=>$d,                                
                                'tipo'=>'2'
                            ));
                        }
                    }
                }
                return $post;
        }

        function proyectos_documentos(){
            $crud = $this->crud_function("","");
            $crud->set_field_upload('documento','img/proyectos_uploads/files/');
            $crud = $crud->render('');    
        }

        function verProyecto($id){
            if(is_numeric($id)){
                $proyecto = $this->querys->showResult($id,true);
                $this->loadView(array('view'=>'proyecto','proyecto'=>$proyecto));
            }
        }

        

        function historial(){
            $this->as['historial'] = 'view_proyectos';
            $crud = $this->crud_function("","");            
            $crud->set_theme('proyectos');
            $crud->set_primary_key('id');
            $where = 'getEmpresaOfProject(view_proyectos.id) = '.$this->user->empresa.' ';
            if(!empty($_POST['filtros'])){
                foreach($_POST['filtros'] as $n=>$v){
                    if(is_array($v)){
                        $where.= 'AND (';
                        foreach($v as $nn=>$vv){
                            if($n=='servicios'){
                                if(!empty($vv)){
                                    $where.= ' OR has_service(view_proyectos.id,'.$vv.',2) = 1';
                                }
                            }
                            if($n=='paises'){
                                if(!empty($vv)){
                                    $where.= ' OR has_country(view_proyectos.id,'.$vv.') = 1';
                                }
                            }
                            else{
                                $where.= ' OR '.$n.' = \''.$vv.'\'';
                            }                        
                        }
                        $where.= ')';
                    }else{                        
                        if(!empty($v)){
                            $where.= ' AND '.$n.' = \''.$v.'\'';
                        }
                    }
                }
            }
            $where = str_replace('( OR ','(',$where);
            $where = str_replace(' AND ()','',$where);
            $crud->where($where,'ESCAPE',FALSE);

            /* ORDENAR*/            
            if(!empty($_POST['ordenar'])){
                $ordenar = explode('-',$_POST['ordenar']);                
                if(count($ordenar)==2){
                    $crud->order_by($ordenar[0],$ordenar[1]);
                }else{
                    $crud->order_by('view_proyectos.id','DESC');
                }
            }else{
                $crud->order_by('view_proyectos.id','DESC');
            }   
            /* END ORDENAR */            
            $crud = $crud->render('','Theme/Cliente/cruds/');     
            $crud->view = 'agencias';                   
            $this->loadView($crud);
        }
         
        function propuestas($x = ''){
            if(is_numeric($x)){
                $this->loadView(array('view'=>'agencias-proyecto','proyecto'=>$this->querys->showResult($x,true)));
            }else{
                $this->loadView('propuestas');
            }
        }

        function enviar_brief(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('proyectos[]','Proyecto','required|integer');            
            $this->form_validation->set_rules('agencias[]','Agencias','required');
            if($this->form_validation->run()){
                $response = '';
                if(count($_POST['agencias'])==1 && strpos($_POST['agencias'][0],',')>-1){
                    $_POST['agencias'] = explode(',',$_POST['agencias'][0]);
                }
                foreach($_POST['agencias'] as $a){
                    foreach($_POST['proyectos'] as $p){
                        $this->db->join('agencias','agencias.id = briefs.agencias_id');
                        $yaenviada = $this->db->get_where('briefs',array('proyectos_id'=>$p,'agencias_id'=>$a));
                        $proyecto = $this->db->get_where('proyectos',array('id'=>$p))->row();
                        if($yaenviada->num_rows()==0){
                            $this->db->insert('briefs',array(
                                'proyectos_id'=>$p,
                                'agencias_id'=>$a,
                                'status'=>1
                            )); 
                            foreach($this->db->get_where('agencias_user',array('agencias_id'=>$a))->result() as $aa){
                                $this->querys->sendPush($aa->user_id,$this->user->nombre.' te ha invitado al proyecto '.$proyecto->nombre,base_url('agencia/proyecto/verProyecto/'.$p));
                            }
                            //Enviar correo
                            $this->db->select('agencias.*,user.email as agemail');
                            $this->db->join('agencias','agencias.id = agencias_user.agencias_id');
                            $this->db->join('user','user.id = agencias_user.user_id');
                            foreach($this->db->get_where('agencias_user',array('agencias_id'=>$a))->result() as $aa){
                                $aa->link = base_url('agencia/proyecto/verProyecto/'.$p);
                                $this->enviarcorreo((object)$aa,10,$aa->agemail);
                            }                            
                        }
                        else{
                            $response.= $this->error('ya se le ha enviado una invitación a la agencia '.$yaenviada->row()->nombre.' para este proyecto');
                        }
                        
                    }
                }
                $response.= $this->success('Invitaciones enviadas');
                echo $response;
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }

        function rankear(){
            $this->form_validation->set_rules('proyectos_id','Proyecto','required');
            $this->form_validation->set_rules('tipo','Tipo','required');
            $this->form_validation->set_rules('value','Valor','required');
            if($this->form_validation->run()){
                $proyecto = $this->querys->showResult($_POST['proyectos_id'],TRUE);
                if($proyecto->porcentaje>=100){
                    //Guardamos calificacion
                    $calif = $proyecto->calificaciones;
                    if($_POST['tipo']=='comentario'){
                        if(empty($calif['comentario']) || !is_array($calif['comentario'])){
                            $calif['comentario'] = array();     
                        }
                        $foto = base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto);
                        $calif['comentario'][] = array($_POST['value'],$this->user->nombre,$foto,$this->user->id);
                    }else{
                        $calif[$_POST['tipo']] = $_POST['value'];
                    }
                    $this->db->update('proyectos',array('calificaciones'=>json_encode($calif)),array('id'=>$_POST['proyectos_id']));
                    echo $this->success('Dato guardado con éxito');
                }
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }
        
    }
?>
