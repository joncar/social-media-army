##NUEW
DROP VIEW IF EXISTS view_proyectos;
CREATE VIEW view_proyectos AS
SELECT 
proyectos.*,
YEAR(proyectos.fecha_registro) as anho,
proyectos_user.user_id AS user_id,
GROUP_CONCAT(paises.nombre SEPARATOR ',') as paises,
get_porcentaje(proyectos.id) AS porcentaje,
get_barcolor(get_porcentaje(proyectos.id)) AS barra,
organizaciones_paises.organizaciones_id AS organizacion
FROM proyectos
INNER JOIN proyectos_paises ON proyectos_paises.proyectos_id = proyectos.id
INNER JOIN paises ON proyectos_paises.paises_id = paises.id
LEFT JOIN proyectos_user ON proyectos.id = proyectos_user.proyectos_id
LEFT JOIN user_empresas ON user_empresas.user_id = proyectos_user.user_id
LEFT JOIN empresas ON empresas.id = user_empresas.empresas_id
LEFT JOIN organizaciones_paises ON organizaciones_paises.paises_id = paises.id
GROUP BY proyectos.id

##Obsolete
DROP VIEW IF EXISTS view_proyectos;
CREATE VIEW view_proyectos AS 
SELECT 
proyectos.id AS id,
proyectos.nombre AS nombre,
proyectos.imagen AS imagen,
proyectos.solicitud_puntual AS solicitud_puntual,
proyectos.objetivo AS objetivo,
proyectos.target AS target,
proyectos.especificaciones AS especificaciones,
proyectos.motivo_cancelacion AS motivo_cancelacion,
proyectos.status AS status,
proyectos.vistas AS vistas,
proyectos_user.user_id AS user_id,
proyectos.fecha_registro AS fecha_registro,
user_empresas.empresas_id AS empresa_id,
empresas.nombre AS empresa,
get_porcentaje(proyectos.id) AS porcentaje,
get_barcolor(get_porcentaje(proyectos.id)) AS barra 
FROM proyectos 
LEFT JOIN proyectos_user ON proyectos.id = proyectos_user.proyectos_id
LEFT JOIN user_empresas ON user_empresas.user_id = proyectos_user.user_id
LEFT JOIN empresas ON empresas.id = user_empresas.empresas_id