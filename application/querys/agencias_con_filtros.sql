DROP VIEW IF EXISTS agencias_con_filtros;
CREATE VIEW agencias_con_filtros AS SELECT 
agencias.id,
agencias.nombre AS nombre,
agencias.categorias_agencias_id as categorias_agencias,
agencias.paises as paises,
agencias.paises_id,
agencias.tipos_certificacion_id as tipos_certificacion,
get_ranking_general(agencias.id) as ranking,
agencias.agencias_grupos_id as agencias_grupos,
GROUP_CONCAT(servicios.nombre) as servicios,
estatus_agencias.nombre AS status_nombre,
estatus_agencias.mostrar_listados AS status,
agencias.horas_hombre,
agencias.estatus_agencias_id,
agencias.super_poder,
organizaciones_paises.organizaciones_id AS organizacion
FROM `agencias`
INNER JOIN agencias_servicios ON agencias_servicios.agencias_id = agencias.id
INNER JOIN servicios ON servicios.id = agencias_servicios.servicios_id
INNER JOIN estatus_agencias ON estatus_agencias.id = agencias.estatus_agencias_id
LEFT JOIN organizaciones_paises ON organizaciones_paises.paises_id = agencias.paises_id
GROUP BY agencias.id

##DEFASED OBSOLETE
DROP VIEW IF EXISTS agencias_con_filtros;
CREATE VIEW agencias_con_filtros AS SELECT
agencias.id AS id,
'0' AS ranking,
agencias.horas_hombre,
agencias.agencias_grupos_id,
agencias_categorias.categorias_agencias_id AS categorias_agencias_id,
agencias_favoritas.user_id AS favorita,
agencias.nombre AS nombre,
agencias.tipos_certificacion_id AS tipos_certificacion_id,
agencias.logo AS logo,
agencias.fecha_ingreso AS fecha_ingreso,
estatus_agencias.nombre AS status_nombre,
estatus_agencias.mostrar_listados AS status,
agencias.presidente AS presidente,
agencias.ceo AS ceo,
agencias.empleados AS empleados,
agencias.paises_id AS paises_id,
paises.icono AS bandera,
agencias.Ciudad AS ciudad,
agencias.direccion AS direccion,
agencias.web AS web,
agencias.telefono AS telefono,
agencias.email AS email,
agencias.mapa AS mapa,
agencias.user_id AS user_id,
content_services.id AS content_services,
creative_services.id AS creative_services,
especial_mkt.id AS especial_mkt,
social_services.id AS social_services,
strategic_service.id AS strategic_service,
tecnical_services.id AS tecnical_services,
tradicional_mkt.id AS tradicional_mkt
FROM agencias 
left join agencias_categorias on agencias_categorias.agencias_id = agencias.id
left join agencia_content_services on agencia_content_services.agencias_id = agencias.id
left join content_services on content_services.id = agencia_content_services.content_services_id
left join paises on paises.id = agencias.paises_id
left join agencia_creative_services on agencia_creative_services.agencias_id = agencias.id
left join creative_services on creative_services.id = agencia_creative_services.creative_services_id
left join agencia_especial_mkt on agencia_especial_mkt.agencias_id = agencias.id
left join especial_mkt on especial_mkt.id = agencia_especial_mkt.especial_mkt_id
left join agencia_social_services on agencia_social_services.agencias_id = agencias.id
left join social_services on social_services.id = agencia_social_services.social_services_id
left join agencia_strategic_service on agencia_strategic_service.agencias_id = agencias.id
left join strategic_service on strategic_service.id = agencia_strategic_service.strategic_service_id
left join agencia_tecnical_services on agencia_tecnical_services.agencias_id = agencias.id
left join tecnical_services on tecnical_services.id = agencia_tecnical_services.tecnical_services_id
left join agencia_tradicional_mkt on agencia_tradicional_mkt.agencias_id = agencias.id
left join tradicional_mkt on tradicional_mkt.id = agencia_tradicional_mkt.tradicional_mkt_id
left join agencias_favoritas on agencias_favoritas.agencias_id = agencias.id
INNER JOIN estatus_agencias ON estatus_agencias.id = agencias.estatus_agencias_id


