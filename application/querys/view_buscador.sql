##QUery para el buscador del topbar
DROP VIEW IF EXISTS view_buscador;
CREATE VIEW view_buscador AS SELECT 
agencias.id,
CONCAT('Agencia ',agencias.nombre) as nombre,
'agencia/verAgencia' as tipo,
'0' as empresa
FROM agencias
INNER JOIN estatus_agencias ON estatus_agencias.id = agencias.estatus_agencias_id
WHERE estatus_agencias.mostrar_listados >= 1
UNION 
SELECT 
agencias_clientes.nombre,
agencias_clientes.nombre,
'agencia/agencias/marca',
'0'
FROM agencias_clientes 
INNER JOIN agencias ON agencias.id = agencias_clientes.agencias_id
WHERE agencias_clientes.nombre IS NOT NULL GROUP BY agencias_clientes.nombre
UNION
SELECT 
servicios.id,
servicios.nombre,
'agencia/agencias/servicio',
'0'
FROM agencias_servicios
INNER JOIN servicios ON servicios.id = agencias_servicios.servicios_id;


##QUery para el buscador del topbar Agencias
DROP VIEW IF EXISTS view_buscador_agencias;
CREATE VIEW view_buscador_agencias AS SELECT 
agencias.id,
CONCAT('Agencia ',agencias.nombre) as nombre,
'agencia/verAgencia' as tipo,
'0' as empresa
FROM agencias
INNER JOIN estatus_agencias ON estatus_agencias.id = agencias.estatus_agencias_id
WHERE estatus_agencias.mostrar_listados >= 1
UNION 
SELECT 
proyectos.id,
CONCAT('Proyecto ',proyectos.nombre),
'proyecto/verProyecto',
briefs.agencias_id AS empresa_id
FROM proyectos
LEFT JOIN briefs ON proyectos.id = briefs.proyectos_id