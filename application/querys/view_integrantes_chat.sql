DROP VIEW view_integrantes_chat;
create view view_integrantes_chat as select 
user.id AS id,
user.ultima_conexion, 
IF(TIMESTAMPDIFF(MINUTE,user.ultima_conexion,NOW()) IS NULL,-1,TIMESTAMPDIFF(MINUTE,user.ultima_conexion,NOW())) as status,
'2' as status_brief,
user.nombre AS nombre,
'1' AS tipo,
empresas.logo AS logo,
proyectos.id AS proyectos_id 
from proyectos 
left join proyectos_user on proyectos_user.proyectos_id = proyectos.id 
left join user on user.id = proyectos_user.user_id
left join user_empresas on user_empresas.user_id = user.id
left join empresas on empresas.id = user_empresas.empresas_id
UNION 
SELECT 
agencias.id AS id,
briefs.agencias_id,
user.ultima_conexion,
IF(TIMESTAMPDIFF(MINUTE,user.ultima_conexion,NOW()) IS NULL,-1,TIMESTAMPDIFF(MINUTE,user.ultima_conexion,NOW())) as status,
briefs.status,
agencias.nombre AS nombre,
'2',
agencias.logo AS logo,
briefs.proyectos_id AS proyectos_id 
from briefs 
JOIN agencias on agencias.id = briefs.agencias_id
LEFT JOIN agencias_user ON agencias.id = agencias_user.agencias_id
LEFT JOIN user ON user.id = agencias_user.user_id