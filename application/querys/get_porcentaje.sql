BEGIN
	DECLARE porcentaje INT;
    DECLARE diferencia_hasta_hoy INT;
    DECLARE diferencia_total INT;
    DECLARE fecha_maxima DATE;
    DECLARE fecha_minima DATE;
	SELECT 

	CASE WHEN proyectos.status = 4 THEN
		 100
		 WHEN DATE(NOW()) > MAX(hasta) THEN
		 100
	   	 WHEN DATE(NOW()) < MIN(desde) THEN
	     0
		 ELSE
	IF(DATEDIFF(MAX(hasta),MIN(desde))>0,TRUNCATE((DATEDIFF(NOW(),MIN(desde))*100)/DATEDIFF(MAX(hasta),MIN(desde)),0),0)
	END,
	DATEDIFF(NOW(),MIN(desde)),
	DATEDIFF(MAX(hasta),MIN(desde)),
	MAX(hasta),
	MIN(desde)

	FROM `proyectos_fechas` 
	INNER JOIN proyectos ON proyectos.id = proyectos_fechas.proyectos_id
	WHERE proyectos_id = proyecto INTO porcentaje,diferencia_hasta_hoy,diferencia_total,fecha_maxima,fecha_minima;
	return porcentaje;
END

##OLD
/* PARAMS proyecto INT */
BEGIN
DECLARE porcentaje INT;
DECLARE proy INT;
SELECT 
TRUNCATE(((count(proyectos_fechas.id)-fechas.id)*100)/count(proyectos_fechas.id),0) into porcentaje
FROM proyectos_fechas
LEFT JOIN (
	SELECT count(id) as id, proyecto as proyectos_id from proyectos_fechas where proyectos_id = proyecto AND NOW() BETWEEN desde AND hasta
) as fechas on fechas.proyectos_id = proyectos_fechas.proyectos_id
WHERE proyectos_fechas.proyectos_id = proyecto;

SELECT proyectos.status into proy from proyectos where id = proyecto;

return porcentaje;

END