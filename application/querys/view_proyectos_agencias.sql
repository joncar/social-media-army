DROP VIEW IF EXISTS view_proyectos_agencias;
CREATE VIEW view_proyectos_agencias as 
SELECT 
proyectos.id,
proyectos.nombre,
proyectos.imagen,
proyectos.solicitud_puntual,
proyectos.objetivo,
proyectos.target,
proyectos.especificaciones,
proyectos.motivo_cancelacion,
proyectos.status,
proyectos.vistas,
briefs.agencias_id as user_id,
proyectos.fecha_registro,
get_porcentaje(proyectos.id) as porcentaje,
get_barcolor(get_porcentaje(proyectos.id)) as barra
FROM briefs
INNER JOIN proyectos on proyectos.id = briefs.proyectos_id