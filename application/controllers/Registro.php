<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Registro extends Main {
        const IDROLUSER = 2;
        public function __construct()
        {
            parent::__construct();         
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        }
        
        public function loadView($param = array('view'=>'main'))
        {
            if(!empty($param->output)){
                $panel = 'panel';
                $param->view = empty($param->view)?$panel:$param->view;
                $param->crud = empty($param->crud)?'user':$param->crud;
                $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
            }
            if(is_string($param)){
                $param = (object)array('view'=>$param);
            }
            if(is_array($param)){
                $param = (object)$param;
            }
            $template = 'Login/panel';

            $this->load->view($template,$param);
        }

        /*public function agencia($url = 'main',$page = 0)
        {
            $crud = new ajax_grocery_CRUD();                            
            $crud->set_theme('bootstrap2');
            $crud->set_table('user');
            $crud->set_theme('registro');            
            $crud->set_field_upload('foto','img/fotos')
                 ->field_type('password','hidden','12345678')
                 ->field_type('tipo_usuario','hidden','Agencia')
                 ->field_type('fecha_registro','hidden',date("Y-m-d"))
                 ->field_type('fecha_actualizacion','hidden',date("Y-m-d"));
            if(!empty($_GET['xys1'])){
                $inv = base64_decode($_GET['xys1']);
                if(is_numeric($inv)){
                    $inv = $this->db->get_where('invitaciones',array('id'=>$inv));
                    if($inv->num_rows()>0){
                        $crud->field_type('email','string',$inv->row()->email);
                    }
                }
            }
            $crud->required_fields_array();
            $crud->callback_before_insert(array($this,'binsertion'));
            $crud->callback_after_insert(array($this,'ainsertion'));
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            $crud->set_lang_string('insert_success_message','Se le ha enviado un correo electrónico de confirmación, por favor revise su bandeja de entrada.');
            $output = $crud->render('','Theme/Login/cruds/');
            $output->view = 'registro';
            $output->crud = 'user';
            $output->title = 'REGISTRAR';              
            $this->loadView($output);   
        }*/

        public function cliente($url = 'main',$page = 0)
        {
            $crud = new ajax_grocery_CRUD();            
            $crud->set_theme('bootstrap2');
            $crud->set_table('user');
            $crud->set_theme('registro');            
            $crud->set_field_upload('foto','img/fotos')
                 ->field_type('password','hidden','12345678')
                 ->field_type('tipo_usuario','hidden','')
                 ->field_type('fecha_registro','hidden',date("Y-m-d"))
                 ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                 ->field_type('nombre','hidden','Default')
                 ->field_type('apellido','hidden','Default');
            $crud->required_fields_array();
            $crud->unset_back_to_list();
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            $crud->callback_before_insert(array($this,'binsertion'));
            $crud->callback_after_insert(array($this,'ainsertion'));
            $crud->set_lang_string('insert_success_message','Se le ha enviado un correo electrónico de confirmación, por favor revise su bandeja de entrada.');                        
            $output = $crud->render('','Theme/Login/cruds/');
            $output->view = 'registro';
            $output->crud = 'user';
            $output->title = 'REGISTRAR';              
            $this->loadView($output);   
        }           

        
        
        function conectar()
        {
            $this->loadView('predesign/login');
        }
        /* Callbacks */
        function binsertion($post)
        {            
            $post['status'] = 1;
            $post['admin'] = 0;
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            $post['password'] = md5($post['password']);
            return $post;
        }
        
        function ainsertion($datos,$primary)
        {              
            //Asignar rol
            
            $datos['linkagencia'] = base_url('registro/validar/Agencia/'.base64_encode($primary));
            $datos['linkempresa'] = base_url('registro/validar/Cliente/'.base64_encode($primary));
            get_instance()->enviarcorreo((object)$datos,21);
            //get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>$grupo));            
            //get_instance()->user->login_short($primary);            
            return true;
        }

        function validar($tipo,$key){
            $key = base64_decode($key);
            if(is_numeric($key) && $this->db->get_where('user',array('id'=>$key))->num_rows()>0){
                get_instance()->db->update('user',array('tipo_usuario'=>$tipo),array('id'=>$key));
                $grupo = $tipo=='Agencia'?3:2;
                get_instance()->db->insert('user_group',array('user'=>$key,'grupo'=>$grupo));            
                
                get_instance()->user->login_short($key);   
                //redirect('panel');
                redirect('cliente/ajustes');
            }
        }
        
       
        function forget($key = '',$ajax = '')
        {
            if(empty($_POST) && empty($key)){
                $this->loadView(array('view'=>'forget'));
            }
            else
            {
                if(empty($key)){
                if(empty($_SESSION['key'])){
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    if($this->form_validation->run())
                    {
                        $user = $this->db->get_where('user',array('email'=>$this->input->post('email')));
                        if($user->num_rows()>0){
                            $_SESSION['key'] = md5(rand(0,2048));
                            $_SESSION['email'] = $this->input->post('email');
                            //correo($this->input->post('email'),'Reestablecimiento de contraseña',$this->load->view('Login/email/forget',array('user'=>$user->row()),TRUE));
                            $this->enviarcorreo((object)array(
                                'usuario'=>$user->row()->nombre.' '.$user->row()->apellido,
                                'email'=>$this->input->post('email'),
                                'key'=>$_SESSION['key'],
                                'enlace'=>base_url('registro/forget/'.$_SESSION['key'])
                            ),29);
                            if(empty($ajax)){
                                $_SESSION['msj'] = $this->success('Los pasos de restablecimiento se han enviado a tu correo');
                                header("Location:".base_url('registro/forget'));
                            }else{
                                echo $this->traduccion->traducir($this->success('Los pasos de restablecimiento se han enviado a tu correo'),$_SESSION['lang']);
                            }
                        }
                        else{
                            if(empty($ajax)){
                                $this->loadView(array('view'=>'forget','msj'=>$this->error('El correo ingresado no se encuentra registrado')));
                            }else{
                                echo $this->traduccion->traducir($this->error('El correo ingresado no se encuentra registrado'),$_SESSION['lang']);
                            }
                        }
                    }
                    else{
                        if(empty($ajax)){
                            $this->loadView(array('view'=>'forget','msj'=>$this->error($this->form_validation->error_string())));
                        }else{
                            $this->error($this->form_validation->error_string());
                        }
                    }
                }
                else
                {
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    $this->form_validation->set_rules('pass','Password','required|min_length[8]');
                    $this->form_validation->set_rules('pass2','Password2','required|min_length[8]|matches[pass]');
                    //$this->form_validation->set_rules('key','Llave','required');
                    if($this->form_validation->run())
                    {
                        /*if($this->input->post('key') == $_SESSION['key'])
                        {*/
                            $this->db->update('user',array('password'=>md5($this->input->post('pass'))),array('email'=>$_SESSION['email']));
                            session_unset();
                            $this->loadView(array('view'=>'forget','msj'=>$this->success('Se ha restablecido su contraseña <a href="'.base_url().'">Volver al inicio</a>')));
                        /*}
                        else
                            $this->loadView(array('view'=>'recover','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));*/
                    }
                    else{
                        if(empty($_POST['key'])){
                            if(empty($ajax)){
                                $this->loadView(array('view'=>'forget','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));    
                            }else{
                                echo $this->traduccion->traducir($this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.'),$_SESSION['lang']);
                            }
                            session_unset();
                        }
                        else{
                            $this->loadView(array('view'=>'recover','key'=>$key,'msj'=>$this->error($this->form_validation->error_string())));
                        }
                    }
                }
                }
                else
                {
                    if(!empty($_SESSION['key']) && $key==$_SESSION['key'])
                    {
                        $this->loadView(array('view'=>'recover','key'=>$key));
                    }
                    else{
                        if(empty($ajax)){
                            $this->loadView(array('view'=>'forget','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));
                        }else{
                            echo $this->traduccion->traducir($this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.'),$_SESSION['lang']);
                        }
                    }
                }
            }
        } 

        function completarAgencia(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_table('agencias');
            $crud->set_subject('Agencia');
            $crud->set_theme('completarAgencia');
            $crud->unset_jquery();
            $crud->set_field_upload('logo','img/agencias');  
            $crud->field_type('status','hidden',1)
                 ->field_type('fecha_ingreso','hidden',date("Y-m-d H:i:s"))
                 ->field_type('tipos_certificacion_id','hidden',1)
                 ->field_type('user_id','hidden',$this->user->id)
                 ->field_type('alcance','enum',array('Local','Nacional','Internacional'))
                 ->field_type('password','password')
                 ->field_type('password2','password');

            $crud->callback_after_insert(function($post,$primary){
                print_r($primary);
                get_instance()->db->update('user',array('nombre'=>$_POST['nombre'],'apellido'=>$_POST['apellido'],'puesto'=>$_POST['puesto'],'password'=>md5($_POST['password'])),array('id'=>get_instance()->user->id));
                get_instance()->db->insert('agencias_user',array(
                    'user_id'=>get_instance()->user->id,
                    'agencias_id'=>$primary
                ));                
                //get_instance()->user->login_short(get_instance()->user->id);
            });

            
            //$crud->field_type('nombre','hidden',$this->user->razon_social);

            if($crud->getParameters(false)=='insert_validation'){
                $crud->set_rules('password','Password','required');
                $crud->set_rules('password2','Repetir password','required|matches[password]');
            }

            if($crud->getParameters(false)=='insert'){
                $crud->fields('logo','razon_social','rfc','paises_id','nombre');
            }else{
                $crud->fields('logo','razon_social','password','password2','rfc','paises_id','puesto','nombre','apellido');
            }

            $crud->unset_back_to_list();

            $crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url().'";</script>');
            $crud->required_fields_array();
            $crud = $crud->render('','Theme/Login/cruds/');
            $crud->view = 'completar-registro';
            $this->loadView($crud);            
        }

        function completarCliente(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_table('empresas');
            $crud->set_subject('Cliente');
            $crud->set_theme('completarCliente');
            $crud->field_type('status','hidden',1);
            $crud->unset_jquery();
            $crud->unset_back_to_list();
            $crud->set_field_upload('logo','img/empresas/logos');
            $crud->callback_after_insert(function($post,$primary){

                
                get_instance()->db->insert('user_empresas',array(
                    'user_id'=>get_instance()->user->id,
                    'empresas_id'=>$primary,
                    'tipo'=>1
                ));

                get_instance()->db->update('user',array(
                    'status'=>'0',
                    'nombre'=>$_POST['nombre_user'],
                    'apellido'=>$_POST['apellido_user'],
                    'puesto'=>$_POST['puesto'],
                    'password'=>md5($_POST['password'])
                ),array('id'=>get_instance()->user->id));

                //get_instance()->user->login_short(get_instance()->user->id);
                get_instance()->user->unlog();
                $_SESSION['msj'] = get_instance()->success('Su cuenta se encuentra en revisión, una vez sea aceptada, nos comunicaremos con usted');
                $datos = (object)array('link'=>base_url('seguridad/user/edit/'.get_instance()->user->id));
                get_instance()->enviarcorreo($datos,35,'jerry@brandsbell.com');
                get_instance()->enviarcorreo($datos,35,'alinne@brandsbell.com');
            });


            if($crud->getParameters(false)=='insert_validation'){
                $crud->set_rules('password','Password','required');
                $crud->set_rules('password2','Repetir password','required|matches[password]');
            }

            if($crud->getParameters(false)=='insert'){
                $crud->fields('logo','razon_social','rfc','paises_id','nombre','email_corporativo');
            }else{
                $crud->fields('logo','razon_social','password','password2','rfc','paises_id','puesto','nombre','nombre_user','apellido_user','email_corporativo');
            }

            $crud->field_type('password','password')
                 ->field_type('password2','password')
                 /*->field_type('nombre','string',$this->user->razon_social)
                 ->field_type('nombre_user','string',$this->user->nombre)
                 ->field_type('apellido_user','string',$this->user->apellido)*/
                 ->field_type('email_corporativo','string',$this->user->email);



            $crud->required_fields_array();
            //$crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url('panel').'";</script>');
            $crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url().'";</script>');
            $crud = $crud->render('','Theme/Login/cruds/');
            $crud->view = 'completar-registro';
            $this->loadView($crud);
        }

        function completarRegistroInvitacion(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_table('user');
            $crud->set_subject('Usuario');
            $crud->set_theme('completarCliente');            
            $crud->unset_jquery();
            $crud->unset_back_to_list();
            $crud->set_field_upload('foto','img/fotos');            
            $crud->callback_before_update(function($post,$primary){
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_after_update(function($post,$primary){
                get_instance()->user->login_short($primary);
            });

            $crud->field_type('password','password')
                 ->field_type('password2','password')
                 ->field_type('email','hidden')
                 ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                 /*->field_type('nombre','string',$this->user->razon_social)
                 ->field_type('nombre_user','string',$this->user->nombre)
                 ->field_type('apellido_user','string',$this->user->apellido)*/
                 ->field_type('email_corporativo','string',$this->user->email);
            $crud->required_fields_array();
            //$crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url('panel').'";</script>');
            $crud->set_lang_string('update_success_message','<script>document.location.href="'.base_url('panel').'";</script>');
            $crud = $crud->render('','Theme/Login/cruds/');
            $crud->view = 'completar-registro';
            $this->loadView($crud);
        }

        function completar_registro(){
            if(empty($_SESSION['user'])){
                redirect('main/index');
                die();
            }

            if($this->db->get_where('agencias_user',array('user_id'=>$this->user->id))->num_rows()==0 && $this->db->get_where('user_empresas',array('user_id'=>$this->user->id))->num_rows()==0){
                if($this->user->tipo_usuario=='Agencia'){
                    $this->completarAgencia();
                }else{
                    $this->completarCliente();
                }
            }else{
                $this->completarRegistroInvitacion();
            }
        }       



        function completar_registro2(){
            
            $crud = new ajax_grocery_CRUD();
            $crud->set_table('agencias');
            $crud->set_subject('Agencias');
            $crud->set_theme('completarAgencia');

            $crud->set_field_upload('logo','img/agencias')
                 ->field_type('status','true_false',array(0=>'Inactiva',1=>'Activa'))
                 ->field_type('filtros','tags')
                 ->field_type('fecha_ingreso','hidden',date("Y-m-d H:i:s"))
                 ->field_type('mapa','map',array('width'=>'300px','height'=>'300px'));
            $crud->display_as('user_id','Usuario Administrador');
            if($crud->getParameters()!='list'){
                $crud->set_relation('user_id','user','{nombre} {email}',array('tipo_usuario'=>'Agencia'));
            }
            $crud->columns('logo','nombre','presidente','agencias.email','status');
            $crud->callback_column('agencias.email',function($val,$row){
                return $row->email;
            })
            ->callback_after_update(function($post,$primary){
                get_instance()->db->update('user',array('status'=>'0'),array('id'=>get_instance()->user->id));
                $datos = (object)array('link'=>base_url('agencia/admin/agencias/edit/'.$primary));
                get_instance()->enviarcorreo($datos,34,'jerry@brandsbell.com');
                get_instance()->enviarcorreo($datos,34,'alinne@brandsbell.com');
                get_instance()->user->unlog();
                $_SESSION['msj'] = get_instance()->success('Su cuenta se encuentra en revisión, una vez sea aceptada, nos comunicaremos con usted');
            })
            ->display_as('agencias.email','Email');
            $crud->set_relation_n_n('tipo_servicio','agencia_tipo_servicio','tipos_servicio','agencias_id','tipos_servicio_id','nombre');
            $crud->set_relation_n_n('content_services','agencia_content_services','content_services','agencias_id','content_services_id','nombre');
            $crud->set_relation_n_n('traditional_digital_mkt','agencia_tradicional_mkt','tradicional_mkt','agencias_id','tradicional_mkt_id','nombre');
            $crud->set_relation_n_n('Creative_Services','agencia_creative_services','creative_services','agencias_id','creative_services_id','nombre');
            $crud->set_relation_n_n('social_services','agencia_social_services','social_services','agencias_id','social_services_id','nombre');
            $crud->set_relation_n_n('specialized_marketing','agencia_especial_mkt','especial_mkt','agencias_id','especial_mkt_id','nombre');
            $crud->set_relation_n_n('strategic_services','agencia_strategic_service','strategic_service','agencias_id','strategic_service_id','nombre');
            $crud->set_relation_n_n('technical_services','agencia_tecnical_services','tecnical_services','agencias_id','tecnical_services_id','nombre');            
            $crud->add_action('<i class="fa fa-edit"></i> Usuarios','',base_url('agencia/admin/agencias_user').'/');
            $crud->add_action('<i class="fa fa-envelope"></i> Dominios','',base_url('agencia/admin/agencias_dominios').'/');
            $crud->add_action('<i class="fa fa-user"></i> Clientes','',base_url('agencia/admin/agencias_clientes').'/');
            $crud->order_by('status','ASC');
            $crud->required_fields('razon_social','rfc','nombre','logo','fecha_constitucion','fecha_inscripcion_proveedor','ceo','empleados','paises_id','calle','numero_interior','numero_exterior','colonia','delegacion','estado','telefono','email');
            $crud->set_lang_string('update_success_message','Datos registrados con éxito <script>setTimeout(function(){document.location.href="'.base_url('panel').'";},1500);</script>');
            $crud = $crud->render('','Theme/Login/cruds/');
            $crud->view = 'completar-registro2';
            $this->loadView($crud);
                    
        }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
