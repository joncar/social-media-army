<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();

class Main extends CI_Controller {
    //public $theme = 'themes/bristol/';
    //Insert body css & js
    public $js = [];
    public $css = [];
    //Insert head css & js
    public $hjs = [];
    public $hcss = [];
    protected $types_lang = array(
        'es'=>'spanish',
        'en'=>'english',
        'fr'=>'french',
        'ru'=>'russian',
        'it'=>'italian',
        'ca'=>'catalan',
        'ch'=>'zh_cn',
        'al'=>'german'
    );
    public $class_lang = array(
        'es'=>array('Español','spain'),
        'en'=>array('English','ingles'),
        'fr'=>array('Frances','frances'),
        'ru'=>array('Pусский','ruso'),
        'it'=>array('Italiano','italiano'),
        'ca'=>array('Catalá','catalunya'),
        'ch'=>array('中国','chino'),
        'al'=>array('Deutsch','aleman')
    );
    public $theme = '';
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->library('redsysAPI');        
        $this->load->library('traduccion');
        if(empty($_SESSION['user']) && empty($_SESSION['tmpsesion'])){
            $_SESSION['tmpsesion'] = '1'.date("his");
        }

        date_default_timezone_set('America/Mexico_City');
        setlocale(LC_ALL, 'es_ES', 'Spanish_Spain', 'Spanish');
        if(empty($_SESSION['lang'])){
            $_SESSION['lang'] = 'es';
        }
        if(file_exists(APPPATH.'language/'.$this->types_lang[$_SESSION['lang']].'/web_lang.php')){
            $this->lang->load('web',$this->types_lang[$_SESSION['lang']]);
            $this->lang->load('form_validation',$this->types_lang[$_SESSION['lang']]);
            $_SESSION['default_lang'] = $this->types_lang[$_SESSION['lang']];
        }else{
            $_SESSION['default_lang'] = 'spanish';
        }       
    }

    public function index() {        
        if(!$this->user->log){
            $this->load->view('Login/login');
        }else{
            redirect('panel');
        }
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function loginShort(){
        if(!empty($_GET['pw']) && is_numeric(base64_decode($_GET['pw']))){
            if($this->user->login_short(base64_decode($_GET['pw']))){
                redirect('panel');
            }else{
                echo $this->success('<script>localStorage.removeItem(\'session\'); document.location.href="' . site_url('main') . '"</script>');
            }
        }else{
            echo $this->success('<script>localStorage.removeItem(\'session\'); document.location.href="' . site_url('main') . '"</script>');
        }
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember'])){
                            $_SESSION['remember'] = 1;
                        }                        
                        $session = !empty($_POST['recordar'])?'localStorage.session = "'.base64_encode($this->user->id).'";':'';
                        if (empty($_POST['redirect'])){
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>'.$session.'document.location.href="' . site_url('panel') . '"</script>');
                        }
                        else{                            
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>'.$session.'document.location.href="' . $_POST['redirect'] . '"</script>');
                        }                        
                    } else{
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                    }
                } else{
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
                }
            } else{
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');
            }

            if (!empty($_SESSION['msj'])){
                header("Location:" . base_url('main'));
            }
        } else
            header("Location:" . base_url('main'));
    }

    public function unlog() {
        $this->user->unlog();
        echo $this->success('<script>localStorage.removeItem(\'session\'); document.location.href="' . site_url('main') . '"</script>');
    }

    function getHead($page){
        $ajustes = $this->db->get('ajustes')->row();
        //$stocookie = '<link href="'.base_url('js/stocookie/stoCookie.css').'">';        
        $stocookie = '';
        $page = str_replace('</head>',$stocookie.'</head>',$page);
        return $page;
    }

    function getBody($page){
        $ajustes = $this->db->get('ajustes')->row();        
        $stocookie = '<script src="'.base_url('js/stocookie/stoCookie.min.js').'"></script>';
        $stocookie.= html_entity_decode($ajustes->cookies);
        $stocookie = str_replace('{{cookiesbar}}',l('cookiesbar'),$stocookie);
        $stocookie = str_replace('{{cookiesbtn}}',l('cookiesbtn'),$stocookie);
        $page = str_replace('</body>',$stocookie.'</body>',$page);
        $page= str_replace('</body>',$ajustes->analytics.'</body>',$page);
        return $page;
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param))
            $param = array('view' => $param);
        $ajustes = $this->db->get('ajustes')->row();
        if(empty($param['title'])){
            $param['title'] = $ajustes->titulo;
        }
        if(empty($param['description'])){
            $param['description'] = $ajustes->description;
        }
        $param['favicon'] = $ajustes->favicon;
        $param['keywords'] = $ajustes->keywords;     
        $param = array_merge($param,['css'=>$this->css,'js'=>$this->js,'hcss'=>$this->hcss,'hjs'=>$this->hjs]);        
        /*if(!empty($param['css_files'])){
            foreach($param['css_files'] as $p){
                $param['hcss'][] = '<link rel="stylesheet" href="'.$p.'" type="text/css">';
            }
        }
        if(!empty($param['js_files'])){
            foreach($param['js_files'] as $p){
                $param['js'][] = '<script src="'.$p.'"></script>';
            }        
        }  */ 
        $page = $this->load->view('template', $param,true);
        $page = $this->getHead($page);
        $page = $this->getBody($page);
        $page = $this->traduccion->traducir($page,$_SESSION['lang']);

        $page = str_replace('data-lang="'.$_SESSION['lang'].'"','class="active"',$page);
        echo $page;
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }
    
    function enviarcorreo($usuario,$idnotificacion,$destinatario = '',$link = ''){
            if(!empty($usuario)){
                $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
                foreach($usuario as $n=>$v){    
                  $v = $n!='link' && $idnotificacion!=29?($v):$v;         
                 @$mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
                 @$mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
                }     
                if(empty($destinatario)){
                    correo($usuario->email,$mensaje->titulo,$mensaje->texto,$link);
                }
                else{
                    correo($destinatario,$mensaje->titulo,$mensaje->texto,$link);
                }
            }
        }

        function showTheme($path,$view)
        {   
            $this->load->view($path.'/'.$view);
        }

        function testMail(){
            $this->enviarcorreo((object)array('id'=>1),44,'Sulkin@socialmediaarmy.com.mx');
            $this->enviarcorreo((object)array('id'=>1),44,'Sulkin@gmail.com');
        }

        public function traduccion($idioma = 'es'){        

            $_SESSION['lang'] = $idioma;
            if(empty($_GET['url'])){            
                $url = $_SERVER['HTTP_REFERER'];
            }else{
                $url = $_GET['url'];
            }
            /*if(!empty($url)){
                $trad = $this->db->get_where('traducciones',array('idioma'=>$_SESSION['lang'],'tipo'=>2));
                foreach($trad->result() as $t){
                    $url = str_replace($t->original,$t->traduccion,$url);
                }
            } */       
            redirect($url);
        }

}
