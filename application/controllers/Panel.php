<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Panel extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                $this->load->model('querys');
                if(empty($_SESSION['user'])){
                    header("Location:".base_url('main'));
                    die();
                }                
                if(!$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posseeix permisos per a realitzar aquesta operació','403');
                    exit;
                }
        }
        
        public function loadView($param = array('view'=>'main'))
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('main'));
            }
            else{
                if($this->router->fetch_class()!=='registro' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posee permiso para realizar esta operación','403');
                }
                else{
                    if(!empty($param->output)){
                        $panel = 'panel';
                        $param->view = empty($param->view)?$panel:$param->view;
                        $param->crud = empty($param->crud)?'user':$param->crud;
                        $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                    }
                    if(is_string($param)){
                        $param = array('view'=>$param);
                    }
                    if(is_object($param)){
                        $param = (array)$param;
                    }
                    
                    $template = $this->user->admin==1?'templateadmin':$this->user->tipo_usuario.'/'.$param['view'];
                    $param = array_merge($param,['css'=>$this->css,'js'=>$this->js,'hcss'=>$this->hcss,'hjs'=>$this->hjs]);                            
                    /*if(!empty($param['css_files'])){
                        foreach($param['css_files'] as $p){
                            $param['hcss'][] = '<link rel="stylesheet" href="'.$p.'" type="text/css">';
                        }
                    }
                    if(!empty($param['js_files'])){
                        foreach($param['js_files'] as $p){
                            $param['js'][] = '<script src="'.$p.'"></script>';
                        }        
                    }   */
                    if(is_array($param)){
                        $param = (object)$param;
                    }
                    $page = $this->load->view($template,$param,TRUE);
                    echo $page;
                }                
            }
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            if($this->user->admin==0){
              //  $crud->set_model('usuario_model');
            }
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }        /*Cruds*/              
        
        public function index() {            
            /*if(!empty($this->user->empresa) || $this->user->admin==1){

                //Completar registro
                if($this->user->nombre=='default' && $this->user->apellido=='default'){
                    redirect('registro/completar_registro/edit/'.$this->user->id);    
                }elseif(!$this->validarRegistro()){
                    redirect('registro/completar_registro2/edit/'.$this->user->empresa);
                }
                else{
                    $panel = $this->user->admin==1?'panel':'dashboard';
                    $this->loadView($panel);
                }

            }else{
                redirect('registro/completar_registro/add');
            }*/
            $panel = $this->user->admin==1?'panel':'dashboard';
            $this->loadView($panel);

        }        

        public function validarRegistro(){
            if($this->user->tipo_usuario=='Agencia'){
                $agencias = $this->db->get_where('agencias',array('id'=>$this->user->empresa));
                $agenciasf = $this->db->get_where('agencias_con_filtros',array('id'=>$this->user->empresa));
                if($agencias->num_rows()==0 || $agenciasf->num_rows()==0){
                    return false;
                }
                $agenciaf = $agenciasf->row();
                $agencia = $agencias->row();
                if(
                    empty($agencia->razon_social) ||
                    empty($agencia->rfc) ||
                    empty($agencia->nombre) ||
                    empty($agencia->logo) ||
                    empty($agencia->fecha_constitucion) ||
                    empty($agencia->fecha_inscripcion_proveedor) ||
                    empty($agencia->ceo) ||
                    empty($agencia->empleados) ||
                    empty($agencia->paises_id) ||
                    empty($agencia->calle) ||
                    empty($agencia->numero_interior) ||
                    empty($agencia->numero_exterior) ||
                    empty($agencia->colonia) ||
                    empty($agencia->delegacion) ||
                    empty($agencia->estado) ||
                    empty($agencia->telefono) ||
                    empty($agencia->email)
                ){
                    return false;
                }
                return true;
            }else{
                return true;
            }
        }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
