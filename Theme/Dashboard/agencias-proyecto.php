<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                  <!-- Inicia Contenido -->
                  <div class="row">
                      <div class="col-sm-12 padding0">

                        <!-- Inicia una agencia -->
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 padding0 datos-proyecto">

                              <div class="col-xs-12 col-sm-3">
                                    <div class="card card-product" data-count="3">
                                        <div class="logo-agencia">
                                          <img class="center-block img-responsive" src="assets/img/Proyectos/img-proyecto4.jpg" alt="Agencia">
                                        </div>
                                        <div class="card-content">
                                            <div class="card-title"><b>Nombre del Proyecto</b></div>
                                            <div class="progress" id="progress-bar">
                                                <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: 100%">
                                                    <b><big>100%</big></b>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <b>Marca: Nombre de la marca</b><br>
                                            <b>Producto: Nombre del producto</b>
                                        </div>
                                    </div>
                              </div>

                              <div class="col-xs-12 col-sm-7 contenedor-descripcion-agencia2 margen-movil-servicios">
                                  <div class="col-sm-12 texto-gris">
                                        <div class="col-xs-4 col-sm-4">
                                            <div class="card card-stats">
                                                <div class="card-header" data-background-color="blue"><i class="material-icons">timeline</i></div>
                                                <div class="card-content">
                                                    <p class="category">Fase</p><h3 class="card-title"><span class="porcentaje-agencia texto-rojo">Inicio</span></h3>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-4 col-sm-4">
                                            <div class="card card-stats">
                                                <div class="card-header" data-background-color="blue"><i class="material-icons">timer_off</i></div>
                                                <div class="card-content">
                                                    <p class="category">Fin de la fase</p><h3 class="card-title"><span class="porcentaje-agencia texto-morado">11/00/000</span></h3>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-4 col-sm-4">
                                            <div class="card card-stats">
                                                <div class="card-header" data-background-color="blue"><i class="material-icons">assignment_ind</i></div>
                                                <div class="card-content">
                                                    <p class="category">Agencias</p>
                                                    <h3 class="card-title">
                                                        <span class="porcentaje-agencia texto-morado">9</span>
                                                        <a href="">
                                                          <i class="fa fa-eye" aria-hidden="true"></i>
                                                        </a>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 basura-proyectos">
                                      <a title="Eliminar Proyecto" href="#eliminar-proyecto" data-toggle="modal"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                      <a title="Pausar Proyecto" href="#pausar-proyecto" data-toggle="modal"><i class="fa fa-pause" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col-sm-12 texto-gris">
                                        <small class="texto-rojo">*Recomendamos no invitar a más agencias después de la fase de preguntas y respuestas </small>
                                    </div>
                              </div>

                              <div class="col-xs-12 col-sm-2 iconos-agencias-izquierda text-left">
                                  <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Editar Proyecto" href="#editar-proyecto" data-toggle="modal">
                                        <button class="btn btn-primary btn-round" id="btn-proyecto-informacion"><i class="material-icons">info</i> Información</button>
                                      </a>
                                  </div>
                                  <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Ver Agencia" href="sala-juntas.php">
                                        <button class="btn btn-primary btn-round" id="btn-proyecto-juntas"><i class="material-icons">people</i> Sala de Juntas</button>
                                      </a>
                                  </div>
                              </div>

                          </div>
                        </div>

                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion">
                          <div class="titulo-top">Agencias Participantes</div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12">

                          <div class="col-sm-3">
                              <div class="col-sm-12 fondo-proyecto-dashboard">
                                  <div class="img-perfil-proyecto text-center">
                                      <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Perfil Dashboard"
                                      class="img-responsive center-block image">
                                      <span>
                                          <img src="assets/img/Banderas/mexico.jpg" alt="Perfil Dashboard" class="img-responsive center-block img-circle" id="perfil-proyecto">
                                      </span>
                                  </div>
                                  <h1 class="titulo-proyecto">Nombre de la Agencia</h1>
                                  <a href="propuestas.php">
                                      <div class="btn-propuesta-agencias"><i class="fa fa-file-text-o" aria-hidden="true"></i>  VER PROPUESTA</div>
                                  </a>

                                  <div class="contenedor-btn-rechazar">
                                      <a title="Rechazar" href="#cancelar-proyecto" data-toggle="modal">
                                          <div class="col-sm-5">
                                              <div class="btn-rechazar-agencia-rechazar">
                                                <i class="fa fa-ban" aria-hidden="true"></i><br>Rechazar
                                              </div>
                                          </div>
                                      </a>
                                      <a title="Rechazar" href="#cancelar-proyecto" data-toggle="modal">
                                          <div class="col-sm-7">
                                              <a title="Editar fecha" href="#invitar-presentacion" data-toggle="modal">
                                                <div class="btn-rechazar-agencia-invitar">
                                                  <i class="fa fa-lightbulb-o" aria-hidden="true"></i><br>Invitar a presentar
                                                </div>
                                              </a>
                                          </div>
                                      </a>
                                  </div>
                              </div>
                          </div>

                          <div class="col-sm-3">
                              <div class="col-sm-12 fondo-proyecto-dashboard">
                                  <div class="img-perfil-proyecto text-center">
                                      <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Perfil Dashboard"
                                      class="img-responsive center-block image">
                                      <span>
                                          <img src="assets/img/Banderas/mexico.jpg" alt="Perfil Dashboard"
                                          class="img-responsive center-block img-circle" id="perfil-proyecto">
                                      </span>
                                  </div>
                                  <h1 class="titulo-proyecto">Nombre de la Agencia</h1>
                                  <a href="propuestas.php">
                                      <div class="btn-propuesta-agencias">
                                          <i class="fa fa-file-text-o" aria-hidden="true"></i>  VER PROPUESTA
                                      </div>
                                  </a>
                                  <div class="contenedor-btn-rechazar2">
                                      <a title="Agradecer Participación" href="#agradecer-participacion" data-toggle="modal">
                                          <div class="col-sm-6">
                                              <div class="btn-rechazar-agencia-agradecer">
                                                <i class="fa fa-quote-left" aria-hidden="true"></i><br>Agradecer Participación
                                              </div>
                                          </div>
                                      </a>
                                      <a title="Ganador" href="#agencia-ganadora" data-toggle="modal">
                                          <div class="col-sm-6">
                                              <div class="btn-rechazar-agencia-ganador">
                                                <i class="fa fa-trophy" aria-hidden="true"></i><br>¡AGENCIA<br>GANADORA!
                                              </div>
                                          </div>
                                      </a>
                                  </div>
                                  <div class="col-sm-12">
                                      <div class="col-sm-6">
                                          <b>Presentación<br>Presencial:</b>
                                      </div>
                                      <div class="col-sm-6">
                                          <a title="Editar fecha" href="#fecha-presentacion" data-toggle="modal">
                                              00 / 00  / 0000<br>
                                              15:00 hrs.  <i class="fa fa-pencil" aria-hidden="true"></i>
                                          </a>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-sm-3">
                              <div class="col-sm-12 fondo-proyecto-dashboard">
                                  <div class="img-perfil-proyecto text-center">
                                      <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Perfil Dashboard"
                                      class="img-responsive center-block image">
                                      <span>
                                          <img src="assets/img/Banderas/mexico.jpg" alt="Perfil Dashboard"
                                          class="img-responsive center-block img-circle" id="perfil-proyecto">
                                      </span>
                                  </div>
                                  <h1 class="titulo-proyecto">Nombre de la Agencia</h1>
                                  <div class="btn-propuesta-proceso">
                                      PROPUESTA EN PROCESO
                                  </div>
                                  <div class="contenedor-btn-rechazar">
                                      <a title="Rechazar" href="#cancelar-proyecto" data-toggle="modal">
                                          <div class="col-sm-5">
                                              <div class="btn-rechazar-agencia-rechazar">
                                                <i class="fa fa-ban" aria-hidden="true"></i><br>Rechazar
                                              </div>
                                          </div>
                                      </a>
                                      <a title="Rechazar" href="#cancelar-proyecto" data-toggle="modal">
                                          <div class="col-sm-7">
                                              <a title="Editar fecha" href="#invitar-presentacion" data-toggle="modal">
                                                  <div class="btn-rechazar-agencia-invitar">
                                                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i><br>Invitar a presentar
                                                  </div>
                                              </a>
                                          </div>
                                      </a>
                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>


                </div>
                <!-- Termina Contenido -->

                </div>
            </div>

        </div>


        <footer class="footer contenedor-footer">
            <?php include('footer.php');?>
        </footer>


    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>
</html>
