<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Sala de juntas</div></div>
                  </div>

                  <!-- Inicia una agencia -->
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 padding0 datos-proyecto">

                        <div class="col-xs-12 col-sm-3">
                              <div class="card card-product" data-count="3">
                                  <div class="logo-agencia">
                                    <img class="center-block img-responsive" src="assets/img/Proyectos/img-proyecto4.jpg" alt="Agencia">
                                  </div>
                                  <div class="card-content">
                                      <div class="card-title"><b>Nombre del Proyecto</b></div>
                                      <div class="progress" id="progress-bar">
                                          <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: 100%">
                                              <b><big>100%</big></b>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="card-footer">
                                      <b>Marca:</b><br>
                                      <b>Producto:</b>
                                  </div>
                              </div>
                        </div>

                        <div class="col-xs-12 col-sm-7 contenedor-descripcion-agencia2 margen-movil-servicios">
                            <div class="col-sm-12 texto-gris">
                                  <div class="col-xs-4 col-sm-4">
                                      <div class="card card-stats">
                                          <div class="card-header" data-background-color="blue"><i class="material-icons">timeline</i></div>
                                          <div class="card-content">
                                              <p class="category">Fase</p><h3 class="card-title"><span class="porcentaje-agencia texto-rojo">Inicio</span></h3>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="col-xs-4 col-sm-4">
                                      <div class="card card-stats">
                                          <div class="card-header" data-background-color="blue"><i class="material-icons">timer_off</i></div>
                                          <div class="card-content">
                                              <p class="category">Fin de la fase</p><h3 class="card-title"><span class="porcentaje-agencia texto-morado">11/00/000</span></h3>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="col-xs-4 col-sm-4">
                                      <div class="card card-stats">
                                          <div class="card-header" data-background-color="blue"><i class="material-icons">assignment_ind</i></div>
                                          <div class="card-content">
                                              <p class="category">Agencias</p>
                                              <h3 class="card-title">
                                                  <span class="porcentaje-agencia texto-morado">9</span>
                                                  <a href="">
                                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                                  </a>
                                              </h3>
                                          </div>
                                      </div>
                                  </div>
                            </div>

                            <div class="col-sm-12 basura-proyectos">
                                  <a title="Eliminar Proyecto" href="#eliminar-proyecto" data-toggle="modal"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                  <a title="Pausar Proyecto" href="#pausar-proyecto" data-toggle="modal"><i class="fa fa-pause" aria-hidden="true"></i></a>
                            </div>

                            <div class="col-sm-12 texto-gris">
                                  <small class="texto-rojo">*Recomendamos no invitar a más agencias después de la fase de preguntas y respuestas </small>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-2 iconos-agencias-izquierda text-left">
                            <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                <a title="Favoritos" href="agencias-proyecto.php">
                                  <button class="btn btn-primary btn-round" id="btn-proyecto-propuestas"><i class="material-icons">remove_red_eye</i> Ver Propuestas</button>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                <a title="Editar Proyecto" href="#editar-proyecto" data-toggle="modal">
                                  <button class="btn btn-primary btn-round" id="btn-proyecto-informacion"><i class="material-icons">info</i> Información</button>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                <a title="Ver Agencia" href="sala-juntas.php">
                                  <button class="btn btn-primary btn-round" id="btn-proyecto-juntas"><i class="material-icons">people</i> Sala de Juntas</button>
                                </a>
                            </div>
                        </div>

                    </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Chat</div></div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12 padding0">

                          <div class="conversation-wrap col-sm-3">
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 1</h5><small>Hello</small></div>
                              </div>
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 2</h5><small>Hello</small></div>
                              </div>
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 3</h5><small>Hello</small></div>
                              </div>
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 4</h5><small>Hello</small></div>
                              </div>
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 5</h5><small>Hello</small></div>
                              </div>
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 6</h5><small>Hello</small></div>
                              </div>
                          </div>

                          <div class="message-wrap col-sm-9">
                              <div class="msg-wrap">
                                  <div class="media msg ">
                                      <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                      <div class="media-body">
                                          <small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small>
                                          <h5 class="media-heading">Mi usuario</h5><small>Datos de contacto</small>
                                      </div>
                                  </div>
                                  <div class="alert alert-info msg-date">
                                      <strong>Today</strong>
                                  </div>
                                  <div class="media msg">
                                      <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                      <div class="media-body">
                                          <small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small><h5 class="media-heading">Usuario</h5><small>Mensaje: "Hola"</small>
                                      </div>
                                  </div>
                                  <div class="media msg">
                                      <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                      <div class="media-body">
                                          <small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small><h5 class="media-heading">Usuario</h5><small>Mensaje: "Hola"</small>
                                      </div>
                                  </div>
                                  <div class="media msg">
                                      <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                      <div class="media-body">
                                          <small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small><h5 class="media-heading">Usuario</h5><small>Mensaje: "Hola"</small>
                                      </div>
                                  </div>
                              </div>

                              <div class="send-wrap "><textarea class="form-control send-message" rows="3" placeholder="Write a reply..."></textarea></div>
                              <div class="btn-panel">
                                  <a href="" class=" col-lg-3 btn   send-message-btn " role="button"><i class="fa fa-cloud-upload"></i> Adjuntar archivos</a>
                                  <a href="" class=" col-lg-4 text-right btn   send-message-btn pull-right" role="button"><i class="fa fa-plus"></i> Enviar mensaje</a>
                              </div>
                          </div>
                      </div>

                  </div>
                </div>
                  <!-- Termina Contenido -->

                </div>

                <footer class="footer contenedor-footer">
                    <?php include('footer.php');?>
                </footer>
                
            </div>



        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>
</html>
