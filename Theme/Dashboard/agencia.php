<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                  <!-- Inicia Contenido -->
                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Agencia</div></div>
                  </div>

                  <div class="row contenedor-nuevo-proyecto-dashboard">
                      <div class="col-xs-12 col-sm-12 padding0">

                          <div class="col-xs-12 col-sm-4">
                                <div class="card card-product" data-count="3">
                                    <div class="logo-agencia">
                                      <img class="center-block img-responsive" src="assets/img/Proyectos/img-marinela.png" alt="Agencia">
                                    </div>
                                    <div class="card-content">
                                        <h4 class="card-title"><b>Nombre de la Agencia</b></h4>
                                        <div class="card-description"><p>5 años trabajando para Grupo BIMBO</p></div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="col-xs-4 col-sm-4 border-right text-center">
                                              <div class="description-block">
                                                  <h5 class="description-header certificado-agencia">
                                                    <small class="description-text">Certificado</small><br><i class="fa fa-certificate" aria-hidden="true"></i> <b>AAA</b>
                                                  </h5>
                                              </div>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 border-right text-center">
                                              <div class="description-block">
                                                  <h5 class="description-header texto-rojo">
                                                      <small class="description-text">Ranking</small><br><i class="fa fa-trophy" aria-hidden="true"></i> <b>Excelente</b>
                                                  </h5>
                                              </div>
                                            </div>

                                            <div class="col-xs-4 col-sm-4 text-center">
                                              <div class="description-block">
                                                  <h5 class="description-header texto-verde">
                                                      <small class="description-text">Status</small><br><i class="fa fa-check-circle-o" aria-hidden="true"></i> <b>Activo</b>
                                                  </h5>
                                              </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-12">
                                            <div class="col-xs-6 col-sm-6 iconos-favoritos-agencias">
                                                <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal">
                                                  <button class="btn btn-primary btn-round" id="btn-enviar-brief">Enviar Brief</button>
                                                </a>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 iconos-favoritos-agencias">
                                                <a title="Contactar Agencia" href="#contacto-agencia" data-toggle="modal">
                                                  <button class="btn btn-primary btn-round" id="btn-ver-agencia">Contactar</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                          </div>

                          <div class="col-xs-12 col-sm-8 contenedor-descripcion-agencia2 margen-movil-servicios">
                              <div class="col-sm-12">
                                  <span class="titulo-servicios"><b>Tipo de Servicios:</b></span>
                              </div>

                              <div class="col-sm-12 texto-gris">
                                    <div class="col-sm-12">
                                        <div class="col-xs-4 col-sm-4">
                                            <div class="card card-stats">
                                                <div class="card-header" data-background-color="blue"><i class="material-icons">equalizer</i></div>
                                                <div class="card-content">
                                                    <p class="category">Core services</p><h3 class="card-title"><span class="porcentaje-agencia texto-rojo">41%</span></h3>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-4 col-sm-4">
                                            <div class="card card-stats">
                                                <div class="card-header" data-background-color="blue"><i class="material-icons">equalizer</i></div>
                                                <div class="card-content">
                                                    <p class="category">Core services</p><h3 class="card-title"><span class="porcentaje-agencia texto-morado">50%</span></h3>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-4 col-sm-4">
                                            <div class="card card-stats">
                                                <div class="card-header" data-background-color="blue"><i class="material-icons">equalizer</i></div>
                                                <div class="card-content">
                                                    <p class="category">Core services</p><h3 class="card-title"><span class="porcentaje-agencia texto-amarillo">5%</span></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <span class="titulo-servicios"><b>Filtros:</b></span>
                                    </div>

                                    <div class="col-sm-12">
                                          <span class="label categorias-agencia">Visual Design</span>
                                          <span class="label categorias-agencia">Social Media Marketing</span>
                                          <span class="label categorias-agencia">User research</span>
                                          <span class="label categorias-agencia">Website development</span>
                                          <span class="label categorias-agencia">Gaming</span>
                                          <span class="label categorias-agencia">Promotions</span>
                                    </div>
                                </div>
                          </div>

                      </div>
                  </div>

                  <div class="row bold-agencia">
                      <div class="col-xs-12 col-sm-12 padding0">
                          <div class="col-xs-12 col-sm-6">
                              <div class="col-xs-12 col-sm-12">
                                  <div class="fondo-agencia-1">
                                      <h1 class="titulo-agencia">Empresa global de servicios de ATL y Digitales</h1>
                                      <b>Fundado en:</b> 11 de Febrero 2001<br>
                                      <b>Presidente:</b> Alberto<br>
                                      <b>CEO:</b> Alejandra<br>
                                      <b>Empleados:</b> 150
                                  </div>
                              </div>

                              <div class="col-xs-12 col-sm-12 padding0 img-agencias">
                                  <h1 class="titulo-agencia">Clientes</h1>
                                  <div class="col-xs-12 col-sm-12 padding0">
                                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                          <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                                      </div>
                                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                          <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                                      </div>
                                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                          <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                                      </div>
                                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                          <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                                      </div>
                                  </div>
                                  <div class="col-xs-12 col-sm-12 padding0">
                                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                          <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                                      </div>
                                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                          <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                                      </div>
                                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                          <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                                      </div>
                                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                          <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Agencias en BIMBO" class="img-responsive center-block">
                                      </div>
                                  </div>
                              </div>

                              <div class="col-xs-12 col-sm-12 margen-calificaciones-movil">
                                  <div class="col-sm-12 fondo-agencia-2 text-center">
                                          <div class="col-sm-6">
                                              <div class="content text-center">
                                                  <b>Número de Calificaciones:</b><br>
                                                  <big>4.8/5</big>
                                                  <div class="calificaciones-agencia">
                                                      <h3 class="card-title">
                                                        <div class="iconos-calificacion-agencias">
                                                            <i class="fa fa-star icono-activo" aria-hidden="true"></i>
                                                            <i class="fa fa-star icono-activo" aria-hidden="true"></i>
                                                            <i class="fa fa-star-half-o icono-activo" aria-hidden="true"></i>
                                                            <i class="fa fa-star-o icono-inactivo" aria-hidden="true"></i>
                                                            <i class="fa fa-star-o icono-inactivo" aria-hidden="true"></i>
                                                        </div>
                                                      </h3>
                                                  </div>
                                                  <p class="card-description">
                                                      This is good if your company size is between 2 and 10 Persons.
                                                  </p>
                                              </div>
                                          </div>

                                          <div class="col-sm-6">
                                              <div class="content text-center">
                                                  <b>Número de Calificaciones:</b><br>
                                                  <big>4.8/5</big>
                                                  <div class="calificaciones-agencia2">
                                                      <h3 class="card-title">
                                                        <div class="iconos-calificacion-agencias">
                                                            <i class="fa fa-star icono-activo" aria-hidden="true"></i>
                                                            <i class="fa fa-star icono-activo" aria-hidden="true"></i>
                                                            <i class="fa fa-star-half-o icono-activo" aria-hidden="true"></i>
                                                            <i class="fa fa-star-o icono-inactivo" aria-hidden="true"></i>
                                                            <i class="fa fa-star-o icono-inactivo" aria-hidden="true"></i>
                                                        </div>
                                                      </h3>
                                                  </div>
                                                  <p class="card-description">
                                                      This is good if your company size is between 2 and 10 Persons.
                                                  </p>
                                              </div>
                                          </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-xs-12 col-sm-6">
                              <div class="col-xs-12 col-sm-12">
                                  <div class="fondo-agencia-1">
                                      <h1 class="titulo-agencia">Datos de Contacto</h1>
                                      <b>Dirección:</b> Ciudad de México<br>
                                      <b>Sitio Web:</b> <a href="#" target="blank">www.dominio.mx</a><br>
                                      <b>Teléfono:</b> (55) 5555-5555<br>
                                      <b>Correo:</b> correo@dominio.com<br>
                                      <div class="dirección-mapa">
                                          <adress>Dirección:<br>Av. Central Esq. Santa Fé, México</adress>
                                      </div>
                                      <!--Mapa -->
                                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3762.6247939573045!2d-99.16386104934756!3d19.428610986822218!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fed4c6178105%3A0x3ccb09543b7378a5!2sReforma+222+Centro+Financiero%2C+Ju%C3%A1rez%2C+06600+Ciudad+de+M%C3%A9xico%2C+CDMX!5e0!3m2!1ses-419!2smx!4v1517323764806" frameborder="0" style="border:0" allowfullscreen></iframe>
                                  </div>
                              </div>
                          </div>

                      </div>
                  </div>

                </div>
                  <!-- Termina Contenido -->
                </div>


                <footer class="footer contenedor-footer">
                    <?php include('footer.php');?>
                </footer>


            </div>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>


<script>
    $(document).ready(function() {
        demo.initSmallGoogleMaps();
    });
</script>
</html>
