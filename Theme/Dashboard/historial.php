<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">
                  <!-- Inicia Contenido -->
                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Historial de proyectos</div></div>
                  </div>

                  <div class="row">
                      <div class="col-xs-3 col-sm-2">
                          <select class="selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7" tabindex="-98">
                          <option class="bs-title-option" value="">Organizar por:</option>
                              <option selected="">Organizar por:</option>
                              <option value="2">A-Z</option>
                              <option value="3">Otro</option>
                          </select>
                      </div>
                      <div class="col-xs-9 col-sm-10 pull-left">
                          <div class="col-xs-7 col-sm-9">
                              <div id="wrap">
                                <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                              </div>
                          </div>
                          <div class="col-xs-3 col-sm-2">
                              <div id="btn-filtros">
                                  <div id="accordion" role="tablist" aria-multiselectable="true">
                                      <div role="tab" id="headingOne">
                                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                              <i class="material-icons">filter_list</i> Filtros
                                          </a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-xs-2 col-sm-1 text-center">
                              <div class="icono-favoritos">
                                  <label class="fancy-checkbox" title="Mostrar sólo favoritos">
                                      <input type="checkbox" />
                                      <i class="fa fa-heart-o unchecked"></i>
                                      <i class="fa fa-heart checked"></i>
                                  </label>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12">
                          <?php include('menu-lateral-historial.php');?>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-xs-12 col-sm-12 padding0 contenedor-listado-agencias">
                          <!-- Inicia una agencia -->
                              <a href="historial-interna.php">
                                  <div class="col-xs-12 col-sm-3">
                                      <div class="texto-historial fondo-proyecto-dashboard">
                                          <img src="assets/img/Proyectos/img-proyecto1.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block media__image">
                                          <div class="progress" id="progress-bar">
                                              <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: 100%"><b><big>100%</big></b></div>
                                          </div>
                                          <div class="nombre-historial">Nombre del Proyecto</div>
                                          <small class="texto-gris">15/Enero/2018 - 30/Enero/2018</small><br>
                                          <i class="fa fa-globe" aria-hidden="true"></i>  México<br>
                                          <i class="fa fa-tag" aria-hidden="true"></i>  Categoría<br>
                                          <i class="fa fa-gift" aria-hidden="true"></i>  Producto
                                      </div>
                                  </div>
                              </a>

                              <div class="col-xs-12 col-sm-3 iconos-agencias-top imagen-historial">
                                  <div class="nombre-historial">Agencia Ganadora</div>
                                  <a title="Ganador Seleccionado" href="#ganador-seleccionado" data-toggle="modal">
                                  	<img src="assets/img/Proyectos/img-proyecto.jpg" alt="Perfil Dashboard" class="img-responsive">
                                  </a>
                              </div>

                              <div class="col-xs-12 col-sm-6">
                                  <div class="nombre-historial">Participantes</div>
                                  <div class="contenedor-participantes">
                                      <ul class="list-inline">
                                          <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                              <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                  <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                              </li>
                                          </a>
                                          <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                              <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                  <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                              </li>
                                          </a>
                                          <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                              <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                  <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                              </li>
                                          </a>
                                          <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                              <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                  <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                              </li>
                                          </a>
                                      </ul>
                                  </div>
                             </div>
                             <!-- Termina una agencia -->
                          </div>
                       </div>


                       <div class="row">
                           <div class="col-xs-12 col-sm-12 padding0 contenedor-listado-agencias">
                               <!-- Inicia una agencia -->
                                   <a href="historial-interna.php">
                                       <div class="col-xs-12 col-sm-3">
                                           <div class="texto-historial fondo-proyecto-dashboard">
                                               <img src="assets/img/Proyectos/img-proyecto1.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block media__image">
                                               <div class="progress" id="progress-bar">
                                                   <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: 100%"><b><big>100%</big></b></div>
                                               </div>
                                               <div class="nombre-historial">Nombre del Proyecto</div>
                                               <small class="texto-gris">15/Enero/2018 - 30/Enero/2018</small><br>
                                               <i class="fa fa-globe" aria-hidden="true"></i>  México<br>
                                               <i class="fa fa-tag" aria-hidden="true"></i>  Categoría<br>
                                               <i class="fa fa-gift" aria-hidden="true"></i>  Producto
                                           </div>
                                       </div>
                                   </a>

                                   <div class="col-xs-12 col-sm-3 iconos-agencias-top imagen-historial">
                                       <div class="nombre-historial">Agencia Ganadora</div>
                                       <a title="Ganador Seleccionado" href="#ganador-seleccionado" data-toggle="modal">
                                       	<img src="assets/img/Proyectos/img-proyecto.jpg" alt="Perfil Dashboard" class="img-responsive">
                                       </a>
                                   </div>

                                   <div class="col-xs-12 col-sm-6">
                                       <div class="nombre-historial">Participantes</div>
                                       <div class="contenedor-participantes">
                                           <ul class="list-inline">
                                               <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                                   <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                       <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                                   </li>
                                               </a>
                                               <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                                   <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                       <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                                   </li>
                                               </a>
                                               <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                                   <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                       <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                                   </li>
                                               </a>
                                               <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                                   <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                       <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                                   </li>
                                               </a>
                                           </ul>
                                       </div>
                                  </div>
                                  <!-- Termina una agencia -->
                               </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 padding0 contenedor-listado-agencias">
                                    <!-- Inicia una agencia -->
                                        <a href="historial-interna.php">
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="texto-historial fondo-proyecto-dashboard">
                                                    <img src="assets/img/Proyectos/img-proyecto1.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block media__image">
                                                    <div class="progress" id="progress-bar">
                                                        <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: 100%"><b><big>100%</big></b></div>
                                                    </div>
                                                    <div class="nombre-historial">Nombre del Proyecto</div>
                                                    <small class="texto-gris">15/Enero/2018 - 30/Enero/2018</small><br>
                                                    <i class="fa fa-globe" aria-hidden="true"></i>  México<br>
                                                    <i class="fa fa-tag" aria-hidden="true"></i>  Categoría<br>
                                                    <i class="fa fa-gift" aria-hidden="true"></i>  Producto
                                                </div>
                                            </div>
                                        </a>

                                        <div class="col-xs-12 col-sm-3 iconos-agencias-top imagen-historial">
                                            <div class="nombre-historial">Agencia Ganadora</div>
                                            <a title="Ganador Seleccionado" href="#ganador-seleccionado" data-toggle="modal">
                                            	<img src="assets/img/Proyectos/img-proyecto.jpg" alt="Perfil Dashboard" class="img-responsive">
                                            </a>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <div class="nombre-historial">Participantes</div>
                                            <div class="contenedor-participantes">
                                                <ul class="list-inline">
                                                    <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                                        <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                            <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                                        </li>
                                                    </a>
                                                    <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                                        <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                            <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                                        </li>
                                                    </a>
                                                    <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                                        <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                            <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                                        </li>
                                                    </a>
                                                    <a title="Motivo de Rechazo" href="#rechazado" data-toggle="modal">
                                                        <li class="col-xs-2 list-group-item img-historial2 text-center">
                                                            <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive center-block"><span><i class="fa fa-ban" aria-hidden="true"></i></span>
                                                        </li>
                                                    </a>
                                                </ul>
                                            </div>
                                       </div>
                                       <!-- Termina una agencia -->
                                    </div>
                                 </div>


                        </div>
                    </div>
                    <!-- Termina Contenido -->

                    <footer class="footer contenedor-footer"><?php include('footer.php');?></footer>

          </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>
</html>
