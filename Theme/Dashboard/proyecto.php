<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                  <!-- Inicia Contenido -->
                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Proyecto</div></div>
                  </div>

                  <div class="row">
                    <div class="col-xs-12 col-sm-12 padding0 datos-proyecto">

                        <div class="col-xs-12 col-sm-3">
                              <div class="card card-product" data-count="3">
                                  <div class="logo-agencia"><img class="center-block img-responsive" src="assets/img/Proyectos/img-proyecto4.jpg" alt="Agencia"></div>
                                  <div class="card-content">
                                      <div class="card-title"><b>Nombre del Proyecto</b></div>
                                      <div class="progress" id="progress-bar">
                                          <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: 100%">
                                              <b><big>100%</big></b>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="card-footer">
                                      <b>Marca: Nombre de la marca</b><br>
                                      <b>Producto: Nombre del producto</b>
                                  </div>
                              </div>
                        </div>

                        <div class="col-xs-12 col-sm-7 contenedor-descripcion-agencia2 margen-movil-servicios">
                              <div class="col-sm-12 texto-gris">
                                    <div class="col-xs-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="blue"><i class="material-icons">timeline</i></div>
                                            <div class="card-content">
                                                <p class="category">Fase</p><h3 class="card-title"><span class="porcentaje-agencia texto-rojo">Inicio</span></h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="blue"><i class="material-icons">timer_off</i></div>
                                            <div class="card-content">
                                                <p class="category">Fin de la fase</p><h3 class="card-title"><span class="porcentaje-agencia texto-morado">11/00/000</span></h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="blue"><i class="material-icons">assignment_ind</i></div>
                                            <div class="card-content">
                                                <p class="category">Agencias</p>
                                                <h3 class="card-title">
                                                    <span class="porcentaje-agencia texto-morado">9</span>
                                                    <a href=""><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                              </div>

                              <div class="col-xs-12 col-sm-12 basura-proyectos">
                                  <a title="Eliminar Proyecto" href="#eliminar-proyecto" data-toggle="modal"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                  <a title="Pausar Proyecto" href="#pausar-proyecto" data-toggle="modal"><i class="fa fa-pause" aria-hidden="true"></i></a>
                                  <a title="Cancelar Proyecto" href="#cancelar-proyecto" data-toggle="modal"><i class="fa fa-times-circle"></i></a>
                              </div>

                              <div class="col-xs-12 col-sm-12 texto-gris">
                                  <small class="texto-rojo">*Recomendamos no invitar a más agencias después de la fase de preguntas y respuestas </small>
                              </div>
                        </div>

                        <div class="col-xs-12 col-sm-2 iconos-agencias-izquierda text-left">
                            <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                                <a title="Favoritos" href="agencias-proyecto.php">
                                  <button class="btn btn-primary btn-round" id="btn-proyecto-propuestas"><i class="material-icons">remove_red_eye</i> Ver Propuestas</button>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                                <a title="Editar Proyecto" href="#editar-proyecto" data-toggle="modal">
                                  <button class="btn btn-primary btn-round" id="btn-proyecto-informacion"><i class="material-icons">info</i> Información</button>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                                <a title="Ver Agencia" href="sala-juntas.php">
                                  <button class="btn btn-primary btn-round" id="btn-proyecto-juntas"><i class="material-icons">people</i> Sala de Juntas</button>
                                </a>
                            </div>
                        </div>

                    </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Agencias relacionadas</div></div>
                  </div>

                  <div class="row">
                      <div class="col-xs-12 col-sm-4">
                          <div class="col-xs-12 col-sm-12 padding0">
                              <div class="col-xs-4 col-sm-4">
                                  <select class="selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7" tabindex="-98">
                                  <option class="bs-title-option" value="">Organizar por:</option>
                                      <option selected="">Organizar por:</option>
                                      <option value="2">A-Z</option>
                                      <option value="3">Otro</option>
                                  </select>
                              </div>
                              <div class="col-xs-4 col-sm-4">
                                  <select class="selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7" tabindex="-98">
                                  <option class="bs-title-option" value="">Agregados recientemente:</option>
                                      <option selected="">Agregados recientemente:</option>
                                      <option value="2">A-Z</option>
                                      <option value="3">Otro</option>
                                  </select>
                              </div>
                              <div class="col-xs-4 col-sm-4">
                                  <select class="selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7" tabindex="-98">
                                  <option class="bs-title-option" value="">Más cercanos:</option>
                                      <option selected="">Más cercanos:</option>
                                      <option value="2">A-Z</option>
                                      <option value="3">Otro</option>
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-8 pull-left">
                          <div class="col-xs-7 col-sm-9">
                              <div id="wrap">
                                <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                              </div>
                          </div>
                          <div class="col-xs-3 col-sm-2">
                              <div id="btn-filtros">
                                  <div id="accordion" role="tablist" aria-multiselectable="true">
                                      <div role="tab" id="headingOne">
                                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                              <i class="material-icons">filter_list</i> Filtros
                                          </a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-xs-2 col-sm-1 text-center">
                              <div class="icono-favoritos">
                                  <label class="fancy-checkbox" title="Mostrar sólo favoritos">
                                      <input type="checkbox" />
                                      <i class="fa fa-heart-o unchecked"></i>
                                      <i class="fa fa-heart checked"></i>
                                  </label>
                              </div>
                          </div>
                      </div>
                  </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?php include('menu-lateral-agencias.php');?>
                        </div>
                    </div>

                    <div class="row">
                          <!-- Inicia una agencia -->
                          <div class="col-xs-12 col-sm-12 contenedor-listado-agencias">
                              <div class="col-xs-4 col-sm-2 padding0 img-agencias">
                                  <div class="img-texto text-center">
                                      <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO"  class="img-responsive center-block media__image">
                                      <span class="img-bandera-agencia">
                                          <img src="assets/img/Banderas/mexico.jpg" alt="Proyectos en BIMBO"  class="img-responsive img-circle">
                                      </span>
                                  </div>
                              </div>
                              <div class="col-xs-8 col-sm-2">
                                  <span class="nombre-agencias">Nombre de la Agencia</span><br>
                                  <b>Certificado AA</b><br>
                                  2 años trabajando para:<br>
                                  Grupo BIMBO<br>
                                  Estatus:<br>
                                  <button class="btn btn-primary btn-round excelente">
                                      <i class="material-icons">check_circle</i> Activo
                                  </button>
                              </div>
                              <div class="col-xs-12 col-sm-6 iconos-agencias-top">
                                  <div class="col-xs-12 col-sm-12 texto-datos-agencia">
                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="green">
                                                  <i class="material-icons">place</i>
                                              </div>
                                              <div class="card-content">
                                                  <p class="category">Dirección<br>Agencia</p>
                                              </div>
                                              <div class="card-footer">
                                                  <div class="stats">
                                                      Texto de la Dirección
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="blue">
                                                  <i class="material-icons">person</i>
                                              </div>
                                              <div class="card-content">
                                                  <p class="category">Categoría<br>Agencia</p>
                                              </div>
                                              <div class="card-footer">
                                                  <div class="stats">
                                                       Categoría
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="orange">
                                                  <i class="material-icons">stars</i>
                                              </div>
                                              <div class="card-content">
                                                  <p class="category">Ranking<br>Usuarios</p>
                                              </div>
                                              <div class="card-footer iconos-ranking">
                                                  <div class="stats" style="color:green">
                                                       <big><i class="material-icons">verified_user</i> Excelente</big>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-xs-12 col-sm-2 iconos-agencias-izquierda">
                                  <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Favoritos" href="#">
                                        <button class="btn btn-primary btn-round" id="btn-favoritos"><i class="material-icons">favorite</i> Favorito</button>
                                      </a>
                                  </div>
                                  <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Invitar Agencia" href="#invitar-proyecto" data-toggle="modal">
                                        <button class="btn btn-primary btn-round" id="btn-enviar-brief"><i class="material-icons">insert_invitation</i> Invitar agencia</button>
                                      </a>
                                  </div>
                                  <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Ver Agencia" href="#">
                                        <button class="btn btn-primary btn-round" id="btn-ver-agencia"><i class="material-icons">remove_red_eye</i> Ver proyecto</button>
                                      </a>
                                  </div>
                              </div>
                          </div>
                          <!-- Termina una agencia -->

                          <!-- Inicia una agencia -->
                          <div class="col-xs-12 col-sm-12 contenedor-listado-agencias">
                              <div class="col-xs-4 col-sm-2 padding0 img-agencias">
                                  <div class="img-texto text-center">
                                      <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO"  class="img-responsive center-block media__image">
                                      <span class="img-bandera-agencia">
                                          <img src="assets/img/Banderas/mexico.jpg" alt="Proyectos en BIMBO"  class="img-responsive img-circle">
                                      </span>
                                  </div>
                              </div>
                              <div class="col-xs-8 col-sm-2">
                                  <span class="nombre-agencias">Nombre de la Agencia</span><br>
                                  <b>Certificado AA</b><br>
                                  2 años trabajando para:<br>
                                  Grupo BIMBO<br>
                                  Estatus:<br>
                                  <button class="btn btn-primary btn-round excelente">
                                      <i class="material-icons">check_circle</i> Activo
                                  </button>
                              </div>
                              <div class="col-xs-12 col-sm-6 iconos-agencias-top">
                                  <div class="col-xs-12 col-sm-12 texto-datos-agencia">
                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="green">
                                                  <i class="material-icons">place</i>
                                              </div>
                                              <div class="card-content">
                                                  <p class="category">Dirección<br>Agencia</p>
                                              </div>
                                              <div class="card-footer">
                                                  <div class="stats">
                                                      Texto de la Dirección
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="blue">
                                                  <i class="material-icons">person</i>
                                              </div>
                                              <div class="card-content">
                                                  <p class="category">Categoría<br>Agencia</p>
                                              </div>
                                              <div class="card-footer">
                                                  <div class="stats">
                                                       Categoría
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="orange">
                                                  <i class="material-icons">stars</i>
                                              </div>
                                              <div class="card-content">
                                                  <p class="category">Ranking<br>Usuarios</p>
                                              </div>
                                              <div class="card-footer iconos-ranking">
                                                  <div class="stats" style="color:green">
                                                       <big><i class="material-icons">verified_user</i> Excelente</big>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-xs-12 col-sm-2 iconos-agencias-izquierda">
                                  <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Favoritos" href="#">
                                        <button class="btn btn-primary btn-round" id="btn-favoritos"><i class="material-icons">favorite</i> Favorito</button>
                                      </a>
                                  </div>
                                  <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Ver Agencia" href="#">
                                        <button class="btn btn-primary btn-round" id="btn-ver-agencia"><i class="material-icons">remove_red_eye</i> Ver proyecto</button>
                                      </a>
                                  </div>
                                  <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Invitación rechazada" href="#rechazo-proyecto" data-toggle="modal">
                                        <button class="btn btn-primary btn-round" id="btn-favoritos"><i class="material-icons">cancel</i> Invitación rechazada</button>
                                      </a>
                                  </div>
                              </div>
                          </div>
                          <!-- Termina una agencia -->
                        </div>
                  </div>
                  <!-- Termina Contenido -->

                </div>

                <footer class="footer contenedor-footer">
                    <?php include('footer.php');?>
                </footer>

            </div>

        </div>

    </div>

</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>
</html>
