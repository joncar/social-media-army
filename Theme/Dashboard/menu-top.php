<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container-fluid">
        <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                <i class="material-icons visible-on-sidebar-mini">view_list</i>
            </button>
        </div> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url("panel") ?>"> <b><?= $this->user->nombre ?> <?= $this->user->apellido ?> </b> </a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="material-icons">messages</i>
                        <span class="notification">5</span>
                        <p class="hidden-lg hidden-md">Notifications<b class="caret"></b></p>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#">Mike John responded to your email</a>
                        </li>
                        <li>
                            <a href="#">You have 5 new tasks</a>
                        </li>
                        <li>
                            <a href="#">You're now friend with Andrew</a>
                        </li>
                        <li>
                            <a href="#">Another Notification</a>
                        </li>
                        <li>
                            <a href="#">Another One</a>
                        </li>
                    </ul>
                </li>
                <!-- Perfil -->
                <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <span class="hidden-xs">
                        <i class="material-icons">person</i>
                      </span>
                  </a>
                  <ul class="dropdown-menu">
                      <li class="user-header text-center texto-gris">
                          <img src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" class="user-image" alt="User Image">
                          <p><?= $this->user->nombre ?><br>
                          <?= $this->user->puesto ?><br>
                          <small>En la plataforma desde <?= date("Y",strtotime($this->user->fecha_registro)) ?></small></p>
                      </li>
                      <li class="user-footer">
                          <a href="<?= base_url('main/unlog') ?>" class="btn btn-default btn-flat" id="btn-notificaciones-top">Cerrar Sesión  <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                      </li>
                  </ul>
                </li>
                <li class="separator hidden-lg hidden-md"></li>
            </ul>

        </div>
    </div>
</nav>
