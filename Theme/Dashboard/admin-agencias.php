<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 padding0 titulo-secccion titulo-top">
                            Nombre de la Agencia
                        </div>
                    </div>

                    <div class="row">
                        <div class="card-content">
                            <div class="menu-filtros">
                                <ul class="nav nav-pills nav-pills-warning">
                                    <li class="active"><a href="#pill1" data-toggle="tab" aria-expanded="true">Usuarios</a></li>
                                    <li class=""><a href="#pill2" data-toggle="tab" aria-expanded="false">Formulario</a></li>
                                    <li class=""><a href="#pill3" data-toggle="tab" aria-expanded="false">Calificaciones</a></li>
                                </ul>
                            </div>

                            <div class="tab-content contenedor-facturas">

                                <div class="tab-pane active" id="pill1">
                                    <?= $users ?>
                                </div>

                                <div class="tab-pane" id="pill2">                                  
                                  <?= $output ?>
                                </div>


                                <div class="tab-pane" id="pill3">
                                  <?php 
                                    $ranking = $this->db->query("
                                      SELECT 
                                      COALESCE(TRUNCATE(SUM(estrategia)/COUNT(id),0)) as estrategia,
                                      COALESCE(TRUNCATE(SUM(servicio_cliente)/COUNT(id),0)) as servicio_cliente,
                                      COALESCE(TRUNCATE(SUM(ejecucion)/COUNT(id),0)) as ejecucion,
                                      COALESCE(TRUNCATE(SUM(satisfaccion)/COUNT(id),0)) as satisfaccion,
                                      COALESCE(COUNT(id)) as cantidad
                                      FROM agencias_ranking
                                      WHERE agencias_id = ".$agencia."
                                    ");
                                    $ranking = $ranking->row();
                                  ?>
                                  <div class="col-sm-12">
                                      <div class="col-sm-7">
                                          <div class="fondo-agencia-1">
                                              <h1 class="titulo-agencia">Calificaciones
                                              Número de calificaciones: <?= $ranking->cantidad ?><br>
                                              <b class="texto-amarillo"><big><?php
                                                $rank = ($ranking->estrategia+$ranking->servicio_cliente+$ranking->satisfaccion+$ranking->ejecucion)/4;
                                                echo round($rank,0);
                                              ?>/5</big></b><br>
                                              <?php $valor = $rank; ?>                                              
                                              <i class="material-icons <?= $valor>=1?'texto-amarillo':'texto-gris'; ?>">star</i>
                                              <i class="material-icons <?= $valor>=2?'texto-amarillo':'texto-gris'; ?>">star</i>
                                              <i class="material-icons <?= $valor>=3?'texto-amarillo':'texto-gris'; ?>">star</i>
                                              <i class="material-icons <?= $valor>=4?'texto-amarillo':'texto-gris'; ?>">star</i>
                                              <i class="material-icons <?= $valor>=5?'texto-amarillo':'texto-gris'; ?>">star</i>
                                              </h1>
                                          </div>
                                      </div>

                                      <div class="col-sm-5">
                                          <div class="col-sm-12">
                                                        <b>Estrategia:</b> 
                                                        <?php $valor = $ranking->estrategia; ?>
                                                        <b class="texto-amarillo"><big><?= $valor  ?>/5</big></b><br>
                                                        <i class="material-icons <?= $valor>=1?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=2?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=3?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=4?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=5?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <?php $valor = $ranking->servicio_cliente; ?>
                                                        <b>Calidad de servicio al cliente:</b> <b class="<?= $valor>=1?'texto-amarillo':'texto-gris'; ?>"><big><?= $valor ?>/5</big></b><br>                                                        
                                                        <i class="material-icons <?= $valor>=2?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=3?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=4?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=5?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <?php $valor = $ranking->ejecucion; ?>
                                                        <b>Calidad en ejecución:</b> <b class="<?= $valor>=1?'texto-amarillo':'texto-gris'; ?>"><big><?= $valor ?>/5</big></b><br>                                                        
                                                        <i class="material-icons <?= $valor>=2?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=3?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=4?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=5?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <?php $valor = $ranking->satisfaccion; ?>
                                                        <b>Satisfacción del cliente:</b> <b class="<?= $valor>=1?'texto-amarillo':'texto-gris'; ?>"><big><?= $valor ?>/5</big></b><br>                                                        
                                                        <i class="material-icons <?= $valor>=2?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=3?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=4?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=5?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                    </div>
                                          </div>
                                      </div>

                                  <?php 
                                    $this->db->select('proyectos.nombre as proyecto,agencias_ranking.*,user.nombre, user.apellido');
                                    $this->db->join('proyectos','proyectos.id = agencias_ranking.proyectos_id');
                                    $this->db->join('proyectos_user','proyectos_user.proyectos_id = agencias_ranking.proyectos_id');
                                    $this->db->join('user','user.id = proyectos_user.user_id');
                                    $this->db->group_by('agencias_ranking.proyectos_id');
                                    foreach($this->db->get_where('agencias_ranking',array('agencias_id'=>$agencia))->result() as $ranking): 

                                  ?>
                                    <div class="col-sm-12">                                  
                                        <div class="fondo-agencia-1 col-sm-12">                                          
                                            <div class="col-sm-12">
                                                <div class="col-sm-9">
                                                    <h1 class="titulo-agencia">Cliente</h1>
                                                    <b class="texto-verde">Proyecto:</b> <?= $ranking->proyecto ?><br>
                                                    <b class="texto-verde">Usuario:</b> <?= $ranking->apellido ?>, <?= $ranking->nombre ?>
                                                </div>

                                                <div class="col-sm-3">
                                                    <h1 class="titulo-agencia">Fecha:</h1>
                                                    <b class="texto-verde"><?= date("d/m/Y",strtotime($ranking->fecha)) ?></b>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-9">
                                                    <?= $ranking->comentario ?>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="col-sm-12">
                                                        <b>Estrategia:</b> 
                                                        <?php $valor = $ranking->estrategia; ?>
                                                        <b class="texto-amarillo"><big><?= $valor  ?>/5</big></b><br>
                                                        <i class="material-icons <?= $valor>=1?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=2?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=3?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=4?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=5?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <?php $valor = $ranking->servicio_cliente; ?>
                                                        <b>Calidad de servicio al cliente:</b> <b class="<?= $valor>=1?'texto-amarillo':'texto-gris'; ?>"><big><?= $valor ?>/5</big></b><br>                                                        
                                                        <i class="material-icons <?= $valor>=2?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=3?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=4?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=5?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <?php $valor = $ranking->ejecucion; ?>
                                                        <b>Calidad en ejecución:</b> <b class="<?= $valor>=1?'texto-amarillo':'texto-gris'; ?>"><big><?= $valor ?>/5</big></b><br>                                                        
                                                        <i class="material-icons <?= $valor>=2?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=3?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=4?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=5?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <?php $valor = $ranking->satisfaccion; ?>
                                                        <b>Satisfacción del cliente:</b> <b class="<?= $valor>=1?'texto-amarillo':'texto-gris'; ?>"><big><?= $valor ?>/5</big></b><br>                                                        
                                                        <i class="material-icons <?= $valor>=2?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=3?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=4?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                        <i class="material-icons <?= $valor>=5?'texto-amarillo':'texto-gris'; ?>">star</i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  <?php endforeach ?>

                                </div>

                        </div>
                    </div>
                </div>



            </div>




        </div>

        <footer class="footer contenedor-footer">
            <?php include('footer.php');?>
        </footer>
        <!-- Termina Contenido -->

    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>
</html>
