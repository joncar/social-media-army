<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>
            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 padding0 titulo-secccion titulo-top">
                            Panel de administración
                        </div>
                    </div>

                    <!-- Inicia una agencia -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 contenedor-listado-agencias">
                            <div class="texto-datos-agencia">
                                <div class="col-xs-4 col-sm-3">
                                    <div class="card card-stats">
                                        <div class="card-header" data-background-color="green"><i class="material-icons">done_all</i></div><br>
                                        <div class="card-content"><p class="category">Clientes Registrados</p></div>
                                        <div class="card-footer"><div class="stats">76</div></div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3">
                                    <div class="card card-stats">
                                        <div class="card-header" data-background-color="blue"><i class="material-icons">touch_app</i></div><br>
                                        <div class="card-content"><p class="category">Agencias Registradas</p></div>
                                        <div class="card-footer"><div class="stats">581</div></div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3">
                                    <div class="card card-stats">
                                        <div class="card-header" data-background-color="orange"><i class="material-icons">grade</i></div><br>
                                        <div class="card-content"><p class="category">Proyectos Activos</p></div>
                                        <div class="card-footer"><div class="stats">105</div></div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3">
                                    <div class="card card-stats">
                                        <div class="card-header" data-background-color="pink"><i class="material-icons">supervisor_account</i></div><br>
                                        <div class="card-content"><p class="category">Usuarios Totales</p></div>
                                        <div class="card-footer"><div class="stats">1538</div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                     <!-- Termina una agencia -->

                     <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                  <div class="card">
                                    <div id="colouredBarsChart" class="ct-chart"><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="300px" class="ct-chart-line" style="width: 100%; height: 300px;"><g class="ct-grids"><line y1="265" y2="265" x1="50" x2="716.8333129882812" class="ct-grid ct-vertical"></line><line y1="240" y2="240" x1="50" x2="716.8333129882812" class="ct-grid ct-vertical"></line><line y1="215" y2="215" x1="50" x2="716.8333129882812" class="ct-grid ct-vertical"></line><line y1="190" y2="190" x1="50" x2="716.8333129882812" class="ct-grid ct-vertical"></line><line y1="165" y2="165" x1="50" x2="716.8333129882812" class="ct-grid ct-vertical"></line><line y1="140" y2="140" x1="50" x2="716.8333129882812" class="ct-grid ct-vertical"></line><line y1="115" y2="115" x1="50" x2="716.8333129882812" class="ct-grid ct-vertical"></line><line y1="90" y2="90" x1="50" x2="716.8333129882812" class="ct-grid ct-vertical"></line><line y1="65" y2="65" x1="50" x2="716.8333129882812" class="ct-grid ct-vertical"></line><line y1="40" y2="40" x1="50" x2="716.8333129882812" class="ct-grid ct-vertical"></line><line y1="15" y2="15" x1="50" x2="716.8333129882812" class="ct-grid ct-vertical"></line></g><g><g class="ct-series ct-series-a"><path d="M50,193.25C60.104,189.167,90.414,177.208,110.621,168.75C130.828,160.292,151.035,149.542,171.242,142.5C191.449,135.458,211.657,130.5,231.864,126.5C252.071,122.5,272.278,124.5,292.485,118.5C312.692,112.5,332.899,95.042,353.106,90.5C373.313,85.958,393.52,93.5,413.727,91.25C433.934,89,454.141,80.875,474.348,77C494.556,73.125,514.763,71.917,534.97,68C555.177,64.083,575.384,60,595.591,53.5C615.798,47,646.109,33.083,656.212,29" class="ct-line"></path><line x1="50" y1="193.25" x2="50.01" y2="193.25" class="ct-point" ct:value="287" opacity="1"></line><line x1="110.62121027166194" y1="168.75" x2="110.63121027166194" y2="168.75" class="ct-point" ct:value="385" opacity="1"></line><line x1="171.24242054332387" y1="142.5" x2="171.25242054332386" y2="142.5" class="ct-point" ct:value="490" opacity="1"></line><line x1="231.86363081498578" y1="126.5" x2="231.87363081498577" y2="126.5" class="ct-point" ct:value="554" opacity="1"></line><line x1="292.48484108664775" y1="118.5" x2="292.49484108664774" y2="118.5" class="ct-point" ct:value="586" opacity="1"></line><line x1="353.1060513583096" y1="90.5" x2="353.1160513583096" y2="90.5" class="ct-point" ct:value="698" opacity="1"></line><line x1="413.72726162997157" y1="91.25" x2="413.73726162997156" y2="91.25" class="ct-point" ct:value="695" opacity="1"></line><line x1="474.3484719016335" y1="77" x2="474.3584719016335" y2="77" class="ct-point" ct:value="752" opacity="1"></line><line x1="534.9696821732955" y1="68" x2="534.9796821732955" y2="68" class="ct-point" ct:value="788" opacity="1"></line><line x1="595.5908924449574" y1="53.5" x2="595.6008924449574" y2="53.5" class="ct-point" ct:value="846" opacity="1"></line><line x1="656.2121027166193" y1="29" x2="656.2221027166192" y2="29" class="ct-point" ct:value="944" opacity="1"></line></g><g class="ct-series ct-series-b"><path d="M50,248.25C60.104,244.708,90.414,230.167,110.621,227C130.828,223.833,151.035,234.875,171.242,229.25C191.449,223.625,211.657,201.25,231.864,193.25C252.071,185.25,272.278,187.417,292.485,181.25C312.692,175.083,332.899,160.5,353.106,156.25C373.313,152,393.52,160.083,413.727,155.75C433.934,151.417,454.141,134.625,474.348,130.25C494.556,125.875,514.763,129.708,534.97,129.5C555.177,129.292,575.384,133.375,595.591,129C615.798,124.625,646.109,107.542,656.212,103.25" class="ct-line"></path><line x1="50" y1="248.25" x2="50.01" y2="248.25" class="ct-point" ct:value="67" opacity="1"></line><line x1="110.62121027166194" y1="227" x2="110.63121027166194" y2="227" class="ct-point" ct:value="152" opacity="1"></line><line x1="171.24242054332387" y1="229.25" x2="171.25242054332386" y2="229.25" class="ct-point" ct:value="143" opacity="1"></line><line x1="231.86363081498578" y1="193.25" x2="231.87363081498577" y2="193.25" class="ct-point" ct:value="287" opacity="1"></line><line x1="292.48484108664775" y1="181.25" x2="292.49484108664774" y2="181.25" class="ct-point" ct:value="335" opacity="1"></line><line x1="353.1060513583096" y1="156.25" x2="353.1160513583096" y2="156.25" class="ct-point" ct:value="435" opacity="1"></line><line x1="413.72726162997157" y1="155.75" x2="413.73726162997156" y2="155.75" class="ct-point" ct:value="437" opacity="1"></line><line x1="474.3484719016335" y1="130.25" x2="474.3584719016335" y2="130.25" class="ct-point" ct:value="539" opacity="1"></line><line x1="534.9696821732955" y1="129.5" x2="534.9796821732955" y2="129.5" class="ct-point" ct:value="542" opacity="1"></line><line x1="595.5908924449574" y1="129" x2="595.6008924449574" y2="129" class="ct-point" ct:value="544" opacity="1"></line><line x1="656.2121027166193" y1="103.25" x2="656.2221027166192" y2="103.25" class="ct-point" ct:value="647" opacity="1"></line></g><g class="ct-series ct-series-c"><path d="M50,259.25C60.104,255.5,90.414,238.583,110.621,236.75C130.828,234.917,151.035,251.458,171.242,248.25C191.449,245.042,211.657,224.667,231.864,217.5C252.071,210.333,272.278,210.125,292.485,205.25C312.692,200.375,332.899,191.125,353.106,188.25C373.313,185.375,393.52,193.5,413.727,188C433.934,182.5,454.141,159.5,474.348,155.25C494.556,151,514.763,161.292,534.97,162.5C555.177,163.708,575.384,166.625,595.591,162.5C615.798,158.375,646.109,141.875,656.212,137.75" class="ct-line"></path><line x1="50" y1="259.25" x2="50.01" y2="259.25" class="ct-point" ct:value="23" opacity="1"></line><line x1="110.62121027166194" y1="236.75" x2="110.63121027166194" y2="236.75" class="ct-point" ct:value="113" opacity="1"></line><line x1="171.24242054332387" y1="248.25" x2="171.25242054332386" y2="248.25" class="ct-point" ct:value="67" opacity="1"></line><line x1="231.86363081498578" y1="217.5" x2="231.87363081498577" y2="217.5" class="ct-point" ct:value="190" opacity="1"></line><line x1="292.48484108664775" y1="205.25" x2="292.49484108664774" y2="205.25" class="ct-point" ct:value="239" opacity="1"></line><line x1="353.1060513583096" y1="188.25" x2="353.1160513583096" y2="188.25" class="ct-point" ct:value="307" opacity="1"></line><line x1="413.72726162997157" y1="188" x2="413.73726162997156" y2="188" class="ct-point" ct:value="308" opacity="1"></line><line x1="474.3484719016335" y1="155.25" x2="474.3584719016335" y2="155.25" class="ct-point" ct:value="439" opacity="1"></line><line x1="534.9696821732955" y1="162.5" x2="534.9796821732955" y2="162.5" class="ct-point" ct:value="410" opacity="1"></line><line x1="595.5908924449574" y1="162.5" x2="595.6008924449574" y2="162.5" class="ct-point" ct:value="410" opacity="1"></line><line x1="656.2121027166193" y1="137.75" x2="656.2221027166192" y2="137.75" class="ct-point" ct:value="509" opacity="1"></line></g></g><g class="ct-labels"><foreignObject style="overflow: visible;" x="50" y="270" width="60.62121027166193" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 61px; height: 20px;">'06</span></foreignObject><foreignObject style="overflow: visible;" x="110.62121027166194" y="270" width="60.62121027166193" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 61px; height: 20px;">'07</span></foreignObject><foreignObject style="overflow: visible;" x="171.24242054332387" y="270" width="60.62121027166192" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 61px; height: 20px;">'08</span></foreignObject><foreignObject style="overflow: visible;" x="231.86363081498578" y="270" width="60.62121027166194" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 61px; height: 20px;">'09</span></foreignObject><foreignObject style="overflow: visible;" x="292.48484108664775" y="270" width="60.62121027166191" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 61px; height: 20px;">'10</span></foreignObject><foreignObject style="overflow: visible;" x="353.1060513583096" y="270" width="60.62121027166194" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 61px; height: 20px;">'11</span></foreignObject><foreignObject style="overflow: visible;" x="413.72726162997157" y="270" width="60.62121027166194" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 61px; height: 20px;">'12</span></foreignObject><foreignObject style="overflow: visible;" x="474.3484719016335" y="270" width="60.62121027166194" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 61px; height: 20px;">'13</span></foreignObject><foreignObject style="overflow: visible;" x="534.9696821732955" y="270" width="60.62121027166194" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 61px; height: 20px;">'14</span></foreignObject><foreignObject style="overflow: visible;" x="595.5908924449574" y="270" width="60.62121027166188" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 61px; height: 20px;">'15</span></foreignObject><foreignObject style="overflow: visible;" x="656.2121027166193" y="270" width="60.621210271661994" height="20"><span class="ct-label ct-horizontal ct-end" xmlns="http://www.w3.org/2000/xmlns/" style="width: 61px; height: 20px;"></span></foreignObject><foreignObject style="overflow: visible;" y="240" x="10" height="25" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 25px; width: 30px;">0</span></foreignObject><foreignObject style="overflow: visible;" y="215" x="10" height="25" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 25px; width: 30px;">100</span></foreignObject><foreignObject style="overflow: visible;" y="190" x="10" height="25" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 25px; width: 30px;">200</span></foreignObject><foreignObject style="overflow: visible;" y="165" x="10" height="25" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 25px; width: 30px;">300</span></foreignObject><foreignObject style="overflow: visible;" y="140" x="10" height="25" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 25px; width: 30px;">400</span></foreignObject><foreignObject style="overflow: visible;" y="115" x="10" height="25" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 25px; width: 30px;">500</span></foreignObject><foreignObject style="overflow: visible;" y="90" x="10" height="25" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 25px; width: 30px;">600</span></foreignObject><foreignObject style="overflow: visible;" y="65" x="10" height="25" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 25px; width: 30px;">700</span></foreignObject><foreignObject style="overflow: visible;" y="40" x="10" height="25" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 25px; width: 30px;">800</span></foreignObject><foreignObject style="overflow: visible;" y="15" x="10" height="25" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 25px; width: 30px;">900</span></foreignObject><foreignObject style="overflow: visible;" y="-15" x="10" height="30" width="30"><span class="ct-label ct-vertical ct-start" xmlns="http://www.w3.org/2000/xmlns/" style="height: 30px; width: 30px;">1000</span></foreignObject></g></svg></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15054.475891433536!2d-99.2619286!3d19.3856443!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1519761064985" width="auto" height="auto" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                     </div>

                    <div class="row margen-clientes-dashboard">
                        <div class="col-xs-12 col-sm-12 padding0">
                            <div class="col-xs-12 col-sm-12 contenedor-agencias-dashboard">
                                <div class="col-xs-12 col-sm-12 contenedor-titulo-agencias-dashboard">
                                  <div class="col-xs-12 col-sm-6 titulo-agencias-dashboard">
                                      Clientes recientes
                                  </div>
                                  <div class="col-xs-12 col-sm-6 btn-agencias-dashboard">
                                  	  <a href="clientes.php">
                                          <button class="btn btn-primary btn-round" title="Ver Proyectos">
                                              Ver Todos<div class="ripple-container"></div>
                                          </button>
                                      </a>
                                  </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 padding0">
                                	<a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-pepsi.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Cliente</b><br>
                                              <small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-cheetos.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Cliente</b><br>
                                              <small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Cliente</b><br>
                                              <small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Cliente</b><br>
                                              <small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Cliente</b><br>
                                              <small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Cliente</b><br>
                                              <small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row margen-clientes-dashboard">
                        <div class="col-xs-12 col-sm-12 padding0">
                            <div class="col-xs-12 col-sm-12 contenedor-agencias-dashboard">
                                <div class="col-xs-12 col-sm-12 contenedor-titulo-agencias-dashboard">
                                  <div class="col-xs-12 col-sm-6 titulo-agencias-dashboard">
                                      Últimos Proyectos
                                  </div>
                                  <div class="col-xs-12 col-sm-6 btn-agencias-dashboard">
                                  	  <a href="proyectos.php">
                                          <button class="btn btn-primary btn-round" title="Ver Proyectos">
                                              Ver Todos<div class="ripple-container"></div>
                                          </button>
                                      </a>
                                  </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 padding0">
                                	 <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-pepsi.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Proyecto</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-cheetos.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Proyecto</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Proyecto</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Proyecto</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Proyecto</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Proyecto</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12 col-sm-12 padding0">
                            <div class="col-xs-12 col-sm-12 contenedor-agencias-dashboard">
                                <div class="col-xs-12 col-sm-12 contenedor-titulo-agencias-dashboard">
                                  <div class="col-xs-12 col-sm-6 titulo-agencias-dashboard">
                                      Agencias recientes
                                  </div>
                                  <div class="col-xs-12 col-sm-6 btn-agencias-dashboard">
                                  	  <a href="agencias.php">
                                          <button class="btn btn-primary btn-round" title="Ver Proyectos">
                                              Ver Todos<div class="ripple-container"></div>
                                          </button>
                                      </a>
                                  </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 padding0">
                                	 <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-pepsi.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Agencia</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-cheetos.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Agencia</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Agencia</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Agencia</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Agencia</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div class="col-xs-6 col-sm-2 links-newsfeed">
                                            <img src="assets/img/Proyectos/img-wacom.png" alt="Perfil Dashboard" class="img-responsive center-block image">
                                            <div class="text-center texto-agencias-dashboard">
                                              <b>Agencia</b><br><small>Hace 5 minutos</small>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <footer class="footer contenedor-footer">
                <?php include('footer.php');?>
            </footer>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<script src="<?= base_url() ?>assets/js/main.js"></script>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>

<script type="text/javascript">
    $(document).ready(function() {
        md.initSliders()
        demo.initFormExtendedDatetimepickers();
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {

        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

        demo.initVectorMap();
    });
</script>
</html>
