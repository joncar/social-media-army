<div class="sidebar-wrapper">

    <div class="user">
        <div class="photo"><img src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" alt="Perfil Dashboard" class="img-responsive center-block"></div>
        <div class="clearfix"></div>
    </div>

    <ul class="nav">
        <li class="active">
            <a href="<?= base_url('panel') ?>">
                <i class="material-icons">home</i><p> Inicio </p>
            </a>
        </li>

        <li>
            <a data-toggle="collapse" href="#pagesExamples" class="collapsed" aria-expanded="false">
                <i class="material-icons">assignment_ind</i><p> Clientes <b class="caret"></b> </p>
            </a>
            <div class="collapse" id="pagesExamples" aria-expanded="false" style="height: 0px;">
                <ul class="nav">
                    <li><a href="<?= base_url('dashboard/clientes/clientes') ?>"><span class="sidebar-normal"> Clientes </span></a></li>
                    <li><a href="<?= base_url('dashboard/admin/user') ?>"><span class="sidebar-normal"> Usuarios </span></a></li>
                </ul>
            </div>
        </li>

        <li>
            <a href="<?= base_url('dashboard/agencias/agencias') ?>">
                <i class="material-icons">redeem</i><p> Agencias </p>
            </a>
        </li>
        <li>
            <a href="<?= base_url('dashboard/proyectos/proyectos') ?>">
                 <i class="material-icons">movie_filter</i><p> Proyectos </p>
            </a>
        </li>
        <li>
            <a href="soporte.php">
                <i class="material-icons">settings</i><p> Soporte </p>
            </a>
        </li>
    </ul>
</div>
