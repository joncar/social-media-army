<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 padding0 titulo-secccion titulo-top">
                            Nombre de la Empresa
                        </div>
                    </div>

                    <div class="row">
                        <div class="card-content">
                            <div class="menu-filtros">
                                <ul class="nav nav-pills nav-pills-warning">
                                    <li class="active"><a href="#pill1" data-toggle="tab" aria-expanded="true">Usuarios</a></li>
                                    <li class=""><a href="#pill2" data-toggle="tab" aria-expanded="false">Proyectos</a></li>
                                    <li class=""><a href="#pill3" data-toggle="tab" aria-expanded="false">Facturación</a></li>
                                </ul>
                            </div>

                            <div class="tab-content contenedor-facturas">

                                <div class="tab-pane active" id="pill1">
                                    <div class="col-sm-12">
                                    <div class="card-content">
                                      <div class="material-datatables">
                                          <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">

                                            <div class="row">
                                                 <div class="col-xs-6 col-sm-6">
                                                        <div class="select-agencias">
                                                            <select class="selectpicker" data-style="select-with-transition"
                                                            multiple title="Filtrar por" data-size="7">
                                                              <option disabled>Filtrar</option>
                                                              <option value="2">Filtro 1</option>
                                                              <option value="2">Filtro 2</option>
                                                              <option value="2">Filtro 3</option>
                                                            </select>
                                                        </div>
                                                  </div>
                                                  <div class="col-xs-6 col-sm-6">
                                                    <div class="dataTables_filter" id="datatables_filter">
                                                          <div id="wrap">
                                                            <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                                                          </div>
                                                    </div>
                                                  </div>
                                              </div>

                                              <div class="row botones-ajustes">
                                                  <div class="col-sm-12">
                                                      <div class="col-sm-10">
                                                          <div class="col-sm-1">
                                                               <a href="#" title="Importar XML"><button class="btn btn-primary btn-round" id="btn-enviar-brief"><i class="material-icons">file_upload</i></button></a>
                                                          </div>
                                                          <div class="col-sm-1">
                                                              <a href="#" title="Exportar XML"><button class="btn btn-primary btn-round" id="btn-enviar-brief"><i class="material-icons">file_download</i></button></a>
                                                          </div>
                                                      </div>

                                                      <div class="col-sm-2 text-left">
                                                          <a title="Agregar usuario" href="#agregar-usuario-admin" data-toggle="modal"><button class="btn btn-primary btn-round" id="btn-ver-agencia">Agregar usuario</button></a>
                                                      </div>
                                                  </div>
                                              </div>

                                              <div class="row">
                                              	<div class="col-sm-12">
                                                  <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">
                                              		<thead>
                                                  		<tr role="row">
                                                      	  <th aria-sort="descending" aria-label="Name: activate to sort column ascending"
                                                          style="width: 163px;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting_desc">ID</th>

                                                          <th aria-label="Position: activate to sort column ascending"
                                                          style="width: 241px;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Usuario</th>

                                                          <th aria-label="Office: activate to sort column ascending"
                                                          style="width: 123px;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Perfil</th>

                                                          <th aria-label="Actions: activate to sort column ascending"
                                                          style="width: 116px;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="disabled-sorting text-right sorting">Acciones</th>
                                                      </tr>
                                              		</thead>

                                                  <tfoot>
                                                      <tr>
                                                      	  <th colspan="1" rowspan="1">ID</th>
                                                          <th colspan="1" rowspan="1">Usuario</th>
                                                          <th colspan="1" rowspan="1">Perfil</th>
                                                          <th colspan="1" rowspan="1" class="text-right">Acciones</th>
                                                       </tr>
                                                  </tfoot>

                                              <tbody>
                                              	<tr class="odd" role="row">
                                                      <td tabindex="0" class="sorting_1">1</td>
                                                      <td class="">correo@correo.com.mx</td>
                                                      <td class="">Administrador</td>
                                                      <td class="text-right">
                                                          <a a title="Editar usuario" href="#editar-usuario" data-toggle="modal" class="btn btn-simple btn-warning btn-icon edit">
                                                          	<i class="material-icons">mode_edit</i>
                                                          </a>
                                                          <a a title="Pausar usuario" href="#pausar-usuario" data-toggle="modal" class="btn btn-simple btn-success btn-icon remove">
                                                          	<i class="material-icons">pause</i>
                                                          </a>
                                                          <a a title="Eliminar usuario" href="#eliminar-usuario" data-toggle="modal" class="btn btn-simple btn-danger btn-icon remove">
                                                          	<i class="material-icons">close</i>
                                                          </a>
                                                      </td>
                                                  </tr>

                                                  <tr class="even" role="row">
                                                      <td tabindex="0" class="sorting_1">2</td>
                                                      <td class="">correo@correo.com.mx</td>
                                                      <td class="">Supervisor</td>
                                                      <td class="text-right">
                                                          <a a title="Editar usuario" href="#editar-usuario" data-toggle="modal" class="btn btn-simple btn-warning btn-icon edit">
                                                          	<i class="material-icons">mode_edit</i>
                                                          </a>
                                                          <a a title="Pausar usuario" href="#pausar-usuario" data-toggle="modal" class="btn btn-simple btn-success btn-icon remove">
                                                          	<i class="material-icons">pause</i>
                                                          </a>
                                                          <a a title="Eliminar usuario" href="#eliminar-usuario" data-toggle="modal" class="btn btn-simple btn-danger btn-icon remove">
                                                          	<i class="material-icons">close</i>
                                                          </a>
                                                      </td>
                                                  </tr>

                                                  <tr class="odd" role="row">
                                                      <td class="sorting_1" tabindex="0">5</td>
                                                      <td class="">correo@correo.com.mx</td>
                                                      <td class="">Usuario</td>
                                                      <td class="text-right">
                                                          <a a title="Editar usuario" href="#editar-usuario" data-toggle="modal" class="btn btn-simple btn-warning btn-icon edit">
                                                          	<i class="material-icons">mode_edit</i>
                                                          </a>
                                                          <a a title="Pausar usuario" href="#pausar-usuario" data-toggle="modal" class="btn btn-simple btn-success btn-icon remove">
                                                          	<i class="material-icons">pause</i>
                                                          </a>
                                                          <a a title="Eliminar usuario" href="#eliminar-usuario" data-toggle="modal" class="btn btn-simple btn-danger btn-icon remove">
                                                          	<i class="material-icons">close</i>
                                                          </a>
                                                      </td>
                                                  </tr>

                                                  <tr class="even" role="row">
                                                      <td tabindex="0" class="sorting_1">6</td>
                                                      <td class="">correo@correo.com.mx</td>
                                                      <td class="">Usuario</td>
                                                      <td class="text-right">
                                                          <a a title="Editar usuario" href="#editar-usuario" data-toggle="modal" class="btn btn-simple btn-warning btn-icon edit">
                                                          	<i class="material-icons">mode_edit</i>
                                                          </a>
                                                          <a a title="Pausar usuario" href="#" class="btn btn-simple btn-success btn-icon remove">
                                                          	<i class="material-icons">play_arrow</i>
                                                          </a>
                                                          <a a title="Eliminar usuario" href="#eliminar-usuario" data-toggle="modal" class="btn btn-simple btn-danger btn-icon remove">
                                                          	<i class="material-icons">close</i>
                                                          </a>
                                                      </td>
                                                  </tr>
                                               </tbody>
                                          </table>
                                          </div>
                                       </div>

                                       <div class="row">
                                           	<div class="col-sm-5">
                                              	<div aria-live="polite" role="status" id="datatables_info" class="dataTables_info">Mostrando 1 a 10 de 40 entradas</div>
                                            </div>

                                            <div class="col-sm-7">
                                              	<div id="datatables_paginate" class="dataTables_paginate paging_full_numbers">
                                                  	<ul class="pagination">
                                                      	  <li id="datatables_first" class="paginate_button first disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatables" href="#">Inicio</a></li>
                                                          <li id="datatables_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="1" aria-controls="datatables" href="#">Anterior</a></li>
                                                          <li class="paginate_button active"><a tabindex="0" data-dt-idx="2" aria-controls="datatables" href="#">1</a></li>
                                                          <li class="paginate_button "><a tabindex="0" data-dt-idx="3" aria-controls="datatables" href="#">2</a></li>
                                                          <li class="paginate_button "><a tabindex="0" data-dt-idx="4" aria-controls="datatables" href="#">3</a></li>
                                                          <li class="paginate_button "><a tabindex="0" data-dt-idx="5" aria-controls="datatables" href="#">4</a></li>
                                                          <li id="datatables_next" class="paginate_button next"><a tabindex="0" data-dt-idx="6" aria-controls="datatables" href="#">Siguiente</a></li>
                                                          <li id="datatables_last" class="paginate_button last"><a tabindex="0" data-dt-idx="7" aria-controls="datatables" href="#">Último</a></li>
                                                      </ul>
                                                  </div>
                                              </div>
                                       </div>


                                        </div>
                                      </div>
                                    </div>

                                  </div>
                                </div>

                                <div class="tab-pane" id="pill2">

                                  <div class="row">
                                     <div class="col-xs-6 col-sm-6">
                                        <b>Proyectos. Mostrando 4 de 44</b>
                                     </div>

                                     <div class="col-xs-6 col-sm-6">
                                            <div class="select-agencias">
                                                <select class="selectpicker" data-style="select-with-transition" multiple title="Filtrar por" data-size="7">
                                                  <option disabled>Fase</option>
                                                  <option value="2">Fase 1</option>
                                                  <option value="2">Fase 2</option>
                                                  <option value="2">Fase 3</option>
                                                </select>
                                            </div>
                                      </div>
                                  </div>

                                  <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped
                                  table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">

                                  <thead>
                                      <tr role="row">
                                          <th aria-sort="descending" aria-label="Name: activate to sort column ascending"
                                          style="width: auto;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting_desc">ID</th>

                                          <th aria-label="Position: activate to sort column ascending"
                                          style="width: auto;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Proyecto</th>

                                          <th aria-label="Office: activate to sort column ascending"
                                          style="width: auto;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Imagen</th>

                                          <th aria-label="Office: activate to sort column ascending"
                                          style="width: auto;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Responsable</th>

                                          <th aria-label="Office: activate to sort column ascending"
                                          style="width: auto;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Agencias invitadas</th>

                                          <th aria-label="Office: activate to sort column ascending"
                                          style="width: auto;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Inicio</th>

                                          <th aria-label="Office: activate to sort column ascending"
                                          style="width: auto;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Fase</th>
                                      </tr>
                                  </thead>

                                  <tfoot>
                                      <tr>
                                          <th colspan="1" rowspan="1">ID</th>
                                          <th colspan="1" rowspan="1">Proyecto</th>
                                          <th colspan="1" rowspan="1">Imagen</th>
                                          <th colspan="1" rowspan="1">Responsable</th>
                                          <th colspan="1" rowspan="1">Agencias invitadas</th>
                                          <th colspan="1" rowspan="1">Inicio</th>
                                          <th colspan="1" rowspan="1">Fase</th>
                                       </tr>
                                  </tfoot>

                              <tbody>
                                  <tr class="odd img-clientes-tabla" role="row">
                                      <td tabindex="0" class="sorting_1">1</td>
                                      <td class="">Nombre del Proyecto</td>
                                      <td class=""><img src="assets/img/Perfil/logo-bimbo.png" alt="Perfil Dashboard" class="img-responsive"></td>
                                      <td class="">nombre@correo.com</td>
                                      <td class="">5</td>
                                      <td class="">00/00/0000</td>
                                      <td class="">Recepción de propuestas</td>
                                  </tr>

                                  <tr class="odd img-clientes-tabla" role="row">
                                      <td tabindex="0" class="sorting_1">1</td>
                                      <td class="">Nombre del Proyecto</td>
                                      <td class=""><img src="assets/img/Perfil/logo-bimbo.png" alt="Perfil Dashboard" class="img-responsive"></td>
                                      <td class="">nombre@correo.com</td>
                                      <td class="">5</td>
                                      <td class="">00/00/0000</td>
                                      <td class="">Recepción de propuestas</td>
                                  </tr>

                                  <tr class="odd img-clientes-tabla" role="row">
                                      <td tabindex="0" class="sorting_1">1</td>
                                      <td class="">Nombre del Proyecto</td>
                                      <td class=""><img src="assets/img/Perfil/logo-bimbo.png" alt="Perfil Dashboard" class="img-responsive"></td>
                                      <td class="">nombre@correo.com</td>
                                      <td class="">5</td>
                                      <td class="">00/00/0000</td>
                                      <td class="">Recepción de propuestas</td>
                                  </tr>

                                  <tr class="odd img-clientes-tabla" role="row">
                                      <td tabindex="0" class="sorting_1">1</td>
                                      <td class="">Nombre del Proyecto</td>
                                      <td class=""><img src="assets/img/Perfil/logo-bimbo.png" alt="Perfil Dashboard" class="img-responsive"></td>
                                      <td class="">nombre@correo.com</td>
                                      <td class="">5</td>
                                      <td class="">00/00/0000</td>
                                      <td class="">Finalizado</td>
                                  </tr>

                                  <tr class="odd img-clientes-tabla" role="row">
                                      <td tabindex="0" class="sorting_1">1</td>
                                      <td class="">Nombre del Proyecto</td>
                                      <td class=""><img src="assets/img/Perfil/logo-bimbo.png" alt="Perfil Dashboard" class="img-responsive"></td>
                                      <td class="">nombre@correo.com</td>
                                      <td class="">5</td>
                                      <td class="">00/00/0000</td>
                                      <td class="">Cancelado</td>
                                  </tr>

                                  <tr class="odd img-clientes-tabla" role="row">
                                      <td tabindex="0" class="sorting_1">1</td>
                                      <td class="">Nombre del Proyecto</td>
                                      <td class=""><img src="assets/img/Perfil/logo-bimbo.png" alt="Perfil Dashboard" class="img-responsive"></td>
                                      <td class="">nombre@correo.com</td>
                                      <td class="">5</td>
                                      <td class="">00/00/0000</td>
                                      <td class="">Preguntas y Respuestas</td>
                                  </tr>

                               </tbody>
                             </table>


                             <div class="row">
                                  <div class="col-sm-5">
                                      <div aria-live="polite" role="status" id="datatables_info" class="dataTables_info">
                                        Mostrando 1 a 10 de 40 entradas
                                      </div>
                                  </div>

                                  <div class="col-sm-7">
                                    <div id="datatables_paginate" class="dataTables_paginate paging_full_numbers">
                                        <ul class="pagination">
                                            <li id="datatables_first" class="paginate_button first disabled">
                                                <a tabindex="0" data-dt-idx="0" aria-controls="datatables" href="#">Inicio</a>
                                              </li>
                                              <li id="datatables_previous" class="paginate_button previous disabled">
                                                <a tabindex="0" data-dt-idx="1" aria-controls="datatables" href="#">Anterior</a>
                                              </li>
                                              <li class="paginate_button active">
                                                <a tabindex="0" data-dt-idx="2" aria-controls="datatables" href="#">1</a>
                                              </li>
                                              <li class="paginate_button ">
                                                <a tabindex="0" data-dt-idx="3" aria-controls="datatables" href="#">2</a>
                                              </li>
                                              <li class="paginate_button ">
                                                <a tabindex="0" data-dt-idx="4" aria-controls="datatables" href="#">3</a>
                                              </li>
                                              <li class="paginate_button ">
                                                <a tabindex="0" data-dt-idx="5" aria-controls="datatables" href="#">4</a>
                                              </li>
                                              <li id="datatables_next" class="paginate_button next">
                                                <a tabindex="0" data-dt-idx="6" aria-controls="datatables" href="#">Siguiente</a>
                                              </li>
                                              <li id="datatables_last" class="paginate_button last">
                                                <a tabindex="0" data-dt-idx="7" aria-controls="datatables" href="#">Último</a>
                                              </li>
                                          </ul>
                                      </div>
                                  </div>
                             </div>

                            </div>


                            <div class="tab-pane" id="pill3">

                              <div class="row">
                                   <div class="col-xs-6 col-sm-3">
                                      <b>Facturación</b>
                                   </div>

                                   <div class="col-xs-6 col-sm-3">
                                          <div class="select-agencias">
                                              <select class="selectpicker" data-style="select-with-transition" multiple title="Filtrar por" data-size="7">
                                                <option disabled>Status</option>
                                                <option value="2">Pagado</option>
                                                <option value="3">Pendiente</option>
                                              </select>
                                          </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3">

                                      <div class="col-sm-12">
                                          <div class="col-xs-6 col-sm-6">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input class="form-control datepicker" value="10/10/2016" style="" type="text">
                                                    <span class="material-input"></span><span class="material-input"></span></div>
                                                </div>
                                          </div>
                                          <div class="col-xs-6 col-sm-6">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input class="form-control datepicker" value="10/10/2016" style="" type="text">
                                                    <span class="material-input"></span><span class="material-input"></span></div>
                                                </div>
                                          </div>
                                      </div>

                                    </div>

                                    <div class="col-xs-6 col-sm-3">
                                      <div class="dataTables_filter" id="datatables_filter">
                                            <div id="wrap">
                                              <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar"><i class="material-icons">search</i></form>
                                            </div>
                                      </div>
                                    </div>
                              </div>

                              <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped
                              table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">

                              <thead>
                                  <tr role="row">
                                      <th aria-sort="descending" aria-label="Name: activate to sort column ascending"
                                      style="width: auto;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting_desc">Fecha</th>

                                      <th aria-label="Position: activate to sort column ascending"
                                      style="width: auto;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Factura</th>

                                      <th aria-label="Office: activate to sort column ascending"
                                      style="width: auto;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Status</th>

                                      <th aria-label="Office: activate to sort column ascending"
                                      style="width: auto;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Acciones</th>
                                  </tr>
                              </thead>

                              <tfoot>
                                  <tr>
                                      <th colspan="1" rowspan="1">Fecha</th>
                                      <th colspan="1" rowspan="1">Factura</th>
                                      <th colspan="1" rowspan="1">Status</th>
                                      <th colspan="1" rowspan="1">Acciones</th>
                                   </tr>
                              </tfoot>

                          <tbody>
                              <tr class="odd img-clientes-tabla" role="row">
                                  <td tabindex="0" class="sorting_1">00/00/0000</td>
                                  <td class="">Clave de la Factura</td>
                                  <td class="texto-verde">Pagado</td>
                                  <td>
                                      <a a title="Ver" href="#" data-toggle="modal" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">remove_red_eye</i>
                                      </a>
                                      <a a title="Descargar" href="#" class="btn btn-simple btn-success btn-icon remove">
                                        <i class="material-icons">file_download</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="odd img-clientes-tabla" role="row">
                                  <td tabindex="0" class="sorting_1">00/00/0000</td>
                                  <td class="">Clave de la Factura</td>
                                  <td class="texto-rojo">Pendiente</td>
                                  <td>
                                      <a a title="Ver" href="#" data-toggle="modal" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">remove_red_eye</i>
                                      </a>
                                      <a a title="Descargar" href="#" class="btn btn-simple btn-success btn-icon remove">
                                        <i class="material-icons">file_download</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="odd img-clientes-tabla" role="row">
                                  <td tabindex="0" class="sorting_1">00/00/0000</td>
                                  <td class="">Clave de la Factura</td>
                                  <td class="texto-gris">Sin status</td>
                                  <td>
                                      <a a title="Ver" href="#" data-toggle="modal" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">remove_red_eye</i>
                                      </a>
                                      <a a title="Descargar" href="#" class="btn btn-simple btn-success btn-icon remove">
                                        <i class="material-icons">file_download</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="odd img-clientes-tabla" role="row">
                                  <td tabindex="0" class="sorting_1">00/00/0000</td>
                                  <td class="">Clave de la Factura</td>
                                  <td class="texto-verde">Pagado</td>
                                  <td>
                                      <a a title="Ver" href="#" data-toggle="modal" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">remove_red_eye</i>
                                      </a>
                                      <a a title="Descargar" href="#" class="btn btn-simple btn-success btn-icon remove">
                                        <i class="material-icons">file_download</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="odd img-clientes-tabla" role="row">
                                  <td tabindex="0" class="sorting_1">00/00/0000</td>
                                  <td class="">Clave de la Factura</td>
                                  <td class="texto-verde">Pagado</td>
                                  <td>
                                      <a a title="Ver" href="#" data-toggle="modal" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">remove_red_eye</i>
                                      </a>
                                      <a a title="Descargar" href="#" class="btn btn-simple btn-success btn-icon remove">
                                        <i class="material-icons">file_download</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="odd img-clientes-tabla" role="row">
                                  <td tabindex="0" class="sorting_1">00/00/0000</td>
                                  <td class="">Clave de la Factura</td>
                                  <td class="texto-verde">Pagado</td>
                                  <td>
                                      <a a title="Ver" href="#" data-toggle="modal" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">remove_red_eye</i>
                                      </a>
                                      <a a title="Descargar" href="#" class="btn btn-simple btn-success btn-icon remove">
                                        <i class="material-icons">file_download</i>
                                      </a>
                                  </td>
                              </tr>

                           </tbody>
                         </table>


                         <div class="row">
                              <div class="col-sm-5">
                                  <div aria-live="polite" role="status" id="datatables_info" class="dataTables_info">
                                    Mostrando 1 a 10 de 40 entradas
                                  </div>
                              </div>

                              <div class="col-sm-7">
                                <div id="datatables_paginate" class="dataTables_paginate paging_full_numbers">
                                    <ul class="pagination">
                                        <li id="datatables_first" class="paginate_button first disabled">
                                            <a tabindex="0" data-dt-idx="0" aria-controls="datatables" href="#">Inicio</a>
                                          </li>
                                          <li id="datatables_previous" class="paginate_button previous disabled">
                                            <a tabindex="0" data-dt-idx="1" aria-controls="datatables" href="#">Anterior</a>
                                          </li>
                                          <li class="paginate_button active">
                                            <a tabindex="0" data-dt-idx="2" aria-controls="datatables" href="#">1</a>
                                          </li>
                                          <li class="paginate_button ">
                                            <a tabindex="0" data-dt-idx="3" aria-controls="datatables" href="#">2</a>
                                          </li>
                                          <li class="paginate_button ">
                                            <a tabindex="0" data-dt-idx="4" aria-controls="datatables" href="#">3</a>
                                          </li>
                                          <li class="paginate_button ">
                                            <a tabindex="0" data-dt-idx="5" aria-controls="datatables" href="#">4</a>
                                          </li>
                                          <li id="datatables_next" class="paginate_button next">
                                            <a tabindex="0" data-dt-idx="6" aria-controls="datatables" href="#">Siguiente</a>
                                          </li>
                                          <li id="datatables_last" class="paginate_button last">
                                            <a tabindex="0" data-dt-idx="7" aria-controls="datatables" href="#">Último</a>
                                          </li>
                                      </ul>
                                  </div>
                              </div>
                         </div>



                            </div>

                        </div>
                    </div>
                </div>



            </div>




        </div>

        <footer class="footer contenedor-footer">
            <?php include('footer.php');?>
        </footer>
        <!-- Termina Contenido -->

    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>
</html>
