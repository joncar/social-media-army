<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                    <div class="row">
                            <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Ajustes</div></div>

                            <div class="card-content">
                                <div class="menu-filtros">
                                    <ul class="nav nav-pills nav-pills-warning">
                                        <li class="active"><a href="#ajustes1" data-toggle="tab" aria-expanded="true">Datos de la empresa</a></li>
                                        <li><a href="#ajustes2" data-toggle="tab" aria-expanded="false">Perfil de Usuario</a></li>
                                        <li><a href="#ajustes3" data-toggle="tab" aria-expanded="false">Alta de Usuarios</a></li>
                                        <li><a href="#ajustes4" data-toggle="tab" aria-expanded="false">Alta de Agencias</a></li>
                                        <li><a href="#ajustes5" data-toggle="tab" aria-expanded="false">Productos</a></li>
                                        <li><a href="#ajustes6" data-toggle="tab" aria-expanded="false">Marcas</a></li>                                        
                                    </ul>
                                </div>
                                <div class="tab-content contenedor-facturas">

                                    <div class="tab-pane active" id="ajustes1">
                                        <div class="col-sm-12">
                                            <?= $output ?>
                                        </div>
                                    </div>


                                    <div class="tab-pane" id="ajustes2">
                                        <div class="col-sm-12">
                                            <?= $user ?>
                                        </div>
                                    </div>


                                    <div class="tab-pane" id="ajustes3">
                                        <div class="col-sm-12">
                                            <?= $users ?>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="ajustes4">
                                        <div class="col-sm-12">
                                            <?= $invitaciones ?>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="ajustes5">
                                        <div class="col-sm-12">
                                            <?= $productos ?>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="ajustes6">
                                        <div class="col-sm-12">
                                            <?= $marcas ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




            </div>




        </div>

        <footer class="footer contenedor-footer">
            <?php include('footer.php');?>
        </footer>
        <!-- Termina Contenido -->

    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);                
    });
})(jQuery);
</script>
</html>
