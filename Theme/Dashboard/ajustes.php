<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">




                  <!-- Inicia Contenido -->
                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Ajustes</div></div>

                      <div class="card-content">
                          <div class="menu-filtros">
                              <ul class="nav nav-pills nav-pills-warning">
                                  <li class="active"><a href="#pill1" data-toggle="tab" aria-expanded="true">Datos de la cuenta</a></li>
                                  <li class=""><a href="#pill2" data-toggle="tab" aria-expanded="false">Alta de Usuarios</a></li>
                                  <li class=""><a href="#pill3" data-toggle="tab" aria-expanded="false">Alta de Agencias</a></li>
                                  <li class=""><a href="#pill4" data-toggle="tab" aria-expanded="false">Productos</a></li>
                              </ul>
                          </div>
                          <div class="tab-content contenedor-facturas">

                          	  <div class="tab-pane active" id="pill1">
                                  <div class="col-xs-12 col-sm-12 padding0">

                                        <!-- CREDIT CARD FORM STARTS HERE -->
                                        <div class="col-xs-12 col-sm-12 margen-registro-movil margen-tabs">
                                            <b>Datos de la Empresa</b>
                                            <div class="card-content">
                                                <form method="#" action="#">
                                                	 <div class="col-sm-4">
                                                        <div class="form-group label-floating is-empty">
                                                            <label class="control-label">Nombre Comercial</label>
                                                            <input class="form-control" type="text">
                                                            <span class="material-input"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating is-empty">
                                                            <label class="control-label">Razón Social</label>
                                                            <input class="form-control" type="text">
                                                            <span class="material-input"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group label-floating is-empty">
                                                            <label class="control-label">RFC</label>
                                                            <input class="form-control" type="text">
                                                            <span class="material-input"></span>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-fill" id="btn-proyecto-propuestas" style="width:auto;">Guardar información</button>
                                              </form>
                                          </div>
                                      </div>
                                		  <!-- CREDIT CARD FORM ENDS HERE -->

                                        <div class="col-xs-12 col-sm-12">
                                            <b>Dirección</b>
                                            <div class="card-content">
                                                <form method="#" action="#">
                                                	<div class="col-sm-12">
                                                        <div class="form-group label-floating is-empty">
                                                            <label class="control-label">Calle</label>
                                                            <input class="form-control" type="text">
                                                            <span class="material-input"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                    	<div class="col-sm-6">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label">Colonia</label>
                                                                <input class="form-control" type="text">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group label-floating is-empty">
                                                                <label class="control-label">Delegación</label>
                                                                <input class="form-control" type="text">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                    	<div class="col-sm-4">
                                                        	<div class="form-group label-floating is-empty">
                                                                <label class="control-label">Número exterior</label>
                                                                <input class="form-control" type="password">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                        	<div class="form-group label-floating is-empty">
                                                                <label class="control-label">Número interior</label>
                                                                <input class="form-control" type="password">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                        	<div class="form-group label-floating is-empty">
                                                                <label class="control-label">Estado</label>
                                                                <input class="form-control" type="password">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                    	<div class="col-sm-4">
                                                        	<div class="form-group label-floating is-empty">
                                                                <label class="control-label">País</label>
                                                                <input class="form-control" type="password">
                                                                <span class="material-input"></span>
                                                            </div>
                                                      </div>





                            <div class="col-lg-5 col-md-6 col-sm-3">
                                <div class="btn-group bootstrap-select show-tick"><button type="button" class="dropdown-toggle bs-placeholder btn select-with-transition" data-toggle="dropdown" role="button" title="Choose City" aria-expanded="false"><span class="filter-option pull-left">Choose City</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span><div class="ripple-container"><div class="ripple-decorator ripple-on ripple-out" style="left: 1247px; top: 450px; background-color: rgb(60, 72, 88); transform: scale(28.8791);"></div><div class="ripple-decorator ripple-on ripple-out" style="left: 1247px; top: 450px; background-color: rgb(60, 72, 88); transform: scale(28.8791);"></div><div class="ripple-decorator ripple-on ripple-out" style="left: 1247px; top: 450px; background-color: rgb(60, 72, 88); transform: scale(28.8791);"></div><div class="ripple-decorator ripple-on ripple-out" style="left: 1247px; top: 450px; background-color: rgb(60, 72, 88); transform: scale(28.8791);"></div></div></button><div class="dropdown-menu open" role="combobox" style="max-height: 273px; overflow: hidden; position: absolute; top: 34px; left: 0px; will-change: top, left;" x-placement="bottom-start"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false" style="max-height: 273px; overflow-y: auto;"><li data-original-index="0" class="disabled"><a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="false"><span class="text"> Multiple Options</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Paris </span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bucharest</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="3"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Rome</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="4"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">New York</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="5"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Miami </span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="6"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Piatra Neamt</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="7"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Paris </span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="8"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bucharest</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="9"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Rome</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="10"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">New York</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="11"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Miami </span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="12"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Piatra Neamt</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="13"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Paris </span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="14"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bucharest</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="15"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Rome</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="16"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">New York</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="17"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Miami </span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="18"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Piatra Neamt</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" data-style="btn select-with-transition" multiple="" title="Choose City" data-size="7" tabindex="-98">
                                    <option disabled=""> Multiple Options</option>
                                    <option value="2">Paris </option>
                                    <option value="3">Bucharest</option>
                                    <option value="4">Rome</option>
                                    <option value="5">New York</option>
                                    <option value="6">Miami </option>
                                    <option value="7">Piatra Neamt</option>
                                    <option value="8">Paris </option>
                                    <option value="9">Bucharest</option>
                                    <option value="10">Rome</option>
                                    <option value="11">New York</option>
                                    <option value="12">Miami </option>
                                    <option value="13">Piatra Neamt</option>
                                    <option value="14">Paris </option>
                                    <option value="15">Bucharest</option>
                                    <option value="16">Rome</option>
                                    <option value="17">New York</option>
                                    <option value="18">Miami </option>
                                    <option value="19">Piatra Neamt</option>
                                </select></div>
                            </div>





                                                    	<div class="col-sm-4">
                                                        	<div class="form-group label-floating is-empty">
                                                                <label class="control-label">CP</label>
                                                                <input class="form-control" type="password">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                        	<div class="form-group label-floating is-empty">
                                                                <label class="control-label">Ciudad</label>
                                                                <input class="form-control" type="password">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-fill" id="btn-proyecto-propuestas" style="width:auto;">
                                                    	Guardar información
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                  </div>
                              </div>


                              <div class="tab-pane" id="pill2">
                                  <div class="col-sm-12">

                                  <div class="card-content">
                                    <div class="material-datatables">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                                        	<div class="row">
                                                <div class="col-xs-6 col-sm-6">
                                                      <div class="select-agencias">
                                                          <select class="selectpicker" data-style="select-with-transition"
                                                          multiple title="Filtrar por" data-size="7">
                                                            <option disabled>Filtrar</option>
                                                            <option value="2">Filtro 1</option>
                                                            <option value="2">Filtro 2</option>
                                                            <option value="2">Filtro 3</option>
                                                          </select>
                                                      </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-6">
                                                	<div class="dataTables_filter" id="datatables_filter">
                                                        <div id="wrap">
                                                          <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row botones-ajustes">
                                                <div class="col-sm-2">
                                            	       <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Importar XML</button></a>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Agregar Usuario</button></a>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Descargar XML</button></a>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a title="Agregar usuario" href="#agregar-usuario-admin" data-toggle="modal"><button class="btn btn-primary btn-round" id="btn-ver-agencia">Agregar usuario</button></a>
                                                </div>
                                            </div>

                                            <div class="row">
                                            	<div class="col-sm-12">
                                                <table aria-describedby="datatables_info" role="grid" id="datatables"
                                                    class="table table-striped table-no-bordered table-hover dataTable dtr-inline"
                                                    style="width: 100%;" cellspacing="0" width="100%">

                                            		<thead>
                                                		<tr role="row">
                                                        	<th aria-sort="descending" aria-label="Name: activate to sort column ascending"
                                                            style="width: 163px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="sorting_desc">ID</th>

                                                            <th aria-label="Position: activate to sort column ascending"
                                                            style="width: 241px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="sorting">Usuario</th>

                                                            <th aria-label="Office: activate to sort column ascending"
                                                            style="width: 123px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="sorting">Perfil</th>

                                                            <th aria-label="Actions: activate to sort column ascending"
                                                            style="width: 116px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="disabled-sorting text-right sorting">Acciones
                                                            </th>
                                                        </tr>
                                            		</thead>

                                                    <tfoot>
                                                        <tr>
                                                        	<th colspan="1" rowspan="1">ID</th>
                                                            <th colspan="1" rowspan="1">Usuario</th>
                                                            <th colspan="1" rowspan="1">Perfil</th>
                                                            <th colspan="1" rowspan="1" class="text-right">Acciones</th>
                                                         </tr>
                                                    </tfoot>

                                            <tbody>
                                            	<tr class="odd" role="row">
                                                    <td tabindex="0" class="sorting_1">1</td>
                                                    <td class="">correo@correo.com.mx</td>
                                                    <td class="">Administrador</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="even" role="row">
                                                    <td tabindex="0" class="sorting_1">2</td>
                                                    <td class="">correo@correo.com.mx</td>
                                                    <td class="">Supervisor</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="odd" role="row">
                                                    <td tabindex="0" class="sorting_1">3</td>
                                                    <td class="">correo@correo.com.mx</td>
                                                    <td class="">Usuario</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="even" role="row">
                                                    <td tabindex="0" class="sorting_1">4</td>
                                                    <td class="">correo@correo.com.mx</td>
                                                    <td class="">Usuario</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                 </tr>

                                                <tr class="odd" role="row">
                                                    <td class="sorting_1" tabindex="0">5</td>
                                                    <td class="">correo@correo.com.mx</td>
                                                    <td class="">Usuario</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="even" role="row">
                                                    <td tabindex="0" class="sorting_1">6</td>
                                                    <td class="">correo@correo.com.mx</td>
                                                    <td class="">Usuario</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>


                                             </tbody>
                                        </table>

                                        </div>
                                     </div>

                                     <div class="row">
                                     	<div class="col-sm-5">
                                        	<div aria-live="polite" role="status" id="datatables_info" class="dataTables_info">
                                            	Mostrando 1 a 10 de 40 entradas
                                            </div>
                                        </div>

                                        <div class="col-sm-7">
                                        	<div id="datatables_paginate" class="dataTables_paginate paging_full_numbers">
                                            	<ul class="pagination">
                                                	<li id="datatables_first" class="paginate_button first disabled">
                                                    	<a tabindex="0" data-dt-idx="0" aria-controls="datatables" href="#">Inicio</a>
                                                    </li>
                                                    <li id="datatables_previous" class="paginate_button previous disabled">
                                                    	<a tabindex="0" data-dt-idx="1" aria-controls="datatables" href="#">Anterior</a>
                                                    </li>
                                                    <li class="paginate_button active">
                                                    	<a tabindex="0" data-dt-idx="2" aria-controls="datatables" href="#">1</a>
                                                    </li>
                                                    <li class="paginate_button ">
                                                    	<a tabindex="0" data-dt-idx="3" aria-controls="datatables" href="#">2</a>
                                                    </li>
                                                    <li class="paginate_button ">
                                                    	<a tabindex="0" data-dt-idx="4" aria-controls="datatables" href="#">3</a>
                                                   	</li>
                                                    <li class="paginate_button ">
                                                    	<a tabindex="0" data-dt-idx="5" aria-controls="datatables" href="#">4</a>
                                                    </li>
                                                    <li id="datatables_next" class="paginate_button next">
                                                    	<a tabindex="0" data-dt-idx="6" aria-controls="datatables" href="#">Siguiente</a>
                                                    </li>
                                                    <li id="datatables_last" class="paginate_button last">
                                                    	<a tabindex="0" data-dt-idx="7" aria-controls="datatables" href="#">Último</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                     </div>

                                  </div>
                                </div>
                              </div>

                                  </div>

                              </div>

                              <div class="tab-pane" id="pill3">
                                   <div class="col-xs-12 col-sm-12 padding0 margen-tabs">
                                        <div class="col-xs-12 col-sm-12 margen-registro-movil">
                                            <b>Ingresa el correo de contacto de la agencia para invitarla a formar parte de su red</b>
                                            <div class="card-content">
                                                <form method="#" action="#">
                                                	<div class="col-sm-6">
                                                        <div class="form-group label-floating is-empty">
                                                            <label class="control-label">Nombre Comercial</label>
                                                            <input class="form-control" type="text">
                                                            <span class="material-input"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group label-floating is-empty">
                                                            <label class="control-label">Correo</label>
                                                            <input class="form-control" type="mail">
                                                            <span class="material-input"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                    	<button type="submit" class="btn btn-fill" id="btn-proyecto-propuestas" style="width:auto;">
                                                        	Enviar invitación
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                  </div>

                                  <div class="col-xs-12 col-sm-12 padding0">
                                        <div class="col-xs-12 col-sm-12 margen-registro-movil">
                                            <b>Invitaciones pendientes</b>

                                            <div class="col-sm-12 padding0">
                                              <!-- Inicia Proyecto -->
                                              <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 clearfix">
                                                  <div class="fondo-proyecto-dashboard">
                                                      <div class="img-perfil-proyecto text-center">
                                                          <img src="assets/img/Proyectos/img-proyecto1.jpg" alt="Perfil Dashboard"
                                                          class="img-responsive center-block image">
                                                          <span>
                                                              <img src="assets/img/Banderas/mexico.jpg" alt="Perfil Dashboard"
                                                              class="img-responsive center-block img-circle" id="perfil-proyecto">
                                                          </span>
                                                    </div>
                                                    <h1 class="titulo-proyecto">Nombre del Proyecto</h1>
                                                    <div class="texto-proyecto-activo">
                                                      <b>Contacto:</b><br>correo@correo.com.mx<br>
                                                      Enviado 12/02/2018<br>
                                                      <a href="#invitacion-admin" data-toggle="modal">
                                                          <button class="btn btn-primary btn-round" id="btn-enviar-brief">Reenviar</button>
                                                      </a>
                                                    </div>
                                                  </div>
                                               </div>
                                               <!-- Termina Proyecto -->

                                               <!-- Inicia Proyecto -->
                                              <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 clearfix">
                                                  <div class="fondo-proyecto-dashboard">
                                                      <div class="img-perfil-proyecto text-center">
                                                          <img src="assets/img/Proyectos/img-proyecto1.jpg" alt="Perfil Dashboard"
                                                          class="img-responsive center-block image">
                                                          <span>
                                                              <img src="assets/img/Banderas/mexico.jpg" alt="Perfil Dashboard"
                                                              class="img-responsive center-block img-circle" id="perfil-proyecto">
                                                          </span>
                                                    </div>
                                                    <h1 class="titulo-proyecto">Nombre del Proyecto</h1>
                                                    <div class="texto-proyecto-activo">
                                                      <b>Contacto:</b><br>correo@correo.com.mx<br>
                                                      Enviado 12/02/2018<br>
                                                      <a href="#invitacion-admin" data-toggle="modal">
                                                          <button class="btn btn-primary btn-round" id="btn-enviar-brief">Reenviar</button>
                                                      </a>
                                                    </div>
                                                  </div>
                                               </div>
                                               <!-- Termina Proyecto -->
                                           </div>
                                        </div>
                                  </div>


                              </div>


                              <div class="tab-pane" id="pill4">
                                  <div class="col-sm-12">

                                  <div class="card-content">
                                    <div class="material-datatables">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                                        	<div class="row">
                                                <div class="col-xs-6 col-sm-3">
                                                      <div class="select-agencias">
                                                          <select class="selectpicker" data-style="select-with-transition"
                                                          multiple title="Filtrar por" data-size="7">
                                                            <option disabled>Filtrar</option>
                                                            <option value="2">Filtro 1</option>
                                                            <option value="2">Filtro 2</option>
                                                            <option value="2">Filtro 3</option>
                                                          </select>
                                                      </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-9">
                                                	<div class="dataTables_filter" id="datatables_filter">
                                                        <div id="wrap">
                                                          <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row botones-ajustes">
                                                <div class="col-sm-2">
                                                  	<a href="#"><button class="btn btn-primary btn-round" id="btn-proyecto-informacion">Descargar todas</button></a>
                                                </div>
                                                <div class="col-sm-2">
                                                  	<a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Agregar producto</button></a>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="#">  <button class="btn btn-primary btn-round" id="btn-enviar-brief">Cargar XML</button></a>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Guardar cambios</button></a>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a title="Agregar producto" href="#agregar-producto-admin" data-toggle="modal"><button class="btn btn-primary btn-round" id="btn-ver-agencia">Agregar producto</button></a>
                                                </div>
                                            </div>

                                            <div class="row">
                                            	<div class="col-sm-12">
                                                <table aria-describedby="datatables_info" role="grid" id="datatables"
                                                    class="table table-striped table-no-bordered table-hover dataTable dtr-inline"
                                                    style="width: 100%;" cellspacing="0" width="100%">

                                            		<thead>
                                                		<tr role="row">
                                                        	<th aria-sort="descending" aria-label="Name: activate to sort column ascending"
                                                            style="width: 163px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="sorting_desc">Producto</th>

                                                            <th aria-label="Position: activate to sort column ascending"
                                                            style="width: 241px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="sorting">Categoría</th>

                                                            <th aria-label="Office: activate to sort column ascending"
                                                            style="width: 123px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="sorting">Marca</th>

                                                            <th aria-label="Office: activate to sort column ascending"
                                                            style="width: 123px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="sorting">Imagen</th>

                                                            <th aria-label="Actions: activate to sort column ascending"
                                                            style="width: 116px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="disabled-sorting text-right sorting">Acciones
                                                            </th>
                                                        </tr>
                                            		</thead>

                                                    <tfoot>
                                                        <tr>
                                                        	<th colspan="1" rowspan="1">Producto</th>
                                                            <th colspan="1" rowspan="1">Categoría</th>
                                                            <th colspan="1" rowspan="1">Marca</th>
                                                            <th colspan="1" rowspan="1">Imagen</th>
                                                            <th colspan="1" rowspan="1" class="text-right">Acciones</th>
                                                         </tr>
                                                    </tfoot>

                                            <tbody>
                                            	<tr class="odd" role="row">
                                                    <td tabindex="0" class="sorting_1">Producto</td>
                                                    <td class="">Categoría</td>
                                                    <td class="">Marca</td>
                                                    <td class="">
                                                    	<img src="assets/img/Proyectos/img-proyecto1.jpg" alt="Perfil Dashboard"
                                                        class="img-responsive center-block image">
                                                    </td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="even" role="row">
                                                    <td tabindex="0" class="sorting_1">Producto</td>
                                                    <td class="">Categoría</td>
                                                    <td class="">Marca</td>
                                                    <td class="">
                                                    	<img src="assets/img/Proyectos/img-proyecto1.jpg" alt="Perfil Dashboard"
                                                        class="img-responsive center-block image">
                                                    </td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="odd" role="row">
                                                    <td tabindex="0" class="sorting_1">Producto</td>
                                                    <td class="">Categoría</td>
                                                    <td class="">Marca</td>
                                                    <td class="">
                                                    	<img src="assets/img/Proyectos/img-proyecto1.jpg" alt="Perfil Dashboard"
                                                        class="img-responsive center-block image">
                                                    </td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="even" role="row">
                                                    <td tabindex="0" class="sorting_1">Producto</td>
                                                    <td class="">Categoría</td>
                                                    <td class="">Marca</td>
                                                    <td class="">
                                                    	<img src="assets/img/Proyectos/img-proyecto1.jpg" alt="Perfil Dashboard"
                                                        class="img-responsive center-block image">
                                                    </td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                 </tr>

                                                <tr class="odd" role="row">
                                                    <td tabindex="0" class="sorting_1">Producto</td>
                                                    <td class="">Categoría</td>
                                                    <td class="">Marca</td>
                                                    <td class="">
                                                    	<img src="assets/img/Proyectos/img-proyecto1.jpg" alt="Perfil Dashboard"
                                                        class="img-responsive center-block image">
                                                    </td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="even" role="row">
                                                    <td tabindex="0" class="sorting_1">Producto</td>
                                                    <td class="">Categoría</td>
                                                    <td class="">Marca</td>
                                                    <td class="">
                                                    	<img src="assets/img/Proyectos/img-proyecto1.jpg" alt="Perfil Dashboard"
                                                        class="img-responsive center-block image">
                                                    </td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>


                                             </tbody>
                                        </table>

                                        </div>
                                     </div>

                                     <div class="row">
                                     	<div class="col-sm-5">
                                        	<div aria-live="polite" role="status" id="datatables_info" class="dataTables_info">
                                            	Mostrando 1 a 10 de 40 entradas
                                            </div>
                                        </div>

                                        <div class="col-sm-7">
                                        	<div id="datatables_paginate" class="dataTables_paginate paging_full_numbers">
                                            	<ul class="pagination">
                                                	<li id="datatables_first" class="paginate_button first disabled">
                                                    	<a tabindex="0" data-dt-idx="0" aria-controls="datatables" href="#">Inicio</a>
                                                    </li>
                                                    <li id="datatables_previous" class="paginate_button previous disabled">
                                                    	<a tabindex="0" data-dt-idx="1" aria-controls="datatables" href="#">Anterior</a>
                                                    </li>
                                                    <li class="paginate_button active">
                                                    	<a tabindex="0" data-dt-idx="2" aria-controls="datatables" href="#">1</a>
                                                    </li>
                                                    <li class="paginate_button ">
                                                    	<a tabindex="0" data-dt-idx="3" aria-controls="datatables" href="#">2</a>
                                                    </li>
                                                    <li class="paginate_button ">
                                                    	<a tabindex="0" data-dt-idx="4" aria-controls="datatables" href="#">3</a>
                                                   	</li>
                                                    <li class="paginate_button ">
                                                    	<a tabindex="0" data-dt-idx="5" aria-controls="datatables" href="#">4</a>
                                                    </li>
                                                    <li id="datatables_next" class="paginate_button next">
                                                    	<a tabindex="0" data-dt-idx="6" aria-controls="datatables" href="#">Siguiente</a>
                                                    </li>
                                                    <li id="datatables_last" class="paginate_button last">
                                                    	<a tabindex="0" data-dt-idx="7" aria-controls="datatables" href="#">Último</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                     </div>

                                  </div>
                                </div>
                              </div>




                                  </div>

                              </div>


                              </div>




                          </div>
                      </div>


                  </div>
                  <!-- Termina Contenido -->

                </div>

                <footer class="footer contenedor-footer">
                    <?php include('footer.php');?>
                </footer>

            </div>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>
</html>
