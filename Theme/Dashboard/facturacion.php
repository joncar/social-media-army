<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">


                  <!-- Inicia Contenido -->
                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Facturación</div></div>

                      <div class="card-content">
                          <div class="menu-filtros">
                              <ul class="nav nav-pills nav-pills-warning">
                                  <li class="active"><a href="#pill1" data-toggle="tab" aria-expanded="true">Facturas</a></li>
                                  <li class=""><a href="#pill2" data-toggle="tab" aria-expanded="false">Datos crediticios</a></li>
                              </ul>
                          </div>
                          <div class="tab-content contenedor-facturas">

                              <div class="tab-pane active" id="pill1">
                                  <div class="col-sm-12">

                                  <div class="card-content">
                                    <div class="material-datatables">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                                        	<div class="row margen-tabs">
                                                <div class="col-xs-6 col-sm-3">
                                                      <div class="card-content">
                                                          <div class="form-group">
                                                              <label class="label-control">Desde</label>
                                                              <input class="form-control datepicker" value="10/10/2016" style="" type="text">
                                                          <span class="material-input"></span></div>
                                                      </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-3">
                                                      <div class="card-content">
                                                          <div class="form-group">
                                                              <label class="label-control">Hasta</label>
                                                              <input class="form-control datepicker" value="10/10/2016" style="" type="text">
                                                          <span class="material-input"></span></div>
                                                      </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-6">
                                                  	<div class="dataTables_filter" id="datatables_filter">
                                                        <div id="wrap">
                                                          <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                            	<div class="col-sm-12">
                                                <table aria-describedby="datatables_info" role="grid" id="datatables"
                                                    class="table table-striped table-no-bordered table-hover dataTable dtr-inline"
                                                    style="width: 100%;" cellspacing="0" width="100%">

                                            		<thead>
                                                		<tr role="row">
                                                        	<th aria-sort="descending" aria-label="Name: activate to sort column ascending"
                                                            style="width: 163px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="sorting_desc">Fecha</th>

                                                            <th aria-label="Position: activate to sort column ascending"
                                                            style="width: 241px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="sorting">Factura</th>

                                                            <th aria-label="Office: activate to sort column ascending"
                                                            style="width: 123px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="sorting">Estatus</th>

                                                            <th aria-label="Actions: activate to sort column ascending"
                                                            style="width: 116px;" colspan="1" rowspan="1" aria-controls="datatables"
                                                            tabindex="0" class="disabled-sorting text-right sorting">Acciones
                                                            </th>
                                                        </tr>
                                            		</thead>

                                                    <tfoot>
                                                        <tr>
                                                        	<th colspan="1" rowspan="1">Fecha</th>
                                                            <th colspan="1" rowspan="1">Factura</th>
                                                            <th colspan="1" rowspan="1">Estatus</th>
                                                            <th colspan="1" rowspan="1" class="text-right">Acciones</th>
                                                         </tr>
                                                    </tfoot>

                                            <tbody>
                                            	<tr class="odd" role="row">
                                                    <td tabindex="0" class="sorting_1">17/03/2017</td>
                                                    <td class="">Pagado</td>
                                                    <td class="">Pendiente</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-info btn-icon like">
                                                        	<i class="material-icons">favorite</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="even" role="row">
                                                    <td tabindex="0" class="sorting_1">17/03/2017</td>
                                                    <td class="">Pagado</td>
                                                    <td class="">Pendiente</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-info btn-icon like">
                                                        	<i class="material-icons">favorite</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="odd" role="row">
                                                    <td tabindex="0" class="sorting_1">17/03/2017</td>
                                                    <td class="">Pagado</td>
                                                    <td class="">Pendiente</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-info btn-icon like">
                                                        	<i class="material-icons">favorite</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="even" role="row">
                                                    <td tabindex="0" class="sorting_1">17/03/2017</td>
                                                    <td class="">Pagado</td>
                                                    <td class="">Pendiente</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-info btn-icon like">
                                                        	<i class="material-icons">favorite</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                 </tr>

                                                <tr class="odd" role="row">
                                                    <td class="sorting_1" tabindex="0">17/03/2017</td>
                                                    <td class="">Pagado</td>
                                                    <td class="">Pendiente</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-info btn-icon like">
                                                        	<i class="material-icons">favorite</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>

                                                <tr class="even" role="row">
                                                    <td tabindex="0" class="sorting_1">17/03/2017</td>
                                                    <td class="">Pagado</td>
                                                    <td class="">Pendiente</td>
                                                    <td class="text-right">
                                                        <a href="#" class="btn btn-simple btn-info btn-icon like">
                                                        	<i class="material-icons">favorite</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                                        	<i class="material-icons">dvr</i>
                                                        </a>
                                                        <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                                        	<i class="material-icons">close</i>
                                                        </a>
                                                    </td>
                                                </tr>


                                             </tbody>
                                        </table>

                                        </div>
                                     </div>

                                     <div class="row">
                                     	<div class="col-sm-5">
                                        	<div aria-live="polite" role="status" id="datatables_info" class="dataTables_info">
                                            	Mostrando 1 a 10 de 40 entradas
                                            </div>
                                        </div>

                                        <div class="col-sm-7">
                                        	<div id="datatables_paginate" class="dataTables_paginate paging_full_numbers">
                                            	<ul class="pagination">
                                                	<li id="datatables_first" class="paginate_button first disabled">
                                                    	<a tabindex="0" data-dt-idx="0" aria-controls="datatables" href="#">Inicio</a>
                                                    </li>
                                                    <li id="datatables_previous" class="paginate_button previous disabled">
                                                    	<a tabindex="0" data-dt-idx="1" aria-controls="datatables" href="#">Anterior</a>
                                                    </li>
                                                    <li class="paginate_button active">
                                                    	<a tabindex="0" data-dt-idx="2" aria-controls="datatables" href="#">1</a>
                                                    </li>
                                                    <li class="paginate_button ">
                                                    	<a tabindex="0" data-dt-idx="3" aria-controls="datatables" href="#">2</a>
                                                    </li>
                                                    <li class="paginate_button ">
                                                    	<a tabindex="0" data-dt-idx="4" aria-controls="datatables" href="#">3</a>
                                                   	</li>
                                                    <li class="paginate_button ">
                                                    	<a tabindex="0" data-dt-idx="5" aria-controls="datatables" href="#">4</a>
                                                    </li>
                                                    <li id="datatables_next" class="paginate_button next">
                                                    	<a tabindex="0" data-dt-idx="6" aria-controls="datatables" href="#">Siguiente</a>
                                                    </li>
                                                    <li id="datatables_last" class="paginate_button last">
                                                    	<a tabindex="0" data-dt-idx="7" aria-controls="datatables" href="#">Último</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                     </div>

                                  </div>
                                </div>
                              </div>

                                  </div>

                              </div>

                              <div class="tab-pane" id="pill2">
                                  <div class="col-xs-12 col-sm-12 padding0">

                                        <!-- CREDIT CARD FORM STARTS HERE -->
                                        <div class="col-xs-12 col-sm-6 margen-registro-movil">

                                            <div class="card-content">
                                                <form method="#" action="#">
                                                    <div class="form-group label-floating is-empty">
                                                        <label class="control-label">Número de Tarjeta</label>
                                                        <input class="form-control" type="text">
                                                    	<span class="material-input"></span>
                                                    </div>
                                                    <div class="form-group label-floating is-empty">
                                                        <label class="control-label">Titular</label>
                                                        <input class="form-control" type="text">
                                                    	<span class="material-input"></span>
                                                    </div>
                                                    <div class="form-group label-floating is-empty">
                                                        <label class="control-label">Password</label>
                                                        <input class="form-control" type="password">
                                                    	<span class="material-input"></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                    	<div class="col-sm-6">
                                                        	<div class="form-group label-floating is-empty">
                                                                <label class="control-label">Vencimiento</label>
                                                                <input class="form-control" type="password">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                        	<div class="form-group label-floating is-empty">
                                                                <label class="control-label">CCV</label>
                                                                <input class="form-control" type="password">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-fill" id="btn-proyecto-propuestas">Validar Tarjeta</button>
                                                </form>
                                            </div>

                                        </div>
                                		<!-- CREDIT CARD FORM ENDS HERE -->

                                        <div class="col-xs-12 col-sm-6">
                                            <b>Caraterísticas (opcionales)</b>
                                            <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                            labore et dolore magna aliqua.
                                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                            commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                            fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                            sunt in culpa qui officia deserunt mollit anim id est laborum.
                                            </p>
                                        </div>
                                  </div>
                              </div>

                          </div>
                      </div>


                  </div>
                  <!-- Termina Contenido -->

                </div>
            </div>

            <footer class="footer contenedor-footer">
                <?php include('footer.php');?>
            </footer>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>
</html>
