<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>
            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Editar Agencia</div></div>
                    </div>

                    <?php if(!empty($_GET['msj'])): ?>
                        <div class="alert alert-warning"><?= $_GET['msj'] ?></div>
                    <?php endif ?>
                    <?= $output ?>



                </div>
            </div>

            <footer class="footer contenedor-footer">
                <?php include('footer.php');?>
            </footer>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<script src="<?= base_url() ?>Theme/Cliente/assets/js/main.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        md.initSliders()
        demo.initFormExtendedDatetimepickers();
    });
</script>
</html>
