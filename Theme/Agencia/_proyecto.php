<div class="col-xs-12 col-sm-3">
  <div class="card card-product" data-count="3">
    <div class="img-historial-proyecto">
      <div class="logo-agencia">
        <img class="center-block img-responsive" src="<?= $proyecto->imagen ?>" alt="<?= $proyecto->nombre ?>" id="foto-proyecto" style="max-width:80%">
      </div>
    </div>
    <div class="card-content">
      <div class="card-title"><b><?= $proyecto->nombre ?></b></div>
      <div class="progress" id="progress-bar">
        <div class="progress-bar progress-bar-striped active <?= $proyecto->porcentaje<100?'progress-bar-warning':'progress-bar-success' ?>" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: <?= $proyecto->porcentaje ?>%">
          <b><big><?= $proyecto->porcentaje ?>%</big></b>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <?php
        $this->db->select('paises.icono, paises.nombre');
        $this->db->join('proyectos_paises','proyectos_paises.proyectos_id = proyectos.id')
             ->join('paises','paises.id = proyectos_paises.paises_id');
        $icono = $this->db->get_where('proyectos',array('proyectos.id'=>$proyecto->id));
        if($icono->num_rows()>0):
      ?>

      <?php endif ?>
      <b>Marca: <?= $proyecto->marcas ?></b><br>
      <b>Producto: <?= $proyecto->productos ?></b><br>      
      <b>País: <?= $icono->row()->nombre ?> <img src="<?= base_url() ?>img/Banderas/<?= $icono->row()->icono ?>" alt="Perfil Dashboard" class="img-responsive center-block img-circle" id="perfil-proyecto"></b><br>
      
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-9 contenedor-descripcion-agencia2 margen-movil-servicios">
  <div class="col-sm-12 texto-gris">
    <div class="col-xs-3">
      <div class="card card-stats">
        <div class="card-header" data-background-color="blue"><i class="material-icons">timeline</i></div>
        <div class="card-content">
          <p class="category">Fase:</p><br>
          <div class="card-title">
            <span class="porcentaje-agencia texto-rojo">
              <?php 
                if($proyecto->invitaciones->num_rows()>0): 
                echo $proyecto->fase?$proyecto->fase->nombre:'Finalizado' ;
                else: ?>
                Proyecto por Iniciar
                <?php endif ?>              
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-3">
      <div class="card card-stats">
        <div class="card-header" data-background-color="blue"><i class="material-icons">timer_off</i></div>
        <div class="card-content">
          <p class="category">Fin de la fase</p><br>
          <div class="card-title">
            <span class="porcentaje-agencia texto-morado">
              <?= $proyecto->fase?date("d/m/Y",strtotime($proyecto->fase->hasta)):date("d/m/Y",strtotime($this->db->query('SELECT * FROM proyectos_fechas where proyectos_id = '.$proyecto->id.' ORDER BY desde DESC')->row()->hasta)) ?>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-3">
      <div class="card card-stats">
        <div class="card-header" data-background-color="blue"><i class="material-icons">assignment_ind</i></div>
        <div class="card-content">
          <p class="category margen-titulos-agencias-tarjetas">Agencias</p><br>
          <div class="card-title">
            <span class="porcentaje-agencia texto-morado"><?= $proyecto->briefs->num_rows() ?></span>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xs-3">
      <?php if($brief->status==1): ?>
        <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
              <a href="javascript:accept()" class="btn btn-primary btn-round" id="btn-proyecto-propuestas">
                <i class="material-icons">remove_red_eye</i> <span>Aceptar invitación</span>
              </a>
        </div>
        <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
              <a href="#rechazarInvitacion" class="btn btn-round btn-rechazar-agencia-rechazar" data-toggle="modal">
                <i class="material-icons" style="color:white">clear</i> <span>Rechazar invitación</span>
              </a>
        </div>            
      <?php endif ?>
      <?php if($brief->status>1): ?>
      <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
        <a class="btn btn-primary btn-round" onclick="shareFolderAgency()" id="btn-ver-propuesta" href="javascript:;" style="width:100%;">
          <i class="material-icons">remove_red_eye</i> Subir propuestas
        </a>
      </div>
      <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
          <a title="Ver Agencia" href="<?= base_url('agencia/chat/salas/'.$proyecto->id) ?>">
            <button class="btn btn-primary btn-round" id="btn-proyecto-juntas">
              <i class="material-icons">people</i> Preguntas y Respuestas
            </button>
          </a>
      </div>
      <?php endif ?>

      <!--<div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
        <a data-toggle="modal" href="javascript:;" onclick="shareFolderAgency()" class="btn btn-primary btn-round" id="btn-proyecto-propuestas">
          <i class="material-icons">folder_special</i> <span>Carpeta compartida</span>            
        </a>
      </div>-->
    </div>



  </div>
  <div class="col-xs-12 col-sm-12 texto-gris">
    
  </div>
  <div class="col-xs-12 col-sm-12">
    <!--<div class="col-sm-8">
      <ul class="list-inline img-historial-proyecto">
        <?php foreach($proyecto->invitaciones->result() as $i): ?>

        <a title="" href="<?= base_url('cliente/proyecto/propuestas/'.$proyecto->id) ?>" data-toggle="modal">
          <li class="col-xs-2 list-group-item img-historial2 text-center">
            <img src="<?= base_url('img/agencias/'.$i->logo) ?>" alt="<?= $i->nombre ?>" class="img-responsive center-block">
            <?php if($proyecto->status!= 3 && $proyecto->status!=2): ?>
                <?php if($i->status_brief==-1): ?>
                <span title="Agencia o invitaciòn rechazada"><i class="fa fa-ban"></i></span>
                <?php endif ?>
                <?php if($i->status_brief==1): ?>
                <span title="Invitación enviada"><i class="fa fa-hourglass"></i></span>
                <?php endif ?>
                <?php if($i->status_brief==2): ?>
                <span title="Invitación aceptada"><i class="fa fa-list"></i></span>
                <?php endif ?>
                <?php if($i->status_brief==3): ?>
                <span title="Invitada a presentar"><i class="fa fa-list"></i></span>
                <?php endif ?>
                <?php if($i->status_brief==4): ?>
                <span title="Agencia Ganadora"><i class="fa fa-check"></i></span>
                <?php endif ?>
            <?php else: ?>
              <?php if($this->db->get_where('proyectos_status',array('proyectos_id'=>$proyecto->id,'estado'=>$proyecto->status,'user_id'=>$i->id))->num_rows()>0): ?>
                <span title="Agencia Enterada"><i class="fa fa-check"></i></span>
              <?php endif ?>
            <?php endif ?>
          </li>
        </a>
        <?php endforeach ?>
      </ul></div>-->

      <div class="contenedor-gant" style="min-height: 0">          
          <h3 style="padding: 0 10px;">Etapas</h3>
          <div class="contenedor-fechas" style="border:0">
              <div id="proyecto_grafico"></div>
          </div>
      </div>
    
  </div>
</div>
