<div class="sidebar-wrapper">

    <div class="user" style="border:0px; margin-top:30px;">
        <div class="photo"><img src="<?= $this->user->logo ?>" alt="Perfil Dashboard" class="img-responsive center-block"></div>
        <div class="clearfix"></div>
    </div>

    <ul class="nav">
        <li class="active">
            <a href="<?= base_url() ?>">
                <i class="material-icons">home</i><p> Inicio </p>
            </a>
        </li>

        <li>
            <a href="<?= base_url() ?>agencia/proyecto/mi_historial">
                <i class="material-icons">today</i><p> Proyectos </p>
            </a>
        </li>

    </ul>

</div>
