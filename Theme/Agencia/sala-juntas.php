<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                  <!-- Inicia Contenido -->
                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Preguntas y Respuestas</div></div>
                  </div>

                  <!-- Inicia una agencia -->
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 padding0 datos-proyecto">
                        <?= $this->load->view('Agencia/_proyecto',array('proyecto'=>$proyecto)) ?>
                        <div class="col-xs-12 col-sm-2 iconos-agencias-izquierda text-left">  
                          <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                              <a title="Ver Agencia" href="<?= base_url('agencia/chat/salas/'.$proyecto->id) ?>">
                                <button class="btn btn-primary btn-round" id="btn-proyecto-juntas">
                                  <i class="material-icons">people</i> Preguntas y Respuestas
                                </button>
                              </a>
                          </div>                 
                        </div>     
                    </div>
                  </div>


                  <div>
                    <?php /*echo $proyecto->porcentaje<100?$this->load->view('Agencia/chat/chat'):'El proyecto ha concluido';*/ ?>
                    <?php $this->load->view('Agencia/chat/chat') ?>
                  </div>

                  <!-- Termina Contenido -->

                </div>

                <footer class="footer contenedor-footer">
                    <?php include('footer.php');?>
                </footer>
                
            </div>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>

(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
var agencia_seleccionada = 0;
var onsearch = false;
var onsend = false;
var timmer = undefined;
var bottomScroll = 0;
var lastMessage = '';
function refresh(){
  var box = document.getElementById('messageBox')
  //console.log(bottomScroll+'==='+box.scrollTop);
  if(!onsearch){
    onsearch = true;
    remoteConnection('agencia/chat/chat/<?= $proyecto->id ?>',{},function(data){
      var data = JSON.parse(data);
      if(lastMessage!=data.fecha){
        lastMessage = data.fecha;
        $("#messageBox").html(data.mensajes);      
        box.scrollTo(0,box.scrollHeight);
        $("#integrantesBox").html(data.integrantes);
        bottomScroll = box.scrollTop;
      }      
      onsearch = false;
      timmer = setTimeout(function(){refresh();},5000);
    });
  }else{
    timmer = setTimeout(function(){refresh();},5000);
  }
}

refresh();

function send(f){
  if(!onsend  && $("input[name='mensaje']").val()!=''){
     onsend = true;
     f = new FormData(f);
     $("input[name='mensaje']").val('');
     $("input[name='mensaje']").attr('placeholder','Enviando mensaje, por favor espere...');
     $("input[name='mensaje']").attr('readonly',true);
    remoteConnection('agencia/chat/sendMessage/<?= $proyecto->id ?>',f,function(data){    
      onsearch = false;
      clearTimeout(timmer);
      refresh();
      $("input[name='mensaje']").attr('placeholder','Escribe un mensaje');    
      $("input[name='mensaje']").attr('readonly',false);
      onsend = false;
    });
  }
  return false;
}
</script>
</html>
