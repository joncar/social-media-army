<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body class="sidebar-mini">
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Mostrando 3 agencias</div></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-3 col-sm-2">
                            <select class="selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7" tabindex="-98">
                            <option class="bs-title-option" value="">Organizar por:</option>
                                <option selected="">Organizar por:</option>
                                <option value="2">A-Z</option>
                                <option value="3">Otro</option>
                            </select>
                        </div>
                        <div class="col-xs-9 col-sm-10 pull-left">
                            <div class="col-xs-7 col-sm-9">
                                <div id="wrap">
                                  <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-2">
                                <div id="btn-filtros">
                                    <div id="accordion" role="tablist" aria-multiselectable="true">
                                        <div role="tab" id="headingOne">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                <i class="material-icons">filter_list</i> Filtros
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-1 text-center">
                                <div class="icono-favoritos">
                                    <label class="fancy-checkbox" title="Mostrar sólo favoritos">
                                        <input type="checkbox" />
                                        <i class="fa fa-heart-o unchecked"></i>
                                        <i class="fa fa-heart checked"></i>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?php include('menu-lateral-agencias.php');?>
                        </div>
                    </div>

                    <!-- Inicia una agencia -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 contenedor-listado-agencias">
                        	  <!-- Movil -->
                            <div class="col-xs-12 padding0 imagen-agencias visible-xs">
                            	  <div class="col-xs-7">
                                    <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive">
                                    <span class="nombre-agencias">Nombre de la Agencia</span><br>
                                    <b>Certificado AA</b><br>2 años trabajando para: Grupo BIMBO<br>
                                    Estatus:<button class="btn btn-primary btn-round excelente"><i class="material-icons">check_circle</i> Activo</button>
                                </div>
                                <div class="col-xs-5 text-right">
                                    <div class="col-xs-12 col-sm-12 padding0 checkbox-radios">
                                        <div class="checkbox" id="btn-seleccionar-agencias"><label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Enviar Brief</label></div>
                                    </div>
                                    <div class="col-xs-12 iconos-favoritos-agencias">
                                        <a title="Favoritos" href="#">
                                          <button class="btn btn-primary btn-round" id="btn-favoritos"><i class="material-icons">favorite</i> Favorito</button>
                                        </a>
                                    </div>
                                    <div class="col-xs-12 iconos-favoritos-agencias">
                                        <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal">
                                          <button class="btn btn-primary btn-round" id="btn-enviar-brief"><i class="material-icons">send</i> Enviar Brief</button>
                                        </a>
                                    </div>
                                    <div class="col-xs-12 iconos-favoritos-agencias">
                                        <a title="Ver Agencia" href="agencia.php">
                                          <button class="btn btn-primary btn-round" id="btn-ver-agencia"><i class="material-icons">remove_red_eye</i> Ver agencia</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Termina Movil -->

                            <div class="col-xs-12 col-sm-3 col-md-3 padding0 imagen-agencias hidden-xs">
                            	  <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive">
                                <span class="nombre-agencias">Nombre de la Agencia</span><br>
                                <b>Certificado AA</b><br>2 años trabajando para:<br>Grupo BIMBO<br>
                                Estatus:<button class="btn btn-primary btn-round excelente"><i class="material-icons">check_circle</i> Activo</button>
                            </div>

                            <div class="col-xs-12 col-sm-7 col-md-7 iconos-agencias-top">
                                <div class="texto-datos-agencia">
                                    <div class="col-xs-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="green"><i class="material-icons">place</i></div>
                                            <div class="card-content"><p class="category">Dirección<br>Agencia</p></div>
                                            <div class="card-footer"><div class="stats">Texto de<br>la Dirección</div></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="blue"><i class="material-icons">person</i></div>
                                            <div class="card-content"><p class="category">Categoría<br>Agencia</p></div>
                                            <div class="card-footer"><div class="stats">Categoría<br>de la agencia</div></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4">
                                        <div class="card card-stats">
                                            <div class="card-header" data-background-color="orange"><i class="material-icons">stars</i></div>
                                            <div class="card-content"><p class="category">Ranking<br>Usuarios</p></div>
                                            <div class="card-footer iconos-ranking">
                                                <div class="stats" style="color:green"><i class="material-icons">verified_user</i>Excelente</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-2 col-md-2 iconos-agencias-izquierda hidden-xs">
                                <div class="col-xs-12 col-sm-12 padding0 checkbox-radios">
                                    <div class="checkbox" id="btn-seleccionar-agencias"><label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Enviar Brief</label></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                    <a title="Favoritos" href="#"><button class="btn btn-primary btn-round" id="btn-favoritos"><i class="material-icons">favorite</i> Favorito</button></a>
                                </div>
                                <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                    <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal"><button class="btn btn-primary btn-round" id="btn-enviar-brief"><i class="material-icons">send</i> Enviar Brief</button></a>
                                </div>
                                <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                    <a title="Ver Agencia" href="agencia.php"><button class="btn btn-primary btn-round" id="btn-ver-agencia"><i class="material-icons">remove_red_eye</i> Ver agencia</button></a>
                                </div>
                            </div>
                        </div>
                     </div>
                     <!-- Termina una agencia -->


                     <!-- Inicia una agencia -->
                     <div class="row">
                         <div class="col-xs-12 col-sm-12 contenedor-listado-agencias">
                         	  <!-- Movil -->
                             <div class="col-xs-12 padding0 imagen-agencias visible-xs">
                             	  <div class="col-xs-7">
                                     <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive">
                                     <span class="nombre-agencias">Nombre de la Agencia</span><br>
                                     <b>Certificado AA</b><br>2 años trabajando para: Grupo BIMBO<br>
                                     Estatus:<button class="btn btn-primary btn-round excelente"><i class="material-icons">check_circle</i> Activo</button>
                                 </div>
                                 <div class="col-xs-5 text-right">
                                     <div class="col-xs-12 col-sm-12 padding0 checkbox-radios">
                                         <div class="checkbox" id="btn-seleccionar-agencias"><label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Enviar Brief</label></div>
                                     </div>
                                     <div class="col-xs-12 iconos-favoritos-agencias">
                                         <a title="Favoritos" href="#">
                                           <button class="btn btn-primary btn-round" id="btn-favoritos"><i class="material-icons">favorite</i> Favorito</button>
                                         </a>
                                     </div>
                                     <div class="col-xs-12 iconos-favoritos-agencias">
                                         <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal">
                                           <button class="btn btn-primary btn-round" id="btn-enviar-brief"><i class="material-icons">send</i> Enviar Brief</button>
                                         </a>
                                     </div>
                                     <div class="col-xs-12 iconos-favoritos-agencias">
                                         <a title="Ver Agencia" href="agencia.php">
                                           <button class="btn btn-primary btn-round" id="btn-ver-agencia"><i class="material-icons">remove_red_eye</i> Ver agencia</button>
                                         </a>
                                     </div>
                                 </div>
                             </div>
                             <!-- Termina Movil -->

                             <div class="col-xs-12 col-sm-3 col-md-3 padding0 imagen-agencias hidden-xs">
                             	  <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive">
                                 <span class="nombre-agencias">Nombre de la Agencia</span><br>
                                 <b>Certificado AA</b><br>2 años trabajando para:<br>Grupo BIMBO<br>
                                 Estatus:<button class="btn btn-primary btn-round excelente"><i class="material-icons">check_circle</i> Activo</button>
                             </div>

                             <div class="col-xs-12 col-sm-7 col-md-7 iconos-agencias-top">
                                 <div class="texto-datos-agencia">
                                     <div class="col-xs-4 col-sm-4">
                                         <div class="card card-stats">
                                             <div class="card-header" data-background-color="green"><i class="material-icons">place</i></div>
                                             <div class="card-content"><p class="category">Dirección<br>Agencia</p></div>
                                             <div class="card-footer"><div class="stats">Texto de<br>la Dirección</div></div>
                                         </div>
                                     </div>
                                     <div class="col-xs-4 col-sm-4">
                                         <div class="card card-stats">
                                             <div class="card-header" data-background-color="blue"><i class="material-icons">person</i></div>
                                             <div class="card-content"><p class="category">Categoría<br>Agencia</p></div>
                                             <div class="card-footer"><div class="stats">Categoría<br>de la agencia</div></div>
                                         </div>
                                     </div>
                                     <div class="col-xs-4 col-sm-4">
                                         <div class="card card-stats">
                                             <div class="card-header" data-background-color="orange"><i class="material-icons">stars</i></div>
                                             <div class="card-content"><p class="category">Ranking<br>Usuarios</p></div>
                                             <div class="card-footer iconos-ranking">
                                                 <div class="stats" style="color:green"><i class="material-icons">verified_user</i>Excelente</div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>

                             <div class="col-xs-12 col-sm-2 col-md-2 iconos-agencias-izquierda hidden-xs">
                                 <div class="col-xs-12 col-sm-12 padding0 checkbox-radios">
                                     <div class="checkbox" id="btn-seleccionar-agencias"><label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Enviar Brief</label></div>
                                 </div>
                                 <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                     <a title="Favoritos" href="#"><button class="btn btn-primary btn-round" id="btn-favoritos"><i class="material-icons">favorite</i> Favorito</button></a>
                                 </div>
                                 <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                     <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal"><button class="btn btn-primary btn-round" id="btn-enviar-brief"><i class="material-icons">send</i> Enviar Brief</button></a>
                                 </div>
                                 <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                     <a title="Ver Agencia" href="agencia.php"><button class="btn btn-primary btn-round" id="btn-ver-agencia"><i class="material-icons">remove_red_eye</i> Ver agencia</button></a>
                                 </div>
                             </div>
                         </div>
                      </div>
                      <!-- Termina una agencia -->



                      <!-- Inicia una agencia -->
                      <div class="row">
                          <div class="col-xs-12 col-sm-12 contenedor-listado-agencias">
                          	  <!-- Movil -->
                              <div class="col-xs-12 padding0 imagen-agencias visible-xs">
                              	  <div class="col-xs-7">
                                      <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive">
                                      <span class="nombre-agencias">Nombre de la Agencia</span><br>
                                      <b>Certificado AA</b><br>2 años trabajando para: Grupo BIMBO<br>
                                      Estatus:<button class="btn btn-primary btn-round excelente"><i class="material-icons">check_circle</i> Activo</button>
                                  </div>
                                  <div class="col-xs-5 text-right">
                                      <div class="col-xs-12 col-sm-12 padding0 checkbox-radios">
                                          <div class="checkbox" id="btn-seleccionar-agencias"><label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Enviar Brief</label></div>
                                      </div>
                                      <div class="col-xs-12 iconos-favoritos-agencias">
                                          <a title="Favoritos" href="#">
                                            <button class="btn btn-primary btn-round" id="btn-favoritos"><i class="material-icons">favorite</i> Favorito</button>
                                          </a>
                                      </div>
                                      <div class="col-xs-12 iconos-favoritos-agencias">
                                          <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal">
                                            <button class="btn btn-primary btn-round" id="btn-enviar-brief"><i class="material-icons">send</i> Enviar Brief</button>
                                          </a>
                                      </div>
                                      <div class="col-xs-12 iconos-favoritos-agencias">
                                          <a title="Ver Agencia" href="agencia.php">
                                            <button class="btn btn-primary btn-round" id="btn-ver-agencia"><i class="material-icons">remove_red_eye</i> Ver agencia</button>
                                          </a>
                                      </div>
                                  </div>
                              </div>
                              <!-- Termina Movil -->

                              <div class="col-xs-12 col-sm-3 col-md-3 padding0 imagen-agencias hidden-xs">
                              	  <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO" class="img-responsive">
                                  <span class="nombre-agencias">Nombre de la Agencia</span><br>
                                  <b>Certificado AA</b><br>2 años trabajando para:<br>Grupo BIMBO<br>
                                  Estatus:<button class="btn btn-primary btn-round excelente"><i class="material-icons">check_circle</i> Activo</button>
                              </div>

                              <div class="col-xs-12 col-sm-7 col-md-7 iconos-agencias-top">
                                  <div class="texto-datos-agencia">
                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="green"><i class="material-icons">place</i></div>
                                              <div class="card-content"><p class="category">Dirección<br>Agencia</p></div>
                                              <div class="card-footer"><div class="stats">Texto de<br>la Dirección</div></div>
                                          </div>
                                      </div>
                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="blue"><i class="material-icons">person</i></div>
                                              <div class="card-content"><p class="category">Categoría<br>Agencia</p></div>
                                              <div class="card-footer"><div class="stats">Categoría<br>de la agencia</div></div>
                                          </div>
                                      </div>
                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="orange"><i class="material-icons">stars</i></div>
                                              <div class="card-content"><p class="category">Ranking<br>Usuarios</p></div>
                                              <div class="card-footer iconos-ranking">
                                                  <div class="stats" style="color:green"><i class="material-icons">verified_user</i>Excelente</div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-xs-12 col-sm-2 col-md-2 iconos-agencias-izquierda hidden-xs">
                                  <div class="col-xs-12 col-sm-12 padding0 checkbox-radios">
                                      <div class="checkbox" id="btn-seleccionar-agencias"><label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Enviar Brief</label></div>
                                  </div>
                                  <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Favoritos" href="#"><button class="btn btn-primary btn-round" id="btn-favoritos"><i class="material-icons">favorite</i> Favorito</button></a>
                                  </div>
                                  <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal"><button class="btn btn-primary btn-round" id="btn-enviar-brief"><i class="material-icons">send</i> Enviar Brief</button></a>
                                  </div>
                                  <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Ver Agencia" href="agencia.php"><button class="btn btn-primary btn-round" id="btn-ver-agencia"><i class="material-icons">remove_red_eye</i> Ver agencia</button></a>
                                  </div>
                              </div>
                          </div>
                       </div>
                       <!-- Termina una agencia -->

                       

                     <div class="row">
                          <div class="col-xs-12 col-sm-12 padding0 checkbox-radios">
                              <div class="col-sm-4 pull-right">
                                  <div class="checkbox" id="btn-seleccionar-agencias"><label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Seleccionar todas las Agencias</label></div>
                              </div>
                          </div>
                          <div class="col-xs-12 col-sm-12 padding0 checkbox-radios btn-seleccionar-agencias">
                              <div class="col-sm-4 pull-right margen-seleccionar-agencias">
                                  <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal">
                                    <button class="btn btn-primary btn-round" id="btn-enviar-brief"><i class="material-icons">send</i> Enviar Brief a los seleccionados</button>
                                  </a>
                              </div>
                          </div>
                      </div>

                </div>
           </div>
           <!-- Contenido -->

           <footer class="footer contenedor-footer"><?php include('footer.php');?></footer>

        </div>

    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>
</html>
