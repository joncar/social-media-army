<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<link rel="stylesheet" href="<?= base_url() ?>Theme/Cliente/assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?= base_url() ?>Theme/Cliente/assets/css/owl.theme.default.min.css">
<script src="<?= base_url() ?>Theme/Cliente/assets/js/owl.carousel.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>
            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Proyectos recientes</div></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 padding0 indicaciones-progreso hidden-xs">
                            <span class="progreso-top"><i class="fa fa-square texto-verde" aria-hidden="true"></i>  Proyecto Finalizado</span>
                            <span class="progreso-top"><i class="fa fa-square texto-amarillo" aria-hidden="true"></i>  Proyecto en Desarrollo</span>
                            <span class="progreso-top"><i class="fa fa-square texto-azul" aria-hidden="true"></i>  Proyecto por Iniciar</span>
                        </div>
                        <!-- movil-->
                        <div class="col-xs-12 col-sm-12 padding0 indicaciones-progreso visible-xs">
                            <div class="col-xs-4 progreso-top">
                              <i class="fa fa-square texto-verde" aria-hidden="true"></i><br>
                                Proyecto<br>Completo
                            </div>
                            <div class="col-xs-4 progreso-top">
                              <i class="fa fa-square texto-amarillo" aria-hidden="true"></i><br>
                                Proyecto<br>en Desarrollo
                            </div>
                            <div class="col-xs-4 progreso-top">
                              <i class="fa fa-square texto-azul" aria-hidden="true"></i><br>
                                Proyecto<br>por Iniciar
                            </div>
                        </div>

                        <div class="col-sm-12 padding0">

                          <?php
                            $this->db->select('proyectos.*');
                            $this->db->limit(4);
                            $this->db->order_by('proyectos.id','DESC');
                            $this->db->join('proyectos','proyectos.id = briefs.proyectos_id');
                            $this->db->group_by('proyectos.id');
                            $proyectos = $this->db->get_where('briefs',array('agencias_id'=>$this->user->empresa));
                          ?>
                          <?php foreach($proyectos->result() as $p):
                            $this->load->model('querys');
                            $p = $this->querys->showResult($p->id,TRUE);
                          ?>
                            <div class="col-xs-6 col-sm-3 clearfix">
                                <a href="<?= base_url('agencia/proyecto/verProyecto/'.$p->id) ?>">
                                    <div class="fondo-proyecto-dashboard">
                                      <div class="img-perfil-proyecto text-center">
                                          <div style="background:url(<?= $p->imagen ?>) no-repeat; background-size:cover; width:100%; height:130px; background-position:center center;  background-size: contain;"></div>
                                          <span>
                                              <!--<img src="<?= base_url() ?>Theme/Cliente/assets/img/Banderas/mexico.jpg" alt="Perfil Dashboard" class="img-responsive center-block img-circle" id="perfil-proyecto">-->
                                              <?php                                                 
                                                foreach($p->paises_res->result() as $pa): ?>
                                                <img src="<?= base_url('img/Banderas/'.$pa->icono) ?>" alt="<?= $pa->nombre ?>" class="img-responsive center-block img-circle" id="perfil-proyecto">
                                              <?php endforeach ?>
                                          </span>
                                      </div>
                                      <h1 class="titulo-proyecto">
                                        <?= $p->nombre ?><br/>
                                        <small><?= @$p->paises_res->row()->nombre ?> - <?= ucfirst($p->creacion) ?></small>
                                      </h1>
                                      <div class="progress" id="progress-bar">
                                          <div class="progress-bar progress-bar-striped active <?= 'progress-bar-'.$p->barra ?>" role="progressbar" aria-valuenow="<?= $p->porcentaje ?>" aria-valuemin="100" aria-valuemax="10" style="width: <?= $p->porcentaje ?>%">
                                              <b><big><?= $p->porcentaje ?>%</big></b>
                                          </div>
                                      </div>
                                    </div>
                                </a>
                            </div>
                          <?php endforeach ?>

                        </div>

                    </div>

                    <?php 
                        $agenciasPaises = $this->querys->get_agencias_por_paises();
                        $agencias = 0;
                        $porcentajes = 0;
                        $ongs = 0;
                    ?>

                    <div class="row">
                        <div class="col-sm-12 padding0 titulo-secccion titulo-original-seccion">
                          <div class="titulo-top">
                            <?= l('Agencias') ?>: <?= $this->db->get('agencias')->num_rows() ?>
                            <span style="margin-left:25px">&nbsp;</span><?= l('paises') ?>: <?= $agenciasPaises->num_rows() ?>
                            <span style="margin-left:25px">&nbsp;</span><?= l('Organizaciones') ?>: <?= $this->db->get_where('organizaciones')->num_rows() ?>
                          </div>
                        </div>
                    </div>

                    <div class="row">
                      <!------------------ Tabla agencias por paises --------------->
                      <div class="col-md-4">
                        <div class="table-responsive table-sales" style="height: 224px;">
                          <table class="table">
                            <thead>
                              <tr>
                                <th colspan="2"><?= l('País') ?></th>
                                <th style="text-align: center;"><?= l('Agencias <br/>de grupo') ?></th>
                                <th style="text-align: center;"><?= l('Agencias <br/>independientes') ?></th>                                
                              </tr>
                            </thead>
                            <tbody>
                              
                              <?php                                 
                                foreach($agenciasPaises->result() as $p):                                   
                              ?>
                                <tr>
                                  <td>
                                    <div class="flag">
                                      <img src="<?= $p->bandera ?>">
                                    </div>
                                  </td>
                                  <td><?= $p->nombre ?></a></td>
                                  <td class="text-center">
                                    <?= $p->agencias ?>
                                  </td>
                                  <td class="text-center">
                                    <?= $p->porcentaje ?>
                                  </td>
                                </tr>
                                <?php 
                                  $agencias+= $p->agencias;
                                  $porcentajes+= $p->porcentaje;                                  
                                ?>
                              <?php endforeach ?>
                                <tr>
                                  <td>
                                    <div class="flag">
                                      <img src="<?= base_url('img/Banderas/948cc-global.png') ?>">
                                    </div>
                                  </td>
                                  <td>
                                    
                                      TOTALES
                                    </a>
                                  </td>
                                  <td class="text-center">
                                    <?= $agencias ?>
                                  </td>
                                  <td class="text-center">
                                    <?= $porcentajes ?>
                                  </td>
                                </tr>


                            </tbody>
                          </table>
                          </div>
                        </div>
                        <!------------------ Mapa --------------->
                        <div class="col-md-4 ml-auto mr-auto">
                          <div id="worldMap" style="height: 300px; width:100%">                              
                          </div>
                        </div>
                        <!------------------ Tabla agencias por organizaciòn --------------->
                        <div class="col-md-4">
                        <div class="table-responsive table-sales" style="height: 224px;">
                          <table class="table">
                            <thead>
                              <tr>
                                <th><?= l('Agencias Por Organización') ?></th>
                                <th style="text-align: center;"><?= l('Cantidad') ?></th>
                              </tr>
                            </thead>
                            <tbody>
                              
                              <?php
                                $ong = $this->querys->get_agencias_por_organizacion();
                                foreach($ong->result() as $p): 
                              ?>
                                <tr>
                                  <td>
                                    <?= $p->nombre; ?>
                                  </td> 
                                  <td style="text-align: center;">
                                    <?= $p->agencias; ?>
                                  </td> 
                                </tr>
                              <?php endforeach ?>
                                


                            </tbody>
                          </table>
                          </div>
                        </div>

                </div>

                <?php
                  $proyectos = $this->querys->get_ultimas_agencias_ganadoras();
                ?>
                <div class="row">
                    <div class="col-sm-12 padding0 titulo-secccion titulo-original-seccion"><div class="titulo-top"><?= l('Asignaciones:') ?> <?= $proyectos->num_rows() ?></div></div>
                </div>
                    

                <div class="row">
                    <div class="col-xs-12 col-sm-12 padding0">
                        

                        <div class="col-xs-12 col-sm-12">
                            <div class="col-xs-12 col-sm-12 contenedor-agencias-dashboard">                                    
                                <div class="col-xs-12 col-sm-12 padding0 owl-carousel owl-theme" data-loop="false">                                      
                                    <?php foreach($proyectos->result() as $p): ?>                                          
                                          <div class="col-xs-12 links-newsfeed item">
                                            <a href="#">
                                              <div style="background:url(<?= base_url('img/agencias/'.$p->logo) ?>) no-repeat; background-size:100%; width:100%; height:130px; background-position:center; background-size: contain;"></div>
                                            </a><br>
                                            <a href="#">
                                              <div class="text-center texto-agencias-dashboard titulo-proyecto" style="color: #397dc3;">
                                                <?= $p->proyecto->nombre ?>
                                              </div>
                                            </a>
                                              <div class="text-center texto-agencias-dashboard titulo-proyecto">                                                    
                                                <center><img src="<?= base_url('img/Banderas/'.$p->bandera) ?>" alt="" style="width:12px" class="text-center"></center>
                                                <b><?= $p->nombre ?></b><br>
                                                <small>
                                                  <?= $p->pais ?> - <?= ucfirst(strftime('%B %Y',strtotime($p->fecha_ganadora))) ?>
                                                </small>
                                              </div>
                                          </div>
                                      
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                </div>
            </div>
            <script src="https://apps.elfsight.com/p/platform.js" defer></script>
            <div class="elfsight-app-15534181-e024-47c8-8774-13e8b6a5eebb"></div>

            <footer class="footer contenedor-footer">
                <?php include('footer.php');?>
            </footer>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<script src="<?= base_url() ?>Theme/Cliente/assets/js/main.js"></script>
<script>
  $(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
      margin: 10,
      nav: true,
      loop: false,
      items:4,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
        1000: {
          items: 4
        }
      }
    })
  })
</script>

<script>
  $(document).ready(function() {    
        var mapData = <?php 
          $paises = array();
          foreach($agenciasPaises->result() as $p){
            $paises[$p->abreviacion] = $p->agencias;
          }
          echo json_encode($paises);
        ?>;

        $('#worldMap').vectorMap({
          map: 'world_mill_en',
          backgroundColor: "transparent",
          zoomOnScroll: false,
          regionStyle: {
            initial: {
              fill: '#e4e4e4',
              "fill-opacity": 0.9,
              stroke: 'none',
              "stroke-width": 0,
              "stroke-opacity": 0
            }
          },

          series: {
            regions: [{
              values: mapData,
              scale: ["#f9e6ef", "#931651"],
              normalizeFunction: 'polynomial'
            }]
          },
        });      
  });
</script>
</html>
