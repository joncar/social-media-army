<div class="modal fade" id="registro-agencia-login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
  </div>
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show" style="display: block; width: 700px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
              </div>
                <div class="logo-modales">
                    <img src="<?= $this->user->logo ?>" alt="Logo Bimbo" class="img-responsive center-block">
                </div>
                <div class="titulo-modal"><b>Registro de Agencia</b></div>

                <div class="wizard">
                    <div id="messageSubmit"></div>
                    <ul class="nav nav-wizard">
                        <li class="active"><a href="#registroagencia1" data-toggle="tab">Datos</a></li>
                        <li><a href="#registroagencia2" data-toggle="tab">Experiencia</a></li>
                        <li><a href="#registroagencia3" data-toggle="tab">Servicios</a></li>
                        <li><a href="#registroagencia4" data-toggle="tab">Terminar registro</a></li>
                    </ul>

                    <form action="" onsubmit="return send(this)">
                        <div class="tab-content">
                            <div class="tab-pane active" id="registroagencia1">
                                <div class="col-sm-12 subtitulo-modal"><b>Completa la información de tu empresa</b></div>

                                <div class="col-xs-12 form-group">
                                    <label class="btn btn-subir-img text-center">
                                        <img src="Theme/Cliente/assets/img/login/subir-imagen.png" alt="Logo Bimbo" class="img-responsive center-block">
                                        Subir logo
                                        <input type="file" name="imagen" class="fileImage">
                                    </label>
                                </div>

                                <div class="col-xs-12 form-group">
                                    <label class="control-label">Nombre comercial de la agencia</label>
                                    <input  maxlength="100" type="text" name="nombre" class="form-control" placeholder=""  />
                                </div>

                                <div class="col-sm-12">
                                    <label class="control-label">Alcance local / global</label>
                                    <select class="form-control">
                                        <option>Opción 1</option>
                                        <option>Opción 2</option>
                                        <option>Opción 3</option>
                                    </select>
                                </div>

                                <div class="col-sm-12">
                                    <label class="control-label">Reconocimientos</label>
                                    <select class="form-control">
                                        <option>Reconocimiento 1</option>
                                        <option>Reconocimiento 2</option>
                                        <option>Reconocimiento 3</option>
                                    </select>
                                </div>

                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 2</button></li>
                                </ul>
                            </div>

                            <div class="tab-pane" id="registroagencia2">
                                <div class="col-sm-12 padding0 subtitulo-modal">
                                    <b>Experiencia</b>
                                </div>

                                <div class="col-sm-12">
                                    <label class="control-label">Año de constitución</label>
                                    <select class="form-control">
                                        <option>Opción 1</option>
                                        <option>Opción 2</option>
                                        <option>Opción 3</option>
                                    </select>
                                </div>

                                <div class="col-sm-12">
                                    <label class="control-label">Año de ingreso como proveedor</label>
                                    <select class="form-control">
                                        <option>Opción 1</option>
                                        <option>Opción 2</option>
                                        <option>Opción 3</option>
                                    </select>
                                </div>

                                <div class="col-sm-12">
                                    <label class="control-label">Certificación</label>
                                    <select class="form-control">
                                        <option>Opción 1</option>
                                        <option>Opción 2</option>
                                        <option>Opción 3</option>
                                    </select>
                                </div>

                                <div class="col-sm-12">
                                    <label class="control-label">Experiencia</label>
                                    <select class="form-control">
                                        <option>Opción 1</option>
                                        <option>Opción 2</option>
                                        <option>Opción 3</option>
                                    </select>
                                </div>

                                <div class="col-sm-12">
                                    <label class="control-label">Procesos y metodología</label>
                                    <select class="form-control">
                                        <option>Opción 1</option>
                                        <option>Opción 2</option>
                                        <option>Opción 3</option>
                                    </select>
                                </div>

                                <div class="col-sm-12">
                                    <label class="control-label">Recursos</label>
                                    <select class="form-control">
                                        <option>Opción 1</option>
                                        <option>Opción 2</option>
                                        <option>Opción 3</option>
                                    </select>
                                </div>

                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 3</button></li>
                                </ul>
                            </div>

                            <div class="tab-pane" id="registroagencia3">
                                <div class="col-sm-12 subtitulo-modal"><b>Completa la información de tu empresa</b></div>






                                <div class="tab-content">
                            <div class="tab-pane" id="paso1">
                                <div class="col-sm-12 subtitulo-modal">
                                    <b>Datos del Proyecto</b>
                                </div>


                                <div class="col-xs-12 form-group">
                                    <label class="btn btn-subir-img text-center">
                                        <img src="http://briefit.com.mx/Theme/Cliente/assets/img/login/subir-imagen.png" alt="Logo Bimbo" class="img-responsive center-block">
                                        Imagen del proyecto
                                        <input name="imagen" class="fileImage" type="file">
                                    </label>
                                </div>

                                <div class="col-xs-12 form-group is-empty">
                                    <label class="control-label">Nombre del Proyecto</label>
                                    <input maxlength="100" name="nombre" class="form-control" placeholder="" type="text">
                                <span class="material-input"></span></div>

                                <div class="col-sm-12 subtitulo-modal">
                                    <b>Agregar Supervisores o Colaboradores</b>
                                </div>
                                <div class="col-sm-12 form-group input-group">
                                	<div class="select-agencias">
                                        <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" title="Seleccione los supervisores"><span class="filter-option pull-left">Seleccione los supervisores</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Seleccione un usuario</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Jerry</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Jonathan Empresa</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select name="proyectos_colaboradores[supervisores][]" data-style="select-with-transition" multiple="" class="selectpicker" title="Seleccione los supervisores" tabindex="-98">
                                            <option>Seleccione un usuario</option>
                                                                                                                                        <option value="59">Jerry</option>
                                                                                            <option value="63">Jonathan Empresa</option>
                                                                                    </select></div>
                                    </div>
                                </div>

                                <div class="col-sm-12 form-group input-group">
                                    <div class="select-agencias">
                                        <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" title="Seleccione los colaboradores"><span class="filter-option pull-left">Seleccione los colaboradores</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Seleccione un usuario</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Jerry</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Jonathan Empresa</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select name="proyectos_colaboradores[colaboradores][]" data-style="select-with-transition" multiple="" class="selectpicker" title="Seleccione los colaboradores" tabindex="-98">
                                            <option>Seleccione un usuario</option>
                                                                                                                                        <option value="59">Jerry</option>
                                                                                            <option value="63">Jonathan Empresa</option>
                                                                                    </select></div>
                                    </div>
                                </div>

                                <div class="col-sm-12 subtitulo-modal">
                                    <b>Datos Generales</b>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" data-id="paises" title="País"><span class="filter-option pull-left">País</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Argentina</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Brazil</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Canada</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="3"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Chile</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="4"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">China</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="5"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Colombia</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="6"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Costa Rica</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="7"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Ecuador</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="8"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">El Salvador</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="9"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Global</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="10"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Guatemala</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="11"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Honduras</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="12"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">India</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="13"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Mexico</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="14"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Morocco</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="15"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Nicaragua</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="16"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Panama</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="17"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Paraguay</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="18"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Peru</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="19"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Portugal</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="20"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Spain</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="21"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">United Kingdom</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="22"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">United States</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="23"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Uruguay</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="24"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Venezuela</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="25"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text"></span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" id="paises" name="proyectos_paises[]" data-style="select-with-transition" multiple="" title="País" data-size="7" tabindex="-98">
                                                  			                                          <option value="1">Argentina</option>
			                                      			                                          <option value="2">Brazil</option>
			                                      			                                          <option value="3">Canada</option>
			                                      			                                          <option value="4">Chile</option>
			                                      			                                          <option value="5">China</option>
			                                      			                                          <option value="6">Colombia</option>
			                                      			                                          <option value="7">Costa Rica</option>
			                                      			                                          <option value="8">Ecuador</option>
			                                      			                                          <option value="9">El Salvador</option>
			                                      			                                          <option value="10">Global</option>
			                                      			                                          <option value="11">Guatemala</option>
			                                      			                                          <option value="12">Honduras</option>
			                                      			                                          <option value="13">India</option>
			                                      			                                          <option value="14">Mexico</option>
			                                      			                                          <option value="15">Morocco</option>
			                                      			                                          <option value="16">Nicaragua</option>
			                                      			                                          <option value="17">Panama</option>
			                                      			                                          <option value="18">Paraguay</option>
			                                      			                                          <option value="19">Peru</option>
			                                      			                                          <option value="20">Portugal</option>
			                                      			                                          <option value="21">Spain</option>
			                                      			                                          <option value="22">United Kingdom</option>
			                                      			                                          <option value="23">United States</option>
			                                      			                                          <option value="24">Uruguay</option>
			                                      			                                          <option value="25">Venezuela</option>
			                                      			                                          <option value="26"></option>
			                                                                                      </select></div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" title="Categoría"><span class="filter-option pull-left">Categoría</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Categoria 1</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" name="proyectos_categorias[]" data-style="select-with-transition" multiple="" title="Categoría" data-size="7" tabindex="-98">
                                                  			                                          <option value="1">Categoria 1</option>
			                                                                                      </select></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" data-id="marcas" title="Marca"><span class="filter-option pull-left">Marca</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Fargo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Lactal</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="3"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Ricard</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="4"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Oroweat</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="5"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Tia Rosa</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="6"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Valente</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="7"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Pullman / PlusVita</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="8"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Nutrella</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="9"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Ana Maria</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="10"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Pingüinos</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="11"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Crocantíssimo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="12"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">RAP 10</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="13"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">California Goldminer &amp; Sourdough</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="14"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Maison Cousin</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="15"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Tenderflake</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="16"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Wholsome Harvest</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="17"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Ben's</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="18"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bon Matin</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="19"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Boulanges des campagnards</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="20"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Cinnabon</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="21"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Dempster's </span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="22"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Eureka</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="23"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Gailuron</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="24"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Olafson's</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="25"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Petite Douceur</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="26"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">POM</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="27"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sanissimo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="28"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sun Maid</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="29"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Tia Rosa</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="30"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Villaggio</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="31"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Weight Watchers</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="32"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Stonemill </span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="33"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Vachon</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="34"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sara Lee</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="35"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Hostess</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="36"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Barcel</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="37"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Agua de Piedra</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="38"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Cena</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="39"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Fuchs</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="40"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Ideal</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="41"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Dulcypas</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="42"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Housebrot</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="43"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Lagos del Sur</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="44"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Marinela</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="45"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sanissimo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="46"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Tia Rosa</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="47"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bimbo  (??)</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="48"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Million Land (????)</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="49"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="50"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Guadalupe</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="51"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Lalo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="52"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Mamá Inés</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="53"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Marinela</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="54"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Tia Rosa</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="55"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Panettiere</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="56"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="57"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Marinela</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="58"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Braun</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="59"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sanissimo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="60"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Character - Osito Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="61"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Character - Pingüinos</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="62"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Character - Gansito</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="63"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Character- Tia Rosa</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="64"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Character - Little Bites</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="65"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Character - Nito</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="66"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Institutional</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="67"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Futbolito Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="68"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">10 Energy</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="69"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Rise and Shine</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="70"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Nutrición Grupo Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="71"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">GBF</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="72"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">La Balance</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="73"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">El Globo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="74"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">El Molino</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="75"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="76"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Tia Rosa</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="77"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Del Hogar</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="78"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Kodyz</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="79"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sanissimo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="80"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Lonchibon</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="81"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Marinela</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="82"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="83"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Oroweat</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="84"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Marinela</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="85"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Maestro Cubano</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="86"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Pantodos</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="87"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="88"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Marinela</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="89"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">PYC</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="90"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Rolly's</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="91"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sanissimo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="92"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Tia Rosa</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="93"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="94"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Eagle</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="95"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">The Rustik Bakery</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="96"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Oroweat</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="97"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Ortiz</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="98"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Donuts</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="99"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bollycao</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="100"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Thins</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="101"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Silueta</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="102"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Donettes</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="103"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="104"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bollycao</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="105"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Donettes</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="106"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Donuts</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="107"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Eagle</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="108"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Weikis</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="109"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">La Bella Easo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="110"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Martinez</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="111"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Oroweat</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="112"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Ortiz</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="113"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Qé</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="114"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Thins</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="115"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Silueta</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="116"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">The Rustik Bakery</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="117"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">ABO (Arnold, Brownberry, Oroweat)</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="118"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Alfaro's</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="119"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Redding</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="120"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Ball Park</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="121"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Boboli</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="122"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Colonial</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="123"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Colombo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="124"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">D'taliano</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="125"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Entenmanns</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="126"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Eureka</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="127"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Francisco</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="128"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Freihofer</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="129"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Grandma Sycamore</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="130"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Goodbye Gluten</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="131"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Heiners</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="132"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Holsum</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="133"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Lumberjack</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="134"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Maiers</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="135"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Master</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="136"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Mothers</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="137"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Mrs Baird's</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="138"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Nature's Harvest</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="139"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Nissen</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="140"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Rainbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="141"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sahara</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="142"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">San Luis Sourdough</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="143"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sara Lee</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="144"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Stroehmann</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="145"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sun Maid</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="146"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sunbeam</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="147"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Thomas'</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="148"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="149"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Marinela</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="150"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Tia Rosa</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="151"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Barcel</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="152"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Ricolino</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="153"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Coronado</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="154"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Vero</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="155"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">California Goldminer &amp; Sourdough</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="156"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Grace Baking</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="157"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Wholsome Harvest</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="158"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Bimbo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="159"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Il Genovese</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="160"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Los Sorchantes</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="161"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Maestro Cubano</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="162"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Pan Catalán</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="163"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Pik</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="164"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Kaiser</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="165"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Doria</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="166"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Ricard</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="167"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Sanissimo</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="168"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Tia Rosa</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="169"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">TEST</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" id="marcas" name="proyectos_marcas[]" data-style="select-with-transition" multiple="" title="Marca" data-size="7" tabindex="-98">
                                                  			                                          <option value="1">Bimbo</option>
			                                      			                                          <option value="2">Fargo</option>
			                                      			                                          <option value="3">Lactal</option>
			                                      			                                          <option value="4">Ricard</option>
			                                      			                                          <option value="5">Oroweat</option>
			                                      			                                          <option value="6">Tia Rosa</option>
			                                      			                                          <option value="7">Valente</option>
			                                      			                                          <option value="8">Pullman / PlusVita</option>
			                                      			                                          <option value="9">Nutrella</option>
			                                      			                                          <option value="10">Ana Maria</option>
			                                      			                                          <option value="11">Pingüinos</option>
			                                      			                                          <option value="12">Crocantíssimo</option>
			                                      			                                          <option value="13">RAP 10</option>
			                                      			                                          <option value="14">California Goldminer &amp; Sourdough</option>
			                                      			                                          <option value="15">Maison Cousin</option>
			                                      			                                          <option value="16">Tenderflake</option>
			                                      			                                          <option value="17">Wholsome Harvest</option>
			                                      			                                          <option value="18">Ben's</option>
			                                      			                                          <option value="19">Bon Matin</option>
			                                      			                                          <option value="20">Boulanges des campagnards</option>
			                                      			                                          <option value="21">Cinnabon</option>
			                                      			                                          <option value="22">Dempster's </option>
			                                      			                                          <option value="23">Eureka</option>
			                                      			                                          <option value="24">Gailuron</option>
			                                      			                                          <option value="25">Olafson's</option>
			                                      			                                          <option value="26">Petite Douceur</option>
			                                      			                                          <option value="27">POM</option>
			                                      			                                          <option value="28">Sanissimo</option>
			                                      			                                          <option value="29">Sun Maid</option>
			                                      			                                          <option value="30">Tia Rosa</option>
			                                      			                                          <option value="31">Villaggio</option>
			                                      			                                          <option value="32">Weight Watchers</option>
			                                      			                                          <option value="33">Stonemill </option>
			                                      			                                          <option value="34">Vachon</option>
			                                      			                                          <option value="35">Sara Lee</option>
			                                      			                                          <option value="36">Hostess</option>
			                                      			                                          <option value="37">Barcel</option>
			                                      			                                          <option value="38">Agua de Piedra</option>
			                                      			                                          <option value="39">Cena</option>
			                                      			                                          <option value="40">Fuchs</option>
			                                      			                                          <option value="41">Ideal</option>
			                                      			                                          <option value="42">Dulcypas</option>
			                                      			                                          <option value="43">Housebrot</option>
			                                      			                                          <option value="44">Lagos del Sur</option>
			                                      			                                          <option value="45">Marinela</option>
			                                      			                                          <option value="46">Sanissimo</option>
			                                      			                                          <option value="47">Tia Rosa</option>
			                                      			                                          <option value="48">Bimbo  (??)</option>
			                                      			                                          <option value="49">Million Land (????)</option>
			                                      			                                          <option value="50">Bimbo</option>
			                                      			                                          <option value="51">Guadalupe</option>
			                                      			                                          <option value="52">Lalo</option>
			                                      			                                          <option value="53">Mamá Inés</option>
			                                      			                                          <option value="54">Marinela</option>
			                                      			                                          <option value="55">Tia Rosa</option>
			                                      			                                          <option value="56">Panettiere</option>
			                                      			                                          <option value="57">Bimbo</option>
			                                      			                                          <option value="58">Marinela</option>
			                                      			                                          <option value="59">Braun</option>
			                                      			                                          <option value="60">Sanissimo</option>
			                                      			                                          <option value="61">Character - Osito Bimbo</option>
			                                      			                                          <option value="62">Character - Pingüinos</option>
			                                      			                                          <option value="63">Character - Gansito</option>
			                                      			                                          <option value="64">Character- Tia Rosa</option>
			                                      			                                          <option value="65">Character - Little Bites</option>
			                                      			                                          <option value="66">Character - Nito</option>
			                                      			                                          <option value="67">Institutional</option>
			                                      			                                          <option value="68">Futbolito Bimbo</option>
			                                      			                                          <option value="69">10 Energy</option>
			                                      			                                          <option value="70">Rise and Shine</option>
			                                      			                                          <option value="71">Nutrición Grupo Bimbo</option>
			                                      			                                          <option value="72">GBF</option>
			                                      			                                          <option value="73">La Balance</option>
			                                      			                                          <option value="74">El Globo</option>
			                                      			                                          <option value="75">El Molino</option>
			                                      			                                          <option value="76">Bimbo</option>
			                                      			                                          <option value="77">Tia Rosa</option>
			                                      			                                          <option value="78">Del Hogar</option>
			                                      			                                          <option value="79">Kodyz</option>
			                                      			                                          <option value="80">Sanissimo</option>
			                                      			                                          <option value="81">Lonchibon</option>
			                                      			                                          <option value="82">Marinela</option>
			                                      			                                          <option value="83">Bimbo</option>
			                                      			                                          <option value="84">Oroweat</option>
			                                      			                                          <option value="85">Marinela</option>
			                                      			                                          <option value="86">Maestro Cubano</option>
			                                      			                                          <option value="87">Pantodos</option>
			                                      			                                          <option value="88">Bimbo</option>
			                                      			                                          <option value="89">Marinela</option>
			                                      			                                          <option value="90">PYC</option>
			                                      			                                          <option value="91">Rolly's</option>
			                                      			                                          <option value="92">Sanissimo</option>
			                                      			                                          <option value="93">Tia Rosa</option>
			                                      			                                          <option value="94">Bimbo</option>
			                                      			                                          <option value="95">Eagle</option>
			                                      			                                          <option value="96">The Rustik Bakery</option>
			                                      			                                          <option value="97">Oroweat</option>
			                                      			                                          <option value="98">Ortiz</option>
			                                      			                                          <option value="99">Donuts</option>
			                                      			                                          <option value="100">Bollycao</option>
			                                      			                                          <option value="101">Thins</option>
			                                      			                                          <option value="102">Silueta</option>
			                                      			                                          <option value="103">Donettes</option>
			                                      			                                          <option value="104">Bimbo</option>
			                                      			                                          <option value="105">Bollycao</option>
			                                      			                                          <option value="106">Donettes</option>
			                                      			                                          <option value="107">Donuts</option>
			                                      			                                          <option value="108">Eagle</option>
			                                      			                                          <option value="109">Weikis</option>
			                                      			                                          <option value="110">La Bella Easo</option>
			                                      			                                          <option value="111">Martinez</option>
			                                      			                                          <option value="112">Oroweat</option>
			                                      			                                          <option value="113">Ortiz</option>
			                                      			                                          <option value="114">Qé</option>
			                                      			                                          <option value="115">Thins</option>
			                                      			                                          <option value="116">Silueta</option>
			                                      			                                          <option value="117">The Rustik Bakery</option>
			                                      			                                          <option value="118">ABO (Arnold, Brownberry, Oroweat)</option>
			                                      			                                          <option value="119">Alfaro's</option>
			                                      			                                          <option value="120">Redding</option>
			                                      			                                          <option value="121">Ball Park</option>
			                                      			                                          <option value="122">Boboli</option>
			                                      			                                          <option value="123">Colonial</option>
			                                      			                                          <option value="124">Colombo</option>
			                                      			                                          <option value="125">D'taliano</option>
			                                      			                                          <option value="126">Entenmanns</option>
			                                      			                                          <option value="127">Eureka</option>
			                                      			                                          <option value="128">Francisco</option>
			                                      			                                          <option value="129">Freihofer</option>
			                                      			                                          <option value="130">Grandma Sycamore</option>
			                                      			                                          <option value="131">Goodbye Gluten</option>
			                                      			                                          <option value="132">Heiners</option>
			                                      			                                          <option value="133">Holsum</option>
			                                      			                                          <option value="134">Lumberjack</option>
			                                      			                                          <option value="135">Maiers</option>
			                                      			                                          <option value="136">Master</option>
			                                      			                                          <option value="137">Mothers</option>
			                                      			                                          <option value="138">Mrs Baird's</option>
			                                      			                                          <option value="139">Nature's Harvest</option>
			                                      			                                          <option value="140">Nissen</option>
			                                      			                                          <option value="141">Rainbo</option>
			                                      			                                          <option value="142">Sahara</option>
			                                      			                                          <option value="143">San Luis Sourdough</option>
			                                      			                                          <option value="144">Sara Lee</option>
			                                      			                                          <option value="145">Stroehmann</option>
			                                      			                                          <option value="146">Sun Maid</option>
			                                      			                                          <option value="147">Sunbeam</option>
			                                      			                                          <option value="148">Thomas'</option>
			                                      			                                          <option value="149">Bimbo</option>
			                                      			                                          <option value="150">Marinela</option>
			                                      			                                          <option value="151">Tia Rosa</option>
			                                      			                                          <option value="152">Barcel</option>
			                                      			                                          <option value="153">Ricolino</option>
			                                      			                                          <option value="154">Coronado</option>
			                                      			                                          <option value="155">Vero</option>
			                                      			                                          <option value="156">California Goldminer &amp; Sourdough</option>
			                                      			                                          <option value="157">Grace Baking</option>
			                                      			                                          <option value="158">Wholsome Harvest</option>
			                                      			                                          <option value="159">Bimbo</option>
			                                      			                                          <option value="160">Il Genovese</option>
			                                      			                                          <option value="161">Los Sorchantes</option>
			                                      			                                          <option value="162">Maestro Cubano</option>
			                                      			                                          <option value="163">Pan Catalán</option>
			                                      			                                          <option value="164">Pik</option>
			                                      			                                          <option value="165">Kaiser</option>
			                                      			                                          <option value="166">Doria</option>
			                                      			                                          <option value="167">Ricard</option>
			                                      			                                          <option value="168">Sanissimo</option>
			                                      			                                          <option value="169">Tia Rosa</option>
			                                      			                                          <option value="171">TEST</option>
			                                                                                      </select></div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" data-id="productos" title="Producto"><span class="filter-option pull-left">Producto</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">TEST</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" id="productos" name="proyectos_productos[]" data-style="select-with-transition" multiple="" title="Producto" data-size="7" tabindex="-98">
                                                                                                      			                                          <option value="585">TEST</option>
			                                                                                      </select></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 2</button></li>
                                </ul>
                            </div>

                            <div class="tab-pane" id="paso2">
                                <div class="col-sm-12 padding0 subtitulo-modal">
                                    <b>Detalles del proyecto</b>
                                </div>
                                <div class="form-group is-empty">
                                    <label class="control-label">Solicitud puntual</label>
                                    <textarea name="solicitud_puntual" class="form-control" id="textarea" placeholder="Máx. 300 carácteres"></textarea>
                                <span class="material-input"></span></div>
                                <div class="form-group is-empty">
                                    <label class="control-label">Objetivo del Proyecto</label>
                                    <textarea name="objetivo" class="form-control" id="textarea" placeholder="¿Cuál es el objetivo que se debe cumplir?"></textarea>
                                <span class="material-input"></span></div>
                                <div class="form-group is-empty">
                                    <label class="control-label">Target</label>
                                    <textarea name="target" class="form-control" id="textarea" placeholder="¿A quién dirigiremos esta iniciativa?"></textarea>
                                <span class="material-input"></span></div>
                                <div class="form-group is-empty">
                                    <label class="control-label">Especificaciones / Observaciones generales</label>
                                    <textarea name="especificaciones" class="form-control" id="textarea" placeholder="Ingresa las especificaciones puntuales que se deben seguir"></textarea>
                                <span class="material-input"></span></div>


                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 3</button></li>
                                </ul>
                            </div>

                            <div class="tab-pane" id="paso3">
                                <div class="col-sm-12 subtitulo-modal" style="position:relative;">
                                    <b>Ingresa la fecha límite para cada etapa del proyecto</b>
                                </div>
                                <div class="DateInfo">
                                    <div class="col-sm-12 dateslist form-group input-group rowDate">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Etapa</label>
                                                        <input name="proyectos_fechas[nombre][]" class="form-control" value="Invitación" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input name="proyectos_fechas[fecha_desde][]" class="form-control datetimepicker" value="15/05/2018 18:27" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input name="proyectos_fechas[fecha_hasta][]" class="form-control datetimepicker" value="15/05/2018 18:27" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" style="display: none" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular" class="noUi-target noUi-ltr noUi-horizontal"><div class="noUi-base"><div class="noUi-connect" style="left: 0%; right: 60%;"></div><div class="noUi-origin" style="left: 40%;"><div class="noUi-handle noUi-handle-lower" data-handle="0" tabindex="0" role="slider" aria-orientation="horizontal" style="z-index: 4;" aria-valuemin="0.0" aria-valuemax="100.0" aria-valuenow="40.0" aria-valuetext="40.00"></div></div></div></div><div id="sliderDouble" class="noUi-target noUi-ltr noUi-horizontal"><div class="noUi-base"><div class="noUi-origin" style="left: 20%;"><div class="noUi-handle noUi-handle-lower" data-handle="0" tabindex="0" role="slider" aria-orientation="horizontal" style="z-index: 5;" aria-valuemin="0.0" aria-valuemax="60.0" aria-valuenow="20.0" aria-valuetext="20.00"></div></div><div class="noUi-connect" style="left: 20%; right: 40%;"></div><div class="noUi-origin" style="left: 60%;"><div class="noUi-handle noUi-handle-upper" data-handle="1" tabindex="0" role="slider" aria-orientation="horizontal" style="z-index: 4;" aria-valuemin="20.0" aria-valuemax="100.0" aria-valuenow="60.0" aria-valuetext="60.00"></div></div></div></div>
                                    </div>

                                    <div class="col-sm-12 dateslist form-group input-group">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Etapa</label>
                                                        <input name="proyectos_fechas[nombre][]" class="form-control" value="Sesión de preguntas y respuestas" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input name="proyectos_fechas[fecha_desde][]" class="form-control datetimepicker" value="15/05/2018" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input name="proyectos_fechas[fecha_hasta][]" class="form-control datetimepicker" value="15/05/2018" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular"></div><div id="sliderDouble"></div>
                                    </div>

                                    <div class="col-sm-12 dateslist form-group input-group">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Etapa</label>
                                                        <input name="proyectos_fechas[nombre][]" class="form-control" value="Desarrollo de propuestas" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input name="proyectos_fechas[fecha_desde][]" class="form-control datetimepicker" value="15/05/2018" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input name="proyectos_fechas[fecha_hasta][]" class="form-control datetimepicker" value="15/05/2018" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular"></div><div id="sliderDouble"></div>
                                    </div>

                                    <div class="col-sm-12 dateslist form-group input-group">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Etapa</label>
                                                        <input name="proyectos_fechas[nombre][]" class="form-control" value="Evaluación de propuestas" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input name="proyectos_fechas[fecha_desde][]" class="form-control datetimepicker" value="15/05/2018" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input name="proyectos_fechas[fecha_hasta][]" class="form-control datetimepicker" value="15/05/2018" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular"></div><div id="sliderDouble"></div>
                                    </div>

                                    <div class="col-sm-12 dateslist form-group input-group">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Etapa</label>
                                                        <input name="proyectos_fechas[nombre][]" class="form-control" value="Selección del ganador" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input name="proyectos_fechas[fecha_desde][]" class="form-control datetimepicker" value="15/05/2018" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input name="proyectos_fechas[fecha_hasta][]" class="form-control datetimepicker" value="15/05/2018" type="text">
                                                    <span class="material-input"></span></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular"></div><div id="sliderDouble"></div>
                                    </div>
                                </div>
                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li><button type="button" class="nonavigate btn btn-primary btn-addDate">Añadir etapa</button></li>
                                    <li><button type="button" id="btn-enviar-brief" class="OrderingDate btn btn-reacomodo-fechas">Guardar cambios</button></li>
                                    <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 4</button></li>
                                </ul>
                            </div>

                            <div class="tab-pane active" id="paso4">
                                <div class="col-sm-12 subtitulo-modal">
                                    <b>Agregar filtros de Agencia</b>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="select-agencias">
                                            <div class="btn-group bootstrap-select show-tick dropup"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" title="Content Services" aria-expanded="false"><span class="filter-option pull-left">Content Services <small></small></span>&nbsp;<span class="bs-caret"><span class="caret"></span></span><div class="ripple-container"><div class="ripple ripple-on ripple-out" style="left: 712px; top: 470px; background-color: rgb(60, 72, 88); transform: scale(81.25);"></div></div></button><div class="dropdown-menu open" role="combobox" style="max-height: 450.217px; overflow: hidden; min-height: 119px;"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false" style="max-height: 450.217px; overflow-y: auto; min-height: 119px;"><li data-original-index="0" class="disabled"><a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="false"><span class="text">CONTENT SERVICES</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Blogging</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">CopyWritting</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="3"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Gaming</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="4"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Promotions</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="5"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">SEO</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="6"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Testing/personalization</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="7"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Video</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" name="proyectos_content_services[]" data-style="select-with-transition" multiple="" title="Content Services <small><i class=" material-icons"="" tabindex="-98">arrow_drop_down" data-size="7"&gt;
                                            	<option disabled="">CONTENT SERVICES</option>
                                              			                                          <option value="1">Blogging</option>
			                                  			                                          <option value="2">CopyWritting</option>
			                                  			                                          <option value="3">Gaming</option>
			                                  			                                          <option value="4">Promotions</option>
			                                  			                                          <option value="5">SEO</option>
			                                  			                                          <option value="6">Testing/personalization</option>
			                                  			                                          <option value="7">Video</option>
			                                                                              </select></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="select-agencias">
                                            <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" title="Traditional Marketing"><span class="filter-option pull-left">Traditional Marketing</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0" class="disabled"><a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="false"><span class="text">TRADITIONAL MARKETING</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Digital media planning and buying</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Display Marketing</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="3"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Email Marketing</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="4"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Search Marketing</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" name="proyectos_traditional_mkt[]" data-style="select-with-transition" multiple="" title="Traditional Marketing" data-size="7" tabindex="-98">
                                            Traditional Marketing arrow_drop_down
                                        	  <option disabled="">TRADITIONAL MARKETING</option>
                                              			                                          <option value="1">Digital media planning and buying</option>
			                                  			                                          <option value="2">Display Marketing</option>
			                                  			                                          <option value="3">Email Marketing</option>
			                                  			                                          <option value="4">Search Marketing</option>
			                                                                              </select></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="select-agencias">
                                            <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" title="Creative Services"><span class="filter-option pull-left">Creative Services <small></small></span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0" class="disabled"><a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="false"><span class="text">CREATIVE SERVICES</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Visual Design</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">User experience</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" name="proyectos_creative_services[]" data-style="select-with-transition" multiple="" title="Creative Services <small><i class=" material-icons"="" tabindex="-98">arrow_drop_down" data-size="7"&gt;
                                              <option disabled="">CREATIVE SERVICES</option>
                                              			                                          <option value="1">Visual Design</option>
			                                  			                                          <option value="2">User experience</option>
			                                                                              </select></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="select-agencias">
                                            <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" title="Social Services"><span class="filter-option pull-left">Social Services <small></small></span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0" class="disabled"><a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="false"><span class="text">SOCIAL SERVICES</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Social media managing arrow_drop_down </span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Social media marketing arrow_drop_down </span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" name="proyectos_social_services[]" data-style="select-with-transition" multiple="" title="Social Services <small><i class=" material-icons"="" tabindex="-98">arrow_drop_down" data-size="7"&gt;
                                            	<option disabled="">SOCIAL SERVICES</option>
                                              			                                          <option value="1">Social media managing arrow_drop_down </option>
			                                  			                                          <option value="2">Social media marketing arrow_drop_down </option>
			                                                                              </select></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="select-agencias">
                                            <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" title="Special marketing"><span class="filter-option pull-left">Special marketing <small></small></span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0" class="disabled"><a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="false"><span class="text">SPECIAL SERVICES</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">AD (or display) Retargeting</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Innovation of speciality marketing</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="3"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Mobile marketing</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" name="proyectos_especial_mkt[]" data-style="select-with-transition" multiple="" title="Special marketing <small><i class=" material-icons"="" tabindex="-98">arrow_drop_down" data-size="7"&gt;
                                            	<option disabled="">SPECIAL SERVICES</option>
                                              			                                          <option value="1">AD (or display) Retargeting</option>
			                                  			                                          <option value="2">Innovation of speciality marketing</option>
			                                  			                                          <option value="3">Mobile marketing</option>
			                                                                              </select></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="select-agencias">
                                            <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" title="Strategic Services"><span class="filter-option pull-left">Strategic Services <small></small></span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0" class="disabled"><a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="false"><span class="text">STRATEGIC SERVICES</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Analytics Insight and Tagging</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Digital Strategy</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="3"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">User Research</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" name="proyectos_strategic_services[]" data-style="select-with-transition" multiple="" title="Strategic Services <small><i class=" material-icons"="" tabindex="-98">arrow_drop_down" data-size="7"&gt;
                                              <option disabled="">STRATEGIC SERVICES</option>
                                              			                                          <option value="1">Analytics Insight and Tagging</option>
			                                  			                                          <option value="2">Digital Strategy</option>
			                                  			                                          <option value="3">User Research</option>
			                                                                              </select></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="select-agencias">
                                            <div class="btn-group bootstrap-select show-tick"><button type="button" class="btn dropdown-toggle bs-placeholder select-with-transition" data-toggle="dropdown" role="button" title="Technical services"><span class="filter-option pull-left">Technical services <small></small></span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false"><li data-original-index="0" class="disabled"><a tabindex="-1" class="" data-tokens="null" role="option" href="#" aria-disabled="true" aria-selected="false"><span class="text">TECHNICAL SERVICES</span><span class="material-icons  check-mark"> done </span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Website Development</span><span class="material-icons  check-mark"> done </span></a></li></ul></div><select class="selectpicker" name="proyectos_tecnical_services[]" data-style="select-with-transition" multiple="" title="Technical services <small><i class=" material-icons"="" tabindex="-98">arrow_drop_down" data-size="7"&gt;
                                              <option disabled="">TECHNICAL SERVICES</option>
                                              			                                          <option value="1">Website Development</option>
			                                                                              </select></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                  <div class="iconos-favoritos-agencias">
                                      <button type="button" class="nonavigate cleanFilters btn btn-primary btn-round" id="btn-borrar-filtros"><i class="material-icons">cached</i> Borrar filtros</button>
                                  </div>
                                </div>

                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 5</button></li>
                                </ul>
                            </div>

                            <div class="tab-pane" id="paso5">
                                <div class="col-sm-12 subtitulo-modal">
                                    <b>Documentos</b><br><small>Carga los documentos relevantes para el Brief. Puedes cargar videos desde un link externo o en formato .mp4</small>
                                </div>

                                <div class="col-sm-12 form-group input-group is-empty is-fileinput">
                                	<label class="control-label">Agregar un Documento (word, pdf, jpg, pptx, mp3 y mp4)
                                        <input name="proyectos_documentos[]" class="projectFileInput form-control" placeholder="..." style="display:none" type="file">
                                        <span class="namefile"></span>
                                    </label>
                                    <span class="input-group-btn">
                                      <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-add">
                                          <small><i class="material-icons">add_circle_outline</i></small>
                                      </button>
                                    </span>
                                <span class="material-input"></span></div>

                                <div class="col-sm-12 form-group input-group is-empty">
                                	<label class="control-label">Agrega vídeos desde una url (youtube, vimeo)</label>
                                    <input name="proyectos_documentos_url[]" class="form-control" placeholder=" &nbsp; &nbsp; &nbsp; https://www.youtube.com/" type="text">
                                    <span class="input-group-btn">
                                      <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-add">
                                          <small><i class="material-icons">add_circle_outline</i></small>
                                      </button>
                                    </span>
                                <span class="material-input"></span></div>

                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li>
                                    	<button type="submit" class="nonavigate btn btn-primary" id="btn-add-project">
                                    		Terminar
	                                    </button>
	                                </li>
                                </ul>
                            </div>

                            <div class="tab-pane" id="paso6">

                                <div class="col-sm-12 mensaje-felicidades">
                                    <b>¡Felicidades!<br>Haz creado el siguiente proyecto</b>
                                </div>
                                <div id="successResult">
                                    Finaliza el proyecto primero
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>






                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 4</button></li>
                                </ul>
                            </div>

                            <div class="tab-pane" id="registroagencia4">
                                <div class="col-sm-12 subtitulo-modal">
                                    <b>Registro de empresa completo</b>
                                </div>

                                <div class="col-sm-12 text-center">
                                    ¡¡Felicidades, has completado el registro de tu empresa!!<br>
                                    Ahora podrás participar en proyectos y enviar tus propuestas.
                                </div>

                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Aceptar</button></li>
                                </ul>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>










<!-- MODAL GUARDAR CAMBIOS -->
<div class="modal fade" id="guardar-cambios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
  </div>
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 5%; background: rgb(255, 255, 255) none repeat scroll 0% 0%;" tabindex="-1">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          </div>
            <div class="titulo-modal">
              <b>Guardar Cambios</b><br>
              ¿Seguro que deseas guardar los cambios?
            </div>
            <button type="button" class="swal2-confirm btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success" data-dismiss="modal">Aceptar</button>
            <span class="swal2-close" style="display: block;" data-dismiss="modal">×</span>
        </div>
    </div>
</div>
<!-- TERMINA MODAL GUARDAR CAMBIOS -->

<!-- MODAL AGREGAR CLIENTE -->
<div class="modal fade" id="modal-agregar-cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
  </div>
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 5%; background: rgb(255, 255, 255) none repeat scroll 0% 0%;" tabindex="-1">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          </div>
            <div class="titulo-modal">
              <b>Agregar Ciente</b>
            </div>

            <div class="form-group" style="padding-bottom: 2%;">
                <input type="text" class="form-control" placeholder="Nombre de la Agencia...">
                <br>
                <div>
                    <span class="btn btn-rose btn-round btn-file">
                        <span class="fileinput-new">Select image</span>
                        <input type="file" name="..." />
                    </span>
                    <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <hr><br>
            <button type="button" class="swal2-confirm btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success" data-dismiss="modal">Aceptar</button>
            <span class="swal2-close" style="display: block;" data-dismiss="modal">×</span>
        </div>
    </div>
</div>
<!-- TERMINA MODAL AGREGAR CLIENTE -->

<!-- MODAL AGREGAR USUARIO -->
<div class="modal fade" id="modal-agregar-usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
  </div>
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 700px; padding: 5%; background: rgb(255, 255, 255) none repeat scroll 0% 0%;" tabindex="-1">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          </div>
            <div class="titulo-modal">
              <b>Agregar Usuario</b>
            </div>

            <div class="form-group is-empty" style="padding-bottom: 30%;">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <label class="control-label">Nombre del Usuario</label>
                        <input type="text" class="form-control" placeholder="">
                        <span class="material-input"></span>
                        <br>
                        <label class="control-label">Apellido del Usuario</label>
                        <input type="text" class="form-control" placeholder="">
                        <span class="material-input"></span>
                        <br>
                        <label class="control-label">Correo Electrónico</label>
                        <input type="email" class="form-control" placeholder="">
                        <span class="material-input"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">Puesto</label>
                        <select class="form-control">
                            <option>Puesto 1</option>
                            <option>Puesto 2</option>
                            <option>Puesto 3</option>
                        </select>
                        <br>
                        <label class="control-label">Contraseña</label>
                        <input type="password" class="form-control">
                        <span class="material-input"></span>
                    </div>
                </div>
            </div>
            <hr><br>
            <button type="button" class="swal2-confirm btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success" data-dismiss="modal">Aceptar</button>
            <span class="swal2-close" style="display: block;" data-dismiss="modal">×</span>
        </div>
    </div>
</div>
<!-- TERMINA MODAL AGREGAR USUARIO -->

<!-- MODAL EDITAR USUARIO -->
<div class="modal fade" id="modal-editar-usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
  </div>
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 700px; padding: 5%; background: rgb(255, 255, 255) none repeat scroll 0% 0%;" tabindex="-1">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          </div>
            <div class="titulo-modal">
              <b>Editar Usuario</b>
            </div>

            <div class="form-group is-empty" style="padding-bottom: 30%;">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <label class="control-label">Nombre del Usuario</label>
                        <input type="text" class="form-control" value="Nombre">
                        <span class="material-input"></span>
                        <br>
                        <label class="control-label">Apellido del Usuario</label>
                        <input type="text" class="form-control" value="Apellido">
                        <span class="material-input"></span>
                        <br>
                        <label class="control-label">Correo Electrónico</label>
                        <input type="email" class="form-control" value="correo@correo.com">
                        <span class="material-input"></span>
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">Puesto</label>
                        <select class="form-control">
                            <option>Puesto 1</option>
                            <option>Puesto 2</option>
                            <option>Puesto 3</option>
                        </select>
                        <br>
                        <label class="control-label">Contraseña</label>
                        <input type="password" class="form-control" value="12345678">
                        <span class="material-input"></span>
                    </div>
                </div>
            </div>
            <hr><br>
            <button type="button" class="swal2-confirm btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success" data-dismiss="modal">Aceptar</button>
            <span class="swal2-close" style="display: block;" data-dismiss="modal">×</span>
        </div>
    </div>
</div>
<!-- TERMINA MODAL EDITAR USUARIO -->

<!-- MODAL ELIMINAR USUARIO -->
<div class="modal fade" id="modal-eliminar-usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
  </div>
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 5%; background: rgb(255, 255, 255) none repeat scroll 0% 0%;" tabindex="-1">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          </div>
            <div class="titulo-modal">
              <b>Eliminar Usuario</b><br>
              ¿Seguro que deseas eliminar este usuario?
            </div>
            <button type="button" class="swal2-confirm btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success" data-dismiss="modal">Aceptar</button>
            <span class="swal2-close" style="display: block;" data-dismiss="modal">×</span>
        </div>
    </div>
</div>
<!-- TERMINA MODAL ELIMINAR USUARIO -->

<!-- MODAL VER FACTURA -->
<div class="modal fade" id="modal-factura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
  </div>
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 5%; background: rgb(255, 255, 255) none repeat scroll 0% 0%;" tabindex="-1">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          </div>
            <div class="titulo-modal">
                <b>Detalle de Factura</b>
                <br><br>
                <p><b>Fecha:</b> 17/08/17</p>
                <p><b>Número de Factura:</b> aabbcc34567890</p>
                <p><b>Estatus:</b> Pagado</p>
                <br>
                <a href="#">Descargar Factura</a>
                <br>
            </div>

            <button type="button" class="swal2-confirm btn btn-success" data-dismiss="modal">Aceptar</button>
            <span class="swal2-close" style="display: block;" data-dismiss="modal">×</span>
        </div>
    </div>
</div>


<!-- TERMINA MODAL VER FACTURA -->

<!-- MODAL EDITAR PROPUESTA RESPALDO
<div class="modal fade" id="modal-editar-propuestas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 700px; padding: 5%; background: rgb(255, 255, 255) none repeat scroll 0% 0%;" tabindex="-1">
            <div class="titulo-modal">
              <b>Propuesta "Nombre del Proyecto"</b>
            </div>

            <div class="form-group is-empty" style="padding-bottom: 30%;">
                <div class="col-sm-12" style="text-align:left;">
                    <p><b>Documentos</b><br>Carga los documentos importantes para el brief</p>
                    <div class="col-sm-6" style="padding:2% 0%;">
                        <div>
                            <span class="btn btn-rose btn-round btn-file">
                                <span class="fileinput-new">Cargar imagen</span>
                                <input type="file" name="..." />
                            </span>
                            <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                        </div>
                    </div><br>
                    <div class="col-sm-12" style="padding: 2% 0% !important;">
                        <p><b>Documentos agregados</b></p>
                        <div class="doc_agregado">
                            <i class="material-icons">picture_as_pdf</i>
                            <p>Nombre_del_documento.pdf</p>
                            <p>Fecha</p>
                            <i class="material-icons btn-danger">close</i>
                        </div>
                        <div class="doc_agregado" style="clear:left;">
                            <i class="material-icons">image</i>
                            <p>Nombre_del_documento.pdf</p>
                            <p>Fecha</p>
                            <i class="material-icons btn-danger">close</i>
                        </div>
                        <div class="doc_agregado" style="clear:left;">
                            <i class="material-icons">insert_drive_file</i>
                            <p>Nombre_del_documento.pdf</p>
                            <p>Fecha</p>
                            <i class="material-icons btn-danger">close</i>
                        </div>
                    </div>
                    <div class="col-sm-12" style="padding: 2% 0% 5% 0%;">
                        <p><b>Agregar video</b></p>
                        <div class="col-sm-9">
                            <label class="control-label">Ingresar URL</label>
                            <input type="text" class="form-control" placeholder="">
                            <span class="material-input"></span>
                        </div>
                        <div class="col-sm-3" style="padding-top:5%;">
                            <button class="btn btn-danger"><i class="material-icons">remove_circle_outline</i></button>
                            <button class="btn btn-success"><i class="material-icons">add_circle_outline</i></button>
                        </div>
                    </div>
                </div>
            </div>
            <hr><br>
            <button type="button" class="swal2-confirm btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="button" class="swal2-confirm btn btn-success" data-dismiss="modal">Aceptar</button>
            <span class="swal2-close" style="display: block;" data-dismiss="modal">×</span>
        </div>
    </div>
</div>
TERMINA MODAL EDITAR PROPUESTA -->

<?php if(!empty($proyecto)): $this->load->view('Cliente/modales/share_folder_agency'); endif; ?>
<!-- Script Proceso de Registro de Proyecto -->

<script>
$(document).ready(function () {
    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
    allWells.hide();
    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });
    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
    $('div.setup-panel div a.btn-primary').trigger('click');
});
</script>

<!--Charts -->
