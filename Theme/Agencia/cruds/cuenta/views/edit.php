<?php
$this->set_css('assets/grocery_crud/themes/bootstrap2/css/flexigrid.css');
$this->set_js_lib('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
$this->set_js_config('assets/grocery_crud/themes/bootstrap2/js/flexigrid-edit.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
$ci = get_instance();
$ci->load->model('querys');
$agencia = $ci->querys->getAgencia($primary_key);
?>
<?php echo form_open( $update_url, 'method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
<!--<div id='report-error' style="display:none" class='alert alert-danger'></div>-->
<!--<div id='report-success' style="display:none" class='alert alert-success'></div>-->
<div class="card-content">
  <div class="menu-filtros">
    <ul class="nav nav-pills nav-pills-warning">
      <li class=""><a href="<?= base_url('agencia/agencia_perfil/edit/'.get_instance()->user->id) ?>">Datos de usuario</a></li>
      <li class="active"><a href="#datos-agencia" data-toggle="tab" aria-expanded="true">Datos generales</a></li>
      <li class=""><a href="#direccion-agencia" data-toggle="tab" aria-expanded="false">Dirección</a></li>
      <li class=""><a href="#clasificacion-agencia" data-toggle="tab" aria-expanded="false">Clasificación</a></li>
    </ul>
  </div>
  <div class="tab-content contenedor-facturas">  
    <div class="tab-pane active" id="datos-agencia">
      <div class="col-sm-12">
        <div class="card-content">
          <h4>Datos Generales</h4>
          <div class="row">
            <div class="col-sm-12 padding0">
              <div class="col-sm-6">
                <div class="col-sm-12">
                  <div class="form-group is-empty">
                    <label class="control-label"><?= $input_fields['nombre']->display_as; ?></label>
                    <?= $input_fields['nombre']->input; ?>
                    <span class="material-input"></span>
                    <span class="material-input"></span></div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group is-empty">                      
                      <label class="control-label"><?= $input_fields['razon_social']->display_as; ?></label>
                      <?= $input_fields['razon_social']->input; ?>
                      <span class="material-input"></span>
                      <span class="material-input"></span>
                    </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group is-empty">
                        <label class="control-label"><?= $input_fields['web']->display_as; ?></label>
                        <?= $input_fields['web']->input; ?>
                        <span class="material-input"></span>
                        <span class="material-input"></span></div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group is-empty">
                          <label class="control-label"><?= $input_fields['telefono']->display_as; ?></label>
                          <?= $input_fields['telefono']->input; ?>
                          <span class="material-input"></span>
                          <span class="material-input"></span></div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group is-empty">
                            <label class="control-label"><?= $input_fields['email']->display_as; ?></label>
                            <?= $input_fields['email']->input; ?>
                            <span class="material-input"></span>
                            <span class="material-input"></span></div>
                          </div>
                          <div class="col-sm-12">
                          <div class="form-group is-empty">
                            <label class="control-label"><?= $input_fields['horas_hombre']->display_as; ?></label>
                            <?= $input_fields['horas_hombre']->input; ?>
                            <span class="material-input"></span>
                            <span class="material-input"></span></div>
                          </div>
                          <div class="col-sm-12">
                            <div class="form-group is-empty">
                              <label class="control-label"><?= $input_fields['logo']->display_as; ?></label>
                              <?= $input_fields['logo']->input; ?>
                              <span class="material-input"></span>
                              <span class="material-input"></span></div>
                              
                              </div>
                          
                          
                          </div>
                          <div class="col-sm-6">
                            <div class="col-sm-12">
                              <div class="form-group is-empty">
                                <label class="control-label"><?= $input_fields['fecha_constitucion']->display_as; ?></label>
                                <?= $input_fields['fecha_constitucion']->input; ?>
                                <span class="material-input"></span>
                                <span class="material-input"></span></div>
                              </div>
                              <div class="col-sm-12">
                                <div class="form-group is-empty">
                                  <label class="control-label"><?= $input_fields['rfc']->display_as; ?></label>
                                  <?= $input_fields['rfc']->input; ?>
                                  <span class="material-input"></span>
                                  <span class="material-input"></span></div>
                                </div>
                                <div class="col-sm-12">
                                  <div class="form-group is-empty">
                                    <label class="control-label"><?= $input_fields['contacto']->display_as; ?></label>
                                    <?= $input_fields['contacto']->input; ?>
                                    <span class="material-input"></span>
                                    <span class="material-input"></span></div>
                                  </div>
                                  <div class="col-sm-12">
                                    <div class="form-group is-empty">
                                      <label class="control-label"><?= $input_fields['ceo']->display_as; ?></label>
                                      <?= $input_fields['ceo']->input; ?>
                                      <span class="material-input"></span>
                                      <span class="material-input"></span></div>
                                    </div>
                                    <div class="col-sm-12">
                                      <div class="form-group is-empty">
                                        <label class="control-label"><?= $input_fields['empleados']->display_as; ?></label>
                                        <?= $input_fields['empleados']->input; ?>
                                        <span class="material-input"></span>
                                        <span class="material-input"></span></div>
                                      </div>
                                      <div class="col-sm-12">
                                        <div class="form-group is-empty">
                                          <label class="control-label"<?= $input_fields['logo2']->display_as; ?></label>
                                          <?= $input_fields['logo2']->input; ?>
                                          <span class="material-input"></span>
                                          <span class="material-input"></span></div>
                                        </div>
                                        <div class="col-sm-12">
                                          <div class="form-group is-empty">
                                            <label class="control-label"<?= $input_fields['presentacion']->display_as; ?></label>
                                            <?= $input_fields['presentacion']->input; ?>
                                            <span class="material-input"></span>
                                            <span class="material-input"></span></div>
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="tab-pane" id="direccion-agencia">
                              <div class="col-xs-12 col-sm-12">
                                <h4>Dirección</h4>
                                <div class="row">
                                  <div class="col-sm-12 padding0">
                                    <div class="col-sm-12">
                                      <div class="col-sm-4">
                                        <div class="form-group is-empty">
                                          <label class="control-label"><?= $input_fields['direccion']->display_as; ?></label>
                                          <?= $input_fields['direccion']->input; ?>
                                          <span class="material-input"></span>
                                          <span class="material-input"></span>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group is-empty">
                                          <label class="control-label"><?= $input_fields['mapa']->display_as; ?></label>
                                          <?= $input_fields['mapa']->input; ?>
                                          <span class="material-input"></span>
                                          <span class="material-input"></span>
                                        </div>
                                      </div>
                                      
                                    </div>
                                    <div class="col-sm-12">
                                      <div class="col-sm-4">
                                        <div class="form-group is-empty">
                                          <label class="control-label"><?= $input_fields['paises_id']->display_as; ?></label>
                                          <?= $input_fields['paises']->input ?>
                                          <span class="material-input"></span>
                                          <span class="material-input"></span>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group is-empty">
                                          <label class="control-label"><?= $input_fields['cp']->display_as; ?></label>
                                          <?= $input_fields['cp']->input; ?>
                                          <span class="material-input"></span>
                                          <span class="material-input"></span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="tab-pane" id="clasificacion-agencia">
                              <div class="col-sm-12">
                                <div class="card-content">
                                  <h4>Clasificación</h4>
                                  <div class="row">
                                    <div class="col-sm-12">
                                      <div class="col-sm-3">
                                        <div class="form-group is-empty">
                                          <label class="control-label">Certificado</label>
                                          <input class="form-control" value="<?= $agencia->certificado ?>" type="text" readonly>
                                          <span class="material-input"></span>
                                          <span class="material-input"></span></div>
                                        </div>
                                        <div class="col-sm-3">
                                          <div class="form-group is-empty">
                                            <label class="control-label">Ranking</label>
                                            <input class="form-control" value="<?= $agencia->ranking ?>" type="text" readonly>
                                            <span class="material-input"></span>
                                            <span class="material-input"></span></div>
                                          </div>
                                          <div class="col-sm-3">
                                            <div class="form-group is-empty">
                                              <label class="control-label">Estatus</label>
                                              <input class="form-control" value="<?= $agencia->estatus_agencias->nombre ?>" type="text" readonly>
                                              <span class="material-input"></span>
                                              <span class="material-input"></span></div>
                                            </div>
                                            <div class="col-sm-3">
                                              <div class="form-group is-empty">
                                                <label class="control-label">Categoria de la agencia</label>
                                                <input class="form-control" value="<?= $agencia->_categorias ?>" type="text" readonly>
                                                <span class="material-input"></span>
                                                <span class="material-input"></span></div>
                                              </div>
                                            </div>
                                            <div class="col-sm-12">
                                              <b>Servicios:</b><br>
                                              <?php foreach(explode(',',$agencia->filtros) as $a): ?>
                                              <span class="label categorias-agencia"><?= $a ?></span>
                                              <?php endforeach ?>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- Start of hidden inputs -->
                                <?php
                                foreach($hidden_fields as $hidden_field){
                                echo $hidden_field->input;
                                }
                                ?>
                                <div class="row">
                                  <div class="col-sm-12 padding0" style="padding: 20px 47px;">
                                    <button id="form-button-save" type='submit' class="btn btn-success"><?php echo $this->l('form_save'); ?></button>
                                  </div>
                                </div>
                                <?php echo form_close(); ?>
                                <script>
                                var validation_url = '<?php echo $validation_url?>';
                                var list_url = '<?php echo $list_url?>';
                                var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
                                var message_update_error = "<?php echo $this->l('update_error')?>";
                                $(document).on('ready',function(){
                                $(".chzn-container").remove();
                                $("#field-paises_id option:first-child").remove();
                                $("#field-paises_id option:first-child").remove();
                                $(".chosen-select").show();
                                $(".chosen-select").selectpicker();
                                });
                                </script>