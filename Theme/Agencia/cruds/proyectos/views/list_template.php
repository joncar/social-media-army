<?php
/** Jquery UI */
$this->load_js_jqueryui();
$this->set_css($this->default_theme_path . 'proyectos/css/flexigrid.css');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');
$this->set_js($this->default_theme_path . 'proyectos/js/cookies.js');
$this->set_js($this->default_theme_path . 'proyectos/js/jquery.form.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js');
$this->set_js($this->default_theme_path . 'proyectos/js/jquery.printElement.min.js');
$this->set_js($this->default_theme_path . 'proyectos/js/pagination.js');
/** Fancybox */
$this->set_css('assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js');
$this->set_js($this->default_theme_path . 'proyectos/js/flexigrid.js');
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>


<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if ($success_message !== null) { ?>style="display:block"<?php } else { ?> style="display:none" <?php } ?>><?php if ($success_message !== null) { ?>
        <p><?php echo $success_message; ?></p>
    <?php }
    ?></div>

<?php echo form_open($ajax_list_url, 'method="post" id="buscador" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>

<div class="container-fluid flexigrid">
    <div class="row">
        <div class="col-sm-12 padding0 titulo-secccion">
            <div class="titulo-top">
                <?php $paging_starts_from = "<span id='page-starts-from' class='page-starts-from'>1</span>"; ?>
                <?php $paging_ends_to = "<span id='page-ends-to' class='page-ends-to'>" . ($total_results < $default_per_page ? $total_results : $default_per_page) . "</span>"; ?>
                <?php $paging_total_results = "<span id='total_items' class='total_items'>$total_results</span>" ?>
                <?php
                    echo str_replace(array('{start}', '{end}', '{results}'), array($paging_starts_from, $paging_ends_to, $paging_total_results),'Mostrando {end} proyectos'
                );
                ?></div>
        </div>
    </div>
    <div class="ajax_list">
        <?= $list_view ?>
    </div>
    <div id="datatables_paginate pageContent" class="dataTables_paginate paging_full_numbers">
        <ul class="pagination">                    
        </ul>
    </div>
    <button class="ajax_refresh_and_loading" style="display: none;"></button>
</div>
<input type="hidden" name='per_page' value="12">
<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
<?php echo form_close() ?>