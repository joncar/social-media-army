<?php
foreach ($list as
        $num_row =>
        $row) {
      $a = get_instance()->querys->getAgencia($row->id);
    ?>
    <!-- Disenyo de agencia -->


<div class="row contenedor-listado-agencias" id="agencia_element">

       <div class="checkbox col-sm-12">

            <label>
              <input type="checkbox" class="agenciaCheck" name="agenciaCheck[]" value="<?= $a->id ?>">
            </label>

            <a title="Ver Agencia" href="<?= base_url('cliente/agencia/verAgencia/') ?>/<?= $a->id ?>">
              <div class="col-sm-12"> <!-- sm-12 -->

                  <div class="col-xs-12 col-sm-3 col-md-3 padding0 ">
                        <div class="imagen-agencias">                          
                          <?php if(file_exists('img/agencias/'.$a->logo)): ?>
                            <img src="<?= base_url() ?>img/agencias/<?= $a->logo?>" alt="Proyectos en BIMBO" class="img-responsive" style="max-width:100%">
                          <?php else: ?>
                            <img src="<?= base_url() ?>img/agencias/8e560-logo-sistema-generico.jpeg" alt="Proyectos en BIMBO" class="img-responsive" style="max-width:100%">
                          <?php endif ?>
                        </div>
                        <div class="nombre-agencias col-sm-12">
                            <div class="icono-favoritos-check">
                                <span style="color:#9e9e9e"><?= $a->nombre?></span>
                                <label class="fancy-checkbox" title="Marcar como favorito">
                                <input type="checkbox" <?= get_instance()->db->get_where('agencias_favoritas',array('agencias_id'=>$a->id,'user_id'=>get_instance()->user->id))->num_rows()>0?'checked':'' ?> value="<?= $a->id ?>" />
                                <i class="fa fa-heart-o unchecked favoritos"></i>
                                <i class="fa fa-heart checked favoritos"></i>
                            </div>
                        </div>
                        
                        <?= !empty($a->sf5fe5a40)?'<br/><b>Grupo </b>'.$a->sf5fe5a40:'' ?>    
                        <br/>
                        <b><?= $a->certificado?></b><br>
                        Proyectos totales: <?php 
                          get_instance()->db->select('view_proyectos.*');
                          get_instance()->db->join('view_proyectos','view_proyectos.id = briefs.proyectos_id');
                          get_instance()->db->group_by('view_proyectos.id');
                          echo get_instance()->db->get_where('briefs',array('agencias_id'=>$a->id))->num_rows() 
                        ?>                        
                  </div>


                  <div class="col-xs-12 col-sm-7 col-md-7 iconos-agencias-top">
                        <div class="texto-datos-agencia">
                              <div class="col-xs-6 col-sm-4">
                                  <div class="card card-stats">
                                    <div class="card-header" data-background-color="blue"><i class="material-icons">place</i></div>
                                    <div class="card-content"><p class="category"><?= l('Pais') ?></p></div>
                                    <div class="card-footer"><div class="stats"><?= !empty($a->ciudad)?$a->ciudad.', ':'' ?><?= $a->paises ?></div></div>
                                  </div>
                              </div>

                              <div class="col-xs-6 col-sm-4">
                                  <div class="card card-stats">
                                    <div class="card-header" data-background-color="orange"><i class="material-icons">person</i></div>
                                    <div class="card-content"><p class="category"><?= l('Categoria') ?></p></div>
                                    <div class="card-footer"><div class="stats"><?= $a->_categorias?></div></div>
                                  </div>
                              </div>

                              <div class="col-xs-6 col-sm-4">
                                  <div class="card card-stats">
                                      <div class="card-header" data-background-color="red"><i class="material-icons">stars</i></div>
                                      <div class="card-content"><p class="category"><?= l('super-poder') ?></p></div>
                                      <div class="card-footer iconos-ranking">
                                          <div class="stats" style="color:green">
                                            <?= $a->super_poder ?>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                               
                        </div>
                   </div>

                  <div class="col-xs-12 col-sm-2 col-md-2 iconos-agencias-izquierda hidden-xs">
                        <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                            <a title="Enviar Brief" href="javascript:selAgenciaBrief(<?= $a->id ?>)"><button type="button" class="btn btn-primary btn-round" id="btn-enviar-brief"><i class="material-icons">send</i> Enviar Brief</button></a>
                        </div>

                        <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                            <a title="Ver Agencia" href="<?= base_url('cliente/agencia/verAgencia/') ?>/<?= $a->id ?>">
                              <button type="button" class="btn btn-primary btn-round" id="btn-ver-agencia"><i class="material-icons">remove_red_eye</i> Ver agencia</button>
                            </a>
                        </div>
                  </div>


            </div><!-- sm-12 -->
            </a>

       </div>

</div>


<!-- Termina una agencia -->
<?php } ?>
<?php if(count($list)==0): ?>
    No se encontraron agencias que coincidan con los criterios de búsqueda.
<?php endif; ?>
