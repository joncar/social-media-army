<?php
/** Jquery UI */
$this->load_js_jqueryui();
$this->set_css($this->default_theme_path . 'agencias/css/flexigrid.css');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');

if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_js($this->default_theme_path . 'agencias/js/cookies.js');
$this->set_js($this->default_theme_path . 'agencias/js/jquery.form.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js');
$this->set_js($this->default_theme_path . 'agencias/js/jquery.printElement.min.js');
$this->set_js($this->default_theme_path . 'agencias/js/pagination.js');
/** Fancybox */
$this->set_css('assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js');
$this->set_js($this->default_theme_path . 'agencias/js/flexigrid.js');
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
    var URL = '<?= base_url() ?>';
</script>


<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if ($success_message !== null) { ?>style="display:block"<?php } else { ?> style="display:none" <?php } ?>><?php if ($success_message !== null) { ?>
        <p><?php echo $success_message; ?></p>
    <?php }
    ?></div>

<?php echo form_open($ajax_list_url, 'method="post" id="buscador" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>

<div class="container-fluid flexigrid">   
    <div class="row">
        <div class="col-sm-12">            
            <?php get_instance()->load->view('Cliente/menu-lateral-agencias.php');?>    
        </div>
    </div>
    <div class="ajax_list">
        <?= $list_view ?>
    </div>
    <button class="ajax_refresh_and_loading" style="display: none;"></button>
</div>


<?php echo form_close() ?> 