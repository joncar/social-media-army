<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Mensajes</div></div>
                  </div>

                  <!-- Inicia una agencia -->
                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Chat</div></div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12 padding0">

                          <div class="conversation-wrap col-sm-3">
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 1</h5><small>Hello</small></div>
                              </div>
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 2</h5><small>Hello</small></div>
                              </div>
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 3</h5><small>Hello</small></div>
                              </div>
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 4</h5><small>Hello</small></div>
                              </div>
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 5</h5><small>Hello</small></div>
                              </div>
                              <div class="media conversation">
                                  <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                  <div class="media-body"><h5 class="media-heading">Usuario 6</h5><small>Hello</small></div>
                              </div>
                          </div>

                          <div class="message-wrap col-sm-9">
                              <div class="msg-wrap">
                                  <div class="media msg ">
                                      <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                      <div class="media-body">
                                          <small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small>
                                          <h5 class="media-heading">Mi usuario</h5><small>Datos de contacto</small>
                                      </div>
                                  </div>
                                  <div class="alert alert-info msg-date">
                                      <strong>Today</strong>
                                  </div>
                                  <div class="media msg">
                                      <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                      <div class="media-body">
                                          <small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small><h5 class="media-heading">Usuario</h5><small>Mensaje: "Hola"</small>
                                      </div>
                                  </div>
                                  <div class="media msg">
                                      <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                      <div class="media-body">
                                          <small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small><h5 class="media-heading">Usuario</h5><small>Mensaje: "Hola"</small>
                                      </div>
                                  </div>
                                  <div class="media msg">
                                      <a class="pull-left" href="#"><img src="http://lorempixel.com/64/64" alt="Perfil Dashboard" class="img-responsive center-block image"></a>
                                      <div class="media-body">
                                          <small class="pull-right time"><i class="fa fa-clock-o"></i> 12:10am</small><h5 class="media-heading">Usuario</h5><small>Mensaje: "Hola"</small>
                                      </div>
                                  </div>
                              </div>

                              <div class="send-wrap "><textarea class="form-control send-message" rows="3" placeholder="Write a reply..."></textarea></div>
                              <div class="btn-panel">
                                  <a href="" class=" col-lg-3 btn   send-message-btn " role="button"><i class="fa fa-cloud-upload"></i> Adjuntar archivos</a>
                                  <a href="" class=" col-lg-4 text-right btn   send-message-btn pull-right" role="button"><i class="fa fa-plus"></i> Enviar mensaje</a>
                              </div>
                          </div>
                      </div>

                  </div>
                </div>
                  <!-- Termina Contenido -->

                </div>

                <footer class="footer contenedor-footer">
                    <?php include('footer.php');?>
                </footer>
                
            </div>



        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
    });
})(jQuery);
</script>
</html>
