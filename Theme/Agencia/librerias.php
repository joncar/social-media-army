
<script src="<?= base_url() ?>Theme/Agencia/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>Theme/Agencia/assets/js/material.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>Theme/Agencia/assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/arrive.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/moment.min.js"></script>
<!--  Charts Plugin, full documentation here: https://gionkunz.github.io/chartist-js/ -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/chartist.min.js"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/jquery.sharrre.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin, full documentation here: https://refreshless.com/nouislider/ -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1_8C5Xz9RpEeJSaJ3E_DeBv8i7j_p6Aw"></script>
<!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin, full documentation here: https://limonte.github.io/sweetalert2/ -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/sweetalert2.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/fullcalendar.min.js"></script>
<!-- Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/material-dashboard.js?v=1.2.1"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= base_url() ?>Theme/Agencia/assets/js/demo.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        // Javascript method's body can be found in <?= base_url() ?>Theme/Agencia/assets/js/demos.js
        demo.initDashboardPageCharts();

        demo.initVectorMap();
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        md.initSliders()
        demo.initFormExtendedDatetimepickers();
    });
</script>

 <?php 
if(!empty($css_files) && !empty($js_files)):
foreach($js_files as $file): ?>
<script src="<?= $file ?>"></script>
<?php endforeach; ?>                
<?php endif; ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>
<!--Charts -->
<script>
function drawChart(data,container) {   

      if(typeof(data)!=='undefined' && data.length>0){
          for(var i in data){
              data[i][3] = new Date(data[i][3]);
              data[i][4] = new Date(data[i][4]);
          }        
          console.log(data);
          var otherData = new google.visualization.DataTable();
          otherData.addColumn('string','Etapa');
          otherData.addColumn('string','invitación');
          otherData.addColumn('string','Preguntas y respuestas');
          otherData.addColumn('date','Evaluacion de propuestas');
          otherData.addColumn('date','Desarrollo de propuestas');
          otherData.addColumn('number','Selección del ganador');
          otherData.addColumn('number','start');
          otherData.addColumn('string','end');
          otherData.addRows(data);
          var options = {
            height: 275
          };
          var chart = new google.visualization.Gantt(document.getElementById(container));        
          chart.draw(otherData, options);

      }
    }
function remoteConnection(url,data,callback){  
    $.ajax({
        url: '<?= base_url() ?>'+url,
        data: data,
        context: document.body,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success:callback
    });
};

</script>
<script>      
      $("body").addClass('sidebar-mini');

      var buscSearch = undefined;
      $(document).on('keyup','#buscador-top',function(){
        if(buscSearch!=undefined){
            buscSearch.abort();
        }
        if($(this).val().length>3){
            buscSearch = $.get('<?= base_url() ?>agencia/buscar',{q:$(this).val()},function(data){
                data = JSON.parse(data);                
                var l = '';
                for(var i in data){
                    l+= '<li><a href="'+data[i].link+'">'+data[i].nombre+'</a></li>';
                }
                if(data.length==0){
                    l+= '<li><a href="javascript:void(0)">Sin resultados</a></li>';
                }
                $(".buscadorTopResult ul").html(l);
                $(".buscadorTopResult").show();
                buscSearch = undefined;                
            });
        }else{
            $(".buscadorTopResult").hide();
        }
      });

      $(document).on('focus','#buscador-top',function(){
        if($(this).val().length>3){
            setTimeout(function(){$(".buscadorTopResult").show();},600);
        }else{
            $(".buscadorTopResult").hide();
        }
      });

      $(document).on('click',function(){
        $(".buscadorTopResult").hide();
      });
</script>


<script>
    window.onload = function(){
        doAfterLoad();
    }

    function doAfterLoad(){
        for(var i in window.afterLoad){
            window.afterLoad[i]();
        }
    }
</script>