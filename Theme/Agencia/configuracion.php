<!doctype html>
<html lang="en">
    <head>
        <!-- Librerias -->
        <?php include('head.php');?>
    </head>

    <body>
        <div class="wrapper switch-trigger">
            <!-- Menu Lateral -->
            <div class="sidebar" id="fondo-menu-lateral">
                <?php include('menu-lateral.php');?>
            </div>

            <div class="main-panel">
                <!-- Menu Top -->
                <?php include('menu-top.php');?>

                <!-- Contenido -->
                <div class="content">
                    <div class="container-fluid">
                        <!-- Inicia Contenido -->
                        <div class="row">
                            <div class="col-sm-12 padding0 titulo-secccion">
                                <div class="titulo-top">Configuración</div>
                            </div>

                            <div class="card-content">
                                <div class="menu-filtros">
                                    <ul class="nav nav-pills nav-pills-warning">
                                        <li class="active"><a href="#pill1" data-toggle="tab" aria-expanded="true">Alta de Usuarios</a></li>
                                        <!--<li class=""><a href="#pill2" data-toggle="tab" aria-expanded="false">Personalización</a></li>
                                        <li class=""><a href="#pill3" data-toggle="tab" aria-expanded="false">Datos de Facturación</a></li>
                                        <li class=""><a href="#pill4" data-toggle="tab" aria-expanded="false">Historial de Facturación</a></li>-->
                                    </ul>
                                </div>
                                <div class="tab-content contenedor-facturas">

                                    <div class="tab-pane active" id="pill1">
                                        <div class="col-sm-12">
                                            <div class="card-content">
                                                <h4>Usuarios</h4>
                                                <?= $user->output ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="pill2">
                                        <div class="col-xs-12 col-sm-12">
                                            <h4>Personalización</h4>
                                            <button type="submit" class="btn btn-fill pull-right" id="btn-proyecto-propuestas" style="width:auto;" data-toggle="modal" data-target="#guardar-cambios">Guardar Cambios</button>
                                        </div>
                                        <div class="col-sm-12">
                                                <div class="col-sm-3 subir-logo">
                                                    <p>Sube el logo de tu empresa en .png con fondo transparente</p>
                                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                                        <a href="<?= base_url('agencia/perfil/edit/'.$this->user->empresa) ?>" class="fileinput-new thumbnail">
                                                            <img src="<?= $this->user->logo ?>" alt="...">
                                                        </a>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="pill3">
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="col-xs-12 col-sm-12 margen-registro-movil margen-tabs">
                                                <h4>Datos Crediticios</h4>
                                                <div class="card-content">
                                                    <button type="submit" class="btn btn-fill pull-right" id="btn-proyecto-propuestas" style="width:auto;" data-toggle="modal" data-target="#guardar-cambios">Guardar Cambios</button>
                                                    <form method="#" action="#">
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-4 bancos">
                                                                <p>Banco:</p>
                                                                <div class="radio">
                                                                    <label>
                                                                        <img src="http://briefit.com.mx/Theme/Agencia/assets/img/Bancos/mastercard.png">
                                                                        <input type="radio" name="optionsRadios" checked="true">
                                                                    </label>
                                                                </div>
                                                                <div class="radio">
                                                                    <label>
                                                                        <img src="http://briefit.com.mx/Theme/Agencia/assets/img/Bancos/VISA.png">
                                                                        <input type="radio" name="optionsRadios">
                                                                    </label>
                                                                </div>
                                                                <div class="radio">
                                                                    <label>
                                                                        <img src="http://briefit.com.mx/Theme/Agencia/assets/img/Bancos/amex.png">
                                                                        <input type="radio" name="optionsRadios">
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">Número de Tarjeta</label>
                                                                    <input class="form-control" type="text">
                                                                    <span class="material-input"></span>
                                                                </div>
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">Nombre de Titular</label>
                                                                    <input class="form-control" type="text">
                                                                    <span class="material-input"></span>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="label-control">Fecha de Vencimiento</label>
                                                                    <input type="text" class="form-control datepicker" value="10/05/2016" />
                                                                </div>
                                                                <div class="form-group label-floating is-empty">
                                                                    <label class="control-label">CCV</label>
                                                                    <input class="form-control" type="text" maxlength="3">
                                                                    <span class="material-input"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="pill4">
                                        <div class="col-sm-12">
                                            <div class="card-content">
                                                <h4>Historial de Facturación</h4>
                                                <div class="material-datatables">
                                                    <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                                                        <div class="row margen-tabs">
                                                            <div class="col-sm-12">
                                                                <div class="dataTables_filter" id="datatables_filter">
                                                                    <div id="wrap" class="col-sm-2">
                                                                        <form action="" autocomplete="on" id="buscador">
                                                                            <input id="search" name="search" type="text" placeholder="Buscar..." class="pull-right"><i class="material-icons">search</i>
                                                                        </form>
                                                                    </div>
                                                                    <div class="col-sm-2 pull-right">
                                                                        <select class="selectpicker" data-style="select-with-transition" multiple title="Filtrar por..." data-size="7">
                                                                            <option disabled> Seleccionar tipo de Estatus:</option>
                                                                            <option value="2">Pagado</option>
                                                                            <option value="3">Pendiente</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-3">
                                                                    <div class="card-content">
                                                                        <div class="form-group">
                                                                            <label class="label-control">Desde</label>
                                                                            <input class="form-control datepicker" value="10/10/2016" style="" type="text">
                                                                            <span class="material-input"></span></div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-6 col-sm-3">
                                                                    <div class="card-content">
                                                                        <div class="form-group">
                                                                            <label class="label-control">Hasta</label>
                                                                            <input class="form-control datepicker" value="10/10/2016" style="" type="text">
                                                                            <span class="material-input"></span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <tr role="row">
                                                                            <th aria-sort="descending" aria-label="Name: activate to sort column ascending" style="width: 163px;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting_desc">Fecha</th>

                                                                            <th aria-label="Position: activate to sort column ascending" style="width: 241px;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Factura</th>

                                                                            <th aria-label="Office: activate to sort column ascending" style="width: 123px;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="sorting">Estatus</th>

                                                                            <th aria-label="Actions: activate to sort column ascending" style="width: 116px;" colspan="1" rowspan="1" aria-controls="datatables" tabindex="0" class="disabled-sorting text-right sorting">Acciones</th>
                                                                        </tr>
                                                                    </thead>

                                                                    <tbody>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pagado</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pendiente</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pagado</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pagado</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pagado</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pagado</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pagado</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pagado</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pagado</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pagado</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pagado</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                        <tr role="row">
                                                                            <td tabindex="0" class="sorting_1">17/03/2018</td>
                                                                            <td class="">aabbcc34567890</td>
                                                                            <td class="">Pagado</td>
                                                                            <td class="text-right">
                                                                                <a href="#modal-factura" class="btn btn-simple btn-info btn-icon edit" data-toggle="modal">
                                                                                    <i class="material-icons">remove_red_eye</i>
                                                                                </a>
                                                                                <a href="#" class="btn btn-simple btn-success btn-icon remove">
                                                                                    <i class="material-icons">file_download</i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <div aria-live="polite" role="status" id="datatables_info" class="dataTables_info">
                                                                    Mostrando 1 a 10 de 40 entradas
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-7">
                                                                <div id="datatables_paginate" class="dataTables_paginate paging_full_numbers">
                                                                    <ul class="pagination">
                                                                        <li id="datatables_first" class="paginate_button first disabled">
                                                                            <a tabindex="0" data-dt-idx="0" aria-controls="datatables" href="#">Inicio</a>
                                                                        </li>
                                                                        <li id="datatables_previous" class="paginate_button previous disabled">
                                                                            <a tabindex="0" data-dt-idx="1" aria-controls="datatables" href="#">Anterior</a>
                                                                        </li>
                                                                        <li class="paginate_button active">
                                                                            <a tabindex="0" data-dt-idx="2" aria-controls="datatables" href="#">1</a>
                                                                        </li>
                                                                        <li class="paginate_button ">
                                                                            <a tabindex="0" data-dt-idx="3" aria-controls="datatables" href="#">2</a>
                                                                        </li>
                                                                        <li class="paginate_button ">
                                                                            <a tabindex="0" data-dt-idx="4" aria-controls="datatables" href="#">3</a>
                                                                        </li>
                                                                        <li class="paginate_button ">
                                                                            <a tabindex="0" data-dt-idx="5" aria-controls="datatables" href="#">4</a>
                                                                        </li>
                                                                        <li id="datatables_next" class="paginate_button next">
                                                                            <a tabindex="0" data-dt-idx="6" aria-controls="datatables" href="#">Siguiente</a>
                                                                        </li>
                                                                        <li id="datatables_last" class="paginate_button last">
                                                                            <a tabindex="0" data-dt-idx="7" aria-controls="datatables" href="#">Último</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- Termina Contenido -->
            </div>



        </div>


        <footer class="footer contenedor-footer">
            <?php include('footer.php');?>
        </footer>
        

    </body>

    <?php include('modales.php');?>
    <!-- Librerias -->
    <?php include('librerias.php');?>
    <!-- Agregar Campos Modal Agregar Proyecto -->
    <script>
        (function ($) {
            $(function () {
                var addFormGroup = function (event) {
                    event.preventDefault();
                    var $formGroup = $(this).closest('.form-group');
                    var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
                    var $formGroupClone = $formGroup.clone();
                    $(this)
                        .toggleClass('btn-default btn-add btn-danger btn-remove')
                        .html('–');
                    $formGroupClone.find('input').val('');
                    $formGroupClone.insertAfter($formGroup);
                    var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                    if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
                    }
                };
                var removeFormGroup = function (event) {
                    event.preventDefault();
                    var $formGroup = $(this).closest('.form-group');
                    var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
                    var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                    if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                        $lastFormGroupLast.find('.btn-add').attr('disabled', false);
                    }
                    $formGroup.remove();
                };
                var countFormGroup = function ($form) {
                    return $form.find('.form-group').length;
                };
                $(document).on('click', '.btn-add', addFormGroup);
                $(document).on('click', '.btn-remove', removeFormGroup);
            });
        })(jQuery);

        function uploadFile(form) {
            var datos = new FormData(form);
            remoteConnection('agencia/empresas/upload_file/logo', datos, function (data) {
                data = JSON.parse(data);
                if (data.success) {
                    $("input[name='logo']").val(data.files[0].name);
                    $("input[name='logo']").parents('form').submit();
                } else {
                    alert(data.message);
                }
            });
            return false;
        }

        function uploadFile2(form) {
            var datos = new FormData(form);
            remoteConnection('agencia/empresas/upload_file/logo', datos, function (data) {
                data = JSON.parse(data);
                if (data.success) {
                    $("input[name='logo']").val(data.files[0].name);
                    $("input[name='logo']").parents('form').submit();
                } else {
                    alert(data.message);
                }
            });
            return false;
        }
    </script>
</html>
