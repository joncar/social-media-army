<div class="conversation-wrap col-sm-3">
  <?php 
  $this->db->order_by('tipo','ASC'); 
  $this->db->group_by('id,tipo'); 
  foreach($this->db->get_where('view_integrantes_chat',array('status_brief >'=>'1','proyectos_id'=>$proyecto->id))->result() as $i): 
  	if($i->tipo==1 || ($i->tipo==2 && $i->id==$this->user->empresa)):
  ?>
	  <div class="media conversation">
	      <div class="pull-left" style="width:64px; height:64px; background:url(<?= $this->querys->getLogo($i->id,$i->tipo) ?>); background-size:100%; background-position: center; background-repeat: no-repeat; margin-right:15px;">	      	
	      </div>
	      <div class="media-body">
	      	<h5 class="media-heading"><?= $this->querys->getAgenciaNombre($i->id,$i->tipo,$i->nombre) ?></h5>
	      	<small>
	      		<?php 
	      			if($i->status >= 0 && $i->status<10){
	      				echo '<span class="label label-success">Conectado</span>';
	      			}else{
	      				echo '<span class="label label-default">Desconectado</span>';
	      			}
	      		?>
	      	</small>
	      </div>
	  </div>
	<?php endif ?>
  <?php endforeach ?>
</div>