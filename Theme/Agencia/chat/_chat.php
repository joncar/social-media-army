<?php foreach($messages as $m): ?>
<div class="media msg ">
  <div class="pull-left" style="margin-right:15px; width:64px; height:64px; background:url(<?= $this->querys->getLogoAgenciaChat($m->id,$m->tipo) ?>); background-size:100%; background-position: center; background-repeat: no-repeat;"></div>
  <div class="media-body">
  	<small class="pull-right time">
  		<i class="fa fa-clock-o"></i> Hace <?= get_antiguedad_string($m->fecha) ?>
  	</small>
		<h5 class="media-heading"><?= $this->querys->getAgenciaNombre($m->id,$m->tipo,$m->nombre) ?></h5>
		<small><?= $m->mensaje ?></small>
  </div>
</div>	      
<?php endforeach ?>