<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">
                  
                  <!-- Inicia Contenido -->
                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Proyecto</div></div>
                  </div>

                  <div class="row">
                      <div class="col-xs-12 col-sm-12 padding0 datos-proyecto">
                        <?= $this->load->view('Agencia/_proyecto') ?>                        
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion">
                          <div class="titulo-top">Historial del Proyecto</div>
                      </div>
                  </div>

                  <div class="row bold-agencia">
                      <div class="col-sm-12 padding0">
                          <div class="col-sm-12">
                              <div class="col-sm-12">
                                  <div class="fondo-agencia-1">
                                      <h1 class="titulo-agencia">Agencias participantes</h1>
                                      <div class="row">
                                        <div class="col-sm-12 propuestasDiv">
                                          <?php foreach($proyecto->invitaciones->result() as $i): ?>
                                                <div class="col-sm-3">
                                                    
                                                    <div class="col-sm-12 fondo-proyecto-dashboard">
                                                        <div class="img-perfil-proyecto text-center">
                                                            <div style="background:url(<?= base_url('img/agencias/'.$i->logo) ?>); width:100%; height:150px; background-size:auto 90%; background-position: center; background-repeat:no-repeat;">
                                                              <img src="<?= base_url('img/agencias/'.$i->logo) ?>" alt="Perfil Dashboard" class="img-responsive center-block image imagen-agencias-particioantes" style="visibility: hidden">
                                                            </div>
                                                            <span>
                                                                
                                                              <?php foreach($this->db->query("SELECT * FROM agencias INNER JOIN paises ON agencias.paises_id = paises.id WHERE agencias.id = $i->id")->result() as $pa): ?>
                                                                <img src="<?= base_url('img/Banderas/'.$pa->icono) ?>" alt="<?= $pa->nombre ?>" class="img-responsive center-block img-circle" id="perfil-proyecto">
                                                              <?php endforeach ?>
                                                                
                                                            </span>
                                                        </div>
                                                        <h1 class="titulo-proyecto"><?= $i->nombre ?></h1>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-sm-12">
                                  <div class="fondo-agencia-1">
                                      <h1 class="titulo-agencia">Detalles del Proyecto</h1>
                                      <b>Responsable:</b> <?= $proyecto->user->nombre ?> <?= $proyecto->user->apellido ?><br>
                                      <b>Supervisor:</b> <?= $proyecto->supervisores ?><br>
                                      <b>Colaboradores:</b> <?= $proyecto->colaboradores ?><br>
                                  </div>
                              </div>

                              <div class="col-sm-12">
                                  <div class="fondo-agencia-1 objetivos-agencia">
                                      <h1 class="titulo-agencia">Brief</h1>
                                      <h4>Solicitud puntual</h4>
                                      <p><?= $proyecto->solicitud_puntual ?></p>
                                      <h4>Objetivo del proyecto</h4>
                                      <p><?= $proyecto->objetivo ?></p>
                                      <h4>Target</h4>
                                      <p><?= $proyecto->target ?></p>
                                      <h4>Especificaciones / Observaciones generales</h4>
                                      <p><?= $proyecto->especificaciones ?></p>
                                  </div>
                              </div>
                          </div>

                          
                      </div>

                      <?php if($proyecto->documentos->num_rows()>0): ?>
                        <div class="col-sm-12">
                            <h1 class="titulo-agencia">Documentos</h1>
                            <div class="col-sm-12 documentos-historial">

                              <?php foreach($proyecto->documentos->result() as $d): ?>
                                <a href="<?= base_url('img/proyectos_uploads/files/'.$d->documento) ?>" target="_new">
                                    <div class="col-sm-3 text-center">
                                      <div class="info-box">
                                          <span class="info-box-icon bg-green">
                                            <i class="material-icons">description</i>
                                          </span>
                                          <div class="info-box-content">
                                            <span class="info-box-text">
                                              <b><?= empty($d->nombre)?$d->documento:$d->nombre ?></b><br>
                                            </span>
                                          </div>
                                        </div>
                                    </div>
                                </a>
                              <?php endforeach ?>

                          </div>
                      </div>

                      <?php endif ?>
                      
                      


                  </div>
                 </div>



        </div>
      </div></div>


        <footer class="footer contenedor-footer">
            <?php include('footer.php');?>
        </footer>


    </div>



</body>

<?php $this->load->view('Agencia/modales/cancelar_invitacion');?>
<?php $this->load->view('Cliente/modales/share_folder_agency'); ?>
<!-- Librerias -->
<?php $this->load->view('Agencia/librerias');?>
<script>
    $(document).on('ready',function(){
      var data = <?php
      $data = array();      
      foreach($proyecto->fechas->result() as $f){          
          if(strtotime($f->desde)>strtotime($f->hasta)){
            $aux = $f->desde;
            $f->hasta = $f->desde;
            $f->desde = $aux;
          }
          $f->hasta = $f->hasta==$f->desde?date("Y-m-d",strtotime('+1 day'.$f->hasta)):$f->hasta;
          $data[] = array(
            $f->nombre,
            $f->nombre,
            $f->nombre,
            date("Y-m-d H:i:s",strtotime($f->desde)),
            date("Y-m-d H:i:s",strtotime($f->hasta)),
            null,
            100,
            null
          );
        }
        echo json_encode($data);
    ?>;
    google.charts.setOnLoadCallback(function(){drawChart(data,'proyecto_grafico')});
  });
</script>


<script>
(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();
            $(this)
                .toggleClass('btn-default btn-add btn-danger btn-remove')
                .html('–');
            $formGroupClone.find('input').val('');
            $formGroupClone.insertAfter($formGroup);
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };
        var removeFormGroup = function (event) {
            event.preventDefault();
            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }
            $formGroup.remove();
        };
        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };
        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
        $(document).on('change','#propuesta',function(){
          $("#btn-ver-propuesta").html('Subiendo por favor espere');
          var content = $(this).parents('label').find('span').html('subiendo por favor espere');
          var form = new FormData();
          form.append('propuesta',document.getElementById('propuesta').files[0]);
          form.append('brief',<?= $brief->id ?>);
          remoteConnection('agencia/proyecto/enviarPropuesta',form,function(data){
            if(data!=='none'){
              $("#btn-ver-propuesta").attr('href',data);
              $("#btn-ver-propuesta").show();
            }else{
              $("#btn-ver-propuesta").hide();
            }
            document.location.reload();
            $("#btn-ver-propuesta").html('Ver propuesta');
            content.html('Subir propuesta');
          });
        });
    });
})(jQuery);

<?php if($brief->status==1): ?>
function accept(){
  var form = new FormData();
  form.append('brief',<?= $brief->id ?>);
  remoteConnection('agencia/proyecto/aceptarInvitacion',form,function(data){
    document.location.reload();
  });
}
<?php endif ?>
</script>



</html>
