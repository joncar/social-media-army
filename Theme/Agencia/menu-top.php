












<!-- <nav class="navbar navbar-transparent navbar-absolute"> -->
<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container-fluid">

        <!--
        <div class="img-logo-top-nav">
            <img src="https://briefsuite.com/Theme/Cliente/assets/img/logo-footer-admin.png" alt="Perfil Dashboard">
        </div>-->

        <!--<div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                <i class="material-icons visible-on-sidebar-mini">view_list</i>
            </button>
        </div>-->

        <div class="navbar-header" style="margin-top: 16px; margin-left:20px;">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div><b><?= $this->user->nombre ?> <?= $this->user->apellido ?> · <?= $this->user->puesto ?> </b></div>
        </div>

        <div class="collapse navbar-collapse">
            <!--
            <ul class="nav navbar-nav navbar-left">
                <li class="img-logo-top">
                    <img src="https://briefsuite.com/Theme/Cliente/assets/img/logo-top.png" alt="Perfil Dashboard">
                </li>
            </ul>-->

            <style>
            .img-top-logo img {
              width:  13%;
              margin-top:  5px;
            }
            </style>

            

            <ul class="nav navbar-nav navbar-right">
                <?php $mensajes = $this->querys->getPush($this->user->id); ?>
                <li class="dropdown" style="width: 42px;">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="margin-top: 10px; padding:5px 0;">
                        <i class="material-icons" style="font-size: 36px; margin-top: -10px;">messages</i>
                        <?php if(count($mensajes)>0): ?>
                          <span class="notification" style="left: 20px; right:inherit"><?= count($mensajes) ?></span>
                        <?php endif ?>
                        <p class="hidden-lg hidden-md">Notifications<b class="caret"></b></p>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <?php foreach($mensajes as $n=>$m): ?>
                              <a href="<?= base_url('agencia/setMessage/'.$n) ?>"><?= $m->mensaje ?></a>
                            <?php endforeach ?>
                            <?php if(count($mensajes)==0): ?>
                              Sin mensajes
                            <?php endif ?>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#soporteAdmin" data-toggle="modal" style="margin-top: 4px; padding:5px 0;">
                        <i class="material-icons" style="font-size:32px">contact_support</i>
                    </a>
                </li>
                <!-- Perfil -->
                <li class="dropdown user user-menu">

                  <a href="#" class="dropdown-toggle imagen-icono-top img-circle photo icono-bandera-lateral" data-toggle="dropdown">
                      <div class="foto-login" style="width:32px; height:32px; background-size: cover !important;  border-radius:50%; background:url(<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>)"></div>
                      <span>
                          <img src="<?= $this->user->bandera ?>" alt="Perfil Dashboard" class="img-responsive center-block img-circle" id="icono-lateral">
                      </span>
                  </a>


                  <ul class="dropdown-menu">
                      <li class="user-header text-center">
                          <img src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" class="user-image" alt="User Image">
                          <p><?= $this->user->nombre ?><br>
                          <?= $this->user->puesto ?><br>
                          <small>En la plataforma desde <?= date("Y",strtotime($this->user->fecha_registro)) ?></small></p>
                      </li>
                      <li class="col-sm-12 user-footer text-center btns-perfil">
                          <div class="col-sm-4">
                              <a href="<?= base_url('agencia/configuracion') ?>" class="btn btn-default" title="Agregar usuarios">
                                <i class="material-icons">group_add</i>
                              </a>
                          </div>
                          <div class="col-sm-4">
                              <a href="<?= base_url('agencia/cuenta') ?>" class="btn btn-default" title="Ajustes">
                                <i class="material-icons">settings</i>
                              </a>
                          </div>
                          <div class="col-sm-4">
                              <a href="<?= base_url('main/unlog') ?>" class="btn btn-default" title="Cerrar sesión">
                                <i class="material-icons">keyboard_tab</i>
                              </a>
                          </div>
                      </li>
                  </ul>
                </li>
                <li class="separator hidden-lg hidden-md"></li>
            </ul>

            <div id="wrap">
              <form autocomplete="off" id="buscador" onsubmit="return false;">
                <input name="q" type="text" placeholder="Buscar" id="buscador-top"><i class="material-icons">search</i>
                <div class="buscadorTopResult">
                  <ul>
                    <li>
                      <a href="#">Result 1</a>
                    </li>
                  </ul>
                </div>
              </form>
            </div>

        </div>

    </div>
</nav>
<?php $this->load->view('Cliente/modales/soporteAdmin'); ?>