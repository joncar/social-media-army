<div class="modal fade" id="rechazarInvitacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          </div>        	
        	<form onsubmit="return cancelBrief(this)">
            <img src="<?= $this->user->logo ?>" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Cancelar Invitación</b><br>
              ¿Seguro que deseas cancelar esta invitación?
            </div>
            <div class="form-group">
                <label class="control-label">Envia una explicación a las agencia participante</label>
                <textarea class="form-control" id="motivo_cancelacion" name="mensaje" placeholder="Se ha cancelado este proyecto, porque..."></textarea>
            </div>

            <input type="hidden" name="brief_id" value="<?= $brief->id ?>">
            <input type="hidden" name="status" value="-1">
            <input type="hidden" name="propuesta" value="">

            <input type="hidden" name="fecha_modificacion" value="<?= date("d/m/Y H:i:s") ?>">
            <div class="row"><div class="response"></div></div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <a href="#" class="swal2-cancel btn btn-danger" data-toggle="modal" data-target="#rechazarInvitacion" style="display: inline-block; width:100%">Cancelar</a>
                    </div>
                    <div class="col-sm-6">
                        <button class="swal2-confirm btn btn-success" type="submit" style="display: inline-block; width:100%">Enviar</button>
                    </div>
                </div>
            </div>

            </form>


        </div>
    </div>
</div>
<!-- Termina Modal Cancelar Proyecto -->
<script>

	function mostrarCancelBrief(id){
		$('#rechazarInvitacion input[name="brief_id"]').val(id);
		$("#rechazarInvitacion").modal('show');
	}
	function cancelBrief(f){
		var form = new FormData(f);
        $('#rechazarInvitacion').find('.response').addClass('alert alert-info').html('Realizando solicitud, espere por favor.');
        remoteConnection('agencia/proyecto/briefs/update/'+$('#rechazarInvitacion input[name="brief_id"]').val(),form,function(data){
            $('#rechazarInvitacion').find('.response').removeClass('alert-info').addClass('alert alert-success').html('Brief Rechazado con éxito');
            setTimeout(function(){document.location.reload()},2000);
        });
        return false;
	}


</script>
