<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">


                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Proyectos</div></div>
                  </div>

                  <div class="row">
                      <div class="col-xs-12 col-sm-12 padding0 indicaciones-progreso hidden-xs">
                            <span class="progreso-top"><i class="fa fa-square texto-verde" aria-hidden="true"></i>  Proyecto Completo</span>
                            <span class="progreso-top"><i class="fa fa-square texto-amarillo" aria-hidden="true"></i>  Proyecto en Desarrollo</span>
                            <span class="progreso-top"><i class="fa fa-square texto-azul" aria-hidden="true"></i>  Proyecto por Iniciar</span>
                        </div>
                        <!-- movil-->
                        <div class="col-xs-12 col-sm-12 padding0 indicaciones-progreso visible-xs">
                            <div class="col-xs-4 progreso-top">
                                <i class="fa fa-square texto-verde" aria-hidden="true"></i><br>
                                Proyecto<br>Completo
                            </div>
                            <div class="col-xs-4 progreso-top">
                                <i class="fa fa-square texto-amarillo" aria-hidden="true"></i><br>
                                Proyecto<br>en Desarrollo
                            </div>
                            <div class="col-xs-4 progreso-top">
                                <i class="fa fa-square texto-azul" aria-hidden="true"></i><br>
                                Proyecto<br>por Iniciar
                            </div>
                        </div>

                      <div class="col-sm-12 padding0">

                          <?php $this->db->group_by('id'); ?>
                          <?php $proyectos = $this->db->get_where('view_agencias_proyectos',array('porcentaje <'=>100,'status <'=>3,'agencias_id'=>$this->user->empresa));  ?>
                          <?php foreach($proyectos->result() as $p): ?>
                          <?php $p->fechas = $this->db->get_where('proyectos_fechas',array('proyectos_id'=>$p->id)); ?>
                          <!-- Inicia Proyecto -->
                          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 clearfix margen-tajeta-proyecto">
                              <a href="<?= base_url('agencia/proyecto/verProyecto/'.$p->id) ?>">
                                  <div class="fondo-proyecto-dashboard">
                                    <div class="img-perfil-proyecto text-center">
                                          <div style="background:url(<?= base_url('img/proyectos_uploads/img/'.$p->imagen) ?>) no-repeat; background-size:cover; width:100%; height:130px; background-position:center;"></div>
                                          <span>
                                              <?php foreach($this->db->query("SELECT * FROM proyectos_paises INNER JOIN paises ON proyectos_paises.paises_id = paises.id WHERE proyectos_id = $p->id")->result() as $pa): ?>
                                                <img src="<?= base_url('img/Banderas/'.$pa->icono) ?>" alt="<?= $pa->nombre ?>" class="img-responsive center-block img-circle" id="perfil-proyecto">
                                              <?php endforeach ?>
                                          </span>
                                    </div>
                                    <h1 class="titulo-proyecto"><?= $p->nombre ?></h1>
                                    <div class="progress" id="progress-bar">
                                        <div class="progress-bar progress-bar-striped active <?= 'progress-bar-'.$p->barra ?>" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: <?= $p->porcentaje ?>%">
                                            <b><big><?= $p->porcentaje ?>%</big></b>
                                        </div>
                                    </div>
                                    <div class="texto-proyecto-activo">
                                      <b>Inicio:</b><br><?= @strftime('%d %b %Y',strtotime($p->fechas->row()->desde)) ?><br><b>Etapa:</b><br>Preguntas y respuestas
                                    </div>
                                  </div>
                              </a>
                          </div>
                          <!-- Termina Proyecto -->
                          <?php endforeach ?>
                          <?php if($proyectos->num_rows()==0): ?>
                            Aún no has sido invitado a proyectos
                          <?php endif ?>
                      </div>

                  </div>

                </div>


                </div>

                <footer class="footer contenedor-footer">
                    <?php include('footer.php');?>
                </footer>
                
            </div>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
</html>
