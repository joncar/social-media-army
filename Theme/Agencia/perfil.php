<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>


<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>
            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Editar Agencia</div></div>
                    </div>
                    <div class="menu-filtros">
                        <ul class="nav nav-pills nav-pills-warning">
                          <li class="active"><a href="<?= base_url('agencia/agencia_perfil/edit/'.get_instance()->user->id) ?>">Datos de usuario</a></li>
                          <li class=""><a href="<?= base_url() ?>agencia/cuenta/edit/<?= $this->user->id ?>" aria-expanded="true">Datos generales</a></li>
                          <li class=""><a href="<?= base_url() ?>agencia/cuenta/edit/<?= $this->user->id ?>" aria-expanded="false">Dirección</a></li>
                          <li class=""><a href="<?= base_url() ?>agencia/cuenta/edit/<?= $this->user->id ?>" aria-expanded="false">Clasificación</a></li>
                        </ul>
                    </div>

                    
                    <?= $output ?>



                </div>
            </div>

            <footer class="footer contenedor-footer">
                <?php include('footer.php');?>
            </footer>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
</html>
