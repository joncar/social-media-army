<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>
<body class="off-canvas-sidebar">
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black" data-image="<?= base_url() ?>Theme/Login/assets/img/fondo-header.jpg">
            <div class="content">
                <div class="container">


                  <div class="row text-center texto-blanco">
                      <div class="col-xs-12 col-sm-12 logo-header">
                          <img src="<?= base_url() ?>Theme/Login/assets/img/logo-brief-header.png" alt="Logo Bimbo" class="img-responsive center-block">
                      </div>
                      <div class="col-sm-12 text-uppercase padding2020 texto-blanco">
                          <div class="hidden-xs">
                              <h1><b>¡Bienvenido!</b></h1><h2 class="subtitulo-header">Plataforma de inteligencia para Briefing</h2>
                          </div>
                          <!-- Movil -->
                          <div class="visible-xs">
                              <h1><b>¡Bienvenido!</b></h1><h2 class="subtitulo-header">Plataforma de inteligencia<br>para Briefing</h2>
                          </div>
                      </div>
                  </div>


                    <div class="row">





                      <!-- Inicia terminar registro -->
                          <?= $output ?>







                    </div>

                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <div class="col-sm-12 text-center link-olvidar-login"><a title="Olvidé mi contraseña" href="<?= base_url('registro/forget') ?>">Olvidé mi contraseña</a></div>
                    <div class="main-footer logo autor text-center">
                        <b>Powered by <a href="https://brandsbell.com/" target="_blank"><br>BrandsBell<sup>®</sup></a></b>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>

<?php include('modales.php');?>









<!--   Core JS Files   -->
<script src="<?= base_url() ?>Theme/Login/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>Theme/Login/assets/js/bootstrap.min.js" type="text/javascript"></script>

<script src="<?= base_url() ?>Theme/Login/assets/js/material.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>Theme/Login/assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="<?= base_url() ?>Theme/Login/assets/js/arrive.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="<?= base_url() ?>Theme/Login/assets/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?= base_url() ?>Theme/Login/assets/js/moment.min.js"></script>
<!--  Charts Plugin, full documentation here: https://gionkunz.github.io/chartist-js/ -->
<script src="<?= base_url() ?>Theme/Login/assets/js/chartist.min.js"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="<?= base_url() ?>Theme/Login/assets/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
<script src="<?= base_url() ?>Theme/Login/assets/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="<?= base_url() ?>Theme/Login/assets/js/jquery.sharrre.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="<?= base_url() ?>Theme/Login/assets/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="<?= base_url() ?>Theme/Login/assets/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin, full documentation here: https://refreshless.com/nouislider/ -->
<script src="<?= base_url() ?>Theme/Login/assets/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1_8C5Xz9RpEeJSaJ3E_DeBv8i7j_p6Aw"></script>
<!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="<?= base_url() ?>Theme/Login/assets/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
<script src="<?= base_url() ?>Theme/Login/assets/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin, full documentation here: https://limonte.github.io/sweetalert2/ -->
<script src="<?= base_url() ?>Theme/Login/assets/js/sweetalert2.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?= base_url() ?>Theme/Login/assets/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->

<!-- Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="<?= base_url() ?>Theme/Login/assets/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?= base_url() ?>Theme/Login/assets/js/material-dashboard.js?v=1.2.1"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= base_url() ?>Theme/Login/assets/js/demo.js"></script>
<script src="<?= base_url() ?>js/jquery.maskedinput.min.js"></script>



<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/ui/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?= base_url() ?>Theme/Login/cruds/registro/js/jquery.form.js"></script>
<script src="<?= base_url() ?>Theme/Login/cruds/registro/js/flexigrid-edit.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js"></script>

<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/tmpl.min.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/load-image.min.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/jquery.iframe-transport.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/jquery.fileupload.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/config/jquery.fileupload.config.js"></script>


<script type="text/javascript">
    $(document).ready(function() {

        if($(".chosen-select").length>0){  
	        $(".chosen-select").selectpicker();  
	        $(".chosen-multiple-select").selectpicker();
    	}
      $(".chosen-multiple-select").selectpicker();

    	$(".datepicker-input").mask("99/99/9999");
    	
    });


</script>


</html>
