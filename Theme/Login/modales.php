<!-- Modal Enviar Brief -->
<div class="modal fade" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <img src="<?= base_url() ?>Theme/Login/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b>Enviar Brief</b><br>
                  Selecciona el Proyecto que deseas enviar
                </div>
                <form class="form-horizontal">
                    <div class="select-agencias">
                        <select class="selectpicker" data-style="select-with-transition" multiple title="Selecciona el Proyecto" data-size="7">
                          <option disabled>Selecciona el Proyecto</option>
                          <option value="2">Servicio 1</option>
                          <option value="3">Servicio 2</option>
                          <option value="4">Servicio 3</option>
                          <option value="5">Servicio 4</option>
                        </select>
                    </div>
                </form>

                <input style="display: none;" class="swal2-input">
                <input style="display: none;" class="swal2-file" type="file">
                <div class="swal2-range" style="display: none;">
                    <output></output><input type="range">
                </div>
                <select style="display: none;" class="swal2-select"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
                <textarea style="display: none;" class="swal2-textarea"></textarea>
                <div class="swal2-validationerror" style="display: none;"></div>
                <hr class="swal2-spacer" style="display: block;">
                <button type="button" class="swal2-confirm btn btn-success">Crear Nuevo Brief</button>
                <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Enviar</button>
                <span class="swal2-close" style="display: none;">×</span>
            </div>
    </div>
</div>
<!-- Termina Modal Enviar Brief -->

<!-- Modal Recuperar contraseña -->
<div class="modal fade" id="recuperar-contraseña" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <img src="<?= base_url() ?>Theme/Login/assets/img/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b>Recuperar contraseña</b><br>
                  Ingresa tu correo electrónico y te enviaremos las instrucciones para reestablecer tu contraseña.
                </div>

                <div class="form-group">
                    <label class="control-label">Correo</label>
                    <input  maxlength="100" type="text" class="form-control" placeholder="..."  />
                </div>

                <input style="display: none;" class="swal2-input">
                <input style="display: none;" class="swal2-file" type="file">
                <div class="swal2-range" style="display: none;">
                    <output></output><input type="range">
                </div>
                <select style="display: none;" class="swal2-select"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
                <textarea style="display: none;" class="swal2-textarea"></textarea>
                <div class="swal2-validationerror" style="display: none;"></div>
                <hr class="swal2-spacer" style="display: block;">
                <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Cancelar</button>
                <button type="button" class="swal2-confirm btn btn-success">Reestablecer contraseña</button>
                <span class="swal2-close" style="display: none;">×</span>
            </div>
    </div>
</div>
<!-- Termina Recuperar contraseña -->

<!-- Modal Recuperar contraseña -->
<div class="modal fade" id="contacto-login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <img src="<?= base_url() ?>Theme/Login/assets/img/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b>¿Necesitas ayuda?</b><br>
                  Contáctanos y nos pondremos en contacto
                </div>

                <div class="form-group">
                    <label class="control-label">Nombre</label>
                    <input  maxlength="100" type="text" class="form-control" placeholder="..."  />
                </div>

                <div class="form-group">
                    <label class="control-label">Correo</label>
                    <input  maxlength="100" type="email" class="form-control" placeholder="..."  />
                </div>

                <input style="display: none;" class="swal2-input">
                <input style="display: none;" class="swal2-file" type="file">
                <div class="swal2-range" style="display: none;">
                    <output></output><input type="range">
                </div>
                <select style="display: none;" class="swal2-select"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
                <textarea style="display: none;" class="swal2-textarea"></textarea>
                <div class="swal2-validationerror" style="display: none;"></div>
                <hr class="swal2-spacer" style="display: block;">
                <button type="button" class="swal2-confirm btn btn-success">Enviar</button>
                <span class="swal2-close" style="display: none;">×</span>
            </div>
    </div>
</div>
<!-- Termina Recuperar contraseña -->

<!-- Script Proceso de Registro de Proyecto -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$(document).ready(function () {
    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
    allWells.hide();
    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });
    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }
        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
    $('div.setup-panel div a.btn-primary').trigger('click');
});
</script>
