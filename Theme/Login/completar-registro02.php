<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body class="off-canvas-sidebar">
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black" data-image="<?= base_url() ?>Theme/Login/assets/img/fondo-header.jpg">
            <div class="content">
                <div class="container">


                  <div class="row text-center texto-blanco">
                      <div class="col-xs-12 col-sm-12 logo-header">
                          <img src="<?= base_url() ?>Theme/Login/assets/img/logo-brief-header.png" alt="Logo Bimbo" class="img-responsive center-block">
                      </div>
                      <div class="col-sm-12 text-uppercase padding2020 texto-blanco">
                          <div class="hidden-xs">
                              <h1><b>¡Bienvenido!</b></h1><h2 class="subtitulo-header">Plataforma de inteligencia para Briefing</h2>
                          </div>
                          <!-- Movil -->
                          <div class="visible-xs">
                              <h1><b>¡Bienvenido!</b></h1><h2 class="subtitulo-header">Plataforma de inteligencia<br>para Briefing</h2>
                          </div>
                      </div>
                  </div>


                    <div class="row">





                      <!-- Inicia terminar registro -->
                          <div class="modal-dialog">
                                  <div class="swal2-modal swal2-show" style="display: block; width: auto; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: auto;" tabindex="-1">
                                      <div class="logo-modales">
                                          <img src="<?= $this->user->logo ?>" alt="Logo Bimbo" class="img-responsive center-block">
                                      </div>
                                      <div class="titulo-modal"><b>Registro de Cliente</b></div>

                                      <div class="wizard">
                                          <div id="messageSubmit"></div>
                                          <ul class="nav nav-wizard">
                                              <li class="active"><a href="#registrocliente1" data-toggle="tab">Datos</a></li>
                                              <li><a href="#registrocliente2" data-toggle="tab">Dirección</a></li>
                                              <li><a href="#registrocliente3" data-toggle="tab">Finalizar</a></li>
                                          </ul>

                                          <form action="" onsubmit="return send(this)">
                                              <div class="tab-content">
                                                  <div class="tab-pane active" id="registrocliente1">
                                                      <div class="col-sm-12 subtitulo-modal"><b>Completa la información de tu empresa</b></div>

                                                      <div class="col-xs-12 form-group">
                                                          <label class="btn btn-subir-img text-center">
                                                              <img src="Theme/Cliente/assets/img/login/subir-imagen.png" alt="Logo Bimbo" class="img-responsive center-block">
                                                              Foto de perfil
                                                              <input type="file" name="imagen" class="fileImage">
                                                          </label>
                                                      </div>

                                                      <div class="col-xs-12 form-group">
                                                          <label class="control-label">Nombre</label>
                                                          <input  maxlength="100" type="text" name="nombre" class="form-control" placeholder=""  />
                                                      </div>

                                                      <div class="col-xs-12 form-group">
                                                          <label class="control-label">Apellidos</label>
                                                          <input  maxlength="100" type="text" name="nombre" class="form-control" placeholder=""  />
                                                      </div>

                                                      <ul class="list-inline pull-right margen-btn-agregar">
                                                          <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 2</button></li>
                                                      </ul>
                                                  </div>




                                                  <div class="tab-pane" id="registrocliente2">
                                                      <div class="col-sm-12 padding0 subtitulo-modal"><b>País</b></div>

                                                      <div class="col-sm-12">
                                                          <label class="control-label">Selecciona un País</label>
                                                          <select class="form-control">
                                                              <option>Opción 1</option>
                                                              <option>Opción 2</option>
                                                              <option>Opción 3</option>
                                                          </select>
                                                      </div>

                                                      <div class="col-xs-12 form-group">
                                                          <label class="control-label">Cargo</label>
                                                          <input  maxlength="100" type="text" name="nombre" class="form-control" placeholder=""  />
                                                      </div>

                                                      <ul class="list-inline pull-right margen-btn-agregar">
                                                          <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 3</button></li>
                                                      </ul>
                                                  </div>


                                                  <div class="tab-pane" id="registrocliente3">
                                                      <div class="col-sm-12 padding0 subtitulo-modal">
                                                          <b>Datos</b>
                                                      </div>

                                                      <div class="col-xs-12 form-group">
                                                          <label class="control-label">Correo</label>
                                                          <input  maxlength="100" type="text" name="nombre" class="form-control" placeholder=""  />
                                                      </div>

                                                      <div class="col-xs-12 form-group">
                                                          <label class="control-label">Crear contraseña</label>
                                                          <input  maxlength="100" type="text" name="nombre" class="form-control" placeholder=""  />
                                                      </div>

                                                      <div class="col-xs-12 form-group">
                                                          <label class="control-label">Repetir contraseña</label>
                                                          <input  maxlength="100" type="text" name="nombre" class="form-control" placeholder=""  />
                                                      </div>


                                                      <ul class="list-inline pull-right margen-btn-agregar">
                                                          <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Finalizar</button></li>
                                                      </ul>
                                                  </div>

                                                  <div class="clearfix"></div>

                                              </div>
                                          </form>

                                      </div>
                                  </div>
                          </div>
                      <!-- Termina registro -->







                    </div>

                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <div class="col-sm-12 text-center link-olvidar-login"><a title="Olvidé mi contraseña" href="<?= base_url('registro/forget') ?>">Olvidé mi contraseña</a></div>
                    <div class="main-footer logo autor text-center">
                        <b>Powered by <a href="https://brandsbell.com/" target="_blank"><br>BrandsBell<sup>®</sup></a></b>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>





<script>
$(document).ready(function () {
    $(document).on('click','.btn-addDate',function(){
        var newRow = $(this).parents('.tab-pane').find(".rowDate").clone();
        newRow.find('input[type="text"]').val('');
        newRow.find('.btn-remove').show();
        newRow.removeClass('rowDate');
        $(this).parents('.tab-pane').find('.DateInfo').append(newRow);
        //Init calendar
        $('.datetimepicker').datetimepicker({format: 'DD/MM/YYYY HH:mm',icons: {time: "fa fa-clock-o"}});
    });
    var fechas = [];
    $(document).on('click','.OrderingDate',function(){
        var content = $(this).parents('.tab-pane').find('.DateInfo');
        var dates = content.find('.dateslist');
        fechas = [];
        dates.each(function(){
            var nombre = $(this).find('input[name="proyectos_fechas[nombre][]"]').val();
            var desde = $(this).find('input[name="proyectos_fechas[fecha_desde][]"]').val();
            var desdef = $(this).find('input[name="proyectos_fechas[fecha_desde][]"]').val();
            var hasta = $(this).find('input[name="proyectos_fechas[fecha_hasta][]"]').val();
            var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
            date = desde.replace(pattern,'$3-$2-$1');
            date = new Date(date);
            desde = date;
            if(fechas.length===0){
                fechas.push({desde:desde,desdef:desdef,hasta:hasta,nombre:nombre});
            }else{
                for(var i in fechas){
                    if(fechas[i].desde>date){
                        var aux = fechas[i];
                        fechas[i] = {desde:desde,desdef:desdef,hasta:hasta,nombre:nombre};
                        date = aux.desde;
                        desde = aux.desde;
                        hasta = aux.hasta;
                        desdef = aux.desdef;
                        nombre = aux.nombre;

                    }
                }
                fechas.push({desde:desde,desdef:desdef,hasta:hasta,nombre:nombre});
            }
            console.log(fechas);
            if(dates.length===fechas.length){
                var x = 0;
                dates.each(function(){
                    $(this).find('input[name="proyectos_fechas[nombre][]"]').val(fechas[x].nombre);
                    $(this).find('input[name="proyectos_fechas[fecha_desde][]"]').val(fechas[x].desdef);
                    $(this).find('input[name="proyectos_fechas[fecha_hasta][]"]').val(fechas[x].hasta);
                    x++;
                });
            }
        });
    });

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var $target = $(e.target);
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });
    $(".btn-primary").click(function (e) {
        if(!$(this).hasClass('nonavigate')){
            if(check()){
                var $active = $('.wizard .nav-wizard li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
            }
        }
    });
});
function closeModal($id){
    $($id).modal('toggle');
}
function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
</script>
<!-- Termina Script Proceso de Registro de Proyecto -->

<!--Calendario-->
<script type="text/javascript">
    $(document).ready(function() {
        md.initSliders()
        demo.initFormExtendedDatetimepickers();
    });
</script>

<!-- Librerias -->
<!--   Core JS Files   -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>Theme/Cliente/assets/js/material.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>Theme/Cliente/assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/arrive.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/moment.min.js"></script>
<!--  Charts Plugin, full documentation here: https://gionkunz.github.io/chartist-js/ -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/chartist.min.js"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/jquery.sharrre.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin, full documentation here: https://refreshless.com/nouislider/ -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1_8C5Xz9RpEeJSaJ3E_DeBv8i7j_p6Aw"></script>
<!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin, full documentation here: https://limonte.github.io/sweetalert2/ -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/sweetalert2.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/fullcalendar.min.js"></script>
<!-- Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/material-dashboard.js?v=1.2.1"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= base_url() ?>Theme/Cliente/assets/js/demo.js"></script>



</html>
