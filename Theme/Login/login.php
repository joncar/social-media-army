<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>

</head>
<body class="off-canvas-sidebar">
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black" data-image="<?= base_url() ?>Theme/Login/assets/img/fondo-header.jpg">
            <div class="content">
                <div class="container">

                    <div class="row text-center texto-blanco">
                        <div class="col-xs-12 col-sm-12 logo-header">
                            <img src="<?= base_url() ?>Theme/Login/assets/img/logo-brief-header.png" alt="Logo Bimbo" class="img-responsive center-block">
                        </div>
                        <div class="col-sm-12 text-uppercase padding2020 texto-blanco margin-welcome-login">
                            <div class="hidden-xs">
                                <h1><b>Welcome!</b></h1><h2 class="subtitulo-header">Briefing Platform</h2>
                            </div>
                            <!-- Movil -->
                            <div class="visible-xs">
                                <h1><b>Welcome!</b></h1><h2 class="subtitulo-header">Briefing Platform</h2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <?php if (!empty($msj)) echo $msj ?>
                            <?php if (!empty($_SESSION['msj'])) echo $_SESSION['msj'] ?>
                            <?php $_SESSION['msj'] = null ?>
                            <form method="post" action="<?= base_url('main/login') ?>">
                                <div class="card card-login card-hidden">
                                    <div class="card-content">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            <i class="material-icons">person_pin</i></span>
                                            <div class="form-group label-floating">
                                                <label class="control-label"><b>Email</b></label>
                                                <input name="email" type="email" class="form-control">
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="material-icons">fingerprint</i></span>
                                            <div class="form-group label-floating">
                                                <label class="control-label"><b>Contraseña</b></label>
                                                <input type="password" name="pass" class="form-control">
                                            </div>

                                            <div class="col-sm-12 text-right link-olvidar-login"><a title="Olvidé mi contraseña" href="<?= base_url('registro/forget') ?>">Olvidé mi contraseña</a></div>

                                        </div>
                                        <div class="input-group margen-btn-login" style="margin-top:10px">
                                            <div>
                                                <center>
                                                    <div class="g-recaptcha" data-sitekey="6LcdoCgbAAAAADsy0hPyajm_JTfaryWNewzJl1aP"></div>                                                    
                                                </center>
                                            </div>
                                        </div>
                                        <div class="input-group margen-btn-login">
                                            <div class="checkbox">
                                               <center> <label class="margen-checkbox">
                                                    <input name="recordar" type="checkbox" value="1" checked=""></span>
                                                    Recordar mis datos
                                                </label></center>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="footer text-center">
                                        <div class="iconos-favoritos-agencias">
                                            <a title="Iniciar Sesión" href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief"> Iniciar sesión</button></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center link-login">
                                    <!--<div class="col-sm-6"><a href="<?= base_url('registro/agencia/add') ?>">Registra una Agencia</a></div>
                                    <div class="col-sm-6"><a href="<?= base_url('registro/cliente/add') ?>">Registra una Empresa</a></div>-->
                                    <div class="col-sm-12"><a href="<?= base_url('registro/cliente/add') ?>">Registro nuevo usuario</a></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <div class="main-footer logo autor text-center">
                        <b>Powered by <a href="https://brandsbell.com/" target="_blank"><br>BrandsBell<sup>®</sup></a></b>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });

    var sess = typeof localStorage.session != 'undefined'?localStorage.session:'';
    if(sess!=''){
        document.location.href="<?= base_url() ?>main/loginShort?pw="+sess;
    }
</script>
</html>
