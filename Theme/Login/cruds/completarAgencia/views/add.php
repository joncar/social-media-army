<?php
$this->set_css($this->default_theme_path.'registro/css/flexigrid.css');
$this->set_js_lib($this->default_theme_path.'registro/js/jquery.form.js');
$this->set_js_config($this->default_theme_path.'registro/js/flexigrid-add.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<?php echo form_open( $insert_url, 'method="post" data-validationurl="'.$validation_url.'" id="crudForm" autocomplete="off" enctype="multipart/form-data" class="crudForm"'); ?>
<div class="modal-dialog">
        <div class="swal2-modal swal2-show" style="display: block; width: 400px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: auto;" tabindex="-1">
            <div class="modal-header">
                <a href="<?= base_url('main/unlog') ?>" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></a>
            </div>

            <div class="logo-modales">
                <img src="<?= base_url(empty(get_instance()->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.get_instance()->user->foto) ?>" alt="Logo Bimbo" class="img-responsive center-block">
            </div>
            <div id='report-error' style="display:none" class='alert alert-danger report-error'></div>
            <div id='report-success' style="display:none" class='alert alert-success report-success'></div>
            <div class="titulo-modal"><b>Registro de Agencia</b></div>

            <div class="wizard">
                <div id="messageSubmit"></div>
                 <!--<ul class="nav nav-wizard">
                    <li class="active"><a href="#registroagencia1" data-toggle="tab">Datos</a></li>
                   <li><a href="#registroagencia2" data-toggle="tab">Experiencia</a></li>
                    <li><a href="#registroagencia3" data-toggle="tab">Servicios</a></li>
                    <li><a href="#registroagencia4" data-toggle="tab">Terminar registro</a></li>
                </ul>-->
                    <div class="tab-content">
                        <div class="tab-pane active" id="registroagencia1">
                            <div class="col-sm-12 subtitulo-modal"><b>Completa la información de tu empresa</b></div>

                            <div class="col-xs-12 form-group" style="width:100%; text-align: center">
                                <?= $input_fields['logo']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Nombre</label>
                                <?= $input_fields['nombre']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Apellido</label>
                                <?= $input_fields['apellido']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Razón Social</label>
                                <?= $input_fields['razon_social']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">RFC</label>
                                <?= $input_fields['rfc']->input ?>
                            </div>


                            <div class="input-group">
                                <span class="input-group-addon"><i class="material-icons">fingerprint</i></span>
                                <div class="form-group label-floating">
                                    <label class="control-label"><b>Contraseña</b></label>
                                    <?= $input_fields['password']->input ?>
                                </div>
                            </div>


                            <div class="input-group">
                                <span class="input-group-addon"><i class="material-icons">fingerprint</i></span>
                                <div class="form-group label-floating">
                                    <label class="control-label"><b>Repetir Contraseña</b></label>
                                    <?= $input_fields['password2']->input ?>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <label class="control-label" style="margin:15px 0">Pais</label>
                                <?= $input_fields['paises_id']->input ?>
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="material-icons">event_seat</i></span>
                                <div class="form-group label-floating">
                                    <label class="control-label"><b>Posición/Cargo</b></label>
                                    <?= $input_fields['puesto']->input ?>
                                </div>
                            </div>

                            <!--
                            <div class="col-xs-12 form-group">
                                <label class="control-label">Tipo de agencia</label>
                                <?= $input_fields['categorias_agencias_id']->input ?>
                            </div>

                            <div class="col-sm-12">
                                <label class="control-label">Alcance local / global</label>
                                <?= $input_fields['alcance']->input ?>
                            </div>-->

                            <!--
                            <ul class="list-inline pull-right margen-btn-agregar">
                                <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 2</button></li>
                            </ul>-->

                            <?php
                                  foreach($hidden_fields as $hidden_field){
                                          echo $hidden_field->input;
                                  }
                              ?>
                            <ul class="list-inline pull-right margen-btn-agregar">
                                <li><button type="submit" class="btn btn-primary nonavigate" id="btn-proyecto-propuestas">Guardar datos</button></li>
                            </ul>
                        </div>

                        <!--
                        <div class="tab-pane" id="registroagencia2">
                            <div class="col-sm-12 padding0 subtitulo-modal">
                                <b>Experiencia</b>
                            </div>

                            <div class="col-sm-12">
                                <label class="control-label">Fecha de constitución</label>
                                <?= $input_fields['fecha_constitucion']->input ?>
                            </div>

                            <div class="col-sm-12">
                                <label class="control-label">Fecha de ingreso como proveedor</label>
                                <?= $input_fields['fecha_proveedor']->input ?>
                            </div>

                            <ul class="list-inline pull-right margen-btn-agregar">
                                <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 3</button></li>
                            </ul>
                        </div>

                        <div class="tab-pane" id="registroagencia3">
                            <div class="col-sm-12 subtitulo-modal">
                                <b>Agregar servicios de Agencia</b>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 padding0">
                                    <div class="select-agencias">
                                        <div class="">
                                          <label class="control-label">Content Services</label>
                                          <?= $input_fields['content_services']->input ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 padding0">
                                    <div class="select-agencias">
                                        <div class="">
                                          <label class="control-label">Traditional Digital Marketing</label>
                                          <?= $input_fields['traditional_digital_mkt']->input ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 padding0">
                                    <div class="select-agencias">
                                        <div class="">
                                          <label class="control-label">Creative Services</label>
                                          <?= $input_fields['Creative_Services']->input ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 padding0">
                                    <div class="select-agencias">
                                        <div class="">
                                          <label class="control-label">Social Services</label>
                                          <?= $input_fields['social_services']->input ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 padding0">
                                    <div class="select-agencias">
                                        <div class="">
                                          <label class="control-label">Strategic Services</label>
                                          <?= $input_fields['strategic_services']->input ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 padding0">
                                    <div class="">
                                          <label class="control-label">Technical Services</label>
                                          <?= $input_fields['technical_services']->input ?>
                                        </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                              <div class="iconos-favoritos-agencias">
                                  <button type="button" class="nonavigate cleanFilters btn btn-primary btn-round" id="btn-borrar-filtros"><i class="material-icons">cached</i> Borrar filtros</button>
                              </div>
                            </div>

                            <ul class="list-inline pull-right margen-btn-agregar">
                                <li><button type="submit" class="btn btn-primary nonavigate" id="btn-proyecto-propuestas">Guardar datos</button></li>
                            </ul>
                        </div>

                        <div class="tab-pane" id="registroagencia4">
                            <div class="col-sm-12 subtitulo-modal">
                                <b>Registro de empresa completo</b>
                            </div>

                            <div class="col-sm-12 text-center">
                                ¡¡Felicidades, has completado el registro de tu empresa!!<br>
                                Ahora podrás participar en proyectos y enviar tus propuestas.
                            </div>

                            <ul class="list-inline pull-right margen-btn-agregar">
                                <li><a href="<?= base_url('panel') ?>" class="btn btn-primary" id="btn-proyecto-propuestas">Aceptar</a></li>
                            </ul>
                        </div>

                        <div class="clearfix"></div>
                    </div>-->
            </div>
        </div>
</div>
<!-- Termina registro -->

<?php
            foreach($hidden_fields as $hidden_field){
                    echo $hidden_field->input;
            }
     ?>
</form>
