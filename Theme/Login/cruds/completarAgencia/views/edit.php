<?php
$this->set_css($this->default_theme_path.'registro/css/flexigrid.css');
$this->set_js_lib($this->default_theme_path.'registro/js/jquery.form.js');
$this->set_js_config($this->default_theme_path.'registro/js/flexigrid-add.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<?php echo form_open( $update_url, 'method="post" data-validationurl="'.$validation_url.'" id="crudForm" autocomplete="off" enctype="multipart/form-data" class="crudForm"'); ?>
<div class="modal-dialog">
        <div class="swal2-modal swal2-show" style="display: block; width: 400px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: auto;" tabindex="-1">
            <div class="modal-header">
                <a href="<?= base_url('main/unlog') ?>" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></a>
            </div>

            <div class="logo-modales">
                <img src="<?= base_url(empty(get_instance()->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.get_instance()->user->foto) ?>" alt="Logo Bimbo" class="img-responsive center-block">
            </div>
            <div id='report-error' style="display:none" class='alert alert-danger report-error'></div>
            <div id='report-success' style="display:none" class='alert alert-success report-success'></div>
            <div class="titulo-modal"><b>Registro de Agencia</b></div>

            <div class="wizard">
                <div id="messageSubmit"></div>

                    <div class="tab-content">
                        <div class="tab-pane active" id="registroagencia1">
                            <div class="col-sm-12 subtitulo-modal"><b>Completa la información de tu empresa</b></div>

                            <div class="col-xs-12 form-group" style="width:100%; text-align: center">
                                <?= $input_fields['logo']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Razón Social</label>
                                <?= $input_fields['razon_social']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">RFC</label>
                                <?= $input_fields['rfc']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Nombre comercial</label>
                                <?= $input_fields['nombre']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Fecha constitución</label>
                                <?= $input_fields['fecha_constitucion']->input ?>
                            </div>
							
							<div class="col-xs-12 form-group">
                                <label class="control-label">Fecha de inscripción como proveedor</label>
                                <?= $input_fields['fecha_inscripcion_proveedor']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Nombre del CEO</label>
                                <?= $input_fields['ceo']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Cantidad de colaboradores</label>
                                <?= $input_fields['empleados']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Países donde se presenta</label>
                                <?= $input_fields['paises']->input ?>
                            </div>

                            <div class="col-sm-12 padding0 subtitulo-modal">
                                <b>Dirección</b>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Calle</label>
                                <?= $input_fields['calle']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">No Interior</label>
                                <?= $input_fields['numero_interior']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">No Exterior</label>
                                <?= $input_fields['numero_exterior']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Colonia</label>
                                <?= $input_fields['colonia']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Municipio o Delegación</label>
                                <?= $input_fields['delegacion']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Estado</label>
                                <?= $input_fields['estado']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">País</label>
                                <?= $input_fields['paises_id']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Teléfono de Contacto + EXT</label>
                                <?= $input_fields['telefono']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Correo General</label>
                                <?= $input_fields['email']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Nombre y Apellido</label>
                                <input type="text" class="form-control" name="user[nombre]" value="<?= $_SESSION['nombre'] ?>" readonly>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Correo Electrónico </label>
                                <input type="text" class="form-control" name="user[email]" value="<?= $_SESSION['email'] ?>" readonly>
                            </div>

                            <div class="col-sm-12 padding0 subtitulo-modal">
                                <b>Servicios que Ofrece</b>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Content Services</label>
                                <?= $input_fields['content_services']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Traditional Digital Marketing</label>
                                <?= $input_fields['traditional_digital_mkt']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Creative Services</label>
                                <?= $input_fields['Creative_Services']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Social Services</label>
                                <?= $input_fields['social_services']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Specialized Marketing</label>
                                <?= $input_fields['specialized_marketing']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Strategic Services</label>
                                <?= $input_fields['strategic_services']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Technical services</label>
                                <?= $input_fields['technical_services']->input ?>
                            </div>

                            <?php
                                  foreach($hidden_fields as $hidden_field){
                                          echo $hidden_field->input;
                                  }
                              ?>
                            <ul class="list-inline pull-right margen-btn-agregar">
                                <li><button type="submit" class="btn btn-primary nonavigate" id="btn-proyecto-propuestas">Guardar datos</button></li>
                            </ul>
                        </div>

                        
            </div>
        </div>
</div>
</form>
