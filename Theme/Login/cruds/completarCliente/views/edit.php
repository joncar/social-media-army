<?php
$this->set_css($this->default_theme_path.'registro/css/flexigrid.css');
$this->set_js_lib($this->default_theme_path.'registro/js/jquery.form.js');
$this->set_js_config($this->default_theme_path.'registro/js/flexigrid-edit.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<?php echo form_open( $update_url, 'method="post" data-validationurl="'.$validation_url.'" id="crudForm" autocomplete="off" enctype="multipart/form-data" class="crudForm"'); ?>
<div class="modal-dialog">
        <div class="swal2-modal swal2-show" style="display: block; width: 400px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: auto;" tabindex="-1">
            <div class="modal-header">
                <a href="<?= base_url('main/unlog') ?>" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></a>
            </div>
            <div class="logo-modales">
                <img src="<?= base_url(empty(get_instance()->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.get_instance()->user->foto) ?>" alt="Logo Bimbo" class="img-responsive center-block">
            </div>
            <div id='report-error' style="display:none" class='alert alert-danger report-error'></div>
            <div id='report-success' style="display:none" class='alert alert-success report-success'></div>
            <div class="titulo-modal"><b>Registro de Empresa</b></div>

            <div class="wizard">
                <div id="messageSubmit"></div>                
                    <div class="tab-content">
                        <div class="tab-pane active" id="registroagencia1">
                            <div class="col-sm-12 subtitulo-modal"><b>Completa tus datos</b></div>

                            <div class="col-xs-12 form-group subir-img-login">
                                <?= $input_fields['foto']->input ?>
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Nombre</label>
                                <input type="text" class="form-control" name="nombre" id="field-nombre">
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Apellido</label>
                                <input type="text" class="form-control" name="apellido" id="field-apellido">
                            </div>

                            <div class="col-xs-12 form-group">
                                <label class="control-label">Contraseña</label>
                                <input type="password" class="form-control" name="password" id="field-password">
                            </div>


                            
                            <ul class="list-inline pull-right margen-btn-agregar">
                                <li><button type="submit" class="btn btn-primary nonavigate" id="btn-proyecto-propuestas">Terminar registro</button></li>
                            </ul>
                        </div>

                       
                        <div class="clearfix"></div>
                    </div>
            </div>
        </div>
</div>
<!-- Termina registro -->

<?php
            foreach($hidden_fields as $hidden_field){
                    echo $hidden_field->input;
            }
     ?>
</form>
