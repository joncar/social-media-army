<?php
$this->set_css($this->default_theme_path.'registro/css/flexigrid.css');
$this->set_js_lib($this->default_theme_path.'registro/js/jquery.form.js');
$this->set_js_config($this->default_theme_path.'registro/js/flexigrid-add.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<?php echo form_open( $insert_url, 'method="post" data-validationurl="'.$validation_url.'" id="crudForm" autocomplete="off" enctype="multipart/form-data" class="crudForm"'); ?>
  <div class="card card-login card-hidden">
      <div class="card-content">

          <!--<div class="input-group">
              <span class="input-group-addon"><i class="material-icons">store</i></span>
              <div class="form-group label-floating">
                  <label class="control-label"><b>Nombre de la Agencia/Empresa</b></label>
                  <?= $input_fields['razon_social']->input ?>
              </div>
          </div>

          <div class="input-group">
              <span class="input-group-addon"><i class="material-icons">account_circle</i></span>
              <div class="form-group label-floating">
                  <label class="control-label"><b>Nombre</b></label>
                  <?= $input_fields['nombre']->input ?>
              </div>
          </div>

          <div class="input-group">
              <span class="input-group-addon"><i class="material-icons">supervised_user_circle</i></span>
              <div class="form-group label-floating">
                  <label class="control-label"><b>Apellido</b></label>
                  <?= $input_fields['apellido']->input ?>
              </div>
          </div>-->

          <div class="input-group">
              <span class="input-group-addon"><i class="material-icons">email</i></span>
              <div class="form-group label-floating">
                  <label class="control-label"><b>Correo electrónico corporativo</b></label>
                  <?= $input_fields['email']->input ?>
              </div>
          </div>

      </div>

      <?php
          foreach($hidden_fields as $hidden_field){
                  echo $hidden_field->input;
          }
      ?>

     <div id='report-error' style="display:none" class='alert alert-danger report-error'></div>
     <div id='report-success' style="display:none" class='alert alert-success report-success'></div>

      <div class="footer text-center">
          <div class="iconos-favoritos-agencias">
              <a title="Iniciar Sesión" href="#">
               <button class="btn btn-primary btn-round" type="submit" id="btn-enviar-brief">
                    Solicitar Registro
               </button>
          </a>
          </div>
      </div>
  </div>
  <div class="col-sm-12 text-center link-login">
      <div class="col-sm-6"><a href="<?= base_url() ?>">Ya tengo cuenta</a></div>
      <div class="col-sm-6"><a title="Olvidé mi contraseña" href="<?= base_url('registro/forget') ?>">Olvidé mi contraseña</a></div>
  </div>


<?php echo form_close(); ?>
