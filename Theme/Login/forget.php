<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>
<body class="off-canvas-sidebar">
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black" data-image="<?= base_url() ?>Theme/Login/assets/img/fondo-header2.jpg">
            <div class="content">
                <div class="container">

                    <div class="row text-center texto-blanco">
                        <div class="col-xs-12 col-sm-12 logo-header">
                            <img src="<?= base_url() ?>Theme/Login/assets/img/logo-brief-header.png" alt="Logo Bimbo" class="img-responsive center-block">
                        </div>
                        <div class="col-sm-12 text-uppercase padding2020 texto-blanco">
                            <div class="hidden-xs">
                                <h1><b>¿Olvidaste tu contraseña?</b></h1><h2 class="subtitulo-header">Plataforma de inteligencia para Briefing</h2>
                            </div>
                            <!-- Movil -->
                            <div class="visible-xs">
                                <h1><b>¡Bienvenido!</b></h1><h2 class="subtitulo-header">Plataforma de inteligencia<br>para Briefing</h2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <form action="<?= base_url('registro/forget') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
                                <div class="card card-login card-hidden">
                                    <div class="card-content">

                                    	<?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?>
							            <?= !empty($msj)?$msj:'' ?>

                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="material-icons">email</i></span>
                                            <div class="form-group label-floating">
                                                <label class="control-label"><b>Correo electrónico</b></label>
                                                <input type="email" name="email" class="form-control">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="footer text-center">
                                        <div class="iconos-favoritos-agencias">
                                            <button class="btn btn-primary btn-round" id="btn-enviar-brief" type="submit"> Recuperar contraseña</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center link-login">
                                    <div class="col-sm-6"><a href="<?= base_url() ?>">Ya tengo cuenta</a></div>
                                    <div class="col-sm-6"><a title="Olvidé mi contraseña" href="<?= base_url('registro/forget') ?>">Olvidé mi contraseña</a></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <div class="main-footer logo autor text-center">
                        <b>Powered by <a href="https://brandsbell.com/" target="_blank"><br>BrandsBell<sup>®</sup></a></b>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>
</html>
