<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>
<body class="off-canvas-sidebar">
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black" data-image="<?= base_url() ?>Theme/Login/assets/img/fondo-header.jpg">
            <div class="content">
                <div class="container">


                  <div class="row text-center texto-blanco">
                      <div class="col-xs-12 col-sm-12 logo-header">
                          <img src="<?= base_url() ?>Theme/Login/assets/img/logo-brief-header.png" alt="Logo Bimbo" class="img-responsive center-block">
                      </div>
                      <div class="col-sm-12 text-uppercase padding2020 texto-blanco">
                          <div class="hidden-xs">
                              <h1><b>¡Bienvenido!</b></h1><h2 class="subtitulo-header">Plataforma de inteligencia para Briefing</h2>
                          </div>
                          <!-- Movil -->
                          <div class="visible-xs">
                              <h1><b>¡Bienvenido!</b></h1><h2 class="subtitulo-header">Plataforma de inteligencia<br>para Briefing</h2>
                          </div>
                      </div>
                  </div>


                    <div class="row">





                      <!-- Inicia terminar registro -->
                          <?= $output ?>







                    </div>

                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <div class="col-sm-12 text-center link-olvidar-login"><a title="Olvidé mi contraseña" href="<?= base_url('registro/forget') ?>">Olvidé mi contraseña</a></div>
                    <div class="main-footer logo autor text-center">
                        <b>Powered by <a href="https://brandsbell.com/" target="_blank"><br>BrandsBell<sup>®</sup></a></b>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<script>
function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
</script>
<!-- Termina Script Proceso de Registro de Proyecto -->



</html>
