<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>
<body class="off-canvas-sidebar">
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black" data-image="<?= base_url() ?>Theme/Login/assets/img/fondo-header.jpg">
            <div class="content">
                <div class="container">

                    <div class="row text-center texto-blanco">
                        <div class="col-xs-12 col-sm-12 logo-header">
                            <img src="<?= base_url() ?>Theme/Login/assets/img/logo-brief-header.png" alt="Logo Bimbo" class="img-responsive center-block">
                        </div>
                        <div class="col-sm-12 text-uppercase padding2020 texto-blanco">
                            <div class="hidden-xs">
                                <h1><b>¡Regístrate!</b></h1><h2 class="subtitulo-header"> BRIEFING PLATFORM</h2>
                            </div>
                            <!-- Movil -->
                            <div class="visible-xs">
                                <h1><b>¡Bienvenido!</b></h1><h2 class="subtitulo-header"> BRIEFING PLATFORM</h2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <?= $output ?>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <div class="main-footer logo autor text-center">
                        <b>Powered by <a href="https://brandsbell.com/" target="_blank"><br>BrandsBell<sup>®</sup></a></b>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<script type="text/javascript">
    $().ready(function() {
        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>
</html>
