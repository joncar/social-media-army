<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>Theme/Login/assets/img/apple-icon.png" />
<link rel="shortcut icon" href="<?= base_url().'img/favicon.ico' ?>" type="image/x-icon"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>BriefSuite</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />
<!-- Canonical SEO -->
<link rel="canonical" href="#" />
<!--  Social tags      -->
<meta name="keywords" content="Keywords">
<meta name="description" content="Description">
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="BriefSuite">
<meta itemprop="description" content="Description">
<meta itemprop="image" content="http://s3.amazonaws.com/creativetim_bucket/products/51/opt_mdp_thumbnail.jpg">
<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@creativetim">
<meta name="twitter:title" content="BIMBO">
<meta name="twitter:description" content="Description">
<meta name="twitter:creator" content="@creativetim">
<meta name="twitter:image" content="http://s3.amazonaws.com/creativetim_bucket/products/51/opt_mdp_thumbnail.jpg">
<!-- Open Graph data -->
<meta property="fb:app_id" content="655968634437471">
<meta property="og:title" content="BIMBO" />
<meta property="og:type" content="article" />
<meta property="og:url" content="http://www.creative-tim.com/product/material-dashboard-pro" />
<meta property="og:image" content="http://s3.amazonaws.com/creativetim_bucket/products/51/opt_mdp_thumbnail.jpg" />
<meta property="og:description" content="Description" />
<meta property="og:site_name" content="Creative Tim" />
<!-- Bootstrap core CSS     -->
<link href="<?= base_url() ?>Theme/Login/assets/css/bootstrap.min.css" rel="stylesheet" />
<!--  Material Dashboard CSS    -->
<link href="<?= base_url() ?>Theme/Login/assets/css/material-dashboard.css" rel="stylesheet" />
<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="<?= base_url() ?>Theme/Login/assets/css/demo.css" rel="stylesheet" />
<!--  Estilos Personales    -->
<link href="<?= base_url() ?>Theme/Login/assets/css/main.css" rel="stylesheet" />
<link href="<?= base_url() ?>Theme/Login/assets/css/queries.css" rel="stylesheet" />
<link href="<?= base_url() ?>Theme/Login/assets/css/cliente.css" rel="stylesheet" />
<!--     Fonts and icons     -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!-- Script Proceso de Registro de Proyecto -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.migrate/1.4.1/jquery-migrate.min.js"></script>

<!-- Script de Captcha  -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script>
   function onSubmit(token) {
     document.getElementById("demo-form").submit();
   }
 </script>


<!-- Script Proceso de Registro de Proyecto -->
<link type="text/css" rel="stylesheet" href=<?= base_url() ?>assets/grocery_crud/css/ui/simple/jquery-ui-1.10.1.custom.min.css" />
<link type="text/css" rel="stylesheet" href=<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/file_upload/file-uploader.css" />
<link type="text/css" rel="stylesheet" href=<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/file_upload/jquery.fileupload-ui.css" />
<link type="text/css" rel="stylesheet" href=<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css" />
<link type="text/css" rel="stylesheet" href=<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/file_upload/fileuploader.css" />
<link type="text/css" rel="stylesheet" href=<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/jquery.ui.datetime.css" />
<link type="text/css" rel="stylesheet" href=<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/jquery-ui-timepicker-addon.css" />
<link type="text/css" rel="stylesheet" href=<?= base_url() ?>Theme/Login/cruds/css/flexigrid.css" />



<?php
if(!empty($css_files) && !empty($js_files)):
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
<?php endforeach; ?>
<?php endif; ?>
