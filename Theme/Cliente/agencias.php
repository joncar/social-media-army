<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<link rel="stylesheet" href="<?= base_url('css/daterangepicker.css') ?>">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url('js/daterangepicker.js') ?>"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <?= $output ?>
            <!-- Contenido -->
            </div>

            <footer class="footer contenedor-footer">
                <?php include('footer.php');?>
            </footer>
            
        </div>
    </div>
    <script>
        $(document).on('change','input[name="search_text[]"]',function(){                                
            $(this).parents('form').submit();
        });
    </script>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>


<script>
    if($('#fecha_desde').length>0){        
        $('#fecha_desde').daterangepicker({
            "startDate": $("#fecha_desde").data('desde'),
            "endDate": $("#fecha_hasta").data('hasta'),
            'locale':{'format':'DD/MM/YYYY'}
        }, function(start, end, label) {
          $(".filtering_form").submit();
        });
    }
</script>
</html>
