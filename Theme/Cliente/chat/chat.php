<div class="row">
      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Chat</div></div>
  </div>

  <div class="row">
      <div class="col-sm-12 padding0">
      	  <div id="integrantesBox">
          	<?php $this->load->view('Cliente/chat/_integrantes'); ?>
      	  </div>
          <div class="message-wrap col-sm-9">
          	  <div class="msg-wrap" style="height: 60vh; overflow-y: auto;" id="messageBox">
		      	  <?php $this->load->view('Cliente/chat/_chat'); ?>
	      	  </div>
	      	  <?php if($proyecto->porcentaje<100): ?>
		          <form onsubmit="return send(this)">
					  <div class="send-wrap ">
					  	<input type="text" class="form-control send-message" name="mensaje" placeholder="Escribe un mensaje">
					  </div>
					  <div class="btn-panel">
					      <!--<a href="" class="col-lg-3 btn send-message-btn " role="button"><i class="fa fa-cloud-upload"></i> Adjuntar archivos</a>-->
					      <button type="submit" class="col-lg-4 text-right btn btn-success send-message-btn pull-right" role="button"><i class="fa fa-plus"></i> Enviar mensaje</button>
					  </div>	  
				  </form>
			  <?php endif ?>
		  </div>
      </div>

  </div>
</div>