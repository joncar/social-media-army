<?php foreach($messages as $n=>$m): ?>
<div class="media msg ">
  <div class="pull-left" style="margin-right:15px; width:64px; height:64px; background:url(<?= $this->querys->getLogo($m->id,$m->tipo) ?>); background-size:100%; background-position: center; background-repeat: no-repeat;"></div>
  <div class="media-body">
  	<small class="pull-right time">
  		<i class="fa fa-clock-o"></i> Hace <?= get_antiguedad_string($m->fecha) ?> <a href="javascript:;" onclick="removeMsj(<?= $n ?>)"> <i class="fa fa-times" style="color:red"></i> </a>
  	</small>
		<h5 class="media-heading"><?= $m->nombre ?></h5>
		<small><?= $m->mensaje ?></small>
  </div>
</div>	      
<?php endforeach ?>