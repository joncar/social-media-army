<!-- Modal Login -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
      </div>
      	<div class="loginmodal-container text-center img-modal-registro" id="modal-login">
            <img src="<?= base_url() ?>img/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="padding2020">
            	<b>Recuperar Contraseña</b><br>Ingresa tu correo electrónico y te enviaremos<br>las instrucciones para reestablecer tu cuenta
            </div>
            <form>
                <input type="text" name="user" placeholder="Tu correo">
                <input type="submit" name="login" class="login loginmodal-submit" value="ENVIAR">
            </form>
            <div class="login-help">
                <a href="registro.php">Crear Cuenta</a> - <a href="#">Más información</a>
            </div>
      	</div>
    </div>
</div>
<!-- Termina Modal Login -->

<!-- Modal Registro -->
<div class="modal fade" id="login-registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
      </div>
      	<div class="loginmodal-container text-center img-modal-registro" id="modal-login">
            <img src="img/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="padding2020">
            	<b>Marcas</b><br>Selecciona las marcas con las que trabajas
            </div>
          	<form>
            	<input type="text" name="user" placeholder="Username">
            	<input type="submit" name="login" class="login loginmodal-submit" value="ACEPTAR">
          	</form>
      	</div>
    </div>
</div>
<!-- Modal Contactar agencia -->
<!-- Termina Contactar agencia -->
<?php $this->load->view('Cliente/modales/contactar'); ?>
<?php $this->load->view('Cliente/modales/soporte'); ?>

<!-- Modal Proceso de Registro -->
<?php $this->load->view('Cliente/modales/add_project'); ?>
<!-- Termina Modal Proceso de Registro -->

<?php if(!empty($proyecto))$this->load->view('Cliente/modales/del_project'); ?>
<?php if(!empty($proyecto))$this->load->view('Cliente/modales/pause_project'); ?>
<?php if(!empty($proyecto))$this->load->view('Cliente/modales/restart_project'); ?>
<!-- Modal Cancelar Proyecto -->
<?php if(!empty($proyecto))$this->load->view('Cliente/modales/cancel_project'); ?>
<!-- Modal Enviar Brief -->
<?php $this->load->view('Cliente/modales/enviar_brief'); ?>

<!-- Modal Invitar Proyecto -->
<div class="modal fade" id="invitar-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
      </div>
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b>Confirmación</b><br>
                  ¿Seguro que deseas invitar a la Agencia<br>
                  "Nombre de la Agencia"<br>
                  a tu Proyecto:<br>"Nombre del Proyecto"?
                </div>

                <input style="display: none;" class="swal2-input">
                <input style="display: none;" class="swal2-file" type="file">
                <div class="swal2-range" style="display: none;">
                    <output></output><input type="range">
                </div>
                <select style="display: none;" class="swal2-select"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
                <textarea style="display: none;" class="swal2-textarea"></textarea>
                <div class="swal2-validationerror" style="display: none;"></div>
                <hr class="swal2-spacer" style="display: block;">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block; width:100%;">Cancelar</button>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="swal2-confirm btn btn-success" style="display: inline-block; width:100%;">Invitar</button>
                        </div>
                    </div>
                </div>



                <span class="swal2-close" style="display: none;">×</span>
            </div>
    </div>
</div>
<!-- Termina Modal Invitar Proyecto -->

<!-- Modal Invitacion Rechazada Proyecto -->
<div class="modal fade" id="rechazo-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
      </div>
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b>Invitación rechazada</b><br>
                  La agencia:<br>"Nombre de la Agencia"<br>
                  rechazó la invitación al Proyecto por el siguiente motivo:<br>
                  Motivo.
                </div>

                <input style="display: none;" class="swal2-input">
                <input style="display: none;" class="swal2-file" type="file">
                <div class="swal2-range" style="display: none;">
                    <output></output><input type="range">
                </div>
                <select style="display: none;" class="swal2-select"></select>
                <div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
                <textarea style="display: none;" class="swal2-textarea"></textarea>
                <div class="swal2-validationerror" style="display: none;"></div>
                <hr class="swal2-spacer" style="display: block;">
                <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
                <span class="swal2-close" style="display: none;">×</span>
            </div>
    </div>
</div>
<!-- Termina Modal Invitacion Rechazada Proyecto -->

<?php if(!empty($proyecto))$this->load->view('Cliente/modales/edit_project'); ?>



<!-- Modal Agradecer participación-->
<div class="modal fade" id="agradecer-participacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
      </div>
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Agradecer participación</b><br>
              Agradece a la agencia y brinda de porqué su propuesta no fue elegida.
            </div>

            <div class="form-group">
                <label class="control-label">Su propuesta fue rechazada porque...</label>
                <textarea class="form-control" id="textarea" name="textarea" placeholder="Comentarios..."></textarea>
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">

            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block; width:100%;">Cancelar</button>
                    </div>
                    <div class="col-sm-6">
                        <button type="button" class="swal2-confirm btn btn-success" style="display: inline-block; width:100%;">Aceptar</button>
                    </div>
                </div>
            </div>



            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal agradecer participación -->



<!-- Modal Agencia ganadora Final-->
<div class="modal fade" id="agencia-ganadora-final" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
      </div>
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Felicidades,<br>has completado tu proceso de selección</b><br>
              Notificaremos a:<br>
              "Nombre de la Agencia"
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-confirm btn btn-success">Finalizar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal agencia ganadora Final -->

<!-- Modal Fecha de presentación-->
<div class="modal fade" id="fecha-presentacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
      </div>
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Editar fecha de presentación</b><br>
              Edita la fecha o modo de presentación, notificaremos a la agencia.
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Presentación Presencial</label>
                </div>
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label class="margen-checkbox"><input type="checkbox" name="optionsCheckboxes"> Presentación AV</label>
                </div>
            </div>

            <div class="form-group">
                <label class="label-control">Selecciona la fecha</label>
                <input class="form-control datepicker" value="10/10/2016" style="" type="text">
                <span class="material-input"></span>
            </div>

            <div class="form-group">
                <label class="label-control">Selecciona el horario</label>
                <input class="form-control timepicker" value="14:00" style="" type="text">
                <span class="material-input"></span>
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">

            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block; width:100%">Cancelar</button>
                    </div>
                    <div class="col-sm-6">
                        <button type="button" class="swal2-confirm btn btn-success" style="display: inline-block; width:100%">Enviar cambios</button>
                    </div>
                </div>
            </div>



            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Fecha de presentación -->

<?php if(!empty($proyecto)): $this->load->view('Cliente/modales/invitar_presentacion'); endif; ?>
<?php if(!empty($proyecto)): $this->load->view('Cliente/modales/cancelar_propuesta'); endif; ?>
<?php if(!empty($proyecto)): $this->load->view('Cliente/modales/agencia_ganadora'); endif; ?>
<?php if(!empty($proyecto)): $this->load->view('Cliente/modales/share_folder'); endif; ?>

<!-- Modal Propuestas-->
<div class="modal fade" id="propuestas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

    <div class="modal-dialog">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
      </div>
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Propuesta</b><br>
              "Nombre de la Agencia"<br>
              Documentos de la Propuesta
            </div>







            <div class="card-content">
              <div class="material-datatables">
                  <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                          <table aria-describedby="datatables_info" role="grid" id="datatables"
                              class="table table-striped table-no-bordered table-hover dataTable dtr-inline"
                              style="width: 100%;" cellspacing="0" width="100%">

                          <thead>
                              <tr role="row">
                                    <th aria-sort="descending" aria-label="Name: activate to sort column ascending"
                                      style="width: 163px;" colspan="1" rowspan="1" aria-controls="datatables"
                                      tabindex="0" class="sorting_desc">ID</th>

                                      <th aria-label="Position: activate to sort column ascending"
                                      style="width: 241px;" colspan="1" rowspan="1" aria-controls="datatables"
                                      tabindex="0" class="sorting">Usuario</th>

                                      <th aria-label="Office: activate to sort column ascending"
                                      style="width: 123px;" colspan="1" rowspan="1" aria-controls="datatables"
                                      tabindex="0" class="sorting">Perfil</th>

                                      <th aria-label="Actions: activate to sort column ascending"
                                      style="width: 116px;" colspan="1" rowspan="1" aria-controls="datatables"
                                      tabindex="0" class="disabled-sorting text-right sorting">Acciones
                                      </th>
                                  </tr>
                          </thead>

                              <tfoot>
                                  <tr>
                                    <th colspan="1" rowspan="1">ID</th>
                                      <th colspan="1" rowspan="1">Usuario</th>
                                      <th colspan="1" rowspan="1">Perfil</th>
                                      <th colspan="1" rowspan="1" class="text-right">Acciones</th>
                                   </tr>
                              </tfoot>

                      <tbody>
                        <tr class="odd" role="row">
                              <td tabindex="0" class="sorting_1">1</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Administrador</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                          </tr>

                          <tr class="even" role="row">
                              <td tabindex="0" class="sorting_1">2</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Supervisor</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                          </tr>

                          <tr class="odd" role="row">
                              <td tabindex="0" class="sorting_1">3</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Usuario</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                          </tr>

                          <tr class="even" role="row">
                              <td tabindex="0" class="sorting_1">4</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Usuario</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                           </tr>

                          <tr class="odd" role="row">
                              <td class="sorting_1" tabindex="0">5</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Usuario</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                          </tr>

                          <tr class="even" role="row">
                              <td tabindex="0" class="sorting_1">6</td>
                              <td class="">correo@correo.com.mx</td>
                              <td class="">Usuario</td>
                              <td class="text-right">
                                  <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                    <i class="material-icons">dvr</i>
                                  </a>
                                  <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                    <i class="material-icons">close</i>
                                  </a>
                              </td>
                          </tr>


                       </tbody>
                  </table>

                  </div>
               </div>







            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>


    <div class="modal-dialog">
      	<div class="loginmodal-container text-center img-modal-registro" id="modal-login">
            <img src="../img/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="padding2020">
            	<b>Propuesta<br>"Nombre de la Agencia"</b><br>
              Documentos de la Propuesta
            </div>
            <div class="padding2020">
                <table class="table text-left">
                    <thead>
                      <tr>
                        <th scope="col">Nombre del Documento</th>
                        <th scope="col">Fecha</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">
                          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>  Nombre del Documento.pfp
                        </th>
                        <td><b>00 / 00/ 00</b></td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <i class="fa fa-file-text" aria-hidden="true"></i> Nombre del archivo.word
                        </th>
                        <td><b>00 / 00/ 00</b></td>
                      </tr>
                      <tr>
                        <th scope="row">
                          <i class="fa fa-video-camera" aria-hidden="true"></i> Nombre del video.mp4
                        </th>
                        <td><b>00 / 00/ 00</b></td>
                      </tr>
                    </tbody>
                </table>










                <div class="card-content">
                  <div class="material-datatables">
                      <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                        <div class="row">
                              <div class="col-xs-6 col-sm-6">
                                    <div class="select-agencias">
                                        <select class="selectpicker" data-style="select-with-transition"
                                        multiple title="Filtrar por" data-size="7">
                                          <option disabled>Filtrar</option>
                                          <option value="2">Filtro 1</option>
                                          <option value="2">Filtro 2</option>
                                          <option value="2">Filtro 3</option>
                                        </select>
                                    </div>
                              </div>
                              <div class="col-xs-6 col-sm-6">
                                <div class="dataTables_filter" id="datatables_filter">
                                      <div id="wrap">
                                        <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row botones-ajustes">
                              <div class="col-sm-2">
                                   <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Importar XML</button></a>
                              </div>
                              <div class="col-sm-2">
                                  <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Agregar Usuario</button></a>
                              </div>
                              <div class="col-sm-2">
                                  <a href="#"><button class="btn btn-primary btn-round" id="btn-enviar-brief">Descargar XML</button></a>
                              </div>
                              <div class="col-sm-2">
                                  <a title="Agregar usuario" href="#agregar-usuario-admin" data-toggle="modal"><button class="btn btn-primary btn-round" id="btn-ver-agencia">Agregar usuario</button></a>
                              </div>
                          </div>

                          <div class="row">
                            <div class="col-sm-12">
                              <table aria-describedby="datatables_info" role="grid" id="datatables"
                                  class="table table-striped table-no-bordered table-hover dataTable dtr-inline"
                                  style="width: 100%;" cellspacing="0" width="100%">

                              <thead>
                                  <tr role="row">
                                        <th aria-sort="descending" aria-label="Name: activate to sort column ascending"
                                          style="width: 163px;" colspan="1" rowspan="1" aria-controls="datatables"
                                          tabindex="0" class="sorting_desc">ID</th>

                                          <th aria-label="Position: activate to sort column ascending"
                                          style="width: 241px;" colspan="1" rowspan="1" aria-controls="datatables"
                                          tabindex="0" class="sorting">Usuario</th>

                                          <th aria-label="Office: activate to sort column ascending"
                                          style="width: 123px;" colspan="1" rowspan="1" aria-controls="datatables"
                                          tabindex="0" class="sorting">Perfil</th>

                                          <th aria-label="Actions: activate to sort column ascending"
                                          style="width: 116px;" colspan="1" rowspan="1" aria-controls="datatables"
                                          tabindex="0" class="disabled-sorting text-right sorting">Acciones
                                          </th>
                                      </tr>
                              </thead>

                                  <tfoot>
                                      <tr>
                                        <th colspan="1" rowspan="1">ID</th>
                                          <th colspan="1" rowspan="1">Usuario</th>
                                          <th colspan="1" rowspan="1">Perfil</th>
                                          <th colspan="1" rowspan="1" class="text-right">Acciones</th>
                                       </tr>
                                  </tfoot>

                          <tbody>
                            <tr class="odd" role="row">
                                  <td tabindex="0" class="sorting_1">1</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Administrador</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="even" role="row">
                                  <td tabindex="0" class="sorting_1">2</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Supervisor</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="odd" role="row">
                                  <td tabindex="0" class="sorting_1">3</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Usuario</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="even" role="row">
                                  <td tabindex="0" class="sorting_1">4</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Usuario</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                               </tr>

                              <tr class="odd" role="row">
                                  <td class="sorting_1" tabindex="0">5</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Usuario</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                              </tr>

                              <tr class="even" role="row">
                                  <td tabindex="0" class="sorting_1">6</td>
                                  <td class="">correo@correo.com.mx</td>
                                  <td class="">Usuario</td>
                                  <td class="text-right">
                                      <a href="#" class="btn btn-simple btn-warning btn-icon edit">
                                        <i class="material-icons">dvr</i>
                                      </a>
                                      <a href="#" class="btn btn-simple btn-danger btn-icon remove">
                                        <i class="material-icons">close</i>
                                      </a>
                                  </td>
                              </tr>


                           </tbody>
                      </table>

                      </div>
                   </div>







            </div>
          	<form>
                <input type="submit" name="login" class="login loginmodal-submit" value="Aceptar">
          	</form>
      	</div>
    </div>
</div>
<!-- Termina Modal Propuestas -->

<!-- Modal Ganador Seleccionado-->
<div class="modal fade" id="ganador-seleccionado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Ganador Seleccionado</b><br>
              La agencia:<br>
              "Nombre de la Agencia"<br>
              fue seleccionada como ganadora por la siguiente razón:<br>
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
              fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Ganador Seleccionado -->

<!-- Modal Rechazado-->
<div class="modal fade" id="rechazado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Motivo de rechazo</b><br>
              La propuesta fue rechazada porque:<br>
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
              fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Rechazado-->



<!-- Modal Invitación Admin-->
<div class="modal fade" id="invitacion-admin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Reenviar invitación</b><br>
              Enviaremos la invitación al siguiente correo
            </div>
            <div class="form-group">
                <label class="control-label">Correo</label>
                <input  maxlength="100" type="email" class="form-control" placeholder="..."  />
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">

            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block;">Cancelar</button>
                    </div>
                    <div class="col-sm-6">
                        <button type="button" class="swal2-confirm btn btn-success">Reenviar</button>
                    </div>
                </div>
            </div>



            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Invitación Admin-->

<!-- Modal Agregar Producto Admin-->
<div class="modal fade" id="agregar-producto-admin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Agregar Producto</b><br>
              Agrega un logotipo de 250 x 250px en formato .png con fondo transparente.
            </div>

            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                <div class="fileinput-new thumbnail img-circle">
                    <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail img-circle" style=""></div>
                <div>
                    <span class="btn btn-round btn-rose btn-file">
                        <span class="fileinput-new">Agregar foto</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="hidden"><input name="..." type="file">
                    <div class="ripple-container"></div></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">Nombre del producto</label>
                <input  maxlength="100" type="text" class="form-control" placeholder="..."  />
            </div>

            <form class="form-horizontal">
                <div class="select-agencias">
                    <select class="selectpicker" data-style="select-with-transition" multiple title="Marca" data-size="7">
                      <option disabled>Selecciona Marca</option>
                      <option value="2">Marca 1</option>
                      <option value="3">Marca 2</option>
                      <option value="4">Marca 3</option>
                      <option value="5">Marca 4</option>
                    </select>
                </div>
            </form>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">

            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <button type="button" class="swal2-cancel btn btn-danger" style="display: inline-block; width:100%">Cancelar</button>
                    </div>
                    <div class="col-sm-6">
                        <button type="button" class="swal2-confirm btn btn-success" style="display: inline-block; width:100%">Agregar producto</button>
                    </div>
                </div>
            </div>



            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Agregar Producto Admin-->

<!-- Modal Agregar imagen Admin-->
<div class="modal fade" id="agregar-imagen-admin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b>Agregar Imagen</b>
            </div>

            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                <div class="fileinput-new thumbnail img-circle">
                    <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail img-circle" style=""></div>
                <div>
                    <span class="btn btn-round btn-rose btn-file">
                        <span class="fileinput-new">Agregar foto</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="hidden"><input name="..." type="file">
                    <div class="ripple-container"></div></span>
                </div>
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">
            <button type="button" class="swal2-confirm btn btn-success">Aceptar</button>
            <span class="swal2-close" style="display: none;">×</span>
        </div>
    </div>
</div>
<!-- Termina Modal Invitación Admin-->









<script>
$(document).ready(function () {
    $(document).on('click','.btn-addDate',function(){
        var newRow = $(this).parents('.tab-pane').find(".rowDate").clone();
        newRow.find('input[type="text"]').val('');
        newRow.find('.btn-remove').show();
        newRow.removeClass('rowDate');
        $(this).parents('.tab-pane').find('.DateInfo').append(newRow);
        //Init calendar
        $('.datetimepicker').datetimepicker({format: 'DD/MM/YYYY HH:mm',icons: {time: "fa fa-clock-o"}});
    });
    var fechas = [];
    $(document).on('click','.OrderingDate',function(){
        var content = $(this).parents('.tab-pane').find('.DateInfo');
        var dates = content.find('.dateslist');
        fechas = [];
        dates.each(function(){
            var nombre = $(this).find('input[name="proyectos_fechas[nombre][]"]').val();
            var desde = $(this).find('input[name="proyectos_fechas[fecha_desde][]"]').val();
            var desdef = $(this).find('input[name="proyectos_fechas[fecha_desde][]"]').val();
            var hasta = $(this).find('input[name="proyectos_fechas[fecha_hasta][]"]').val();
            var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
            date = desde.replace(pattern,'$3-$2-$1');
            date = new Date(date);
            desde = date;
            if(fechas.length===0){
                fechas.push({desde:desde,desdef:desdef,hasta:hasta,nombre:nombre});
            }else{
                for(var i in fechas){
                    if(fechas[i].desde>date){
                        var aux = fechas[i];
                        fechas[i] = {desde:desde,desdef:desdef,hasta:hasta,nombre:nombre};
                        date = aux.desde;
                        desde = aux.desde;
                        hasta = aux.hasta;
                        desdef = aux.desdef;
                        nombre = aux.nombre;

                    }
                }
                fechas.push({desde:desde,desdef:desdef,hasta:hasta,nombre:nombre});
            }
            console.log(fechas);
            if(dates.length===fechas.length){
                var x = 0;
                dates.each(function(){
                    $(this).find('input[name="proyectos_fechas[nombre][]"]').val(fechas[x].nombre);
                    $(this).find('input[name="proyectos_fechas[fecha_desde][]"]').val(fechas[x].desdef);
                    $(this).find('input[name="proyectos_fechas[fecha_hasta][]"]').val(fechas[x].hasta);
                    x++;
                });
            }
        });
    });

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var $target = $(e.target);
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });
    $(".btn-primary").click(function (e) {
        if(!$(this).hasClass('nonavigate')){
            if(check()){
                var $active = $('.wizard .nav-wizard li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
            }
        }
    });
});
function closeModal($id){
    $($id).modal('toggle');
}
function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
</script>
<!-- Termina Script Proceso de Registro de Proyecto -->

<!--Calendario-->
<script type="text/javascript">
    $(document).ready(function() {
        md.initSliders()
        demo.initFormExtendedDatetimepickers();
    });
</script>
