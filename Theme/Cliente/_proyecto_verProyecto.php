<div class="col-xs-12 col-sm-3">
  <div class="card card-product" data-count="3">
    <div class="img-historial-proyecto">
      <div class="logo-agencia">
        <img class="center-block img-responsive" src="<?= $proyecto->imagen ?>" alt="<?= $proyecto->nombre ?>" id="foto-proyecto" style="max-width:80%">
      </div>
    </div>
    <div class="card-content">
      <div class="card-title"><b><?= $proyecto->nombre ?></b></div>
      <div class="progress" id="progress-bar">
        <div class="progress-bar progress-bar-striped active progress-bar-<?= $proyecto->barra ?>" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: <?= $proyecto->porcentaje ?>%">
          <b><big><?= $proyecto->porcentaje ?>%</big></b>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <?php
        $this->db->select('paises.icono, paises.nombre');
        $this->db->join('proyectos_paises','proyectos_paises.proyectos_id = proyectos.id')
             ->join('paises','paises.id = proyectos_paises.paises_id');
        $icono = $this->db->get_where('proyectos',array('proyectos.id'=>$proyecto->id));
        if($icono->num_rows()>0):
      ?>

      <?php endif ?>
      <b>Marca: <?= $proyecto->marcas ?></b><br>
      <b>Producto: <?= $proyecto->productos ?></b><br>
      <!-- <b>Estado: <?= $proyecto->status_label ?></b><br> -->
      <b>País: <?= $icono->row()->nombre ?> <img src="<?= base_url() ?>img/Banderas/<?= $icono->row()->icono ?>" alt="Perfil Dashboard" class="img-responsive center-block img-circle" id="perfil-proyecto"></b><br>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-9 contenedor-descripcion-agencia2 margen-movil-servicios">
  <div class="col-sm-12 texto-gris">
    <div class="col-xs-3">
      <div class="card card-stats">
        <div class="card-header" data-background-color="blue"><i class="material-icons">timeline</i></div>
        <div class="card-content">
          <p class="category">Fase:</p><br>
          <div class="card-title">
            <span class="porcentaje-agencia texto-rojo">
              <?php 
                if($proyecto->invitaciones->num_rows()>0): 
                echo $proyecto->fase?$proyecto->fase->nombre:'Finalizado' ;
                else: ?>
                Proyecto por Iniciar
                <?php endif ?>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-3">
      <div class="card card-stats">
        <div class="card-header" data-background-color="blue"><i class="material-icons">timer_off</i></div>
        <div class="card-content">
          <p class="category">Fin de la fase</p><br>
          <div class="card-title">
            <span class="porcentaje-agencia texto-morado">
              <?= $proyecto->fase?date("d/m/Y",strtotime($proyecto->fase->hasta)):date("d/m/Y",strtotime($this->db->query('SELECT * FROM proyectos_fechas where proyectos_id = '.$proyecto->id.' ORDER BY desde DESC')->row()->hasta)) ?>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-3">
      <div class="card card-stats">
        <div class="card-header" data-background-color="blue"><i class="material-icons">assignment_ind</i></div>
        <div class="card-content">
          <p class="category margen-titulos-agencias-tarjetas">Agencias</p><br>
          <div class="card-title">
            <span class="porcentaje-agencia texto-morado"><?= $proyecto->briefs->num_rows() ?></span>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xs-3">
      <?php if($proyecto->porcentaje<100): ?>
        <div class="">
            <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
              <a href="<?= base_url('cliente/agencia/agencias/'.$proyecto->id) ?>" class="btn btn-info btn-round" style="width:100%;">
                  <i class="material-icons">forward</i>  Invitar Agencias
              </a>
            </div>
            <!--<div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                <a title="Favoritos" href="<?= base_url('cliente/proyecto/propuestas/'.$proyecto->id) ?>">
                  <button class="btn btn-primary btn-round" id="btn-proyecto-propuestas">
                    <i class="material-icons">remove_red_eye</i> Ver Propuestas
                  </button>
                </a> 
            </div>-->
            <?php if($proyecto->porcentaje<100): ?>
              <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                  <a title="Editar Proyecto" href="#editar-proyecto" data-toggle="modal">
                    <button class="btn btn-danger btn-round" id="btn-proyecto-informacion" style="width:100%; background:#EF8104">
                      <i class="material-icons">border_color</i> Editar Proyecto
                    </button>
                  </a>
              </div>
            <?php endif ?>
            <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
                <a title="Ver Agencia" href="<?= base_url('cliente/chat/salas/'.$proyecto->id) ?>">
                  <button class="btn btn-danger btn-round" id="btn-proyecto-juntas">
                    <i class="material-icons">people</i> Preguntas y Respuestas
                    <?php 
                      echo $this->querys->getMessagesCount($proyecto->id);
                    ?>                                    
                  </button>
                </a>
            </div>
            <div class="col-xs-12 col-sm-12 basura-proyectos text-center">
                <a title="Eliminar Proyecto" href="#eliminar-proyecto" data-toggle="modal"><i class="fa fa-trash" aria-hidden="true"></i></a>
                <?php if($proyecto->status==1): ?>
                  <a title="Pausar Proyecto" href="#pausar-proyecto" data-toggle="modal"><i class="fa fa-pause" aria-hidden="true"></i></a>
                <?php elseif($proyecto->status==2): ?>
                  <a title="Iniciar Proyecto" href="#restart-proyecto" data-toggle="modal"><i class="fa fa-play" aria-hidden="true"></i></a>
                <?php endif ?>
                <!--<a title="Generar PDF" href="<?= base_url('reportes/rep/verReportes/1/pdf/proyectos_id/'.$proyecto->id) ?>">
                  <i class="material-icons margen-icono-descripcion">description</i>
                </a>-->
                <a title="Generar PDF" href="<?= base_url('cliente/proyecto/verProyecto/'.$proyecto->id) ?>?pdf=1">
                  <i class="material-icons margen-icono-descripcion">description</i>
                </a>

            </div>
        </div>
      <?php else: ?>
        <div class="col-xs-4 col-sm-12 iconos-favoritos-agencias">
            <!--<a title="Favoritos" href="<?= base_url('cliente/proyecto/propuestas/'.$proyecto->id) ?>">
              <button class="btn btn-primary btn-round" id="btn-proyecto-propuestas">
                <i class="material-icons">remove_red_eye</i> Ver Propuestas
              </button>
            </a> 

            <a title="Favoritos" href="#valoraciones">
              <button class="btn btn-black btn-round" style="width: 100%; margin-top:10px" id="btn-proyecto-juntas">
                <i class="material-icons">remove_red_eye</i> Valoraciones
              </button>
            </a>
        </div>-->
      <?php endif ?>
    </div>



  </div>
  <div class="col-xs-12 col-sm-12 texto-gris">
    
  </div>
  <div class="col-xs-12 col-sm-12">    
      <div class="contenedor-gant" style="min-height: 0">          
          <h3 style="padding: 0 10px;">Etapas</h3>
          <div class="contenedor-fechas" style="border:0">
              <div id="proyecto_grafico"></div>
          </div>
      </div>
  </div>
</div>
