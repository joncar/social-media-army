<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>
            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Editar Agencia</div></div>
                    </div>


                    <div class="card-content">
                        <div class="menu-filtros">
                            <ul class="nav nav-pills nav-pills-warning">
                                <li class="active"><a href="#datos-agencia" data-toggle="tab" aria-expanded="true">Datos generales</a></li>
                                <li class=""><a href="#direccion-agencia" data-toggle="tab" aria-expanded="false">Dirección</a></li>
                                <li class=""><a href="#clasificacion-agencia" data-toggle="tab" aria-expanded="false">Clasificación</a></li>
                            </ul>
                        </div>

                        <div class="tab-content contenedor-facturas">

                            <div class="tab-pane active" id="datos-agencia">
                                <div class="col-sm-12">
                                    <div class="card-content">
                                        <h4>Datos Generales</h4>





                                        <div class="row">
                                            <div class="col-sm-12 padding0">
                                                  <div class="col-sm-6">

                                                        <div class="col-sm-12">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label">Nombre</label>
                                                                <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                                <span class="material-input"></span>
                                                            <span class="material-input"></span></div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label">Razón social</label>
                                                                <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                                <span class="material-input"></span>
                                                            <span class="material-input"></span></div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label">Sitio web</label>
                                                                <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                                <span class="material-input"></span>
                                                            <span class="material-input"></span></div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label">Teléfono</label>
                                                                <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                                <span class="material-input"></span>
                                                            <span class="material-input"></span></div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label">Correo de contacto</label>
                                                                <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                                <span class="material-input"></span>
                                                            <span class="material-input"></span></div>
                                                        </div>

                                                  </div>

                                                  <div class="col-sm-6">

                                                        <div class="col-sm-12">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label">Fecha de constitución</label>
                                                                <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                                <span class="material-input"></span>
                                                            <span class="material-input"></span></div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label">RFC</label>
                                                                <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                                <span class="material-input"></span>
                                                            <span class="material-input"></span></div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label">Contacto</label>
                                                                <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                                <span class="material-input"></span>
                                                            <span class="material-input"></span></div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label">CEO</label>
                                                                <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                                <span class="material-input"></span>
                                                            <span class="material-input"></span></div>
                                                        </div>


                                                        <div class="col-sm-12">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label">Número de empleados</label>
                                                                <input class="form-control" name="nombre" value="Grupo Bimbo" type="number">
                                                                <span class="material-input"></span>
                                                            <span class="material-input"></span></div>
                                                        </div>

                                                  </div>
                                            </div>
                                        </div>





                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="direccion-agencia">
                                <div class="col-xs-12 col-sm-12">
                                    <h4>Dirección</h4>





                                    <div class="row">
                                        <div class="col-sm-12 padding0">

                                              <div class="col-sm-12">
                                                  <div class="col-sm-4">
                                                      <div class="form-group is-empty">
                                                          <label class="control-label">País</label>
                                                          <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                          <span class="material-input"></span>
                                                      <span class="material-input"></span></div>
                                                  </div>
                                                  <div class="col-sm-4">
                                                      <div class="form-group is-empty">
                                                          <label class="control-label">Ciudad</label>
                                                          <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                          <span class="material-input"></span>
                                                      <span class="material-input"></span></div>
                                                  </div>
                                                  <div class="col-sm-4">
                                                      <div class="form-group is-empty">
                                                          <label class="control-label">Estado</label>
                                                          <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                          <span class="material-input"></span>
                                                      <span class="material-input"></span></div>
                                                  </div>
                                              </div>

                                              <div class="col-sm-12">
                                                  <div class="col-sm-6">
                                                      <div class="form-group is-empty">
                                                          <label class="control-label">Colonia</label>
                                                          <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                          <span class="material-input"></span>
                                                      <span class="material-input"></span></div>
                                                  </div>
                                                  <div class="col-sm-6">
                                                      <div class="form-group is-empty">
                                                          <label class="control-label">Delegación</label>
                                                          <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                          <span class="material-input"></span>
                                                      <span class="material-input"></span></div>
                                                  </div>
                                              </div>

                                              <div class="col-sm-12">
                                                  <div class="col-sm-4">
                                                      <div class="form-group is-empty">
                                                          <label class="control-label">No. Exterior</label>
                                                          <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                          <span class="material-input"></span>
                                                      <span class="material-input"></span></div>
                                                  </div>
                                                  <div class="col-sm-4">
                                                      <div class="form-group is-empty">
                                                          <label class="control-label">No. Interior</label>
                                                          <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                          <span class="material-input"></span>
                                                      <span class="material-input"></span></div>
                                                  </div>
                                              </div>

                                        </div>
                                    </div>





                                </div>
                            </div>

                            <div class="tab-pane" id="clasificacion-agencia">
                                <div class="col-sm-12">
                                    <div class="card-content">
                                        <h4>Clasificación</h4>







                                        <div class="row">
                                              <div class="col-sm-12">
                                                  <div class="col-sm-3">
                                                      <div class="form-group is-empty">
                                                          <label class="control-label">Certificado</label>
                                                          <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                          <span class="material-input"></span>
                                                      <span class="material-input"></span></div>
                                                  </div>
                                                  <div class="col-sm-3">
                                                      <div class="form-group is-empty">
                                                          <label class="control-label">Ranking</label>
                                                          <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                          <span class="material-input"></span>
                                                      <span class="material-input"></span></div>
                                                  </div>
                                                  <div class="col-sm-3">
                                                      <div class="form-group is-empty">
                                                          <label class="control-label">Estatus</label>
                                                          <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                          <span class="material-input"></span>
                                                      <span class="material-input"></span></div>
                                                  </div>
                                                  <div class="col-sm-3">
                                                      <div class="form-group is-empty">
                                                          <label class="control-label">Tipo de servicios</label>
                                                          <input class="form-control" name="nombre" value="Grupo Bimbo" type="text">
                                                          <span class="material-input"></span>
                                                      <span class="material-input"></span></div>
                                                  </div>
                                              </div>

                                              <div class="col-sm-12">
                                                  <b>Servicios:</b><br>
                                                  <span class="label categorias-agencia">Gaming</span>
                                                  <span class="label categorias-agencia">AD (or display) Retargeting</span>
                                                  <span class="label categorias-agencia">Analytics Insight and Tagging</span>
                                                  <span class="label categorias-agencia">Promotions</span>
                                                  <span class="label categorias-agencia">Mobile marketing</span>
                                                  <span class="label categorias-agencia">Digital media planning and buying</span>
                                                  <span class="label categorias-agencia">Website Development</span>
                                                  <span class="label categorias-agencia">Digital Strategy</span>
                                                  <span class="label categorias-agencia">Video</span>
                                                  <span class="label categorias-agencia">Social media marketing</span>
                                                  <span class="label categorias-agencia">Email Marketing</span>
                                                  <span class="label categorias-agencia">Blogging</span>
                                                  <span class="label categorias-agencia">Visual Design</span>
                                                  <span class="label categorias-agencia">Social media managing</span>
                                                  <span class="label categorias-agencia">Search Marketing</span>
                                                  <span class="label categorias-agencia">CopyWritting</span>
                                              </div>

                                        </div>










                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>



                </div>
            </div>

            <footer class="footer contenedor-footer">
                <?php include('footer.php');?>
            </footer>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<script src="<?= base_url() ?>Theme/Cliente/assets/js/main.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        md.initSliders()
        demo.initFormExtendedDatetimepickers();
    });
</script>
</html>
