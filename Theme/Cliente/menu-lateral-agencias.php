<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
    <div class="panel-body contenedor-filtros-desplegable">

      <div class="card-content">
          <div class="menu-filtros">
              <ul class="nav nav-pills nav-pills-warning">
                  <li class="active"><a href="#pill1" data-toggle="tab" aria-expanded="true">Filtros Generales</a></li>
              </ul>
          </div>
          <div class="tab-content">
              <div class="tab-pane active" id="pill1">

                  <div class="col-sm-12">
                      <div class="col-sm-4 border-lateral">
                          <div class="col-sm-12">
                              <div class="select-agencias">
                                  <select class="selectpicker" name="filtros[categorias_agencias][]" data-style="select-with-transition" multiple title="Categoría" data-size="7">
                                    <option disabled>Categoría</option>
                                    <?php foreach($this->db->get_where('categorias_agencias')->result() as $t): ?>
                                          <option value="<?= $t->id ?>"><?= $t->nombre ?></option>
                                    <?php endforeach ?>
                                  </select>
                              </div>
                          </div>
                          <div class="col-sm-12">
                              <div class="select-agencias">
                                  <select class="selectpicker" name="filtros[paises][]" data-style="select-with-transition" multiple title="País" data-size="7">
                                    <option disabled>País</option>
                                    <?php 
                                      $this->db->order_by('nombre','ASC');
                                      foreach($this->db->get_where('paises')->result() as $t): ?>
                                          <option value="<?= $t->id ?>" <?= @$_GET['pais']==$t->id?'selected':'' ?>><?= $t->nombre ?></option>
                                    <?php endforeach ?>
                                  </select>
                              </div>
                              <div class="select-agencias">
                                  <select class="selectpicker" name="filtros[cobertura][]" data-style="select-with-transition" multiple title="Cobertura" data-size="7">
                                    <option disabled>Cobertura</option>
                                    <?php 
                                      $this->db->order_by('nombre','ASC');
                                      foreach($this->db->get_where('paises')->result() as $t): ?>
                                          <option value="<?= $t->id ?>" <?= @$_GET['cobertura']==$t->id?'selected':'' ?>><?= $t->nombre ?></option>
                                    <?php endforeach ?>
                                  </select>
                              </div>
                              
                          </div>                          
                      </div>

                      <div class="col-sm-4">
                          <div class="col-sm-12">
                              <div class="select-agencias">
                                  <select name="filtros[tipos_certificacion][]" class="selectpicker" data-style="select-with-transition" multiple title="Validación" data-size="7">
                                    <option disabled>Validación</option>                                    
                                    <option value="6">Validada</option>
                                    <option value="7">No validada</option>
                                    <option value="1">En trámite</option>
                                  </select>
                              </div>
                          </div>
                          <div class="col-sm-12">
                              <div class="select-agencias">
                                  <select class="selectpicker" name="filtros[estatus_agencias_id][]" data-style="select-with-transition" multiple title="Status" data-size="7">
                                    <option disabled>Status</option>
                                    <?php foreach($this->db->get_where('estatus_agencias')->result() as $t): ?>
                                          <option value="<?= $t->id ?>" <?= @$_GET['estatus_agencias_id']==$t->id?'selected':'' ?>><?= $t->nombre ?></option>
                                    <?php endforeach ?>
                                  </select>
                              </div>
                          </div>
                          <div class="col-sm-12">
                              <div class="select-agencias">
                                  <select class="selectpicker" name="filtros[super_poder][]" data-style="select-with-transition" multiple title="Super Poder" data-size="7">
                                    <option disabled>Super Poder</option>
                                    <?php foreach($this->db->get_where('super_poderes')->result() as $t): ?>
                                          <option value="<?= $t->nombre ?>" <?= @$_GET['super_poder']==$t->id?'selected':'' ?>><?= $t->nombre ?></option>
                                    <?php endforeach ?>
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-sm-4 border-lateral">
                        <div class="col-sm-12">
                            <div class="select-agencias">
                                <select name="filtros[servicios][]" class="selectpicker" data-style="select-with-transition" multiple title="Servicios" data-size="7">
                                  <option disabled>Servicios</option>                                  
                                  <?php $this->db->order_by('nombre','ASC'); foreach($this->db->get_where('servicios')->result() as $t): ?>
                                    <?php
                                        $selected = '';                                        
                                        if(!empty(get_instance()->filtros)){
                                          $this->db->join('agencias_servicios','agencias_servicios.servicios_id = servicios.id');
                                          $selected = $this->db->get_where('servicios',array('servicios.id'=>$t->id,'agencias_id'=>get_instance()->filtros->id))->num_rows()>0?'selected':'';
                                        }
                                        if(!empty($_POST['servicio'])){                                          
                                          $selected = $t->id==$_POST['servicio']?'selected':'';
                                        }
                                    ?>
                                          <option value="<?= $t->id ?>" <?= $selected ?>><?= $t->nombre ?></option>
                                  <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="select-agencias">
                                <select name="filtros[agencias_grupos][]" class="selectpicker" data-style="select-with-transition" multiple title="Grupo" data-size="7">

                                  <option disabled>Grupo</option>
                                  <?php foreach($this->db->get_where('agencias_grupos')->result() as $t): ?>
                                          <?php
                                              $selected = '';
                                          ?>
                                          <option value="<?= $t->id ?>" <?= $selected ?>><?= $t->nombre ?></option>
                                  <?php endforeach ?>
                                </select>
                            </div>
                            <div class="select-agencias">
                                  <select class="selectpicker" name="filtros[organizacion][]" data-style="select-with-transition" multiple title="Organizaciones" data-size="7">
                                    <option disabled>Organizaciones</option>
                                    <?php foreach($this->db->get_where('organizaciones')->result() as $t): ?>
                                          <option value="<?= $t->id ?>" <?= @$_GET['organizacion']==$t->id?'selected':'' ?>><?= $t->nombre ?></option>
                                    <?php endforeach ?>
                                  </select>
                              </div>
                        </div>
                          
                      </div>

                      

                      <div class="col-sm-12">
                        <div class="iconos-favoritos-agencias">
                            <button type="button" class="btn btn-primary btn-round cleanFilters" id="btn-borrar-filtros"><i class="material-icons">cached</i> Borrar filtros</button>
                        </div>
                      </div>

                  </div>

              </div>


          </div>



      </div>  

    </div>
</div>

<?php if(!empty($_POST['marca'])): ?>
<input type="hidden" name="filtros[marca][]" value="<?= $_POST['marca'] ?>">
<?php endif ?>