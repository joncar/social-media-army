<!doctype html>
<html lang="en">
    <head>
        <!-- Librerias -->
        <?php include('head.php'); ?>
    </head>

    <body>
        <div class="wrapper switch-trigger">
            <!-- Menu Lateral -->
            <div class="sidebar" id="fondo-menu-lateral">
                <?php include('menu-lateral.php'); ?>
            </div>

            <div class="main-panel">
                <!-- Menu Top -->
                <?php include('menu-top.php'); ?>

                <!-- Contenido -->
                <div class="content">
                    <div class="container-fluid">




                        <!-- Inicia Contenido -->
                        <div class="row">
                            <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Ajustes</div></div>

                            <div class="card-content">
                                <div class="menu-filtros">
                                    <ul class="nav nav-pills nav-pills-warning">
                                        <li class="active"><a href="#ajustes1" data-toggle="tab" aria-expanded="true"><?= l('Datos de la empresa') ?></a></li>
                                        <li><a href="#ajustes2" data-toggle="tab" aria-expanded="false"><?= l('Perfil de Usuario') ?></a></li>
                                        <li><a href="#ajustes3" data-toggle="tab" aria-expanded="false"><?= l('Alta de Usuarios') ?></a></li>
                                        <li><a href="#ajustes4" data-toggle="tab" aria-expanded="false"><?= l('Alta de Agencias') ?></a></li>
                                        <li><a href="#ajustes5" data-toggle="tab" aria-expanded="false"><?= l('Productos') ?></a></li>
                                        <li><a href="#ajustes6" data-toggle="tab" aria-expanded="false"><?= l('Marcas') ?></a></li>                                        
                                    </ul>
                                </div>

                                <div class="tab-content contenedor-facturas">

                                    <div class="tab-pane active" id="ajustes1">
                                        <div id='messageEditEmpresa'></div>

                                        <form onsubmit="return uploadFile(this)">
                                            <div class="col-sm-12 padding0 contenedor-subir-logo-ajustes">
                                                <div class="col-sm-4">
                                                    <div class="col-sm-3 padding0">
                                                        <label class="text-center">
                                                            <!--<b>Sube el logo de tu empresa en .png con fondo transparente.</b>-->
                                                            <img src="<?= $this->user->logo ?>" alt="Perfil Dashboard" class="img-responsive center-block" style="margin-top:20px;">
                                                            <input style="display: none" class="fileImage" type="file" class="form-control" name="s96d6f2e7" placeholder="Examinar" autocomplete="cc-number" required autofocus/>
                                                        </label>
                                                    </div>

                                                    <div class="col-sm-8 instrucciones-subir-logo">
                                                        <b><?= l('Instrucciones') ?></b><br>
                                                        <?= l('instrucciones2') ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <div class="col-xs-12 col-sm-12 padding0">
                                            <form method="#" action="#" onsubmit="return saveEmpresa(this)">
                                                <!-- CREDIT CARD FORM STARTS HERE -->
                                                <div class="col-xs-12 col-sm-12 margen-registro-movil margen-tabs">
                                                    <b><?= l('Datos de la Empresa') ?></b>
                                                    <div class="card-content">

                                                        <div class="col-sm-4">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label"><?= l('Nombre Comercial') ?></label>
                                                                <input class="form-control" type="text" name="nombre" value="<?= $empresa->nombre ?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label"><?= l('Razón Social') ?></label>
                                                                <input class="form-control" type="text" name="razon_social" value="<?= $empresa->razon_social ?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label"><?= l('RFC') ?></label>
                                                                <input class="form-control" type="text" name="rfc" value="<?= $empresa->rfc ?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- CREDIT CARD FORM ENDS HERE -->

                                                <div class="col-xs-12 col-sm-12">
                                                    <b><?= l('Direccion') ?></b>
                                                    <div class="card-content">
                                                            <div class="col-sm-12">
                                                                <div class="form-group is-empty">
                                                                    <label class="control-label"><?= l('Calle') ?></label>
                                                                    <input class="form-control" type="text" name="calle" value="<?= $empresa->calle ?>">
                                                                    <span class="material-input"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4">
                                                                    <div class="form-group is-empty">
                                                                        <label class="control-label"><?= l('Número exterior') ?></label>
                                                                        <input class="form-control" type="text" name="numero_exterior" value="<?= $empresa->numero_exterior ?>">
                                                                        <span class="material-input"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group is-empty">
                                                                        <label class="control-label"><?= l('Número interior') ?></label>
                                                                        <input class="form-control" type="text" name="numero_interior" value="<?= $empresa->numero_interior ?>">
                                                                        <span class="material-input"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group is-empty">
                                                                        <label class="control-label"><?= l('Colonia') ?></label>
                                                                        <input class="form-control" type="text" name="colonia" value="<?= $empresa->colonia ?>">
                                                                        <span class="material-input"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4">
                                                                    <div class="form-group is-empty">
                                                                        <label class="control-label"><?= l('Delegación') ?></label>
                                                                        <input class="form-control" type="text" name="delegacion" value="<?= $empresa->delegacion ?>">
                                                                        <span class="material-input"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group is-empty">
                                                                        <label class="control-label"><?= l('Ciudad') ?></label>
                                                                        <input class="form-control" type="text" name="ciudad" value="<?= $empresa->ciudad ?>">
                                                                        <span class="material-input"></span>
                                                                    </div>
                                                                </div>
                                                                 <div class="col-sm-4">
                                                                    <div class="form-group is-empty">
                                                                        <label class="control-label"><?= l('Estado2') ?></label>
                                                                        <input class="form-control" type="text" name="estado" value="<?= $empresa->estado ?>">
                                                                        <span class="material-input"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group is-empty">
                                                                        <label class="control-label"><?= l('Pais') ?></label>
                                                                        <?= form_dropdown_from_query('paises_id', 'paises', 'id', 'nombre', $empresa->paises_id) ?>
                                                                        <span class="material-input"></span>
                                                                    </div>
                                                                </div>


                                                                <div class="col-sm-6">
                                                                    <div class="form-group is-empty">
                                                                        <label class="control-label"><?= l('CP') ?></label>
                                                                        <input class="form-control" type="text"  name="cp" value="<?= $empresa->cp ?>">
                                                                        <span class="material-input"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name='logo' value="<?= $empresa->logo ?>">
                                                            <button type="submit" class="btn btn-fill" id="btn-empresa-edit" style="width:auto;">
                                                                <?= l('Guardar información') ?>
                                                            </button>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>


                                    <div class="tab-pane" id="ajustes2">
                                        <div id='messageEditperfil'></div>
                                        <div class="col-xs-12 col-sm-12 padding0">

                                          <form onsubmit="return uploadFile2(this)">
                                              <div class="col-sm-12 padding0 contenedor-subir-logo-ajustes">
                                                  <div class="col-sm-4">
                                                      <div class="col-sm-3 padding0">
                                                          <label class="text-center">
                                                              <!--<b>Sube el logo de tu empresa en .png con fondo transparente.</b>-->
                                                              <img src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" alt="Perfil Dashboard" class="img-responsive center-block img-circle" style="margin-top:10px;">
                                                              <input style="display: none" class="fileImage" type="file" class="form-control" name="s66c9eed1" placeholder="Examinar" autocomplete="cc-number" required autofocus/>
                                                          </label>
                                                      </div>


                                                      <!--Back
                                                      <div class="col-sm-5">
                                                          <label class="fondo-proyecto-dashboard text-center">
                                                              <img src="<?= base_url('img/fotos/'.$this->user->foto) ?>" alt="Perfil Dashboard" class="img-responsive center-block">
                                                              <input style="display: none" class="fileImage" type="file" class="form-control" name="s66c9eed1" placeholder="Examinar" autocomplete="cc-number" required autofocus/>
                                                          </label>
                                                      </div>-->

                                                      <div class="col-sm-8 instrucciones-subir-logo">
                                                          <b><?= l('Instrucciones') ?></b><br>
                                                          <?= l('instrucciones2') ?>
                                                      </div>
                                                  </div>
                                              </div>
                                          </form>

                                          <form method="#" action="#" onsubmit="return savePerfil(this)">
                                                <!-- CREDIT CARD FORM STARTS HERE -->
                                                <div class="col-xs-12 col-sm-12 margen-registro-movil margen-tabs">
                                                    <b><?= l('Datos de Usuario') ?></b>
                                                    <div class="card-content">

                                                        <div class="col-sm-6">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label"><?= l('nombres') ?></label>
                                                                <input class="form-control" type="text" name="nombre" value="<?= $this->user->nombre ?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label"><?= l('Apellidos') ?></label>
                                                                <input class="form-control" type="text" name="apellido" value="<?= $this->user->apellido ?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label"><?= l('Correo') ?></label>
                                                                <input class="form-control" type="email" name="email" value="<?= $this->user->email ?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label"><?= l('Contraseña') ?></label>
                                                                <input class="form-control" type="password" name="password" value="<?= $this->db->get_where('user',array('id'=>$this->user->id))->row()->password ?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>


                                                        <!-- Agregar Funciones -->
                                                        <div class="col-sm-4">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label"><?= l('Puesto') ?></label>
                                                                <input class="form-control" type="text" name="puesto" value="<?= $this->user->puesto ?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label"><?= l('Área') ?></label>
                                                                <input class="form-control" type="text" name="area" value="<?= $this->user->area ?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group is-empty">
                                                                <label class="control-label"><?= l('Tipo de Usuario') ?></label>
                                                                <input class="form-control" type="text" name="tipo_usuario" disabled value="<?= $this->user->tipo_usuario ?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                                <!-- CREDIT CARD FORM ENDS HERE -->

                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="card-content">
                                                            <input type="hidden" name='foto' value="<?= $this->user->foto ?>">
                                                            <button type="submit" class="btn btn-fill" id="perfil-edit" style="width:auto;">
                                                                <?= l('Guardar información') ?>
                                                            </button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>


                                    <div class="tab-pane" id="ajustes3">
                                        <div class="col-sm-12">
                                            <div class="card-content">
                                                <?= $cruds[0]->output ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="ajustes4">
                                        <?= $cruds[2]->output ?>
                                    </div>

                                    <div class="tab-pane" id="ajustes5">
                                        <div class="col-sm-12">
                                            <div class="card-content">
                                                <?= $cruds[1]->output ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="ajustes6">
                                        <div class="col-sm-12">
                                            <div class="card-content">
                                                <?= $cruds[3]->output ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="ajustes7"></div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <!-- Termina Contenido -->

        </div>

        <!--
        <footer class="footer contenedor-footer">
            <?php include('footer.php'); ?>
        </footer> -->

<script>
    function savePerfil(form) {

        $("#perfil-edit").attr('disabled', true);
        $("#perfil-edit").html('<?= l('Guardando información') ?>');
        $("#messageEditperfil").html('').removeClass('alert-success alert alert-danger');
        var datos = new FormData(form);
        remoteConnection('seguridad/perfil/update_validation/<?= $this->user->id ?>', datos, function (data) {
            data = data.replace('<textarea>', '');
            data = data.replace('</textarea>', '');
            data = JSON.parse(data);
            if (data.success) {
                remoteConnection('seguridad/perfil/update/<?= $this->user->id ?>', datos, function (data) {
                    data = data.replace('<textarea>', '');
                    data = data.replace('</textarea>', '');
                    data = JSON.parse(data);
                    $("#perfil-edit").attr('disabled', false);
                    $("#perfil-edit").html('<?= l('Guardar información') ?>');
                    $("#messageEditperfil").html('<?= l('La información ha sido actualizada con éxito') ?>').removeClass('alert-danger').addClass('alert alert-success');
                });
            } else {
                $("#messageEditperfil").html(data.error_message).removeClass('alert-success').addClass('alert alert-danger');
                $("#perfil-edit").attr('disabled', false);
                $("#perfil-edit").html('<?= l('Guardar información') ?>');
            }
        });
        return false;
    }
    function saveEmpresa(form) {
        $("#empresa-edit").attr('disabled', true);
        $("#empresa-edit").html('<?= l('Guardando información') ?>');
        $("#messageEditEmpresa").html('').removeClass('alert-success alert alert-danger');
        var datos = new FormData(form);
        remoteConnection('cliente/empresas/update_validation/<?= $this->user->empresa ?>', datos, function (data) {
            data = data.replace('<textarea>', '');
            data = data.replace('</textarea>', '');
            data = JSON.parse(data);
            if (data.success) {
                remoteConnection('cliente/empresas/update/<?= $this->user->empresa ?>', datos, function (data) {
                    data = data.replace('<textarea>', '');
                    data = data.replace('</textarea>', '');
                    data = JSON.parse(data);
                    $("#empresa-edit").attr('disabled', false);
                    $("#empresa-edit").html('<?= l('Guardar información') ?>');
                    $("#messageEditEmpresa").html('<?= l('La información ha sido actualizada con éxito') ?>').removeClass('alert-danger').addClass('alert alert-success');
                });
            } else {
                $("#messageEditEmpresa").html(data.error_message).removeClass('alert-success').addClass('alert alert-danger');
                $("#empresa-edit").attr('disabled', false);
                $("#empresa-edit").html('Guardar información');
            }
        });
        return false;
    }
    $(document).on('change','input[name="s66c9eed1"]',function(){
        var datos = new FormData($("#ajustes2").find('form')[0]);
        remoteConnection('seguridad/perfil/upload_file/foto', datos, function (data) {
            data = JSON.parse(data);
            if (data.success) {
                $("input[name='foto']").val(data.files[0].name);
                $("input[name='foto']").parents('form').submit();
            } else {
                alert(data.message);
            }
        });
        return false;
    });
    $(document).on('change','input[name="s96d6f2e7"]',function(){
        //$(this).parents('form').submit();
    });
    function uploadFile(form) {
        var datos = new FormData(form);
        remoteConnection('cliente/empresas/upload_file/logo', datos, function (data) {
            data = JSON.parse(data);
            if (data.success) {
                $("input[name='logo']").val(data.files[0].name);
                $("input[name='logo']").parents('form').submit();
            } else {
                alert(data.message);
            }
        });
        return false;
    }

    function uploadFile2(form) {
        var datos = new FormData(form);
        remoteConnection('seguridad/perfil/upload_file/foto', datos, function (data) {
            data = JSON.parse(data);
            if (data.success) {
                $("input[name='foto']").val(data.files[0].name);
                $("input[name='foto']").parents('form').submit();
            } else {
                alert(data.message);
            }
        });
        return false;
    }
</script>


<?php include('modales.php'); ?>
<!-- Librerias -->
<?php include('librerias.php'); ?>
<!-- Agregar Campos Modal Agregar Proyecto -->
</body></html>
