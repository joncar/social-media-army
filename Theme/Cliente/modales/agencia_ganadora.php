<!-- Modal Agencia ganadora-->
<div class="modal fade" id="agencia-ganadora" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <form action onsubmit="return elegir(this)">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 350px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 273px;" tabindex="-1">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          </div>
        	<div class="response"></div>
            <div style="text-align: center">
              <img src="<?= $this->user->logo ?>" alt="Logo Bimbo" class="img-responsive" style="display:inline-block;">
            </div>
            <div class="titulo-modal">              
              <?= l('¿Asignar Proyecto <span class="proyectoNombre">Nombre del Proyecto</span> a <span class="agenciaNombre">Nombre de la Agencia</span>?') ?>
            </div>
            <input type="hidden" name="brief_id" value="0">
            <input type="hidden" name="status" value="4">

            <input type="hidden" name="fecha_modificacion" value="<?= date("d/m/Y H:i:s") ?>">

            <div class="row">
                <div class="col-sm-12" style="text-align: center;">
                    <a href="javascript:closeModal('#agencia-ganadora')" class="swal2-cancel btn btn-sm btn-danger" style="display: inline-block; "><?= l('Cancelar') ?></a> 
                    <button class="swal2-confirm btn btn-success btn-sm" type="submit" style="display: inline-block;"><?= l('Aceptar') ?></button>
                </div>
            </div>          
        </form>
        </div>
    </div>
</div>
<!-- Termina Modal agencia ganadora -->
<script>

	function mostrarAgenciaGanadora(id,agencia,proyecto){
		$('#agencia-ganadora input[name="brief_id"]').val(id);
		$('#agencia-ganadora .agenciaNombre').html(agencia);
		$('#agencia-ganadora .proyectoNombre').html(proyecto);
		$("#agencia-ganadora").modal('show');
	}
	function elegir(f){
		var form = new FormData(f);
        remoteConnection('cliente/proyecto/briefs/update/'+$('#agencia-ganadora input[name="brief_id"]').val(),form,function(data){
            $('#cancelar-propuesta').find('.response').addClass('alert alert-success').html('<?= l('Brief Rechazado con éxito') ?>');
            document.location.href="<?= base_url('cliente/proyecto/verProyecto/'.$proyecto->id) ?>";
        });
        return false;
	}


</script>
