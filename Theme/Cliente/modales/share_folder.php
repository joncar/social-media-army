<!-- Modal Ganador Seleccionado-->
<div class="modal fade" id="share_folder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          	</div>
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">           
			<?php foreach($proyecto->participantes->result() as $b): ?>
				<div class="folders" id="componentFolder<?= $b->brief_id ?>" style="display: none">
		            <div class="titulo-modal">
		              Agencia <?= $b->nombre ?>
		            </div>
		            <div>
		            	<table class="table">
			            	<?php foreach(explode(',',$b->ficheros) as $f): ?>
			            		<tr>
					            	<td style="text-align:left; border:0">
					            		<a title="Descargar" href="<?= base_url().'proyectosFiles/'.$f ?>" target="_blank"><img src="<?= base_url() ?>img/documentos.jpeg" alt="" style="width:30px"><?= explode('-',$f,2)[1] ?></a>
					            	</td>
				            	</tr>
			            	<?php endforeach ?>
		            	</table>
		            </div>
	            </div>
			<?php endforeach ?>

        </div>
    </div>
</div>
<!-- Termina Modal Ganador Seleccionado -->

<script>
	window.activeFolderBrief = 0;
	function shareFolder(brief){
		window.activeFolderBrief = brief;
		$('.folders').hide();
		$("#componentFolder"+brief).show();
		$("#share_folder").modal('toggle');
	}

	window.afterUploadFile = function(file,response){
		$.post('<?= base_url() ?>proyectos/admin/briefs/'+window.activeFolderBrief+'/addFile',{fichero:$('#field-folder'+window.activeFolderBrief).val(),'file':file.remoteName},function(data){});
	}
	window.afterRemoveFile = function(file,response){
		$.post('<?= base_url() ?>proyectos/admin/briefs/'+window.activeFolderBrief+'/removeFile',{fichero:$('#field-folder'+window.activeFolderBrief).val(),'file':file.remoteName},function(data){});
	}
</script>