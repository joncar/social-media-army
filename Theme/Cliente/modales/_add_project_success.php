<div class="col-sm-12 mensaje-felicidades">
    <?= l('<b>¡Felicidades!<br>Haz creado el siguiente proyecto</b>') ?>
</div>
 <div class="row">
    <div class="card-content">
        <div class="menu-filtros col-sm-12">
            <ul class="nav nav-pills nav-pills-warning btn-registro-modal">
                <li class="active"><a href="#pill1" data-toggle="tab" aria-expanded="true"><?= l('Datos') ?></a></li>
                <li><a href="#pill2" data-toggle="tab" aria-expanded="false"><?= l('Especificaciones') ?></a></li>
                <li><a href="#pill3" data-toggle="tab" aria-expanded="false"><?= l('Fechas') ?></a></li>
                <li><a href="#pill4" data-toggle="tab" aria-expanded="false"><?= l('Filtros') ?></a></li>
                <li><a href="#pill5" data-toggle="tab" aria-expanded="false"><?= l('Documentos') ?></a></li>
            </ul>
        </div>
        <div class="tab-content contenedor-facturas">

            <div class="tab-pane active" id="pill1">

            <div class="col-sm-12">
                <div class="card-content">
                  <div class="material-datatables">
                      <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                          <div class="row">
                          	<div class="col-sm-12">
                              <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">
                                <tbody class="text-left">
                              	<tr class="odd" role="row">
                                      <td tabindex="0" class="sorting_1"><?= l('Nombre del proyecto') ?>:</td>
                                      <td class="texto-tabla-registro"><?= $proyecto->nombre ?></td>
                                  </tr>
                                  <tr class="even" role="row">
                                      <td tabindex="0" class="sorting_1"><?= l('Supervisor') ?></td>
                                      <td class="texto-tabla-registro"><?= $proyecto->supervisores ?></td>
                                  </tr>
                                  <tr class="odd" role="row">
                                      <td tabindex="0" class="sorting_1"><?= l('Colaboradores') ?></td>
                                      <td class="texto-tabla-registro"><?= $proyecto->colaboradores ?></td>
                                  </tr>
                                  <tr class="even" role="row">
                                      <td tabindex="0" class="sorting_1"><?= l('Pais') ?></td>
                                      <td class="texto-tabla-registro"><?= $proyecto->paises ?></td>
                                   </tr>
                                  <tr class="odd" role="row">
                                      <td class="sorting_1" tabindex="0"><?= l('Categoria') ?></td>
                                      <td class="texto-tabla-registro"><?= $proyecto->categorias ?></td>
                                  </tr>
                                  <tr class="even" role="row">
                                      <td tabindex="0" class="sorting_1"><?= l('Marca') ?></td>
                                      <td class="texto-tabla-registro"><?= $proyecto->marcas ?></td>
                                  </tr>

                                  <tr class="even" role="row">
                                      <td tabindex="0" class="sorting_1"><?= l('Producto') ?></td>
                                      <td class="texto-tabla-registro"><?= $proyecto->productos ?></td>
                                  </tr>
                                  </tbody>
                              </table>
                          </div>
                       </div>
                    </div>
                </div>
              </div>
            </div>
            </div>

            <!-- Opcion 2 -->
            <div class="tab-pane" id="pill2">

              <div class="col-sm-12">
                  <div class="card-content">
                    <div class="material-datatables">
                        <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                            <div class="row">
                              <div class="col-sm-12">
                                <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">
                                  <tbody class="text-left">
                                  <tr class="odd" role="row">
                                        <td tabindex="0" class="sorting_1"><?= l('Solicitud puntual') ?>:</td>
                                        <td class="texto-tabla-registro"><?= $proyecto->solicitud_puntual ?></td>
                                    </tr>
                                    <tr class="even" role="row">
                                        <td tabindex="0" class="sorting_1"><?= l('Objetivo del proyecto') ?>:</td>
                                        <td class="texto-tabla-registro"><?= $proyecto->objetivo ?></td>
                                    </tr>
                                    <tr class="odd" role="row">
                                        <td tabindex="0" class="sorting_1"><?= l('Target') ?>:</td>
                                        <td class="texto-tabla-registro"><?= $proyecto->target ?></td>
                                    </tr>
                                    <tr class="even" role="row">
                                        <td tabindex="0" class="sorting_1"><?= l('Especificaciones') ?>:</td>
                                        <td class="texto-tabla-registro"><?= $proyecto->especificaciones ?></td>
                                     </tr>
                                    </tbody>
                                </table>
                            </div>
                         </div>
                      </div>
                  </div>
                </div>
              </div>

            </div>
            <!-- Opcion 2 -->


            <!-- Opcion 3 -->
            <div class="tab-pane" id="pill3">

              <div class="col-sm-12">
                  <div class="card-content">
                    <div class="material-datatables">
                        <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                          <div class="row">
                            <div class="col-sm-12">
                              <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">
                                <tbody class="text-left">
                                  <?php foreach($proyecto->fechas->result() as $f): ?>
                                  	<tr class="odd" role="row">
                                        <td tabindex="0" class="sorting_1" style="width:30%"><?= $f->nombre ?>:</td>
                                        <td class="texto-tabla-registro"><?= date("d/m/Y",strtotime($f->desde)); ?></td>
                                        <td class="texto-tabla-registro"><?= date("d/m/Y",strtotime($f->hasta)); ?></td>
                                    </tr>
                                  <?php endforeach ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
              </div>

            </div>
            <!-- Opcion 3 -->



            <!-- Opcion 4 -->
            <div class="tab-pane" id="pill4">

              <div class="col-sm-12">
                  <div class="card-content">
                    <div class="material-datatables">
                        <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                            <div class="row">
                              <div class="col-sm-12">                                
                                <?php foreach(explode(',',$proyecto->filtros) as $f): ?>
                                  <span class="label categorias-agencia" style="text-transform: lowercase;"><?= $f ?></span>
                                <?php endforeach ?>
                            </div>
                         </div>
                      </div>
                  </div>
                </div>
              </div>

            </div>
            <!-- Opcion 4 -->

            <!-- Opcion 5 -->
            <div class="tab-pane" id="pill5">

              <div class="col-sm-12">
                  <div class="card-content">
                    <div class="material-datatables">
                        <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
                            <div class="row">
                              <div class="col-sm-12">
                                <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%;" cellspacing="0" width="100%">
                                  <tbody class="text-left">
                                    <tr class="even" role="row">
                                        <td tabindex="0" class="sorting_1"><?= l('Documentos') ?></td>
                                     </tr>
                                      <?php

                                        foreach($proyecto->documentos->result() as $d):
                                        $name = count(explode('-',$d->documento,2))==2?explode('-',$d->documento,2)[1]:explode('-',$d->documento,2)[0];
                                      ?>
                                    <tr class="even" role="row">
                                         <td class="texto-tabla-registro">
                                           <a target="_new" href="<?= base_url('img/proyectos_uploads/files/'.$d->documento) ?>"><?= $name ?></a>
                                         </td>
                                      </tr>
                                      <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                         </div>
                      </div>
                  </div>
                </div>
              </div>

            </div>



            <div class="col-sm-12 padding2020">
                <h3 class="text-uppercase"><b><?= l('Resumen de fechas') ?></b></h3>
            </div>

            <!--Gant -->
            <div class="col-sm-12 full-img" id="chart_div">

            </div>

            <div class="col-sm-12 invitacion-modal-registro text-center">
                <div class="col-sm-12"><?= l('Ahora selecciona las agencias que deseas<br>invitar a participar de este pitch') ?></div>
                <div class="col-sm-12">
                    <a href="<?= base_url('cliente/agencia/agencias/brief/'.$proyecto->id) ?>" class="btn btn-success btn-lg margen-btn-agregar">
                        <?= l('Invitar Agencias al Proyecto') ?> <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <!-- Opcion 5 -->
        </div>
    </div>
</div>
<script>
  var data = <?php
    $data = array();
    foreach($proyecto->fechas->result() as $f){
      $f->hasta = $f->hasta==$f->desde?date("Y-m-d",strtotime('+1 day'.$f->hasta)):$f->hasta;
      $data[] = array(
        $f->nombre,
        $f->nombre,
        $f->nombre,
        date("Y-m-d",strtotime($f->desde)),
        date("Y-m-d",strtotime($f->hasta)),
        null,
        100,
        null
      );
    }
    echo json_encode($data);
  ?>;
  google.charts.setOnLoadCallback(function(){drawChart(data,'chart_div')});
</script>
