<!-- Modal Ganador Seleccionado-->
<div class="modal fade" id="shareFolderAgency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          	</div>
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">           
			<div class="folders" id="componentFolderAgency" style="display: none">
	            <div class="titulo-modal">
	              Agencia <?= $brief->nombre ?>
	            </div>
	            <?php $this->load->view('predesign/dropzone',[
	            	'handle'=>'proyectos/admin/briefs',
	            	'name'=>'folderAgency'.$brief->id,
	            	'inpName'=>'ficheros',
	            	'path'=>'proyectosFiles/',
		  			'files'=>array_filter(explode(',',$brief->ficheros)),
		  			'unset_delete'=>true
	            ]) 
            	?>
            </div>

        </div>
    </div>
</div>
<!-- Termina Modal Ganador Seleccionado -->

<script>
	window.activeFolderBrief = 0;
	function shareFolderAgency(){
		window.activeFolderBrief = '<?= $brief->id ?>';
		$('.folders').hide();
		$("#componentFolderAgency").show();
		$("#shareFolderAgency").modal('toggle');
	}

	window.afterUploadFile = function(file,response){
		$.post('<?= base_url() ?>proyectos/admin/briefs/'+window.activeFolderBrief+'/addFile',{fichero:$('#field-folderAgency'+window.activeFolderBrief).val(),'file':file.remoteName},function(data){});
	}
	window.afterRemoveFile = function(file,response){
		$.post('<?= base_url() ?>proyectos/admin/briefs/'+window.activeFolderBrief+'/removeFile',{fichero:$('#field-folderAgency'+window.activeFolderBrief).val(),'file':file.remoteName},function(data){});
	}
</script>