<?php
  $this->db->group_by('id');
  $proyectos = $this->db->get_where('view_proyectos',array('getEmpresaOfProject(view_proyectos.id)'=>$this->user->empresa,'porcentaje <'=>100));
?>
<div class="modal fade" id="enviar-brief" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
              </div>
                <img src="<?= $this->user->logo ?>" alt="Logo Bimbo" class="img-responsive center-block">
                

                <?php if($proyectos->num_rows()>0): ?>
                <div class="titulo-modal">
                  <b><?= l('Enviar invitación') ?></b><br>
                  <?= l('Selecciona el Proyecto que deseas enviar') ?>
                </div>
                <form class="form-horizontal" onsubmit="return enviarBrief(this)">
                    <div id="messageSubmitAgencia"></div>
                	<div class="form-group">
	                    <div class="select-agencias">
                            <?php if(empty($filtros)): ?>
	                        <select name="proyectos[]" class="selectpicker" data-style="select-with-transition" multiple title="Selecciona el Proyecto" data-size="7">
	                          <option disabled><?= l('Selecciona el Proyecto') ?></option>
	                          <?php
	                          	foreach($proyectos->result() as $p):
	                  		  ?>
	                          	<option value="<?= $p->id ?>"><?= $p->nombre ?></option>
	                          <?php endforeach ?>
	                        </select>
                            <?php else: ?>
                                <input type="hidden" name="proyectos[]" value="<?= $filtros->id ?>">
                                <?= $filtros->nombre ?>
                            <?php endif ?>
	                    </div>
                	</div>

                  <div class="row">
                      <div class="col-sm-12">
                          <div class="col-sm-6">
                              <a href="javascript:closeModal('#enviar-brief')" type="button" class="swal2-cancel btn btn-danger" style="display: inline-block; width:100%;"><?= l('Cancelar') ?></a>
                          </div>
                          <div class="col-sm-6">
                              <button type="text" class="swal2-cancel btn btn-success" style="display: inline-block; width:100%;"><?= l('Enviar') ?></button>
                          </div>
                      </div>
                  </div>
                  <?php if(!empty($agencia)): ?>
                    <input type="hidden" name="agencias[]" value="<?= $agencia->id ?>">
                  <?php else: ?>
                    <input type="hidden" name="agencias[]">
                  <?php endif ?>
                </form>
                <?php else: ?>
                  <div class="titulo-modal">
                    <b><?= l('No existen proyectos abiertos en este momento') ?></b><br><br><br>
                    <a title="<?= l('Agregar Proyecto') ?>" href="javascript:void(0)" data-toggle="modal" id="btn-notificaciones-top" style="padding: 12px;font-weight: 600;font-size: 14px;" onclick="$('#enviar-brief').modal('toggle'); setTimeout(function(){$('#btn-notificaciones-top').trigger('click');},1000)">
                        <i class="fa fa-plus-square" aria-hidden="true"></i>  <?= l('Crear nuevo proyecto') ?>
                        <div class="ripple-container"></div>
                    </a>
                  </div>
                <?php endif ?>


            </div>
    </div>
</div>
<!-- Termina Modal Enviar Brief -->
<script>
    var selecteds = [];
    <?php if(!empty($agencia)): ?>
      selecteds.push(<?= $agencia->id ?>);
      $(".agenciaCheck").trigger('click');
    <?php endif ?>

    function selAgenciaBrief(id){
      selecteds.push(id);      
      $("input[name='agencias[]']").val(selecteds);
      $('#enviar-brief').modal('show');
    }
      
    
    $(document).on('click','.agenciaCheck',function(){
        selecteds = [];
        var x = 0;
        $('.agenciaCheck').each(function(){
            if($(this).prop('checked')){
                selecteds.push($(this).val());
            }
            x++;
            if($('.agenciaCheck').length===x){
                $("input[name='agencias[]']").val(selecteds);
            }
        });
    });

    function enviarBrief(form){
        $("#messageSubmitAgencia").html('');
        if(selecteds.length>0){
            form = new FormData(form);
            remoteConnection(
                'cliente/proyecto/enviar_brief',
                form,
                function(data){
                    $("#messageSubmitAgencia").html(data)
                }
            );
        }else{
            $("#messageSubmitAgencia").html("Seleccione las agencias que desea invitar").addClass('alert alert-success alert-danger');
        }
        return false;
    }
    
</script>
