
<!-- Termina Modal Editar Proyecto -->
<script>
    function sendEdit(form){
        $(".OrderingDate").trigger('click');
        $(".btn-edit-project").html('Guardando por favor espere..').attr('disabled',true);
        var data = new FormData(form);
        validateEdit(data);
        $("#messageSubmitEdit").html("").removeClass('alert alert-danger');
        return false;
    }

    function validateEdit(form){
        remoteConnection(
            'cliente/proyecto/proyectos/update_validation/<?= $proyecto->id ?>',
            form,
            function(data){
                data = data.replace('<textarea>','');
                data = data.replace('</textarea>','');
                data = JSON.parse(data);
                if(data.success){
                    saveEdit(form);
                }else{
                    $(".btn-edit-project").html('Guardar').attr('disabled',false);
                    $("#messageSubmitEdit").html(data.error_message).addClass('alert alert-success alert-danger');
                }
            }
        );

    }

    function saveEdit(form){
        remoteConnection(
            'cliente/proyecto/proyectos/update/<?= $proyecto->id ?>',
            form,
            function(data){
                data = data.replace('<textarea>','');
                data = data.replace('</textarea>','');
                data = JSON.parse(data);
                if(data.success){
                    $(".btn-edit-project").html('Proyecto guardado con éxito');
                    setTimeout(function(){document.location.reload();},1000);
                }else{
                    $(".btn-edit-project").html('Guardar').attr('disabled',false);
                    $("#messageSubmitEdit").html("Ha ocurrido un error interno, comuniquese con el administrador del sistema").addClass('alert alert-danger');
                }
            }
        );
    }
</script>

<!-- Modal Editar proyecto Proyecto -->
<div class="modal fade" id="editar-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
    	<form onsubmit="return sendEdit(this)">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 800px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; height: 900px; overflow: scroll;" tabindex="-1">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
              </div>
                <img src="<?= $this->user->logo ?>" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b><?= l('Editar información del proyecto') ?></b>
                </div>

                <div class="card-content">
                	<div id="messageSubmitEdit"></div>
                    <div class="menu-filtros">

                        <ul class="nav nav-wizard">
                            <li class="active"><a href="#editar1" data-toggle="tab"><?= l('Datos') ?></a></li>                        
                            <li><a href="#editar2" data-toggle="tab"><?= l('Especificaciones') ?></a></li>
                            <li><a href="#editar3" data-toggle="tab"><?= l('Fechas') ?></a></li>
                            <li><a href="#editar4" data-toggle="tab"><?= l('Filtros') ?></a></li>
                            <li><a href="#editar5" data-toggle="tab"><?= l('Documentos') ?></a></li>                        
                        </ul>

                    </div>

                    <div class="tab-content contenedor-facturas">
                        <div class="tab-pane active" id="editar1">

                                <div class="col-sm-12 subtitulo-modal">
                                    <b><?= l('Datos del Proyecto') ?></b>
                                </div>

                                <div class="col-xs-12 form-group">                                    
                                    <span class="fileinput-button btn btn-app btn-purple btn-sm" id="upload-button-281658623e" style="display:none; width:auto; padding:10px;">
                                        <i class="ace-icon fa fa-cloud-upload bigger-200"></i>
                                        <span><?= l('Subir Foto') ?></span>
                                        <input type="file" name="s8c373fd1" class="gc-file-upload" rel="<?= base_url() ?>cliente/proyecto/proyectos/upload_file/imagen" id="281658623e">
                                        <input class="hidden-upload-input" type="hidden" name="imagen" value="<?= $proyecto->_imagen ?>" rel="s8c373fd1">
                                    </span>
                                    <div id="uploader_281658623e" rel="281658623e" class="grocery-crud-uploader" style=""></div>
                                    <div id="success_281658623e" class="upload-success-url" style="padding-top:7px;">
                                        <a href="<?= base_url() ?>img/proyectos_uploads/img/<?= $proyecto->_imagen ?>" id="file_281658623e" class="open-file" target="_blank">
                                            <img src="<?= base_url() ?>img/proyectos_uploads/img/<?= $proyecto->_imagen ?>" alt="">
                                        </a>
                                        <a href="javascript:void(0)" id="delete_281658623e" class="delete-anchor"><?= l('eliminar') ?></a> 
                                    </div>
                                    <div style="clear:both"></div>
                                    <div id="loading-281658623e" style="display:none">
                                        <span id="upload-state-message-281658623e"></span>
                                        <span class="qq-upload-spinner"></span> 
                                        <span id="progress-281658623e"></span>
                                    </div>
                                    <div style="display:none">
                                        <a href="<?= base_url() ?>cliente/proyecto/proyectos/upload_file/imagen" id="url_281658623e"></a>
                                    </div>
                                    <div style="display:none">
                                        <a href="<?= base_url() ?>cliente/proyecto/proyectos/delete_file/imagen" id="delete_url_281658623e" rel=""></a>
                                    </div>
                                </div>

                                <div class="col-xs-12 form-group">
                                    <label class="control-label"><?= l('Nombre del Proyecto') ?></label>
                                    <input  maxlength="100" type="text" name="nombre" class="form-control" placeholder="" value="<?= $proyecto->nombre ?>" />
                                </div>

                                <div class="col-sm-12 subtitulo-modal">
                                    <?= l('<b>Agregar Supervisores o Colaboradores</b><small>*Puedes agregar más campos</small>') ?>
                                </div>
                                <div class="col-sm-12 form-group input-group">
                                    <div class="select-agencias">
                                        <select name="proyectos_colaboradores[supervisores][]" data-style="select-with-transition" multiple  class="selectpicker" title="<?= l('Seleccione un usuario') ?>">
                                            <option><?= l('Seleccione un usuario') ?></option>
                                            <?php
                                                $this->db->select('user.nombre, user_empresas.user_id');
                                                $this->db->join('user','user.id = user_empresas.user_id');
                                            ?>
                                            <?php foreach($this->db->get_where('user_empresas',array('empresas_id'=>$this->user->empresa))->result() as $e): ?>
                                                <?php $selected = $this->db->get_where('proyectos_user',array('proyectos_id'=>$proyecto->id,'user_id'=>$e->user_id,'tipo'=>2))->num_rows()>0?'selected':''; ?>
                                                <option value="<?= $e->user_id ?>" <?= $selected ?>><?= $e->nombre ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-12 form-group input-group">
                                    <div class="select-agencias">
                                        <select name="proyectos_colaboradores[colaboradores][]" data-style="select-with-transition" multiple  class="selectpicker" title="<?= l('Seleccione un usuario') ?>">
                                            <option><?= l('Seleccione un usuario') ?></option>
                                            <?php
                                                $this->db->select('user.nombre, user_empresas.user_id');
                                                $this->db->join('user','user.id = user_empresas.user_id');
                                            ?>
                                            <?php foreach($this->db->get_where('user_empresas',array('empresas_id'=>$this->user->empresa))->result() as $e): ?>
                                                <?php $selected = $this->db->get_where('proyectos_user',array('proyectos_id'=>$proyecto->id,'user_id'=>$e->user_id,'tipo'=>3))->num_rows()>0?'selected':''; ?>
                                                <option value="<?= $e->user_id ?>" <?= $selected ?>><?= $e->nombre ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>



                                <div class="col-sm-12 subtitulo-modal">
                                    <b><?= l('Datos Generales') ?></b>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <select class="selectpicker" name="paises[]" data-style="select-with-transition" multiple title="<?= l('Pais') ?>" data-size="7">
                                                  <?php foreach($this->db->get_where('paises')->result() as $t): ?>
                                              		<?php $selected = $this->db->get_where('proyectos_paises',array('proyectos_id'=>$proyecto->id,'paises_id'=>$t->id))->num_rows()>0?'selected':'' ?>
			                                          <option value="<?= $t->id ?>" <?= $selected ?>><?= $t->nombre ?></option>
			                                      <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <select class="selectpicker" name="categorias_proyectos[]" data-style="select-with-transition" multiple title="<?= l('Categoria') ?>" data-size="7">
                                                  <?php foreach($this->db->get_where('categorias_proyectos')->result() as $t): ?>
                                                  	  <?php $selected = $this->db->get_where('proyectos_categorias_proyectos',array('proyectos_id'=>$proyecto->id,'categorias_proyectos_id'=>$t->id))->num_rows()>0?'selected':'' ?>
			                                          <option value="<?= $t->id ?>" <?= $selected ?>><?= $t->nombre ?></option>
			                                      <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <select class="selectpicker" name="marcas[]" data-style="select-with-transition" multiple title="<?= l('Marca') ?>" data-size="7">
                                                  <?php foreach($this->db->get_where('marcas')->result() as $t): ?>
			                                          <?php $selected = $this->db->get_where('proyectos_marcas',array('proyectos_id'=>$proyecto->id,'marcas_id'=>$t->id))->num_rows()>0?'selected':'' ?>
			                                          <option value="<?= $t->id ?>" <?= $selected ?>><?= $t->nombre ?></option>
			                                      <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <select class="selectpicker" name="productos[]" data-style="select-with-transition" multiple title="<?= l('Producto') ?>" data-size="7">
                                                  <?php foreach($this->db->get_where('productos')->result() as $t): ?>
			                                          <?php $selected = $this->db->get_where('proyectos_productos',array('proyectos_id'=>$proyecto->id,'productos_id'=>$t->id))->num_rows()>0?'selected':'' ?>
			                                          <option value="<?= $t->id ?>" <?= $selected ?>><?= $t->nombre ?></option>
			                                      <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li>
                                    	<button type="submit" class="btn-edit-project nonavigate btn btn-primary" id="btn-add-project">
                                    		<?= l('Guardar') ?>
	                                    </button>
	                                </li>
                                </ul>

                        </div>

                        <div class="tab-pane" id="editar2">

                            	<div class="col-sm-12 padding0 subtitulo-modal">
                                    <b><?= l('Detalles del proyecto') ?></b>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= l('Solicitud puntual') ?></label>
                                    <textarea name="solicitud_puntual" class="form-control" id="textarea" placeholder="Máx. 300 carácteres"><?= $proyecto->solicitud_puntual ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= l('Objetivo del Proyecto') ?></label>
                                    <textarea name="objetivo" class="form-control" id="textarea" placeholder="¿Cuál es el objetivo que se debe cumplir?"><?= $proyecto->objetivo ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= l('Target') ?></label>
                                    <textarea name="target" class="form-control" id="textarea" placeholder="¿A quién dirigiremos esta iniciativa?"><?= $proyecto->target ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?= l('Especificaciones / Observaciones generales') ?></label>
                                    <textarea name="especificaciones" class="form-control" id="textarea" name="textarea" placeholder="Ingresa las especificaciones puntuales que se deben seguir"><?= $proyecto->especificaciones ?></textarea>
                                </div>


                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li>
                                    	<button type="submit" class="btn-edit-project nonavigate btn btn-primary" id="btn-add-project">
                                    		<?= l('Guardar'); ?>
	                                    </button>
	                                </li>
                                </ul>
                        </div>

                        <div class="tab-pane" id="editar3">
                            <div class="col-sm-12 subtitulo-modal" style="position:relative;">
                                <b><?= l('Ingresa la fecha límite para cada etapa del proyecto') ?></b>                                
                            </div>
                    		<div class="DateInfo">
                                <?php foreach($proyecto->fechas->result() as $n=>$f): ?>
                                    <div class="col-sm-12 form-group input-group dateslist <?= ($n==0)?'rowDate':'' ?>">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control"><?= l('Etapa') ?></label>
                                                        <input type="text" name="proyectos_fechas[nombre][]" class="form-control" value="<?= $f->nombre ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control"><?= l('Desde') ?></label>
                                                        <input type="text" name="proyectos_fechas[fecha_desde][]" class="form-control desde edit" value="<?= date("d/m/Y",strtotime($f->desde)) ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control"><?= l('Hasta') ?></label>
                                                        <input type="text" name="proyectos_fechas[fecha_hasta][]" class="form-control hasta edit" value="<?= date("d/m/Y",strtotime($f->hasta)) ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" style="<?= $n==0?'display: none':'' ?>" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular"></div><div id="sliderDouble"></div>
                                    </div>
                                <?php endforeach ?>
                            </div>

                            <ul class="list-inline pull-right margen-btn-agregar">
                                <li><button type="button" class="nonavigate btn btn-primary btn-addDate"><?= l('Anadir etapa') ?></button></li>
                                <!--<li><button type="button" id="btn-enviar-brief" class="OrderingDate btn btn-reacomodo-fechas"><?= l('Guardar cambios') ?></button></li>-->
                                <li>

                                    <button type="submit" class="btn-edit-project nonavigate btn btn-primary" id="btn-add-project">
                                        <?= l('Guardar'); ?>
                                    </button>
                                </li>
                            </ul>


                        </div>

                        <div class="tab-pane" id="editar4">

                            	<div class="col-sm-12 subtitulo-modal">
                                    <b><?= l('Selecciona criterios de búsqueda') ?></b>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="select-agencias">
                                            <select class="selectpicker" name="servicios[]" data-style="select-with-transition" multiple title="<?= l('Servicios') ?>"><i class="material-icons">arrow_drop_down</i></small>
                                                <option disabled><?= l('Servicios') ?></option>
                                              <?php foreach($this->db->get_where('servicios')->result() as $t): ?>
                                                <?php $selected = $this->db->get_where('proyectos_servicios',array('proyectos_id'=>$proyecto->id,'servicios_id'=>$t->id))->num_rows()>0?'selected':'' ?>
                                                <option value="<?= $t->id ?>" <?= $selected ?>><?= $t->nombre ?></option>
                                              <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                  <div class="iconos-favoritos-agencias">
                                      <button type="button" class="nonavigate cleanFilters btn btn-primary btn-round" id="btn-borrar-filtros"><i class="material-icons">cached</i> <?= l('Borrar filtros') ?></button>
                                  </div>
                                </div>


                                <!-- Back -->
                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li>

                                        <button type="submit" class="btn-edit-project nonavigate btn btn-primary" id="btn-add-project">
                                            <?= l('Guardar'); ?>
                                        </button>
                                    </li>
                                </ul> 
                            </div>

                            <div class="tab-pane" id="editar5">
                                <div class="col-sm-12 subtitulo-modal">
                                    <?= l('tutorial-documentos') ?>
                                </div>



                                 <div class="col-xs-12 form-group">
                                        <div class="form-group" id="foto_field_box">
                                            <!--<label for="field-foto" id="foto_display_as_box" style="width:100%">
                                                <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/subir-imagen.png" alt="Logo Bimbo" class="img-responsive center-block">
                                            </label>    -->
                                             <span class="fileinput-button btn btn-app btn-purple btn-sm" id="upload-button-85470123e" style=" width:auto; padding:10px;">
                                                    <i class="ace-icon fa fa-cloud-upload bigger-200"></i>
                                                    <span><?= l('Subir fichero') ?></span>
                                                    <input type="file" name="s34240997" class="gc-file-upload" rel="<?= base_url() ?>cliente/proyecto/proyectos_documentos/upload_file/documento" id="85470123e">
                                                    <input class="hidden-upload-input" type="hidden" name="proyectos_documentos[]" value="" rel="s34240997">
                                            </span>
                                            <div id="uploader_85470123e" rel="85470123e" class="grocery-crud-uploader" style=""></div>
                                            <div id="success_85470123e" class="upload-success-url" style="display:none; padding-top:7px;">
                                                <a href="<?= base_url() ?>img/proyectos_uploads/files/" id="file_85470123e" class="open-file" target="_blank"></a>
                                                <a href="javascript:void(0)" id="delete_85470123e" class="delete-anchor">eliminar</a> 
                                            </div>
                                            <div style="clear:both"></div>
                                            <div id="loading-85470123e" style="display:none">
                                                <span id="upload-state-message-85470123e"></span>
                                                <span class="qq-upload-spinner"></span> 
                                                <span id="progress-85470123e"></span>
                                            </div>
                                            <div style="display:none">
                                                <a href="<?= base_url() ?>cliente/proyecto/proyectos_documentos/upload_file/documento" id="url_85470123e"></a>
                                            </div>
                                            <div style="display:none">
                                                <a href="<?= base_url() ?>cliente/proyecto/proyectos_documentos/delete_file/documento" id="delete_url_85470123e" rel=""></a>
                                            </div>
                                        </div>
                                    </div>

                                <div class="col-sm-12 form-group input-group">
                                	<label class="control-label"><?= l('agregar-url') ?></label>
                                    <input type="text" name="proyectos_documentos_url[]" class="form-control" placeholder="&nbsp &nbsp &nbsp https://www.youtube.com/">
                                    <span class="input-group-btn">
                                      <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-add">
                                          <small><i class="material-icons">add_circle_outline</i></small>
                                      </button>
                                    </span>
                                </div>

                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li>
                                    	<button type="submit" class="btn-edit-project nonavigate btn btn-primary" id="btn-add-project">
                                    		<?= l('Guardar'); ?>
	                                    </button>
	                                </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="user_id" value="<?= $this->user->id ?>">
        </form>
    </div>
</div>


<script type="text/javascript">
    var upload_info_85470123e = upload_info_281658623e = {
            accepted_file_types: /(\.|\/)(gif|jpeg|jpg|png|tiff|doc|docx|txt|odt|xls|xlsx|pdf|ppt|pptx|pps|ppsx|mp3|m4a|ogg|wav|mp4|m4v|mov|wmv|flv|avi|mpg|ogv|3gp|3g2)$/i,
            accepted_file_types_ui : ".gif,.jpeg,.jpg,.png,.tiff,.doc,.docx,.txt,.odt,.xls,.xlsx,.pdf,.ppt,.pptx,.pps,.ppsx,.mp3,.m4a,.ogg,.wav,.mp4,.m4v,.mov,.wmv,.flv,.avi,.mpg,.ogv,.3gp,.3g2",
            max_file_size: 20971520,
            max_file_size_ui: "20MB"
    };
    var string_upload_file  = "<?= l('Subir') ?>";
    var string_delete_file  = "<?= l('Eliminando archivo') ?>";
    var string_progress             = "<?= l('Progreso') ?>: ";
    var error_on_uploading          = "<?= l('Ha ocurrido un error al intentar subir el archivo.') ?>";
    var message_prompt_delete_file  = "<?= l('¿Esta seguro que quiere eliminar este archivo?') ?>";
    var error_max_number_of_files   = "<?= l('Solo puede subir un archivo a la vez.') ?>";
    var error_accept_file_types     = "<?= l('No se permite este tipo de extension.') ?>";
    var error_max_file_size         = "<?= l('El archivo excede el tamaño 20MB que fue especificado.') ?>";
    var error_min_file_size         = "<?= l('No puede subir un archivo vacío.') ?>";

    var base_url = "<?= base_url() ?>";
    var upload_a_file_string = "Subir";
</script>