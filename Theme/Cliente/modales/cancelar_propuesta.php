<div class="modal fade" id="cancelar-propuesta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          </div>
        	<div class="response"></div>
        	<form onsubmit="return cancelBrief(this)">
            <img src="<?= $this->user->logo ?>" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b><?= l('Cancelar Propuesta') ?></b><br>
              <?= l('¿Seguro que deseas cancelar esta propuesta?') ?>
            </div>
            <div class="form-group">
                <label class="control-label"><?= l('Envia una explicación a las agencia participante') ?></label>
                <textarea class="form-control" id="motivo_cancelacion" name="mensaje" placeholder="<?= l('Se ha cancelado este proyecto, porque') ?>"></textarea>
            </div>

            <input type="hidden" name="brief_id" value="0">
            <input type="hidden" name="status" value="2">
            <input type="hidden" name="propuesta" value="">

            <input type="hidden" name="fecha_modificacion" value="<?= date("d/m/Y H:i:s") ?>">

            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <a href="javascript:closeModal('#cancelar-propuesta')" class="swal2-cancel btn btn-danger" style="display: inline-block; width:100%"><?= l('Cancelar') ?></a>
                    </div>
                    <div class="col-sm-6">
                        <button class="swal2-confirm btn btn-success" type="submit" style="display: inline-block; width:100%"><?= l('Enviar') ?></button>
                    </div>
                </div>
            </div>

            </form>


        </div>
    </div>
</div>
<!-- Termina Modal Cancelar Proyecto -->
<script>

	function mostrarCancelBrief(id){
		$('#cancelar-propuesta input[name="brief_id"]').val(id);
		$("#cancelar-propuesta").modal('show');
	}
	function cancelBrief(f){
		var form = new FormData(f);
        remoteConnection('cliente/proyecto/briefs/update/'+$('#cancelar-propuesta input[name="brief_id"]').val(),form,function(data){
            $('#cancelar-propuesta').find('.response').addClass('alert alert-success').html('Brief Rechazado con éxito');
        });
        return false;
	}


</script>
