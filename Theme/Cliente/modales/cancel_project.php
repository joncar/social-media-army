<div class="modal fade" id="cancelar-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
          </div>
            <img src="<?= $this->user->logo ?>" alt="Logo Bimbo" class="img-responsive center-block">
            <div class="titulo-modal">
              <b><?= l('Cancelar Proyecto') ?></b><br>
              ¿<?= l('Seguro que deseas cancelar el proyecto') ?><br>"<?= @$proyecto->nombre ?>"?
            </div>

            <div class="form-group">
                <label class="control-label"><?= l('Envia una explicación a las agencias participantes') ?></label>
                <textarea class="form-control" id="motivo_cancelacion" name="motivo_cancelacion" placeholder="<?= l('Se ha cancelado este proyecto, porque') ?>"></textarea>
            </div>

            <input style="display: none;" class="swal2-input">
            <input style="display: none;" class="swal2-file" type="file">
            <div class="swal2-range" style="display: none;">
                <output></output><input type="range">
            </div>
            <select style="display: none;" class="swal2-select"></select>
            <div class="swal2-radio" style="display: none;"></div>
            <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;"><input type="checkbox"></label>
            <textarea style="display: none;" class="swal2-textarea"></textarea>
            <div class="swal2-validationerror" style="display: none;"></div>
            <hr class="swal2-spacer" style="display: block;">

            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <a href="javascript:closeModal('#cancelar-proyecto')" class="swal2-cancel btn btn-danger" style="display: inline-block; width:100%;"><?= l('Cancelar') ?></a>
                    </div>
                    <div class="col-sm-6">
                        <a href="javascript:cancelProject(<?= @$proyecto->id ?>)" class="swal2-confirm btn btn-success" style="display: inline-block; width:100%;"><?= l('Enviar') ?></a>
                    </div>
                </div>
            </div>

            <span class="swal2-close" style="display: none;">×</span>

        </div>
    </div>
</div>
<!-- Termina Modal Cancelar Proyecto -->
<script>
	function cancelProject(id){
		$.post('<?= base_url('cliente/proyecto/proyectos/update/') ?>/'+id,{
			status:3,
			motivo_cancelacion:$("#motivo_cancelacion").val()
		},function(){
			document.location.reload();
		});
	}


</script>
