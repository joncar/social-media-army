<div class="modal fade" id="soporteAdmin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <div class="modal-header" style="position: absolute;right: 14px;">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
                </div>
            	<div id="contactoMessageSoporte"></div>
                <form action="paginas/frontend/contacto" onsubmit="return sendForm(this,'.result')">
                    <div style="text-align: center">
                        <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive" style="display:inline-block">
                    </div>
                    <div class="titulo-modal">
                      <b><?= l('Contactar a soporte') ?></b>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= l('Asunto') ?></label>
                        <input  maxlength="100" name="extras[asunto]" type="text" class="form-control" placeholder=""  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= l('Nombre') ?></label>
                        <input  maxlength="100" value="<?= $this->user->nombre ?>" name="nombre" type="text" class="form-control" placeholder="..."  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= l('Email') ?></label>
                        <input  maxlength="100" value="<?= $this->user->email ?>" name="email" type="email" class="form-control" placeholder="..."  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= l('Solicitud') ?></label>
                        <input  maxlength="100" name="extras[comentarios]" type="textarea" class="form-control" placeholder=""  />
                    </div>
                    <input type="hidden" name="titulo" value="<?= $this->user->nombre ?> solicita ayuda con la plataforma">
                    <div class="result"></div>
                    <a href="javascript:closeModal('#soporteAdmin')" class="swal2-cancel btn btn-danger"><?= l('Cancelar') ?></a>
                    <button type="submit" class="swal2-confirm btn btn-success"><?= l('Enviar solicitud') ?></button>                
                </form>
            </div>            
    </div>
</div>
<!-- Termina Contactar agencia -->
<script>
	function contactar(form){
		var contacto = new FormData(form);
		contacto.append('user_id',<?= $this->user->id ?>);
		remoteConnection('paginas/frontend/contactoAdmin',contacto,function(data){
			$("#contactoMessageSoporte").html(data);
		});
		return false;
	}
</script>