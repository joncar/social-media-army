<div class="modal fade" id="contacto-agencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
                </div>
            	<div id="contactoMessage"></div>
                <form onsubmit="return contactar(this)">
                    <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                    <div class="titulo-modal">
                      <b><?= l('Contactar Agencia') ?></b>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= l('Asunto') ?></label>
                        <input  maxlength="100" name="asunto" type="text" class="form-control" placeholder="..."  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= l('Nombre') ?></label>
                        <input  maxlength="100" value="<?= $this->user->nombre.' '.$this->user->apellido ?>" name="nombre" type="text" class="form-control" placeholder="..."  />
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?= l('Mensaje') ?></label>
                        <input  maxlength="100" name="comentarios" type="textarea" class="form-control" placeholder="..."  />
                    </div>
                    <a href="javascript:closeModal('#contacto-agencia')" class="swal2-cancel btn btn-danger"><?= l('Cancelar') ?></a>
                    <button type="submit" class="swal2-confirm btn btn-success"><?= l('Enviar') ?></button>                
                </form>
            </div>            
    </div>
</div>
<!-- Termina Contactar agencia -->
<script>
	function contactar(form){
		var contacto = new FormData(form);
		contacto.append('agencias_id',<?= @$agencia->id ?>);
		remoteConnection('paginas/frontend/contacto',contacto,function(data){
			$("#contactoMessage").html(data);
		});
		return false;
	}
</script>