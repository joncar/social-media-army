<link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/file_upload/file-uploader.css" />
<link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/file_upload/jquery.fileupload-ui.css" />
<link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css" />
<link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/file_upload/fileuploader.css" />


<div class="modal fade" id="agregar-proyecto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">

            <div class="swal2-modal swal2-show" style="display: block; width: 700px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
              </div>
                <div class="col-sm-12 logo-modales">
                    <img src="<?= $this->user->logo ?>" alt="Logo Bimbo" class="img-responsive center-block">
                </div>
                <div class="titulo-modal" style="margin-top:90px;">
                  <b>Nuevo Proyecto</b><br>
                  A continuación llena todos los campos
                </div>

                <div class="wizard">
                    <div id="messageSubmit"></div>
                    <ul class="nav nav-wizard">
                        <li class="active"><a href="#paso1" data-toggle="tab">Datos</a></li>                        
                        <li><a href="#paso2" data-toggle="tab">Especificaciones</a></li>
                        <li><a href="#paso3" data-toggle="tab">Fechas</a></li>
                        <li><a href="#paso4" data-toggle="tab">Filtros</a></li>
                        <li><a href="#paso5" data-toggle="tab">Documentos</a></li>
                        <li><a href="#paso6" data-toggle="tab">Finalizar</a></li>
                    </ul>

                    <form action="" onsubmit="return send(this)">
                        <div class="tab-content">
                            <div class="tab-pane active" id="paso1">
                                <div class="col-sm-12 subtitulo-modal">
                                    <b>Datos del Proyecto</b>
                                </div>


                                <div class="col-xs-12 form-group">                                    
                                    <span class="fileinput-button btn btn-app btn-purple btn-sm" id="upload-button-281658623" style=" width:auto; padding:10px;">
                                        <i class="ace-icon fa fa-cloud-upload bigger-200"></i>
                                        <span>Subir Foto</span>
                                        <input type="file" name="s8c373fd1" class="gc-file-upload" rel="<?= base_url() ?>cliente/proyecto/proyectos/upload_file/imagen" id="281658623">
                                        <input class="hidden-upload-input" type="hidden" name="imagen" value="" rel="s8c373fd1">
                                    </span>
                                    <div id="uploader_281658623" rel="281658623" class="grocery-crud-uploader" style=""></div>
                                    <div id="success_281658623" class="upload-success-url" style="display:none; padding-top:7px;">
                                        <a href="<?= base_url() ?>img/proyectos_uploads/img/" id="file_281658623" class="open-file" target="_blank"></a>
                                        <a href="javascript:void(0)" id="delete_281658623" class="delete-anchor">eliminar</a> 
                                    </div>
                                    <div style="clear:both"></div>
                                    <div id="loading-281658623" style="display:none">
                                        <span id="upload-state-message-281658623"></span>
                                        <span class="qq-upload-spinner"></span> 
                                        <span id="progress-281658623"></span>
                                    </div>
                                    <div style="display:none">
                                        <a href="<?= base_url() ?>cliente/proyecto/proyectos/upload_file/imagen" id="url_281658623"></a>
                                    </div>
                                    <div style="display:none">
                                        <a href="<?= base_url() ?>cliente/proyecto/proyectos/delete_file/imagen" id="delete_url_281658623" rel=""></a>
                                    </div>
                                </div>

                                <div class="col-xs-12 form-group">
                                    <label class="control-label">Nombre del Proyecto</label>
                                    <input  maxlength="100" type="text" name="nombre" class="form-control" placeholder=""  />
                                </div>

                                <div class="col-sm-12 subtitulo-modal">
                                    <b>Agregar Supervisores o Colaboradores</b>
                                </div>
                                <div class="col-sm-12 form-group input-group">
                                	<div class="select-agencias">

                                        <select name="proyectos_colaboradores[supervisores][]" data-style="select-with-transition" multiple  class="selectpicker" title="Seleccione los supervisores">
                                          <option disabled>Seleccione Supervisores</option>
                                            <?php
                                                $this->db->select('user.nombre, user_empresas.user_id');
                                                $this->db->join('user','user.id = user_empresas.user_id');
                                            ?>
                                            <?php 
                                                $this->db->where('empresas_id = '.$this->user->empresa.' AND (tipo=2 OR tipo = 3)',null,TRUE);
                                                foreach($this->db->get_where('user_empresas')->result() as $e): 
                                            ?>
                                                <option value="<?= $e->user_id ?>"><?= $e->nombre ?></option>
                                            <?php endforeach ?>

                                            <button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Cerrar</button>

                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-12 form-group input-group">
                                    <div class="select-agencias">
                                        <select name="proyectos_colaboradores[colaboradores][]" data-style="select-with-transition" multiple  class="selectpicker" title="Seleccione los colaboradores">
                                            <option disabled>Seleccione colaboradores</option>
                                            <?php
                                                $this->db->select('user.nombre, user_empresas.user_id');
                                                $this->db->join('user','user.id = user_empresas.user_id');
                                            ?>
                                            <?php foreach($this->db->get_where('user_empresas',array('empresas_id'=>$this->user->empresa,'tipo'=>4))->result() as $e): ?>
                                                <option value="<?= $e->user_id ?>"><?= $e->nombre ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-12 subtitulo-modal">
                                    <b>Datos Generales</b>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <select class="selectpicker" id="paises" name="paises[]" data-style="select-with-transition" multiple title="País" data-size="7">
                                                  <option disabled>Selecciona país</option>
                                                  <?php foreach($this->db->get_where('paises')->result() as $t): ?>
			                                          <option value="<?= $t->id ?>"><?= $t->nombre ?></option>
			                                      <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <select class="selectpicker" name="categorias_proyectos[]" data-style="select-with-transition" multiple title="Categoría" data-size="7">
                                                  <option disabled>Selecciona categoría de proyecto</option>
                                                  <?php foreach($this->db->get_where('categorias_proyectos')->result() as $t): ?>
			                                          <option value="<?= $t->id ?>"><?= $t->nombre ?></option>
			                                      <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <select class="selectpicker" id="marcas" name="marcas[]" data-style="select-with-transition" multiple title="Marca" data-size="7">
                                                  <option disabled>Selecciona marca</option>
                                                  <?php foreach($this->db->get_where('marcas')->result() as $t): ?>
			                                          <option value="<?= $t->id ?>"><?= $t->nombre ?></option>
			                                      <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="select-agencias">
                                                <select class="selectpicker" id="productos" name="productos[]" data-style="select-with-transition" multiple title="Producto" data-size="7">
                                                  <option disabled>Selecciona el producto</option>
                                                    <?php
                                                        $this->db->select('productos.*');
                                                        $this->db->join('marcas','marcas.id = productos.marcas_id');
                                                    ?>
                                                  <?php foreach($this->db->get_where('productos',array('empresas_id'=>$this->user->empresa))->result() as $t): ?>
			                                          <option value="<?= $t->id ?>"><?= $t->nombre ?></option>
			                                      <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Back -->
                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 2</button></li>
                                </ul>
                            </div>

                            <div class="tab-pane" id="paso2">
                                <div class="col-sm-12 padding0 subtitulo-modal">
                                    <b>Detalles del proyecto</b>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Solicitud puntual</label>
                                    <textarea name="solicitud_puntual" class="form-control" id="textarea" placeholder=""></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Objetivo del Proyecto</label>
                                    <textarea name="objetivo" class="form-control" id="textarea" placeholder="¿Cuál es el objetivo que se debe cumplir?"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Target</label>
                                    <textarea name="target" class="form-control" id="textarea" placeholder="¿A quién dirigiremos esta iniciativa?"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Especificaciones / Observaciones generales</label>
                                    <textarea name="especificaciones" class="form-control" id="textarea" name="textarea" placeholder="Ingresa las especificaciones puntuales que se deben seguir"></textarea>
                                </div>


                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 3</button></li>
                                </ul>
                            </div>

                            <div class="tab-pane" id="paso3">
                                <div class="col-sm-12 subtitulo-modal" style="position:relative;">
                                    <b>Ingresa la fecha límite para cada etapa del proyecto</b>
                                </div>
                                <div class="DateInfo">
                                    <div class="col-sm-12 dateslist form-group input-group rowDate">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Etapa</label>
                                                        <input type="text" name="proyectos_fechas[nombre][]" class="form-control" value="Invitación" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input type="text" name="proyectos_fechas[fecha_desde][]" class="form-control desde" value="<?= date("d/m/Y H:i") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input type="text" name="proyectos_fechas[fecha_hasta][]" class="form-control hasta" value="<?= date("d/m/Y H:i") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" style="display: none" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular"></div><div id="sliderDouble"></div>
                                    </div>

                                    <div class="col-sm-12 dateslist form-group input-group rowDate">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Etapa</label>
                                                        <input type="text" name="proyectos_fechas[nombre][]" class="form-control" value="Briefing" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input type="text" name="proyectos_fechas[fecha_desde][]" class="form-control desde" value="<?= date("d/m/Y H:i") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input type="text" name="proyectos_fechas[fecha_hasta][]" class="form-control hasta" value="<?= date("d/m/Y H:i") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" style="display: none" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular"></div><div id="sliderDouble"></div>
                                    </div>

                                    <div class="col-sm-12 dateslist form-group input-group">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Etapa</label>
                                                        <input type="text" name="proyectos_fechas[nombre][]" class="form-control" value="Sesión de preguntas y respuestas" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input type="text" name="proyectos_fechas[fecha_desde][]" class="form-control desde" value="<?= date("d/m/Y") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input type="text" name="proyectos_fechas[fecha_hasta][]" class="form-control hasta" value="<?= date("d/m/Y") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular"></div><div id="sliderDouble"></div>
                                    </div>

                                    <div class="col-sm-12 dateslist form-group input-group">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Etapa</label>
                                                        <input type="text" name="proyectos_fechas[nombre][]" class="form-control" value="Desarrollo de propuestas" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input type="text" name="proyectos_fechas[fecha_desde][]" class="form-control desde" value="<?= date("d/m/Y") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input type="text" name="proyectos_fechas[fecha_hasta][]" class="form-control hasta" value="<?= date("d/m/Y") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular"></div><div id="sliderDouble"></div>
                                    </div>

                                    <div class="col-sm-12 dateslist form-group input-group">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Etapa</label>
                                                        <input type="text" name="proyectos_fechas[nombre][]" class="form-control" value="Evaluación de propuestas" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input type="text" name="proyectos_fechas[fecha_desde][]" class="form-control desde" value="<?= date("d/m/Y") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input type="text" name="proyectos_fechas[fecha_hasta][]" class="form-control hasta" value="<?= date("d/m/Y") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular"></div><div id="sliderDouble"></div>
                                    </div>

                                    <div class="col-sm-12 dateslist form-group input-group">
                                            <div class="col-xs-12 col-sm-5">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Etapa</label>
                                                        <input type="text" name="proyectos_fechas[nombre][]" class="form-control" value="Selección del ganador" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Desde</label>
                                                        <input type="text" name="proyectos_fechas[fecha_desde][]" class="desde form-control" value="<?= date("d/m/Y") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="card-content">
                                                    <div class="form-group">
                                                        <label class="label-control">Hasta</label>
                                                        <input type="text" name="proyectos_fechas[fecha_hasta][]" class="hasta form-control" value="<?= date("d/m/Y") ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-1">
                                                <span class="input-group-btn">
                                                  <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-remove">
                                                      <i class="material-icons">remove_circle_outline</i>
                                                  </button>
                                                </span>
                                            </div>

                                        <div id="sliderRegular"></div><div id="sliderDouble"></div>
                                    </div>
                                </div>
                                <ul class="list-inline pull-left margen-btn-agregar">
                                    <li><button type="button" class="nonavigate btn btn-primary btn-addDate">Añadir etapa</button></li>
                                    <!--<li><button type="button" id="btn-enviar-success" class="OrderingDate btn btn-reacomodo-fechas">Guardar fechas</button></li>-->

                                    <!-- Back -->                                    
                                </ul>
                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li>
                                        <button type="button" class="btn btn-primary OrderingDate" id="btn-proyecto-propuestas">Continuar a Paso 4</button>
                                    </li>
                                </ul>
                            </div>

                            <div class="tab-pane" id="paso4">
                                <div class="col-sm-12 subtitulo-modal">
                                    <b>Selecciona criterios de búsqueda (Opcional)</b>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 padding0">
                                        <div class="select-agencias">
                                            <select class="selectpicker" name="servicios[]" data-style="select-with-transition" multiple title="Servicios <small><i class="material-icons">arrow_drop_down</i></small>" data-size="7">
                                            	<option disabled>Servicios</option>
                                              <?php foreach($this->db->get_where('servicios')->result() as $t): ?>
			                                          <option value="<?= $t->id ?>"><?= $t->nombre ?></option>
			                                  <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                  <div class="iconos-favoritos-agencias">
                                      <button type="button" class="nonavigate cleanFilters btn btn-primary btn-round" id="btn-borrar-filtros"><i class="material-icons">cached</i> Borrar filtros</button>
                                  </div>
                                </div>


                                <!-- Back -->
                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li><button type="button" class="btn btn-primary" id="btn-proyecto-propuestas">Continuar a Paso 5</button></li>
                                </ul> 
                            </div>

                            <div class="tab-pane" id="paso5">
                                <div class="col-sm-12 subtitulo-modal">
                                    <b>Documentos</b><br><small>Carga los documentos relevantes para el Brief. Puedes cargar videos desde un link externo o en formato .mp4</small>
                                </div>

                                <div class="col-sm-12 form-group input-group">


                                    <!--<div class="col-xs-12 form-group">
                                        <label class="btn btn-subir-img text-center subir-img-proyecto-02">
                                            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/subir-imagen.png" alt="Logo Bimbo" class="img-responsive center-block">
                                            <small>Agregar archivo</small>
                                            <input type="file" name="proyectos_documentos[]" class="fileImage">
                                        </label>
                                    </div>-->
                                    <div class="col-xs-12 form-group">
                                        <div class="form-group" id="foto_field_box">
                                            <!--<label for="field-foto" id="foto_display_as_box" style="width:100%">
                                                <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/subir-imagen.png" alt="Logo Bimbo" class="img-responsive center-block">
                                            </label>    -->
                                             <span class="fileinput-button btn btn-app btn-purple btn-sm" id="upload-button-85470123" style=" width:auto; padding:10px;">
                                                    <i class="ace-icon fa fa-cloud-upload bigger-200"></i>
                                                    <span>Subir Foto/Fichero/ZIP</span>
                                                    <input type="file" name="s34240997" class="gc-file-upload" rel="<?= base_url() ?>cliente/proyecto/proyectos_documentos/upload_file/documento" id="85470123">
                                                    <input class="hidden-upload-input" type="hidden" name="proyectos_documentos[]" value="" rel="s34240997">
                                            </span>
                                            <div id="uploader_85470123" rel="85470123" class="grocery-crud-uploader" style=""></div>
                                            <div id="success_85470123" class="upload-success-url" style="display:none; padding-top:7px;">
                                                <a href="<?= base_url() ?>img/proyectos_uploads/files/" id="file_85470123" class="open-file" target="_blank"></a>
                                                <a href="javascript:void(0)" id="delete_85470123" class="delete-anchor">eliminar</a> 
                                            </div>
                                            <div style="clear:both"></div>
                                            <div id="loading-85470123" style="display:none">
                                                <span id="upload-state-message-85470123"></span>
                                                <span class="qq-upload-spinner"></span> 
                                                <span id="progress-85470123"></span>
                                            </div>
                                            <div style="display:none">
                                                <a href="<?= base_url() ?>cliente/proyecto/proyectos_documentos/upload_file/documento" id="url_85470123"></a>
                                            </div>
                                            <div style="display:none">
                                                <a href="<?= base_url() ?>cliente/proyecto/proyectos_documentos/delete_file/documento" id="delete_url_85470123" rel=""></a>
                                            </div>
                                        </div>
                                    </div>

                                    <!--<span class="input-group-btn">
                                      <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-add">
                                          <small><i class="material-icons">add_circle_outline</i></small>
                                      </button>
                                    </span>-->
                                </div>

                                <div class="col-sm-12 form-group input-group">
                                	<label class="control-label">Agrega vídeos desde una url (youtube, vimeo)</label>
                                    <input type="text" name="proyectos_documentos_url[]" class="form-control" placeholder=" &nbsp &nbsp &nbsp https://www.youtube.com/">
                                    <span class="input-group-btn">
                                      <button type="button" class="nonavigate btn btn-primary btn-round btn-fab btn-fab-mini btn-default btn-add">
                                          <small><i class="material-icons">add_circle_outline</i></small>
                                      </button>
                                    </span>
                                </div>

                                <ul class="list-inline pull-right margen-btn-agregar">
                                    <li>
                                    	<button type="submit" class="nonavigate btn btn-primary" id="btn-add-project">
                                    		Terminar
	                                    </button>
	                                </li>
                                </ul>
                            </div>

                            <div class="tab-pane" id="paso6">                                
                                <div id="successResult">
                                    Cargando parámetros del proyecto
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                    <input type="hidden" name="user_id" value="<?= $this->user->id ?>">
                    </form>
                </div>
            </div>
    </div>
</div>
<script>
    $(document).on('ready',function(){
        //showResult(14);
        $("select").on('change',function(){
            $(".bootstrap-select").removeClass('open');
        });

        $(".desde,.hasta").datetimepicker({
            format: 'DD/MM/YYYY',
            icons: {time: "fa fa-clock-o"},
            keepOpen: false,
        }).on('dp.change', function (e) {
            if(!$(this).hasClass('edit')){
                 $(this).parents('.dateslist').find(".desde").data("DateTimePicker").hide();    
                 $(this).parents('.dateslist').find(".hasta").data("DateTimePicker").hide();    
                 var mainBookingForm = $(this).parents('.dateslist');
                 var newDate    = new Date(e.date),
                     newDateStr = ("0" + newDate.getDate()).slice(-2) + '/' + ("0" + (newDate.getMonth() + 1)).slice(-2) + '/' + newDate.getFullYear();                            


                    if (e.target.className.search('desde') >= 0) {
                            checkInDate = newDate.getTime();
                            //Verificamos el hasta
                            var hasta = mainBookingForm.find('.hasta').val();
                            var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;                    
                            hasta = new Date(hasta.replace(pattern,'$3-$2-$1'));
                            if(hasta.getTime()<checkInDate){
                                mainBookingForm.find('.hasta').val(newDateStr);
                            }                        
                    }
                    else {
                        checkOutDate = newDate.getTime();
                        //Verificamos el desde
                        var desde = mainBookingForm.find('.desde').val();
                        var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;                    
                        desde = new Date(desde.replace(pattern,'$3-$2-$1'));                    
                        if(desde.getTime()>checkOutDate){
                            mainBookingForm.find('.desde').val(newDateStr);
                        }    
                    }
                
                for(var i=parseInt(mainBookingForm.index())+1;i<parseInt($('.dateslist').length);i++){
                    var y = $('.dateslist')[i];
                    $(y).find('.desde,.hasta').val(newDateStr);
                }
            }
        });
    });
    //Select paises_id - marcas
    var onSearch = false;
    $(document).on('change','#paises',function(){
        if(onSearch){
            onSearch.abort();
        }
        var form = new FormData();
        var valores = $(this).val();
        for(var i in valores){
            form.append('paises[]',valores[i]);
        }
        onSearch = remoteConnection('cliente/marcas/json_list',form,function(data){
            var data = JSON.parse(data);
            var str = '';
            for(var i in data){
                str += '<option value="'+data[i].Id+'">'+data[i].nombre+' '+data[i].País+'</option>';
            }
            $("#marcas").html(str);
            $("#marcas").selectpicker('refresh');
            onSearch = false;
        });
        console.log($(this).val());
    });

    //Select marcas - productos
    var onSearch = false;
    $(document).on('change','#marcas',function(){
        if(onSearch){
            onSearch.abort();
        }
        var form = new FormData();
        var valores = $(this).val();
        for(var i in valores){
            form.append('marcas[]',valores[i]);
        }
        onSearch = remoteConnection('cliente/productos/json_list',form,function(data){
            var data = JSON.parse(data);
            var str = '';
            for(var i in data){
                str += '<option value="'+data[i].Id+'">'+data[i].nombre+' '+data[i].S280d925f+'</option>';
            }
            $("#productos").html(str);
            $("#productos").selectpicker('refresh');
            onSearch = false;
        });
        console.log($(this).val());
    });
	function send(form){
        var proyecto = $("#agregar-proyecto input[name='nombre']").val();
        if(confirm('Estás por crear el proyecto '+proyecto)){
            $(".OrderingDate").trigger('click');
            $("#btn-add-project").html('Guardando por favor espere!..').attr('disabled',true);
            var data = new FormData(form);
            validate(data);
            $("#messageSubmit").html("").removeClass('alert alert-danger');		
        }
        return false;
	}

    function validate(form){
        remoteConnection(
            'cliente/proyecto/proyectos/insert_validation',
            form,
            function(data){
                data = data.replace('<textarea>','');
                data = data.replace('</textarea>','');
                data = JSON.parse(data);
                if(data.success){
                    save(form);
                }else{
                    $("#btn-add-project").html('Terminar').attr('disabled',false);
                    $("#messageSubmit").html(data.error_message).addClass('alert alert-success alert-danger');
                }
            }
        );

    }

    function save(form){

        remoteConnection(
            'cliente/proyecto/proyectos/insert',
            form,
            function(data){
                data = data.replace('<textarea>','');
                data = data.replace('</textarea>','');
                data = JSON.parse(data);
                if(data.success){
                    $("#btn-add-project").html('Proyecto guardado con exito');
                    showResult(data.insert_primary_key);
                    $(".nav-wizard").hide();
                }else{
                    $("#btn-add-project").html('Terminar').attr('disabled',false);
                    $("#messageSubmit").html("Ha ocurrido un error interno, comuniquese con el administrador del sistema").addClass('alert alert-danger');
                }
            }
        );
    }

    function showResult(id){
        remoteConnection(
            'cliente/proyecto/showResult/'+id,
            new FormData(),
            function(data){
                $("#successResult").html(data);
                $active = $('.wizard .nav-wizard li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
            }
        );
    }

    function check(){
        var active = $('.wizard .nav-wizard li.active a').attr('href');
        switch(active){
            case '#paso1':
                var str = '';
                if($("input[name='nombre']").val()===''){
                    str+= "Por favor complete el nombre del proyecto";
                }
                if(str!==''){
                    $("#messageSubmit").html(str).addClass('alert alert-danger');
                    return false;
                }
            break;
            case '#paso2':
                var str = '';
                if($("textarea[name='solicitud_puntual']").val()===''){
                    str+= "<p>Por favor complete las solicitudes puntuales del proyecto</p>";
                }
                if($("textarea[name='objetivo']").val()===''){
                    str+= "<p>Por favor complete el objetivo del proyecto</p>";
                }
                if($("textarea[name='target']").val()===''){
                    str+= "<p>Por favor complete el target del proyecto</p>";
                }
                if($("textarea[name='especificaciones']").val()===''){
                    str+= "<p>Por favor complete las especificaciones</p>";
                }
                if(str!==''){
                    $("#messageSubmit").html(str).addClass('alert alert-danger');
                    return false;
                }
            break;
        };
        $("#messageSubmit").html("").removeClass('alert alert-success alert-danger');
        return true;
    }

</script>

<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/config/jquery.fancybox.config.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/ui/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/jquery.fileupload.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/config/jquery.fileupload.config.js"></script>
<script type="text/javascript">
    var upload_info_85470123 = upload_info_281658623 = {
            accepted_file_types: /(\.|\/)(gif|jpeg|jpg|png|tiff|doc|docx|txt|odt|xls|xlsx|pdf|ppt|pptx|pps|ppsx|mp3|m4a|ogg|wav|mp4|m4v|mov|wmv|flv|avi|mpg|ogv|3gp|3g2)$/i,
            accepted_file_types_ui : ".gif,.jpeg,.jpg,.png,.tiff,.doc,.docx,.txt,.odt,.xls,.xlsx,.pdf,.ppt,.pptx,.pps,.ppsx,.mp3,.m4a,.ogg,.wav,.mp4,.m4v,.mov,.wmv,.flv,.avi,.mpg,.ogv,.3gp,.3g2",
            max_file_size: 50971520,
            max_file_size_ui: "50MB"
    };
    var string_upload_file  = "Subir";
    var string_delete_file  = "Eliminando archivo";
    var string_progress             = "Progreso: ";
    var error_on_uploading          = "Ha ocurrido un error al intentar subir el archivo.";
    var message_prompt_delete_file  = "¿Esta seguro que quiere eliminar este archivo?";
    var error_max_number_of_files   = "Solo puede subir un archivo a la vez.";
    var error_accept_file_types     = "No se permite este tipo de extension.";
    var error_max_file_size         = "El archivo excede el tamaño 20MB que fue especificado.";
    var error_min_file_size         = "No puede subir un archivo vacío.";

    var base_url = "<?= base_url() ?>";
    var upload_a_file_string = "Subir";
</script>