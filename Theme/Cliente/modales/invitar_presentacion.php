<!-- Modal Invitar presentación-->
<div class="modal fade" id="invitar-presentacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <form onsubmit="return invitarAPresentar(this)">
            <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
              </div>
                <div class="response"></div>
                <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
                <div class="titulo-modal">
                  <b><?= l('Invitar a presentar') ?></b><br>
                  <?= l('Invita a la agencia a presentar su propuesta') ?>
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label class="margen-checkbox">
                            <input type="checkbox" name="presencial" value="1" class="pres"> <?= l('Presentación Presencial') ?>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label class="margen-checkbox">
                            <input type="checkbox" name="av" value="0" class="pres"> <?= l('Presentación Remota') ?>
                        </label>
                    </div>
                </div>

                <div class="form-group" style="margin-top:30px;">
                    <label class="label-control text-center"><?= l('Selecciona la fecha') ?></label>
                    <input class="form-control datetimepicker" name="presentacion" value="<?= date("d/m/Y H:i:s") ?>" style="" type="text">
                    <span class="material-input"></span>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <a href="javascript:closeModal('#invitar-presentacion')" class="swal2-cancel btn btn-danger" style="display: inline-block; width:100%"><?= l('Cancelar') ?></a>
                        </div>
                        <div class="col-sm-6">
                            <button type="submit" class="swal2-confirm btn btn-success" style="display: inline-block; width:100%"><?= l('Invitar') ?></button>
                        </div>
                    </div>
                </div>


                <input type="hidden" name="brief_id" value="0">
                <input type="hidden" name="status" value="3">
                <input type="hidden" name="fecha_modificacion" value="<?= date("d/m/Y H:i:s") ?>">

            </div>
        </form>
    </div>
</div>
<!-- Termina Modal Invitar presentación-->
<script>
    function fillDataBrief(id){
        $('#invitar-presentacion input[name="brief_id"]').val(id);
        $("#invitar-presentacion").find('.response').removeClass('alert alert-success').html('');
        var form = new FormData();
        form.append('search_field[]','id');
        form.append('search_text[]',id);
        remoteConnection('cliente/proyecto/briefs/json_list',form,function(data){
            data = JSON.parse(data);
            if(data.length>0){
                data = data[0];
                if(data.presencial==='activo'){
                    $("input[name='presencial']").prop('checked',true);
                }
                if(data.av==='activo'){
                    $("input[name='av']").prop('checked',true);
                }
                $("input[name='presentacion']").val(data.presentacion);
            }
            $("#invitar-presentacion").modal('show');
        });
    }

    function invitarAPresentar(f){
        var form = new FormData(f);
        remoteConnection('cliente/proyecto/briefs/update/'+$('#invitar-presentacion input[name="brief_id"]').val(),form,function(data){
            $(f).find('.response').addClass('alert alert-success').html('Invitado a presentar satisfactoriamente');
            setTimeout(function(){document.location.reload()},600);
        });
        return false;
    }

    $(document).on('change','.pres',function(){
        var val = $(this).prop('checked');
        $(".pres").prop('checked',false);
        $(this).prop('checked',val);
    })
</script>
