<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<style>
  #calificar .rank{
    cursor:pointer;
  }
  #calificar .rank::before {
    content: "\f006";
  }

  #calificar .rank.active::before, 
  #calificar .rank:hover::before {
    content: "\f005";
  }
</style>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                  <!-- Inicia Contenido -->
                  <div class="row">
                      <div class="col-sm-12 padding0">

                        <!-- Inicia una agencia -->
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 padding0 datos-proyecto">

                              <?= $this->load->view('Cliente/_proyecto') ?>

                              <div class="col-xs-12 col-sm-2 iconos-agencias-izquierda text-left">
                                  <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Editar Proyecto" href="#editar-proyecto" data-toggle="modal">
                                        <button class="btn btn-primary btn-round" id="btn-proyecto-informacion"><i class="material-icons">info</i> Información</button>
                                      </a>
                                  </div>
                                  <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Sala de juntas" href="<?= base_url('cliente/chat/salas/'.$proyecto->id) ?>">
                                        <button class="btn btn-primary btn-round" id="btn-proyecto-juntas">
                                          Preguntas y Respuestas
                                        </button>
                                      </a>
                                  </div>
                              </div>

                          </div>
                        </div>

                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion">
                          <div class="titulo-top">Agencias Participantes</div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12 propuestasDiv">

                          <?php foreach($proyecto->participantes->result() as $b): ?>
                              <div class="col-sm-3">
                                  <?php if(empty($b->calificacion_data)): ?>
                                    <div style="position: absolute;height: auto;right: 13px;z-index: 1;top: 12px;">
                                      <a href="javascript:void(0)" onclick="calificar(<?= $b->brief_id ?>)">
                                        <i class="fa fa-book fa-2x"></i>
                                      </a>
                                    </div>
                                  <?php else: ?>
                                    <div style="position: absolute;height: auto;right: 13px;z-index: 1;top: 12px;">
                                      <a href="javascript:void(0)" data-calificacion='<?= $b->calificacion_data ?>' onclick="verCalificacion(this)">
                                        <i class="fa fa-book fa-2x"></i>
                                      </a>
                                    </div>
                                  <?php endif ?>
                                  <div class="col-sm-12 fondo-proyecto-dashboard">
                                      <div class="img-perfil-proyecto text-center">
                                          <div style="background:url(<?= $this->querys->getLogo($b->id,2) ?>); width:100%; height:150px; background-size:auto 100%; background-position: center; background-repeat:no-repeat;">
                                            <img src="<?= $this->querys->getLogo($b->id,2) ?>" alt="Perfil Dashboard" class="img-responsive center-block image imagen-agencias-particioantes" style="visibility: hidden">
                                          </div>
                                          <span>
                                              
                                            <?php foreach($this->db->query("SELECT * FROM agencias INNER JOIN paises ON agencias.paises_id = paises.id WHERE agencias.id = $b->id")->result() as $pa): ?>
                                              <img src="<?= base_url('img/Banderas/'.$pa->icono) ?>" alt="<?= $pa->nombre ?>" class="img-responsive center-block img-circle" id="perfil-proyecto">
                                            <?php endforeach ?>
                                              
                                          </span>
                                      </div>
                                      <h1 class="titulo-proyecto"><?= $b->nombre ?></h1>

                                      <?php if($proyecto->porcentaje<100): ?>
                                          <?php if($b->status_brief==-1): ?>
                                            <div class="btn-propuesta-proceso">
                                                Propuesta rechazada
                                            </div>
                                          <?php elseif($b->status_brief==1): ?>
                                            <a href="#">
                                                <div class="btn-propuesta-agencias">
                                                  <i class="fa fa-envelope" aria-hidden="true"></i>  Invitación enviada
                                                </div>
                                            </a>
                                          <?php elseif($b->status_brief==2 && empty($b->propuesta)): ?>
                                            <div class="btn-propuesta-proceso">
                                                PROPUESTA EN PROCESO
                                            </div>
                                          <?php elseif($b->status_brief==4): ?>
                                            <a href="<?= base_url('files/'.$b->propuesta) ?>" target="_blank">
                                                <div class="btn-propuesta-agencias">
                                                  <i class="fa fa-file-text-o" aria-hidden="true"></i>  PROPUESTA GANADORA
                                                </div>
                                            </a>
                                          <?php else: ?>
                                            <a href="<?= base_url('files/'.$b->propuesta) ?>" target="_blank">
                                                <div class="btn-propuesta-agencias">
                                                  <i class="fa fa-file-text-o" aria-hidden="true"></i>  VER PROPUESTA
                                                </div>
                                            </a>
                                          <?php endif ?>
                                          <?php if($b->status_brief==2 && !empty($b->propuesta)): ?>
                                          <div class="contenedor-btn-rechazar">
                                              <a title="Rechazar" href="javascript:mostrarCancelBrief(<?= $b->brief_id ?>)">
                                                  <div class="col-sm-6">
                                                      <div class="btn-rechazar-agencia-rechazar">
                                                        <i class="fa fa-ban" aria-hidden="true"></i><br>Rechazar
                                                      </div>
                                                  </div>
                                              </a>
                                              <div class="col-sm-6">
                                                  <a title="Editar fecha" href="javascript:fillDataBrief(<?= $b->brief_id ?>)">
                                                    <div class="btn-rechazar-agencia-invitar btn-programar-agencia-invitar">
                                                      <i class="fa fa-lightbulb-o" aria-hidden="true"></i><br>
                                                        Invitar a presentar
                                                    </div>
                                                  </a>
                                              </div>
                                          </div>
                                          <?php elseif($b->status_brief==3): ?>
                                            <div class="contenedor-btn-rechazar2">
                                                <div class="col-sm-4">
                                                    <a title="Editar fecha" href="javascript:fillDataBrief(<?= $b->brief_id ?>)">
                                                      <div class="btn-rechazar-agencia-invitar">
                                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i><br>
                                                          Modificar presentación
                                                      </div>
                                                    </a>
                                                </div>
                                                <a title="Agradecer Participación" href="javascript:mostrarCancelBrief(<?= $b->brief_id ?>)" data-toggle="modal">
                                                    <div class="col-sm-4">
                                                        <div class="btn-rechazar-agencia-agradecer">
                                                          <i class="fa fa-quote-left" aria-hidden="true"></i><br>Agradecer Participación
                                                        </div>
                                                    </div>
                                                </a>
                                                <a title="Ganador" href="javascript:mostrarAgenciaGanadora(<?= $b->brief_id ?>,'<?= $b->nombre ?>','<?= $proyecto->nombre ?>')">
                                                    <div class="col-sm-4">
                                                        <div class="btn-rechazar-agencia-ganador">
                                                          <i class="fa fa-trophy" aria-hidden="true"></i><br>¡AGENCIA<br>GANADORA!
                                                        </div>
                                                    </div>
                                                </a>

                                            </div>
                                          <?php endif ?>
                                        <?php else: ?>
                                            <a href="<?= base_url('files/'.$b->propuesta) ?>" target="_blank">
                                                <div class="btn-propuesta-agencias">
                                                  <i class="fa fa-file-text-o" aria-hidden="true"></i>  VER PROPUESTA
                                                </div>
                                            </a>
                                            <?php if($b->status_brief==4): ?>
                                              <a title="Ganador">
                                                  <div class="" style="text-align: center;margin-top: 12px;">
                                                    <i class="fa fa-trophy" aria-hidden="true"></i> ¡AGENCIA GANADORA!
                                                  </div>
                                              </a>
                                            <?php endif ?>
                                        <?php endif ?>


                                  </div>
                              </div>
                          <?php endforeach ?>


                      </div>
                  </div>


                </div>
                <!-- Termina Contenido -->

                </div>
            </div>

        </div>


        <footer class="footer contenedor-footer">
            <?php include('footer.php');?>
        </footer>


    </div>

    <!-- Modal Registro -->
    <div class="modal fade" id="calificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 690px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">
            <form action="cliente/agencia/calificar" onsubmit="sendForm(this,'.response'); return false;">
              <div class="titulo-modal">
                <div class="row"><b>Califica la participación de la agencia</b></div>
                
                <?php foreach($this->querys->ranking_categorias()->result() as $e): ?>
                  <div class="row">
                      
                      <div class="col-xs-12" style="margin:10px 0">
                        <b><?= $e->nombre ?></b>
                      </div>
                      <?php foreach($e->items->result() as $ee): ?>
                        <div class="col-xs-8" style="text-align: left">
                          <?= $ee->nombre ?>
                        </div>
                        <div class="col-xs-4" style="text-align: right">
                          <?php for($i=1;$i<=5;$i++): ?>
                            <i class="rank fa fa-star-o <?= $i==1?'active':'' ?>" aria-hidden="true" data-val="<?= $i ?>"></i>
                          <?php endfor ?>
                          <input type="hidden" class="valoresOcultos" name="rank[values][<?= $ee->id ?>]" value="1">
                          <input type="hidden" name="rank[tags][<?= $ee->id ?>]" value="<?= $ee->nombre ?>">
                        </div>                      
                      <?php endforeach ?>
                  </div>
                <?php endforeach ?>

                 <div class="row">
                      <div class="col-xs-12" style="margin:10px 0">
                        <b>¿Deseas añadir alguna observación?</b>
                      </div>
                      <div class="col-xs-12" style="text-align: left">
                        <textarea name="rank[observaciones]" class="form-control" placeholder="Escribe aqui alguna observación que tengas con la participación de la agencia"></textarea>
                      </div>  
                  </div>
                  <div class="response"></div>
                  <div class="row">
                      <div class="col-xs-12" style="margin:10px 0">
                        <button type="submit" class="swal2-confirm btn btn-success">Calificar</button>
                        <button type="button" data-toggle="modal" data-dismiss="modal" class="swal2-confirm btn btn-danger">Cerrar</button>
                      </div>
                  </div>
                  <input type="hidden" id="agencias_id" name="rank[agencias_id]" value="0">
              </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Registro -->
<div class="modal fade" id="verCalificacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 690px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
            <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">            
              <div class="titulo-modal">
                <div class="row"><b>Califica la participación de la agencia</b></div>
                
                <?php foreach($this->querys->ranking_categorias()->result() as $e): ?>
                  <div class="row">
                      
                      <div class="col-xs-12" style="margin:10px 0">
                        <b><?= $e->nombre ?></b>
                      </div>
                      <?php foreach($e->items->result() as $n=>$ee): ?>
                        <div class="col-xs-8" style="text-align: left">
                          <?= $ee->nombre ?>
                        </div>
                        <div class="col-xs-4 n<?= $n ?>" style="text-align: right">
                          <?php for($i=1;$i<=5;$i++): ?>
                            <i class="rank fa fa-star-o nstars <?= $i==1?'active':'' ?>" aria-hidden="true" data-val="<?= $i ?>"></i>
                          <?php endfor ?>
                        </div>                      
                      <?php endforeach ?>
                  </div>
                <?php endforeach ?>

                 <div class="row">
                      <div class="col-xs-12" style="margin:10px 0">
                        <b>¿Deseas añadir alguna observación?</b>
                      </div>
                      <div class="col-xs-12" style="text-align: left">
                        <textarea name="rank[observaciones]" class="form-control" placeholder="Escribe aqui alguna observación que tengas con la participación de la agencia"></textarea>
                      </div>  
                  </div>
                  <div class="response"></div>
                  <div class="row">
                      <div class="col-xs-12" style="margin:10px 0">                        
                        <button type="button" data-toggle="modal" data-dismiss="modal" class="swal2-confirm btn btn-danger">Cerrar</button>
                      </div>
                  </div>                  
              </div>
        </div>
    </div>
</div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->


<!-- Termina Modal Registro -->

<script>
  function calificar(id){
    $("#calificar .rank").removeClass('fa-star').addClass('fa-star-o');
    $("#calificar .valoresOcultos").val(1);
    $("#calificar").modal('toggle');
    $("#calificar #agencias_id").val(id);
  }

  function verCalificacion(l){    
    var cal = $(l).data('calificacion');    
    if(typeof(cal)!='undefined'){
      cali = cal.calificaciones;
      $(".nstars").removeClass('fa-star').addClass('fa-star-o');
      for(var i in cali){
        var c = cali[i][0];
        c = parseInt(c);
        for(var k=1;k<=c;k++){
          $(".n"+i).find('i[data-val="'+k+'"]').removeClass('fa-star-o').addClass('fa-star');
        }
      }
      $("#verCalificacion").find('textarea').val(cal.observaciones);
    }
    $("#verCalificacion").modal('toggle');
  }

  $(document).on('click','.rank',function(){
      var val = parseInt($(this).data('val'));
      var ranks = $(this).parent();
      ranks.find('.rank').removeClass('active');
      for(var i=1;i<=val;i++){
        ranks.find('.rank[data-val="'+i+'"]').addClass('active');
      }
      ranks.find('.valoresOcultos').val(i);
  });
</script>
</html>
