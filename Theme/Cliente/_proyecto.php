<div class="col-xs-12 col-sm-3">
  <div class="card card-product" data-count="3">
    <div class="img-historial-proyecto">
      <div class="logo-agencia">
        <img class="center-block img-responsive" src="<?= $proyecto->imagen ?>" alt="<?= $proyecto->nombre ?>" id="foto-proyecto">
      </div>
    </div>
    <div class="card-content">
      <div class="card-title"><b><?= $proyecto->nombre ?></b></div>
      <div class="progress" id="progress-bar">
        <div class="progress-bar progress-bar-striped active progress-bar-<?= $proyecto->barra ?>" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: <?= $proyecto->porcentaje ?>%">
          <b><big><?= $proyecto->porcentaje ?>%</big></b>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <?php
        $this->db->select('paises.icono, paises.nombre');
        $this->db->join('proyectos_paises','proyectos_paises.proyectos_id = proyectos.id')
             ->join('paises','paises.id = proyectos_paises.paises_id');
        $icono = $this->db->get_where('proyectos',array('proyectos.id'=>$proyecto->id));
        if($icono->num_rows()>0):
      ?>

      <?php endif ?>
      <b>Marca: <?= $proyecto->marcas ?></b><br>
      <b>Producto: <?= $proyecto->productos ?></b><br>      
      <b>País: <?= $icono->row()->nombre ?> <img src="<?= base_url() ?>img/Banderas/<?= $icono->row()->icono ?>" alt="Perfil Dashboard" class="img-responsive center-block img-circle" id="perfil-proyecto"></b><br>
    </div>
  </div>
</div>
<div class="col-xs-12 col-sm-7 contenedor-descripcion-agencia2 margen-movil-servicios">
  <div class="col-sm-12 texto-gris">
    <div class="col-xs-4 col-sm-4">
      <div class="card card-stats">
        <div class="card-header" data-background-color="blue"><i class="material-icons">timeline</i></div>
        <div class="card-content">
          <p class="category">Fase:</p><br>
          <div class="card-title">
            <span class="porcentaje-agencia texto-rojo">
              <?php 
                if($proyecto->invitaciones->num_rows()>0): 
                echo $proyecto->fase?$proyecto->fase->nombre:'Finalizado' ;
                else: ?>
                Proyecto por Iniciar
                <?php endif ?>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-4 col-sm-4">
      <div class="card card-stats">
        <div class="card-header" data-background-color="blue"><i class="material-icons">timer_off</i></div>
        <div class="card-content">
          <p class="category">Fin de la fase</p><br>
          <div class="card-title">
            <span class="porcentaje-agencia texto-morado">
              <?= $proyecto->fase?date("d/m/Y",strtotime($proyecto->fase->hasta)):date("d/m/Y",strtotime($this->db->query('SELECT * FROM proyectos_fechas where proyectos_id = '.$proyecto->id.' ORDER BY desde DESC')->row()->hasta)) ?>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-4 col-sm-4">
      <div class="card card-stats">
        <div class="card-header" data-background-color="blue"><i class="material-icons">assignment_ind</i></div>
        <div class="card-content">
          <p class="category margen-titulos-agencias-tarjetas">Agencias</p><br>
          <div class="card-title">
            <span class="porcentaje-agencia texto-morado"><?= $proyecto->briefs->num_rows() ?></span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 texto-gris">
    
  </div>
  
</div>
