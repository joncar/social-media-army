<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">
                  <!-- Inicia Contenido -->
                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Historial de proyectos</div></div>
                  </div>

                  <div class="row">
                      <div class="col-xs-3 col-sm-2">
                          <select class="selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7" tabindex="-98">
                          <option class="bs-title-option" value="">Organizar por:</option>
                              <option selected="">Organizar por:</option>
                              <option value="2">A-Z</option>
                              <option value="3">Otro</option>
                          </select>
                      </div>
                      <div class="col-xs-9 col-sm-10 pull-left">
                          <div class="col-xs-7 col-sm-9 col-md-9 col-lg-10">
                              <div id="wrap">
                                <form action="" autocomplete="on" id="buscador"><input id="search" name="search" type="text" placeholder="Buscar agencia"><i class="material-icons">search</i></form>
                              </div>
                          </div>
                          <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1">
                              <div id="btn-filtros">
                                  <div id="accordion" role="tablist" aria-multiselectable="true">
                                      <div role="tab" id="headingOne">
                                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                              <i class="material-icons">filter_list</i> <small>Filtros</small>
                                          </a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-xs-2 col-sm-1 text-center">
                              <div class="icono-favoritos">
                                  <label class="fancy-checkbox" title="Mostrar sólo favoritos">
                                      <input type="checkbox" />
                                      <i class="fa fa-heart-o unchecked"></i>
                                      <i class="fa fa-heart checked"></i>
                                  </label>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12">
                          <?php include('menu-lateral-historial.php');?>
                      </div>
                  </div>
                   <div><?= $output ?></div>
                </div>
              </div>
              <!-- Termina Contenido -->

              <footer class="footer contenedor-footer"><?php include('footer.php');?></footer>

          </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
    function buscar(data){
        $("#ajax_list").html('<p>Cargando proyectos por favor espere</p>');

        remoteConnection('cliente/proyecto/historial/json_list',data,function(data){
                data = JSON.parse(data);
                console.log(data);
                $("#ajax_list").html('');
                $("#totalRows").html(data.length);
                for(var i in data){
                    var element = $("#agencia_element").clone();
                    element.show();
                    var html = element.html();
                    for(var k in data[i]){
                        html = html.replace('{'+k+'}',data[i][k]);
                    }
                    element.html(html);
                    $("#ajax_list").append(element);
                }
                if(data.length===0){
                    $("#ajax_list").html('<p>Proyectos no encontrados, intente con otros criterios de busqueda</p>');
                }
        });
        return false;
    }

    function triggerSearch(){
        var Form = new FormData();
        var x = 0;
        $('*[name="search_text[]"]').each(function(){
            if($(this).val()!==''){
                var criterios = $(this).val();
                if(typeof(criterios)==='array'){
                    for(var k in criterios){
                        Form.append('search_text[]',criterios[k]);
                        Form.append('search_field[]',$(this).data('name'));
                    }
                }else{
                    Form.append('search_text[]',criterios);
                    Form.append('search_field[]',$(this).data('name'));
                }
            }
            x++;
            if(x===$('*[name="search_text[]"]').length){
                buscar(Form);
            }
        });
        return false;
    }

    (function($){
        $(function(){
            buscar(new FormData());
        });
    })(jQuery);

    $(document).on('change','input[name="search_text[]"]',function(){      
        $('form').submit();
    });
</script>
</html>
