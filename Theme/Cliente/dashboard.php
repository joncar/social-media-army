<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
<link rel="stylesheet" href="<?= base_url() ?>Theme/Cliente/assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?= base_url() ?>Theme/Cliente/assets/css/owl.theme.default.min.css">
<script src="<?= base_url() ?>Theme/Cliente/assets/js/owl.carousel.js"></script>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>
            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top"><?= l('Proyectos recientes') ?></div></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 padding0 indicaciones-progreso hidden-xs">
                            <span class="progreso-top"><i class="fa fa-square" style="color: #85dc00;" aria-hidden="true"></i>  <?= l('Proyecto Finalizado') ?></span>
                            <span class="progreso-top"><i class="fa fa-square" style="color: #EE497A;" aria-hidden="true"></i>  <?= l('Proyecto en Desarrollo') ?></span>
                            <span class="progreso-top"><i class="fa fa-square texto-azul" aria-hidden="true"></i>  <?= l('Proyecto por Iniciar') ?></span>
                        </div>
                        <!-- movil-->
                        <div class="col-xs-12 col-sm-12 padding0 indicaciones-progreso visible-xs">
                            <div class="col-xs-4 progreso-top">
                              <i class="fa fa-square texto-verde" aria-hidden="true"></i><br>
                                <?= l('Proyecto Completo tab') ?>
                            </div>
                            <div class="col-xs-4 progreso-top">
                              <i class="fa fa-square texto-amarillo" aria-hidden="true"></i><br>
                                <?= l('Proyecto en Desarrollo tab') ?>
                            </div>
                            <div class="col-xs-4 progreso-top">
                              <i class="fa fa-square texto-azul" aria-hidden="true"></i><br>
                                <?= l('Proyecto por Iniciar tab') ?>
                            </div>
                        </div>

                        <div class="col-sm-12 padding0 owl-carousel owl-theme">

                          <?php
                            $this->db->select('proyectos.*');                            
                            $this->db->order_by('proyectos.id','DESC');
                            $this->db->join('proyectos_user','proyectos_user.proyectos_id = proyectos.id');
                            $this->db->group_by('proyectos.id');
                            $proyectos = $this->db->get_where('proyectos',array());
                          ?>
                          <?php foreach($proyectos->result() as $p):
                            $this->load->model('querys');
                            $p = $this->querys->showResult($p->id,TRUE);
                          ?>
                            <div class="col-xs-12 clearfix item">
                                <a href="<?= base_url('cliente/proyecto/verProyecto/'.$p->id) ?>">
                                    <div class="fondo-proyecto-dashboard">
                                      <div class="img-perfil-proyecto text-center">
                                          <div style="background:url(<?= $p->imagen ?>) no-repeat; background-size:cover; width:100%; height:130px; background-position:center center;  background-size: contain;"></div>
                                          <span>
                                              <!--<img src="<?= base_url() ?>Theme/Cliente/assets/img/Banderas/mexico.jpg" alt="Perfil Dashboard" class="img-responsive center-block img-circle" id="perfil-proyecto">-->
                                              <?php                                                 
                                                foreach($p->paises_res->result() as $pa): ?>
                                                <img src="<?= base_url('img/Banderas/'.$pa->icono) ?>" alt="<?= $pa->nombre ?>" class="img-responsive center-block img-circle" id="perfil-proyecto">
                                              <?php endforeach ?>
                                          </span>
                                      </div>
                                      <h1 class="titulo-proyecto">
                                        <?= $p->nombre ?><br/>
                                        <small><?= @$p->paises_res->row()->nombre ?> - <?= ucfirst($p->creacion) ?></small>
                                      </h1>
                                      <div class="progress" id="progress-bar">
                                          <div class="progress-bar progress-bar-striped active <?= 'progress-bar-'.$p->barra ?>" role="progressbar" aria-valuenow="<?= $p->porcentaje ?>" aria-valuemin="100" aria-valuemax="10" style="width: <?= $p->porcentaje ?>%">
                                              <b><big><?= $p->porcentaje ?>%</big></b>
                                          </div>
                                      </div>
                                    </div>
                                </a>
                            </div>
                          <?php endforeach ?>

                        </div>

                    </div>

                    <?php 
                        $agenciasPaises = $this->querys->get_agencias_por_paises();
                        $agencias = 0;
                        $porcentajes = 0;
                        $ongs = 0;
                    ?>

                    <div class="row">
                        <div class="col-sm-12 padding0 titulo-secccion titulo-original-seccion">
                          <div class="titulo-top">
                            <?= l('Agencias') ?>: <?= $this->db->get('agencias')->num_rows() ?>
                            <span style="margin-left:25px">&nbsp;</span><?= l('paises') ?>: <?= $agenciasPaises->num_rows() ?>
                            <span style="margin-left:25px">&nbsp;</span><?= l('Organizaciones') ?>: <?= $this->db->get_where('organizaciones')->num_rows() ?>
                          </div>
                        </div>
                    </div>

                    <div class="row">
                      <!------------------ Tabla agencias por paises --------------->
                      <div class="col-md-3">
                        <div class="table-responsive table-sales" style="height: 300px;">
                          <table class="table">
                            <thead>
                              <tr  style="font-size: 10px;">
                                <th colspan="2"><?= l('País') ?></th>
                                <th style="text-align: center;"><?= l('Agencias <br/>de grupo') ?></th>
                                <th style="text-align: center;"><?= l('Agencias <br/>independientes') ?></th>                                
                              </tr>
                            </thead>
                            <tbody>
                              
                              <?php                                 
                                foreach($agenciasPaises->result() as $p):                                   
                              ?>
                                <tr>
                                  <td>
                                    <div class="flag">
                                      <img src="<?= $p->bandera ?>">
                                    </div>
                                  </td>
                                  <td><a href="<?= base_url() ?>cliente/agencia/agencias?pais=<?= $p->id ?>"><?= $p->nombre ?></a></td>
                                  <td class="text-center">
                                    <?= $p->agencias ?>
                                  </td>
                                  <td class="text-center">
                                    <?= $p->porcentaje ?>
                                  </td>
                                </tr>
                                <?php 
                                  $agencias+= $p->agencias;
                                  $porcentajes+= $p->porcentaje;                                  
                                ?>
                              <?php endforeach ?>
                                <tr>
                                  <td>
                                    <div class="flag">
                                      <img src="<?= base_url('img/Banderas/948cc-global.png') ?>">
                                    </div>
                                  </td>
                                  <td>
                                    <a href="<?= base_url() ?>cliente/agencia/agencias">
                                      TOTALES
                                    </a>
                                  </td>
                                  <td class="text-center">
                                    <?= $agencias ?>
                                  </td>
                                  <td class="text-center">
                                    <?= $porcentajes ?>
                                  </td>
                                </tr>


                            </tbody>
                          </table>
                          </div>
                        </div>
                        <!------------------ Mapa --------------->
                        <div class="col-md-6 ml-auto mr-auto">
                          <div id="worldMap" style="height: 350px; width:100%;">                              
                          </div>
                        </div>
                        <!------------------ Tabla agencias por organizaciòn --------------->
                        <div class="col-md-3">
                        <div class="table-responsive table-sales" style="height: 300px;">
                          <table class="table">
                            <thead>
                              <tr style="font-size: 10px;">
                                <th><?= l('Organización') ?></th><br>
                                <th style="text-align: center;"><?= l('Agencias') ?></th>
                              </tr>
                            </thead>
                            <tbody>
                              
                              <?php
                                $ong = $this->querys->get_agencias_por_organizacion();
                                foreach($ong->result() as $p): 
                              ?>
                                <tr>
                                  <td>
                                    <a href="<?= base_url('cliente/agencia/agencias/') ?>?organizacion=<?= $p->id ?>">
                                      <?= $p->nombre; ?>
                                    </a>
                                  </td> 
                                  <td style="text-align: center;">
                                    <?= $p->agencias; ?>
                                  </td> 
                                </tr>
                              <?php endforeach ?>
                                


                            </tbody>
                          </table>
                          </div>
                        </div>

                </div>

                <?php
                  $proyectos = $this->querys->get_ultimas_agencias_ganadoras();
                ?>
                <div class="row">
                    <div class="col-sm-12 padding0 titulo-secccion titulo-original-seccion"><div class="titulo-top"><?= l('Asignaciones:') ?> <?= $proyectos->num_rows() ?></div></div>
                </div>
                    

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 padding0">
                            

                            <div class="col-xs-12 col-sm-12">
                                <div class="col-xs-12 col-sm-12 contenedor-agencias-dashboard">                                    
                                    <div class="col-xs-12 col-sm-12 padding0 owl-carousel owl-theme" data-loop="false">                                      
                                        <?php foreach($proyectos->result() as $p): ?>                                          
                                              <div class="col-xs-12 links-newsfeed item">
                                                <a href="<?= base_url('cliente/agencia/verAgencia/'.$p->id) ?>">
                                                  <div style="background:url(<?= base_url('img/agencias/'.$p->logo) ?>) no-repeat; background-size:100%; width:100%; height:130px; background-position:center; background-size: contain;"></div>
                                                </a><br>
                                                <a href="<?= base_url('cliente/proyecto/verProyecto/'.$p->proyecto->id) ?>">
                                                  <div class="text-center texto-agencias-dashboard titulo-proyecto" style="color: #397dc3;">
                                                    <?= $p->proyecto->nombre ?>
                                                  </div>
                                                </a>
                                                  <div class="text-center texto-agencias-dashboard titulo-proyecto">                                                    
                                                    <center><img src="<?= base_url('img/Banderas/'.$p->bandera) ?>" alt="" style="width:25px" class="text-center"></center>
                                                    <a href="<?= base_url('cliente/agencia/verAgencia/'.$p->id) ?>"><?= $p->nombre ?></a><br>
                                                    <small>
                                                      <?= $p->pais ?> - <?= ucfirst(strftime('%B %Y',strtotime($p->fecha_ganadora))) ?>
                                                    </small>
                                                  </div>
                                              </div>
                                          
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <script src="https://apps.elfsight.com/p/platform.js" defer></script>
            <div class="elfsight-app-9a75bcec-89d5-4add-9de3-c1c6ebf85a6d"></div>
            <br><br>

            <footer class="footer contenedor-footer">
                <?php include('footer.php');?>
            </footer>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<script src="<?= base_url() ?>Theme/Cliente/assets/js/main.js"></script>
<script>
  $(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
      margin: 10,
      nav: true,
      loop: false,
      items:4,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
        1000: {
          items: 4
        }
      }
    })
  })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        //md.initSliders()
        //demo.initFormExtendedDatetimepickers();
    });
</script>

<script>
  $(document).ready(function() {    
        var mapData = <?php 
          $paises = array();
          foreach($agenciasPaises->result() as $p){
            $paises[$p->abreviacion] = $p->agencias;
          }
          echo json_encode($paises);
        ?>;

        $('#worldMap').vectorMap({
          map: 'world_mill_en',
          backgroundColor: "transparent",
          zoomOnScroll: false,
          regionStyle: {
            initial: {
              fill: '#e4e4e4',
              "fill-opacity": 0.9,
              stroke: 'none',
              "stroke-width": 0,
              "stroke-opacity": 0
            }
          },

          series: {
            regions: [{
              values: mapData,
              scale: ["#f9e6ef", "#931651"],
              normalizeFunction: 'polynomial'
            }]
          },
        });      
  });
</script>


</html>
