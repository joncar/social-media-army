<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">
                	<?= $output ?>
                  <!-- Termina Contenido -->
                </div>
            </div>

        </div>

    </div>

    <footer class="footer contenedor-footer">
        <?php include('footer.php');?>
    </footer>
    
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
</html>
