<meta charset="utf-8" />
<link rel="shortcut icon" href="<?= base_url().'img/favicon.ico' ?>" type="image/x-icon"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title><?= empty($title)?'BriefSuite':$title ?></title>
<script>
	window.afterLoad = [];
</script>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />
<!-- Canonical SEO -->
<link rel="canonical" href="#" />
<!--  Social tags      -->
<meta name="keywords" content="Keywords">
<meta name="description" content="Description">
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="BIMBO">
<meta itemprop="description" content="Description">
<meta itemprop="image" content="http://s3.amazonaws.com/creativetim_bucket/products/51/opt_mdp_thumbnail.jpg">
<!-- Bootstrap core CSS     -->
<link href="<?= base_url() ?>Theme/Cliente/assets/css/bootstrap.min.css" rel="stylesheet" />
<!--  Material Dashboard CSS    -->
<link href="<?= base_url() ?>Theme/Cliente/assets/css/material-dashboard.css" rel="stylesheet" />
<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="<?= base_url() ?>Theme/Cliente/assets/css/demo.css" rel="stylesheet" />
<!--  Estilos Personales    -->
<link href="<?= base_url() ?>Theme/Cliente/assets/css/main.css?v=2.3" rel="stylesheet" />
<link href="<?= base_url() ?>Theme/Cliente/assets/css/queries.css" rel="stylesheet" />
<link href="<?= base_url() ?>Theme/Cliente/assets/css/cliente.css" rel="stylesheet" />

<!--     Fonts and icons     -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	google.charts.load('current', {'packages':['gantt'], 'language': 'es'});
</script>
<!-- Script Proceso de Registro de Proyecto -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.migrate/1.4.1/jquery-migrate.min.js"></script>
<?php
if(!empty($css_files)):
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
<?php endforeach; ?>
<?php endif; ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css" type="text/css" rel="stylesheet" />
<style>
	.dropzone{
		padding: 0;
		border: 1px solid #e2e5ed;
	}
	.dropzone .dz-preview .dz-image{
		border-radius:0;
	}
	.dz-error-message{
		top: 60px !important;
		opacity: 1 !important;
	}

	.dropzone.dz-started .dz-message {
	    display: block;
	    /*position: absolute;*/
	    width: 100%;
	    margin:10px auto;
	}

	.dropzone .dz-preview .dz-image{
		width:60px;
		height:60px;
	}
	.dropzone .dz-preview .dz-image img {	    
	    width: 100%;
	}

	.dz-size{
		display: none;
	}

	.dropzone .dz-preview .dz-details .dz-filename:not(:hover){			
		/*position: absolute;
		top: 0;
		left: -50%;*/
		margin:0 10px;
	}
</style>