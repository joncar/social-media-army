<div class="sidebar-wrapper">

    <div class="user" style="border:0px; margin-top:30px;">
        <div class="photo">
            <img src="<?= $this->user->logo ?>" alt="Perfil Dashboard" class="img-responsive center-block">
        </div>

        <div class="clearfix"></div>
    </div>

    <ul class="nav">
        <li class="active">
            <a href="<?= base_url('panel') ?>">
                <i class="material-icons">home</i><p> <?= l('Inicio') ?> </p>
            </a>
        </li>
        <li>
            <a href="<?= base_url() ?>cliente/agencia/agencias">
                <i class="material-icons">assignment_ind</i><p> <?= l('Agencias') ?> </p>
            </a>
        </li>
        <!--<li>
            <a href="<?= base_url() ?>cliente/proyecto/proyectos">
                <i class="material-icons">movie_filter</i><p> Proyectos </p>
            </a>
        </li>-->
        <li>
            <a href="<?= base_url() ?>cliente/proyecto/historial">
                <i class="material-icons">today</i><p> <?= l('Proyectos') ?> </p>
            </a>
        </li>
    </ul>


    <!--
    <div class="logo autor text-center">
        <b>VERSIÓN 11.07.18</b>
    </div>-->

</div>
