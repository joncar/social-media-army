<?php
/** Jquery UI */
$this->load_js_jqueryui();
$this->set_css($this->default_theme_path . 'agencias/css/flexigrid.css');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');

if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_js($this->default_theme_path . 'agencias/js/cookies.js');
$this->set_js($this->default_theme_path . 'agencias/js/jquery.form.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js');
$this->set_js($this->default_theme_path . 'agencias/js/jquery.printElement.min.js');
$this->set_js($this->default_theme_path . 'agencias/js/pagination.js');
/** Fancybox */
$this->set_css('assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js');
$this->set_js($this->default_theme_path . 'agencias/js/flexigrid.js');
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
    var URL = '<?= base_url() ?>';
</script>


<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if ($success_message !== null) { ?>style="display:block"<?php } else { ?> style="display:none" <?php } ?>><?php if ($success_message !== null) { ?>
        <p><?php echo $success_message; ?></p>
    <?php }
    ?></div>

<?php echo form_open($ajax_list_url, 'method="post" id="buscador" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>

<div class="container-fluid flexigrid">
    <div class="row">
        <div class="col-sm-12 padding0 titulo-secccion">
            <div class="titulo-top">
                <?php $paging_starts_from = "<span id='page-starts-from' class='page-starts-from'>1</span>"; ?>
                <?php $paging_ends_to = "<span id='page-ends-to' class='page-ends-to'>" . ($total_results < $default_per_page ? $total_results : $default_per_page) . "</span>"; ?>
                <?php $paging_total_results = "<span id='total_items' class='total_items'>$total_results</span>" ?>
                <?php
                    echo str_replace(array('{start}', '{end}', '{results}'), array($paging_starts_from, $paging_ends_to, $paging_total_results),'Mostrando {results} agencias'
                );
                ?></div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-3 col-sm-2">
            <select name="ordenar" class="selectpicker" data-style="btn btn-danger btn-round" title="Single Select" data-size="7" tabindex="-98">
                <option class="bs-title-option" value="nombre-ASC">Organizar por:</option>
                <option value="nombre-ASC" selected="">Organizar por:</option>
                <option value="nombre-ASC">A-Z</option>                
                <option value="categorias_agencias-ASC">Categoría</option>                
                <option value="tipos_certificacion-ASC">Validación</option>
            </select>
        </div>
        <div class="col-xs-9 col-sm-10 pull-left">
            <div class="col-xs-7 col-sm-9 col-md-9 col-lg-10">
                <div id="wrap-filtros">
                    <input id="search" name="search_text[]" type="text" placeholder="Buscar agencia" value="<?= @$_GET['q'] ?>"> 
                    <input type="hidden" name="search_field[]" value="agencias_con_filtros.nombre">
                    <i class="material-icons">search</i>
                </div>
            </div>
            <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1">
                <div id="btn-filtros">
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <div role="tab" id="headingOne">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                <i class="material-icons">filter_list</i> <small>Filtros</small>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-2 col-sm-1 text-center">
                <div class="icono-favoritos">
                    <label class="fancy-checkbox" title="Mostrar solo favoritos" style="cursor:pointer;">
                        <input type="checkbox" name="favorito" value="<?= get_instance()->user->id ?>" />
                        <i class="fa fa-heart-o unchecked filtrarfavorito"></i>
                        <i class="fa fa-heart checked filtrarfavorito"></i>
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?php get_instance()->load->view('Cliente/menu-lateral-agencias.php');?>
        </div>
    </div>
    <div class="ajax_list">
        Consultando por favor espere...
    </div>
    <div class="paginacion">
        <ul class="pagination"></ul>
    </div>
    <div class="col-xs-12 col-sm-12 padding0 checkbox-radios btn-seleccionar-agencias center-block">
        <div class="col-sm-4 col-sm-offset-4 col-lg-4 col-lg-offset-4 margen-seleccionar-agencias">
            <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal">
              <button type="button" class="btn btn-primary btn-round" id="btn-enviar-brief"><small>Enviar Brief a los seleccionados</small></button>
            </a>
        </div>
    </div>
    <input type="hidden" name="per_page" class="per_page" id="per_page" value="10">
    <button class="ajax_refresh_and_loading" style="display: none;"></button>
</div>


<?php echo form_close() ?>
