<?php
/** Jquery UI */
$this->load_js_jqueryui();
$this->set_css($this->default_theme_path . 'generic/css/flexigrid.css');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');

if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_js($this->default_theme_path . 'generic/js/cookies.js');
$this->set_js($this->default_theme_path . 'generic/js/jquery.form.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js');
$this->set_js($this->default_theme_path . 'generic/js/jquery.printElement.min.js');
$this->set_js($this->default_theme_path . 'generic/js/pagination.js');
/** Fancybox */
$this->set_css('assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js');
$this->set_js($this->default_theme_path . 'generic/js/flexigrid.js');
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>


<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if ($success_message !== null) { ?>style="display:block"<?php } else { ?> style="display:none" <?php } ?>><?php if ($success_message !== null) { ?>
        <p><?php echo $success_message; ?></p>
    <?php }
    ?></div>

<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<div class="material-datatables flexigrid">
        <div class="dataTables_wrapper form-inline dt-bootstrap" id="datatables_wrapper">
            <div class="row botones-ajustes">
                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <?php if(!$unset_add):?>
                            <div class="col-sm-1">
                                <a href="<?php echo $add_url?>" title="<?php echo $this->l('list_add'); ?>">
                                    <button type="button" class="btn btn-primary">
                                        <i class="fa fa-plus-circle"></i>
                                    </button>
                                </a>
                            </div>
                        <?php endif; ?>
                        <div class="col-sm-1">
                            <button title='Realizar busqueda' type="button" class="searchActionClick dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="">
                                <i class="fa fa-search bigger-110 blue"></i>
                            </button>
                        </div>
                        <div class="col-sm-1">
                            <button title='Refrescar listado'  type="button" class="ajax_refresh_and_loading dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="">
                                <span>
                                    <i class="ace-icon fa fa-refresh bigger-110 grey"></i>
                                </span>
                            </button>
                        </div>
                        <?php if(!$unset_export): ?>
                        <div class="col-sm-1">
                            <a href="<?php echo $export_url; ?>" title="exportar a excel">
                                <button type="button" class="btn btn-white btn-primary btn-bold">
                                    <i class="fa fa-file-excel-o bigger-110 orange"></i>
                                </button>
                            </a>
                        </div>
                        <?php endif; ?>
                        <?php if(!$unset_print): ?>
                            <div class="col-sm-1">
                                <a href="<?php echo $print_url; ?>" title="imprimir listado">
                                    <button type="button" class="btn btn-white btn-primary btn-bold">
                                        <i class="fa fa-print bigger-110 grey"></i>
                                    </button>
                                </a>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>

            <?php if (!empty($list)): ?>
            <div class="row">
                <div class="col-sm-12">
                <table aria-describedby="datatables_info" role="grid" id="datatables" class="table table-striped table-no-bordered table-hover dataTable dtr-inline"style="width: 100%;" cellspacing="0" width="100%">
                    <thead>
                        <tr role="row">
                            <?php foreach ($columns as $column): ?>
                                <th class="field-sorting sorting" rel='<?php echo $column->field_name ?>' style="padding:10px !important;">
                                    <?php echo $column->display_as ?>
                                        <span id="th_<?= $column->field_name ?>"></span>
                                        <?php if (isset($order_by[0]) && $column->field_name == $order_by[0]) { ?>
                                            <?php if ($order_by[1] == 'asc'): ?>
                                                <i class="fa fa-arrow-up"></i>
                                            <?php else: ?>
                                                <i class="fa fa-arrow-down"></i>
                                        <?php endif; ?>
                                    <?php } ?>
                                </th>
                            <?php endforeach ?>
                            <?php if (!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)): ?>
                            <th align="right" abbr="tools" axis="col1" class="" width='20%'>
                            <div class="text-right" style="padding:10px;">
                            <?php echo $this->l('list_actions'); ?>
                            </div>
                            </th>
                            <?php endif ?>
                        </tr>
                        <tr class="hide searchRow">
                                <?php foreach($columns as $column):?>
                                <th>
                                    <?php if(!in_array($column->field_name,$unset_searchs)): ?>
                                        <input type="hidden" name="search_field[]" value="<?= $column->field_name ?>">
                                        <?php
                                           $value = '';
                                           if(!empty($_POST['search_text']) && !empty($_POST['search_field'])){
                                               foreach($_POST['search_field'] as $n=>$v){
                                                   if($v==$column->field_name && !empty($_POST['search_text'][$n])){
                                                       $value = $_POST['search_text'][$n];
                                                   }
                                               }
                                           }
                                           echo form_input('search_text[]',$value,'style="width:100%" class="form-control" placeholder="Filtrar por '.$column->display_as.'"')
                                        ?>
                                    <?php endif ?>
                                </th>
                                <?php endforeach?>
                        </tr>
                </thead>
                <tbody class="ajax_list">
                    <?php echo $list_view?>
                </tbody>
        </table>

        </div>
     </div>
     <?php else: ?>
        Sin datos para mostrar
    <?php endif; ?>

     <div class="row">
        <div class="col-sm-5">
            <div aria-live="polite" role="status" id="datatables_info" class="dataTables_info">
                <?php $paging_starts_from = "<span id='page-starts-from' class='page-starts-from'>1</span>"; ?>
                <?php $paging_ends_to = "<span id='page-ends-to' class='page-ends-to'>" . ($total_results < $default_per_page ? $total_results : $default_per_page) . "</span>"; ?>
                <?php $paging_total_results = "<span id='total_items' class='total_items'>$total_results</span>" ?>
                <?php
                echo str_replace(array('{start}', '{end}', '{results}'), array($paging_starts_from, $paging_ends_to, $paging_total_results), $this->l('list_displaying')
                );
                ?>
            </div>
        </div>

        <div class="col-sm-7">
            <div id="datatables_paginate pageContent" class="dataTables_paginate paging_full_numbers">
                <ul class="pagination">
                </ul>
            </div>
        </div>
     </div>

  </div>
</div>
<input type="hidden" name='per_page' value="10">
<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
<?php echo form_close() ?>
