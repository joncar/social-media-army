<?php
    $this->db->order_by('fecha_registro','ASC');
    $desde = $this->db->get_where('proyectos');
    $desde = $desde->num_rows()>0?$desde->row()->fecha_registro:date("Y-m-d");        
    $desde = date("d/m/Y",strtotime($desde));
    $hasta = date("d/m/Y");
?>
<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
    <div class="panel-body contenedor-filtros-desplegable">

      <div class="card-content">
          <div class="menu-filtros">
              <ul class="nav nav-pills nav-pills-warning">
                  <li class="active"><a href="#pill1" data-toggle="tab" aria-expanded="true"><?= l('Filtros Generales') ?></a></li>
              </ul>
          </div>
          <div class="tab-content">
              <div class="tab-pane active" id="pill1">

                  <div class="col-sm-12">
                      <div class="col-sm-4 border-lateral">
                          <div class="col-sm-12">
                              <div class="select-agencias">
                                  <select class="selectpicker" name="filtros[paises][]" data-style="select-with-transition" multiple title="<?= l('País') ?>" data-size="7">
                                    <option disabled><?= l('País') ?></option>
                                    <?php foreach($this->db->get_where('paises')->result() as $t): ?>
                                          <option value="<?= $t->id ?>"><?= $t->nombre ?></option>
                                    <?php endforeach ?>
                                  </select>
                              </div>
                          </div>

                          <div class="col-sm-12">
                            <div class="select-agencias">
                                <select class="selectpicker" name="filtros[organizacion][]" data-style="select-with-transition" multiple title="Organizaciones" data-size="7">
                                  <option disabled>Organizaciones</option>
                                  <?php foreach($this->db->get_where('organizaciones')->result() as $t): ?>
                                        <option value="<?= $t->id ?>" <?= @$_GET['organizacion']==$t->id?'selected':'' ?>><?= $t->nombre ?></option>
                                  <?php endforeach ?>
                                </select>
                            </div>
                          </div>
                      </div>

                      <div class="col-sm-4 border-lateral">
                        <div class="col-sm-12">
                            <div class="select-agencias">
                                <select name="filtros[servicios][]" class="selectpicker" data-style="select-with-transition" multiple title="<?= l('Servicios') ?>" data-size="7">
                                  <option disabled><?= l('Servicios') ?></option>                                  
                                  <?php foreach($this->db->get_where('servicios')->result() as $t): ?>
                                    <option value="<?= $t->id ?>"><?= $t->nombre ?></option>
                                  <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                      </div>

                      <div class="col-sm-4">  
                        <div class="col-sm-12">
                            <select name="filtros[anho]" id="anho" placeholder="Selecciona una fecha" class="selectpicker"  data-style="select-with-transition" title="<?= l('Selecciona una fecha') ?>">
                              <option value="">Fecha</option>
                              <?php for($i = 2018;$i<=date("Y");$i++): ?>
                                <option value="<?= $i ?>"><?= $i ?></option>
                              <?php endfor ?>
                            </select>
                        </div>    

                        <div class="col-sm-12">
                          <div class="iconos-favoritos-agencias">
                              <button type="button" class="btn btn-primary btn-round cleanFilters" id="btn-borrar-filtros"><i class="material-icons">cached</i> <?= l('Borrar filtros') ?></button>
                          </div>
                        </div>

                      </div>



                  </div>


              </div>



          </div>
      </div>

    </div>
</div>