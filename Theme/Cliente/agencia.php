<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>




</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
             <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
              <div class="container-fluid">

                    <!-- Inicia Contenido -->
                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion">
                        <div class="titulo-top">Agencia</div>
                        <div style="float:right"><button type="button" class="btn btn-info" onclick="exportar();">Exportar</button></div>
                      </div>
                  </div>

                  <div class="row contenedor-nuevo-proyecto-dashboard">
                      <div class="col-xs-12 col-sm-12 padding0">

                          <div class="col-xs-12 col-sm-4">
                                <div class="card card-product fondo-agencia-1" data-count="3">
                                    <div class="logo-agencia">
                                      <?php if(file_exists('img/agencias/'.$agencia->logo)): ?>
                                        <img class="center-block img-responsive" src="<?= base_url() ?>img/agencias/<?= $agencia->logo ?>" alt="Agencia" style="width:25%; padding-top: 20px;">
                                      <?php else: ?>
                                        <img src="<?= base_url() ?>img/agencias/8e560-logo-sistema-generico.jpeg" alt="Proyectos en BIMBO" class="img-responsive">
                                      <?php endif ?>
                                      
                                    </div>
                                    <div class="card-content">
                                        <h4 class="card-title"><b><?= $agencia->nombre ?></b></h4>
                                        <div class="card-description">
                                          <p>
                                            Proyectos Pitcheados: 
                                            <a href="<?= base_url() ?>cliente/proyecto/proyectos?agencias_id=<?= $agencia->id ?>&tipo=1">
                                              <?php 
                                                $this->db->select('view_proyectos.*');
                                                $this->db->join('view_proyectos','view_proyectos.id = briefs.proyectos_id');
                                                $this->db->group_by('view_proyectos.id');
                                                echo $this->db->get_where('briefs',array('agencias_id'=>$agencia->id))->num_rows() 
                                              ?>
                                              <br/>
                                            </a>
                                            Proyectos Ganados: 
                                            <a href="<?= base_url() ?>cliente/proyecto/proyectos?agencias_id=<?= $agencia->id ?>&tipo=4">
                                              <?php 
                                                $this->db->select('view_proyectos.*');
                                                $this->db->join('view_proyectos','view_proyectos.id = briefs.proyectos_id');
                                                $this->db->group_by('view_proyectos.id');
                                                echo $this->db->get_where('briefs',array('agencias_id'=>$agencia->id,'briefs.status'=>4))->num_rows() 
                                              ?><br/>
                                            </a>
                                          </p>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="col-xs-6 col-sm-6 border-right text-center">
                                              <div class="description-block">
                                                  <h5 class="description-header certificado-agencia">
                                                    <small class="description-text">Validación</small>
                                                    <br>
                                                    <i class="fa fa-certificate" aria-hidden="true"></i><br>
                                                    <b><?= $agencia->certificado ?></b>
                                                  </h5>
                                              </div>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 text-center">
                                              <div class="description-block">
                                                  <h5 class="description-header texto-verde">
                                                      <small class="description-text">Status</small><br>
                                                      <i class="fa fa-check-circle-o" aria-hidden="true"></i><br>
                                                      <b><?= $agencia->estatus_agencias ?></b>
                                                  </h5>
                                              </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-12">
                                            <div class="col-xs-6 col-sm-6 iconos-favoritos-agencias">
                                                <a title="Enviar Brief" href="#enviar-brief" data-toggle="modal">
                                                  <button class="btn btn-primary btn-round" id="btn-enviar-brief">Enviar Brief</button>
                                                </a>
                                            </div>

                                            <div class="col-xs-6 col-sm-6 iconos-favoritos-agencias">
                                                <a title="Contactar Agencia" href="#contacto-agencia" data-toggle="modal">
                                                  <button class="btn btn-primary btn-round" id="btn-ver-agencia">Contactar</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                          </div>

                          <div class="col-xs-12 col-sm-8 contenedor-descripcion-agencia2 margen-movil-servicios">
                              

                              <div class="col-sm-12 texto-gris" style="margin-top: 20px;">
                                    <div class="col-sm-12">
                                      <div class="col-xs-6 col-sm-4">
                                          <div class="card card-stats fondo-agencia-1">
                                            <div class="card-header" data-background-color="blue"><i class="material-icons">place</i></div>
                                            <div class="card-content"><p class="category"><?= l('País') ?></p></div>
                                            <div class="card-footer text-uppercase"><div class="stats"><?= !empty($agencia->ciudad)?$agencia->ciudad.', ':'' ?><?= $agencia->pais ?></div></div>
                                          </div>
                                      </div>

                                      <div class="col-xs-6 col-sm-4">
                                          <div class="card card-stats fondo-agencia-1">
                                            <div class="card-header" data-background-color="orange"><i class="material-icons">person</i></div>
                                            <div class="card-content"><p class="category"><?= l('Categoria') ?></p></div>
                                            <div class="card-footer text-uppercase"><div class="stats"><?= $agencia->_categorias?></div></div>
                                          </div>
                                      </div>

                                      <div class="col-xs-6 col-sm-4">
                                          <div class="card card-stats fondo-agencia-1">
                                              <div class="card-header" data-background-color="red"><i class="material-icons">stars</i></div>
                                              <div class="card-content"><p class="category"><?= l('super-poder') ?></p></div>
                                              <div class="card-footer iconos-ranking">
                                                  <div class="stats text-uppercase">
                                                    <?= $agencia->super_poder ?>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <span class="titulo-servicios"><b>Servicios:</b></span>
                                    </div>

                                    <div class="col-sm-12">
                                          <?php foreach(explode(',',$agencia->filtros) as $a): ?>
                                            <span class="label categorias-agencia"><?= $a ?></span>
                                          <?php endforeach ?>
                                    </div>
                                </div>
                          </div>

                      </div>
                  </div>
                  <?php if(!empty($agencia->thinking_mode)): ?>
                  <div class="col-lg-12 ml-auto mr-auto">
                                          <div class="card card-stats fondo-agencia-1">
                                            <div class="card-header" data-background-color="red"><i class="material-icons">psychology</i></div>
                                            <div class="card-content"><p class="category"><?= l('Thinking model') ?></p></div>
                                            <div class="text-center"><h2><?= $agencia->thinking_mode?></h2></div>
                                          </div>
                                              <?php endif ?>
                  <div class="row bold-agencia">
                      <div class="col-xs-12 col-sm-12 padding0">
                          <div class="col-xs-12 col-sm-6">
                              <div class="col-xs-12 col-sm-12">
                                  <div class="fondo-agencia-1">
                                      <h1 class="titulo-agencia">
                                        <?= $agencia->nombre ?>
                                      </h1>
                                      <b>Grupo:</b>  <?= @$agencia->grupo->nombre ?><br/>
                                      <b>CEO:</b>  <?= $agencia->ceo ?><br>
                                      
                                      <b>Colaboradores:</b>  <?= $agencia->empleados ?><br/> 
                                      <b>Última actualización: </b> <?= date("d/m/Y",strtotime($agencia->fecha_actualizacion)) ?><br/>  
                                      <?php if(!empty($agencia->paises)): ?>
                                      <b>Cobertura: </b> <?= $agencia->paises ?> <br/> 
                                  <?php endif ?>
                                      <?php if(!empty($agencia->bimbo_accounts)): ?>
                                        <b>Bimbo accounts: </b> <?= $agencia->bimbo_accounts ?><br/>   
                                      <?php endif ?>
                                      
                                      <br>
                                      <?php if(!empty($agencia->presentacion)): ?>
                                        <a class="btn btn-danger" href="<?= $agencia->presentacion ?>" target="_new">Credenciales</a>
                                      <?php endif ?>
                                      <?php if(!empty($agencia->documentos)): ?>
                                        <a class="btn btn-danger" data-toggle="modal" href="#documentos">Documentos</a>
                                      <?php endif ?>

                                  </div>

                              </div>
                              <div class="col-xs-12 col-sm-12 padding0 img-agencias">
                                  <h1 class="titulo-agencia"><?= l('top-accounts') ?></h1>
                                  <div style="display: flex; margin:10px 0">
                                    <?php 
                                      $this->db->limit(5);
                                      foreach($this->db->get_where('agencias_clientes',array('agencias_id'=>$agencia->id))->result() as $a): 
                                    ?>
                                      <div style="text-align: center;">
                                          <img src="<?= base_url('img/clientes/'.$a->logo) ?>" alt="Agencias en BIMBO" class="img-responsive center-block" style="max-width: none; padding:0 10px">                                          
                                      </div>
                                    <?php endforeach ?>
                                  </div>
                              </div>

                              

                             
                          </div>

                          <div class="col-xs-12 col-sm-6">
                              <div class="col-xs-12 col-sm-12">
                                  <div class="fondo-agencia-1">
                                      <h1 class="titulo-agencia">Datos de Contacto</h1>
                                      <b>Razón Social:</b>  <?= $agencia->razon_social ?><br>
                                      <b>Dirección:</b>  <?= $agencia->direccion ?><br>
                                      <b>Sitio Web:</b> <a href="<?= $agencia->web ?>" target="blank"> <?= $agencia->web ?></a><br>
                                      <b>Teléfono:</b>  <?= $agencia->telefono ?><br>
                                      <b>Correo:</b> <a href="mailto:<?= $agencia->email ?>"><?= $agencia->email ?></a><br>
                                      
                                      <b>¿Como llegar? <a href="https://www.google.com/maps/dir/?api=1&destination=<?= urlencode($agencia->direccion) ?>" target="_new"><i class="fa fa-map"></i></a></b><br/>                                      
                                      <!--Mapa -->
                                      <?php if(!empty($agencia->mapa)): ?>
                                        <?php list($lat,$lng) = explode(',',str_replace(['(',')'],['',''],$agencia->mapa)); ?>                                                                            
                                        <div id="mapa" style="width:100%; height:300px;" data-lat="<?= $lat ?>" data-lng="<?= $lng ?>" data-zoom="16"></div>
                                      <?php endif ?>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <br><br>
                  <?php if(!empty($agencia->qualitative_findings)): ?>
                  <div class="content">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-lg-12 padding0 titulo-secccion">
                        <div class="titulo-top">Findings</div>
                      </div></div></div></div>
                  <div class="row justify-content-center" >
                  
                                    <div class="col-lg-12 ml-auto mr-auto">
                                      
                                          <div class="quote-card">
                                            
                                            <blockquote class="quote-card text-center">
                                                <b><p><?= $agencia->qualitative_findings ?></p></b>
                                                <cite>Qualitative findings...</cite>
                                                    </blockquote>
                                          </div>
                                      </div>
                                      </div>
                                    </div>
                                  </div>
                                <?php endif ?>
                  <div class="content">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-sm-12  titulo-secccion">
                        <div class="titulo-top">Reviews</div>
</div>
</div><br><br>
<div class="col-lg-12">
                  <div id="wpac-review">
<script type="text/javascript">
wpac_init = window.wpac_init || [];
wpac_init.push({widget: 'Review', id: 31716});
(function() {
    if ('WIDGETPACK_LOADED' in window) return;
    WIDGETPACK_LOADED = true;
    var mc = document.createElement('script');
    mc.type = 'text/javascript';
    mc.async = true;
    mc.src = 'https://embed.widgetpack.com/widget.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
})();
</script>
<a href="https://widgetpack.com" class="wpac-cr"></a></div>









    

    <?php if(!empty($agencia->documentos)): ?>

       <!-- Modal Ganador Seleccionado-->
        <div class="modal fade" id="documentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
            <div class="modal-dialog">
                <div class="swal2-modal swal2-show logo-modales" style="display: block; width: 500px; padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 333px;" tabindex="-1">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
                    </div>
                    <img src="<?= base_url() ?>Theme/Cliente/assets/img/login/logo-bimbo.png" alt="Logo Bimbo" class="img-responsive center-block">           
              
                <div class="folders">
                        <div class="titulo-modal">
                          Agencia <?= $agencia->nombre ?>
                        </div>
                        <div>
                          <table class="table">
                            <?php foreach(explode(',',$agencia->documentos) as $b): ?>
                              <tr>
                                <td style="text-align:left; border:0">
                                  <a title="Descargar" href="<?= base_url().'img/agencias/'.$b ?>" target="_blank"><img src="<?= base_url() ?>img/documentos.jpeg" alt="" style="width:30px"><?= explode('-',$b,2)[1] ?></a>
                                </td>
                              </tr>
                            <?php endforeach ?>
                          </table>
                        </div>
                      </div>
              

                </div>
            </div>
        </div>
        <!-- Termina Modal Ganador Seleccionado -->
      <?php endif ?>
<footer class="footer contenedor-footer">
        <?php include('footer.php');?>
    </footer>
</body>


<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=drawing,geometry,places"></script>
<script>
  var opt = $("#mapa");
  var mapOptions = {
      zoom: opt.data('zoom'),
      center: new google.maps.LatLng(opt.data('lat'),opt.data('lng')),
      mapTypeId: google.maps.MapTypeId.ROADMAP
  };        
  var map = new google.maps.Map(document.getElementById('mapa'),mapOptions);                            
  var marker = new google.maps.Marker({
  position: new google.maps.LatLng(opt.data('lat'),opt.data('lng')),
  map: map,
  title: '<?= $agencia->nombre ?>'});  
</script>
<!-- Agregar Campos Modal Agregar Proyecto -->
<script>
    $(document).ready(function() {
        demo.initSmallGoogleMaps();
    });
</script>


<script>
$(document).ready(function() {
  //carousel options
  $('#quote-carousel').carousel({
    pause: true, interval: 10000,
  });
});

function exportar(){
  var html = $("body");  
  html.find(".modal").attr('data-html2canvas-ignore',true);
  html.find("#lo-engage-ext-container").attr('data-html2canvas-ignore',true);
  html.find("#mapa").attr('data-html2canvas-ignore',true);
  html.find("iframe").attr('data-html2canvas-ignore',true);
  html.find("footer").attr('data-html2canvas-ignore',true);
  html.find("nav").attr('data-html2canvas-ignore',true);
  html.find(".sidebar").attr('data-html2canvas-ignore',true);
  html.find(".card,.fondo-agencia-1").attr('style','box-shadow:initial');
  html2canvas(html.get(0),{
    allowTaint:true,
    useCORS:true,
    logging : true,
  }).then(canvas => {  
      html.find(".card,.fondo-agencia-1").removeAttr('style');
      var pdf = new jspdf.jsPDF({
        orientation: "landscape",
        unit: "px",
        format: [1600,1600]
      });    
      pdf.html($("body").clone().html(canvas).get(0), {
        callback: function (pdf) {
          pdf.save("<?= $agencia->nombre ?>.pdf");
          //window.open(pdf.output('datauristring'));
        }
      });
  });  
}


</script>
</html>
