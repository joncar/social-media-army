<!doctype html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

<body>
    <div class="wrapper switch-trigger">
        <!-- Menu Lateral -->
        <div class="sidebar" id="fondo-menu-lateral">
            <?php include('menu-lateral.php');?>
        </div>

        <div class="main-panel">
            <!-- Menu Top -->
            <?php include('menu-top.php');?>

            <!-- Contenido -->
            <div class="content">
                <div class="container-fluid">

                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion"><div class="titulo-top">Historial de proyectos</div></div>
                  </div>

                  <!-- Inicia Contenido -->
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 padding0 datos-proyecto">

                        <div class="col-xs-12 col-sm-3">
                              <div class="card card-product" data-count="3">
                                  <div class="logo-agencia">
                                    <img class="center-block img-responsive" src="assets/img/Proyectos/img-proyecto4.jpg" alt="Agencia">
                                  </div>
                                  <div class="card-content">
                                      <div class="card-title"><b>Nombre del Proyecto</b></div>
                                      <div class="progress" id="progress-bar">
                                          <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="10" aria-valuemax="10" style="width: 100%">
                                              <b><big>100%</big></b>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="card-footer">
                                      <b>Marca:</b><br>
                                      <b>Producto:</b>
                                  </div>
                              </div>
                        </div>

                        <div class="col-xs-12 col-sm-7 contenedor-descripcion-agencia2 margen-movil-servicios">
                            <div class="col-sm-12 texto-gris">
                                  <div class="col-xs-4 col-sm-4">
                                      <div class="card card-stats">
                                          <div class="card-header" data-background-color="blue"><i class="material-icons">timeline</i></div>
                                          <div class="card-content">
                                              <p class="category">Fase</p><h3 class="card-title"><span class="porcentaje-agencia texto-rojo">Inicio</span></h3>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="col-xs-4 col-sm-4">
                                      <div class="card card-stats">
                                          <div class="card-header" data-background-color="blue"><i class="material-icons">timer_off</i></div>
                                          <div class="card-content">
                                              <p class="category">Fin de la fase</p><h3 class="card-title"><span class="porcentaje-agencia texto-morado">11/00/000</span></h3>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="col-xs-4 col-sm-4">
                                      <div class="card card-stats">
                                          <div class="card-header" data-background-color="blue"><i class="material-icons">assignment_ind</i></div>
                                          <div class="card-content">
                                              <p class="category">Agencias</p>
                                              <h3 class="card-title">
                                                  <span class="porcentaje-agencia texto-morado">9</span>
                                                  <a href="">
                                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                                  </a>
                                              </h3>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-sm-12 basura-proyectos">
                                <a title="Eliminar Proyecto" href="#eliminar-proyecto" data-toggle="modal"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                <a title="Pausar Proyecto" href="#pausar-proyecto" data-toggle="modal"><i class="fa fa-pause" aria-hidden="true"></i></a>
                              </div>
                              <div class="col-sm-12 texto-gris">
                                  <small class="texto-rojo">*Recomendamos no invitar a más agencias después de la fase de preguntas y respuestas </small>
                              </div>
                        </div>

                        <div class="col-xs-12 col-sm-2 iconos-agencias-izquierda text-left">
                            <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                <a title="Favoritos" href="agencias-proyecto.php">
                                  <button class="btn btn-primary btn-round" id="btn-proyecto-propuestas"><i class="material-icons">remove_red_eye</i> Ver Propuestas</button>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                <a title="Editar Proyecto" href="#editar-proyecto" data-toggle="modal">
                                  <button class="btn btn-primary btn-round" id="btn-proyecto-informacion"><i class="material-icons">border_color</i> Editar proyecto</button>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                <a title="Ver Agencia" href="sala-juntas.php">
                                  <button class="btn btn-primary btn-round" id="btn-proyecto-juntas"><i class="material-icons">people</i> Sala de Juntas <span class="badge">5</span></button>
                                </a>
                            </div>
                        </div>

                    </div>


                      <div class="col-sm-12">

                          <!-- Inicia una agencia -->
                          <div class="col-xs-12 col-sm-12 contenedor-listado-agencias">
                              <div class="col-xs-4 col-sm-2 padding0 img-agencias">
                                  <div class="img-texto text-center">
                                      <img src="assets/img/Proyectos/img-proyecto.jpg" alt="Proyectos en BIMBO"  class="img-responsive center-block media__image">
                                      <span class="img-bandera-agencia">
                                          <img src="assets/img/Banderas/mexico.jpg" alt="Proyectos en BIMBO"  class="img-responsive img-circle">
                                      </span>
                                  </div>
                              </div>
                              <div class="col-xs-8 col-sm-2">
                                  <span class="nombre-agencias">Nombre de la Agencia ganadora</span><br>
                                  <b>Certificado AA</b><br>
                                  2 años trabajando para:<br>
                                  Grupo BIMBO<br>
                                  Estatus:<br>
                                  <button class="btn btn-primary btn-round excelente">
                                      <i class="material-icons">check_circle</i> Activo
                                  </button>
                              </div>
                              <div class="col-xs-12 col-sm-6 iconos-agencias-top">
                                  <div class="col-xs-12 col-sm-12 texto-datos-agencia">
                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="green">
                                                  <i class="material-icons">place</i>
                                              </div>
                                              <div class="card-content">
                                                  <p class="category">Dirección<br>Agencia</p>
                                              </div>
                                              <div class="card-footer">
                                                  <div class="stats">
                                                      Texto de la Dirección
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="blue">
                                                  <i class="material-icons">person</i>
                                              </div>
                                              <div class="card-content">
                                                  <p class="category">Categoría<br>Agencia</p>
                                              </div>
                                              <div class="card-footer">
                                                  <div class="stats">
                                                       Categoría
                                                  </div>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-xs-4 col-sm-4">
                                          <div class="card card-stats">
                                              <div class="card-header" data-background-color="orange">
                                                  <i class="material-icons">stars</i>
                                              </div>
                                              <div class="card-content">
                                                  <p class="category">Ranking<br>Usuarios</p>
                                              </div>
                                              <div class="card-footer iconos-ranking">
                                                  <div class="stats" style="color:green">
                                                       <big><i class="material-icons">verified_user</i> Excelente</big>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-xs-12 col-sm-2 iconos-agencias-izquierda">
                                  <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Favoritos" href="#">
                                        <button class="btn btn-primary btn-round" id="btn-favoritos"><i class="material-icons">favorite</i> Favorito</button>
                                      </a>
                                  </div>
                                  <div class="col-xs-12 col-sm-12 iconos-favoritos-agencias">
                                      <a title="Ver Agencia" href="agencia.php">
                                        <button class="btn btn-primary btn-round" id="btn-ver-agencia"><i class="material-icons">remove_red_eye</i> Ver agencia</button>
                                      </a>
                                  </div>
                              </div>
                          </div>
                          <!-- Termina una agencia -->


                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-12 padding0 titulo-secccion">
                          <div class="titulo-top">Historial de Participación</div>
                      </div>
                  </div>

                  <div class="row bold-agencia">
                      <div class="col-sm-12 padding0">
                          <div class="col-sm-6">
                              <div class="col-sm-12">
                                  <div class="fondo-agencia-1">
                                      <h1 class="titulo-agencia">Datos del Proyecto</h1>
                                      <b>Responsable:</b> Alberto Pliego<br>
                                      <b>Marca:</b> Marca<br>
                                      <b>CEO:</b> Alejandra<br>
                                      <b>Ubicación:</b> Dirección
                                  </div>
                              </div>

                              <div class="col-sm-12">
                                  <div class="fondo-agencia-1">
                                      <h1 class="titulo-agencia">Objetivo general</h1>
                                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        illum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                  </div>
                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="fondo-agencia-1 contenedor-gant">
                                  <h1 class="titulo-agencia">Etapas</h1>
                                  <div class="contenedor-fechas">
                                      <div id="chart_div"></div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-sm-12">
                          <h1 class="titulo-agencia">Documentos</h1>
                          <div class="col-sm-12 documentos-historial">
                              <a href="#">
                                  <div class="col-sm-3 text-center">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-green"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span>
                                        <div class="info-box-content">
                                          <span class="info-box-text"><b>PDF</b></span>
                                          <span class="info-box-number"><small>Nombre del archivo</small></span>
                                        </div>
                                      </div>
                                  </div>
                              </a>

                              <a href="#">
                                  <div class="col-sm-3 text-center">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-green"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span>
                                        <div class="info-box-content">
                                          <span class="info-box-text"><b>PDF</b></span>
                                          <span class="info-box-number"><small>Nombre del archivo</small></span>
                                        </div>
                                      </div>
                                  </div>
                              </a>

                              <a href="#">
                                  <div class="col-sm-3 text-center">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-green"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span>
                                        <div class="info-box-content">
                                          <span class="info-box-text"><b>PDF</b></span>
                                          <span class="info-box-number"><small>Nombre del archivo</small></span>
                                        </div>
                                      </div>
                                  </div>
                              </a>

                              <a href="#">
                                  <div class="col-sm-3 text-center">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-green"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span>
                                        <div class="info-box-content">
                                          <span class="info-box-text"><b>PDF</b></span>
                                          <span class="info-box-number"><small>Nombre del archivo</small></span>
                                        </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                      </div>

                  </div>

                </div>
                  <!-- Termina Contenido -->

                </div>

                <footer class="footer contenedor-footer">
                    <?php include('footer.php');?>
                </footer>

            </div>

        </div>
    </div>
</body>

<?php include('modales.php');?>
<!-- Librerias -->
<?php include('librerias.php');?>
<!-- Agregar Campos Modal Agregar Proyecto -->
</html>
